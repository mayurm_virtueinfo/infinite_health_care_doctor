import 'package:shared_preferences/shared_preferences.dart';

class AppPreferences{
  static const String is_app_opened = "is_app_opened";
  static const String user = "user";

  static final String PHONE = "phone";
  static final String GOOGLE = "google";
  static final String FACEBOOK = "facebook";
}
