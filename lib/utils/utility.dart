import 'dart:convert';
import
'dart:io';

// import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:device_info/device_info.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:intl/intl.dart';
import 'package:infinite_health_care_doctor/models/appointment_type.dart';
import 'package:infinite_health_care_doctor/models/schedule_conversation.dart';
import 'package:infinite_health_care_doctor/services/navigation_service.dart';
import 'package:infinite_health_care_doctor/ui/conversation/personal_chat.dart';
import 'package:infinite_health_care_doctor/main.dart';

class Utility{
  // static String googleApiKey = "AIzaSyBOdHyor4jT_nywJoDxKcFClmCIVubiKW8"; // Old working
  static String googleApiKey = "AIzaSyCgQ8hhsW4VR9MsIUzkYm-k_nPLNuVlQGE";

  static void checkForNotification(Map<String,dynamic> data){try {
    String screen = data['screen'];

    if (screen == 'PersonalChat') {
      String appointmentType = data['appointmentType'];
      AppointmentType apType;
      if (appointmentType == AppointmentType.SCHEDULED_APPOINTMENT.toString()) {
        apType = AppointmentType.SCHEDULED_APPOINTMENT;
      } else {
        apType = AppointmentType.PAST_APPOINTMENT;
      }
      String userId = data['id_user'];
      Map<String, dynamic> patient = json.decode(data['patient']);
      String patientId = patient['id_user'].toString();
      if (patient['id_user'] is String) {
        String pId = patient['id_user'];
        patient['id_user'] = int.parse(pId);
      }
      ScheduledConversation scheduledConversation = ScheduledConversation.fromMapDynamic(patient);
      locator<NavigationService>().navigatorKey.currentState.pushNamed(PersonalChat.routeName, arguments: [userId, apType, scheduledConversation]);
    }
  } catch (e) {
    debugPrint("parsing error : ${e.toString()}");
  }}

  static double getDevicePixelRatio(BuildContext context){
    double devicePixelRatio = MediaQuery.of(context).devicePixelRatio;
    if(devicePixelRatio>2){
      devicePixelRatio = 1.75;
    }
    return devicePixelRatio;
  }
  static Future<Position> requestForLocationPermission() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.deniedForever) {
      return Future.error(
          'Location permissions are permantly denied, we cannot request permissions.');
    }

    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission != LocationPermission.whileInUse &&
          permission != LocationPermission.always) {
        return Future.error(
            'Location permissions are denied (actual value: $permission).');
      }
    }

    return await Geolocator.getCurrentPosition();
  }

  static Future<String> getDeviceUUID() async {
    String deviceName;
    String deviceVersion;
    String identifier;
    final DeviceInfoPlugin deviceInfoPlugin = new DeviceInfoPlugin();
    try {
      if (Platform.isAndroid) {
        var build = await deviceInfoPlugin.androidInfo;
        deviceName = build.model;
        deviceVersion = build.version.toString();
        identifier = build.androidId;  //UUID for Android
      } else if (Platform.isIOS) {
        var data = await deviceInfoPlugin.iosInfo;
        deviceName = data.name;
        deviceVersion = data.systemVersion;
        identifier = data.identifierForVendor;  //UUID for iOS
      }
    } on PlatformException {
      throw  Exception('Failed to get platform version');
    }

//if (!mounted) return;
    return identifier;
  }
  static String getFormatFromString(String date,String time){
    DateFormat parseFormat= DateFormat("dd-MMM-yyyy HH:mm:ss");
    DateTime  dtTime = parseFormat.parse('$date $time');

    DateFormat showFormat= DateFormat("dd-MMM-yyyy hh:mm a");
    return showFormat.format(dtTime);
  }
  static double getCoverHeight(BuildContext context){
    return Utility.getDevicePixelRatio(context) * 140;
  }
  static double getProfilePicSize(BuildContext context){
    return Utility.getDevicePixelRatio(context) * 75;
  }
  static String formatTimestamp(timestamp){
    debugPrint("debug timestamp : ${timestamp}");
    var date = DateTime.fromMillisecondsSinceEpoch(timestamp.millisecondsSinceEpoch);
    var formattedDate = DateFormat("dd-MM-yyyy HH:mm:ss").format(date);
    return formattedDate;
  }


  static String getFormatedCurrentTime(){
    DateFormat format= DateFormat("yyyy-MM-dd HH:mm:ss");
    String formatedCurrentTime = format.format(DateTime.now());
    debugPrint("formattedCurrentTime : ${formatedCurrentTime}");
    return formatedCurrentTime;
  }

  static Future<String> uploadFile(imageFile, storePath,chield_path) async {
    Reference storageReference =FirebaseStorage.instance.ref().child(chield_path).child(storePath);
    UploadTask uploadTask = storageReference.putFile(imageFile);
    await uploadTask.snapshot;
    return await storageReference.getDownloadURL();
  }

  static Future<File> cropImage(image) async {
    File croppedFile = await ImageCropper.cropImage(
        sourcePath: image.path,
        aspectRatioPresets: Platform.isAndroid
            ? [
          CropAspectRatioPreset.square,
          CropAspectRatioPreset.ratio3x2,
          CropAspectRatioPreset.original,
          CropAspectRatioPreset.ratio4x3,
          CropAspectRatioPreset.ratio16x9
        ]
            : [
          CropAspectRatioPreset.original,
          CropAspectRatioPreset.square,
          CropAspectRatioPreset.ratio3x2,
          CropAspectRatioPreset.ratio4x3,
          CropAspectRatioPreset.ratio5x3,
          CropAspectRatioPreset.ratio5x4,
          CropAspectRatioPreset.ratio7x5,
          CropAspectRatioPreset.ratio16x9
        ],
        androidUiSettings: AndroidUiSettings(
            toolbarTitle: 'Cropper',
            toolbarColor: Colors.deepOrange,
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: false),
        iosUiSettings: IOSUiSettings(
          title: 'Cropper',
        ));
    if (croppedFile != null) {
      image = croppedFile;

    }
    return image;
  }

  static Future<String> showFilterDialog(BuildContext context,String groupValue,Function onSelect) async {
    return showDialog<String>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Select Blood Group'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                RadioListTile<String>(
                  title: const Text('A+'),
                  value: "A+",
                  groupValue: groupValue,
                  onChanged: (String value) {
                    onSelect(value);
                    Navigator.pop(context);
                  },
                ),
                RadioListTile<String>(
                  title: const Text('O+'),
                  value: "O+",
                  groupValue: groupValue,
                  onChanged: (String value) {
                    onSelect(value);
                    Navigator.pop(context);
                  },
                ),
                RadioListTile<String>(
                  title: const Text('B+'),
                  value: "B+",
                  groupValue: groupValue,
                  onChanged: (String value) {
                    onSelect(value);
                    Navigator.pop(context);
                  },
                ),
                RadioListTile<String>(
                  title: const Text('AB+'),
                  value: "AB+",
                  groupValue: groupValue,
                  onChanged: (String value) {
                    onSelect(value);
                    Navigator.pop(context);
                  },
                ),
                RadioListTile<String>(
                  title: const Text('A-'),
                  value: "A-",
                  groupValue: groupValue,
                  onChanged: (String value) {
                    onSelect(value);
                    Navigator.pop(context);
                  },
                ),
                RadioListTile<String>(
                  title: const Text('O-'),
                  value: "O-",
                  groupValue: groupValue,
                  onChanged: (String value) {
                    onSelect(value);
                    Navigator.pop(context);
                  },
                ),
                RadioListTile<String>(
                  title: const Text('B-'),
                  value: "B-",
                  groupValue: groupValue,
                  onChanged: (String value) {
                    onSelect(value);
                    Navigator.pop(context);
                  },
                ),
                RadioListTile<String>(
                  title: const Text('AB-'),
                  value: "AB-",
                  groupValue: groupValue,
                  onChanged: (String value) {
                    onSelect(value);
                    Navigator.pop(context);
                  },
                ),
                RadioListTile<String>(
                  title: const Text('Reset'),
                  value: "",
                  groupValue: groupValue,
                  onChanged: (String value) {
                    onSelect(value);
                    Navigator.pop(context);
                  },
                ),
              ],
            ),
          ),
          /* actions: <Widget>[
            TextButton(
              child: Text('Agree'),
              onPressed: () {
                Navigator.of(context).pop();

              },
            ),
          ],*/
        );
      },
    );
  }



  static String getFormattedDate(String date, String time) {
    DateFormat format = DateFormat("yyyy-MM-dd HH:mm:ss");
    DateTime newDt = format.parse('$date $time');
    DateFormat dateFormat = DateFormat('dd MMM yyyy hh:mm a');
    return dateFormat.format(newDt);
  }
  static String getFormattedDateSingle(String datetime) {
    DateFormat format = DateFormat("yyyy-MM-dd HH:mm:ss");
    DateTime newDt = format.parse('$datetime');
    DateFormat dateFormat = DateFormat('dd MMM yyyy hh:mm a');
    return dateFormat.format(newDt);
  }
  static String getFormattedDateSingleHome(String date,String time) {
    DateFormat format = DateFormat('dd-MMM-yyyy HH:mm:ss');
    DateTime newDt = format.parse('$date $time');
    DateFormat dateFormat = DateFormat('dd MMM yyyy hh:mm a');
    return dateFormat.format(newDt);
  }
  static String getProfileString(address,city,state,zipCode){
    List<String> listString = [];
    debugPrint('address $address');
    if((address != null) && (address != '') && (address != 'null')){
      listString.add(address);
    }
    if((city != null) && (city != '') && (city != 'null')){
      listString.add(city);
    }
    if((state != null) && (state != '') && (state != 'null')){
      listString.add(state);
    }
    if((zipCode != null) && (zipCode != '') && (zipCode != 'null')){
      listString.add(zipCode);
    }
    String profile = listString.join(', ');
    debugPrint('profile $profile');
    return profile;
//    return strProfile;
  }
  static Future<bool> logoutUser() async {
    await FirebaseAuth.instance.signOut();
    await FacebookLogin().logOut();
    await GoogleSignIn().signOut();
    return Future.value(true);
  }
  static Future<bool> isUserSignInWithMobile() async {
    User fUser = FirebaseAuth.instance.currentUser;
    bool isMobileSignIn = false;
    if (fUser != null && fUser.phoneNumber != null) {
      isMobileSignIn = true;
    }
    return Future.value(isMobileSignIn);
  }
  static Future<bool> isUserSignInWithGmail() async {
    GoogleSignIn googleSignIn = GoogleSignIn();
    bool isGoogleSignIn = await googleSignIn.isSignedIn();
    return Future.value(isGoogleSignIn);
  }
  static Future<bool> isUserSignInWithFacebook() async {

    FacebookLogin facebookSignIn = FacebookLogin();
    bool isFacebookSignIn = await facebookSignIn.isLoggedIn;

    return Future.value(isFacebookSignIn);

  }
}