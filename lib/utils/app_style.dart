import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:infinite_health_care_doctor/utils/my_colors.dart';
import 'package:infinite_health_care_doctor/config/app_config.dart' as config;

class AppStyles{
  static TextStyle errorStyle = TextStyle(

    color: Colors.red
  );
  static OutlineInputBorder focusBorder = OutlineInputBorder(
    borderSide: BorderSide(color: config.Colors().mainColor(1)),
  );
  static OutlineInputBorder mBorder = OutlineInputBorder(
    borderSide: BorderSide(color: Colors.red),
  );
  static Color lightButtonColor = Color(0xFF757575);
  static const double roundedCornerBorderRadius = 10.0;
  static RoundedRectangleBorder roundedRectangleBorder = RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(roundedCornerBorderRadius),
      side: BorderSide(width: 0, style: BorderStyle.none));

}