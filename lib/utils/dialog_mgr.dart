import 'package:flutter/material.dart';
import 'package:infinite_health_care_doctor/config/app_config.dart' as config;

class DialogMgr{
  static Future<String> showDialogLogout(BuildContext context) async {
    return showDialog<String>(
      context: context,
      barrierDismissible: true, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(15.0))),
          content: Container(

              width: MediaQuery.of(context).size.width,
              height: 140,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(height: 20,),
                  Text("Are you sure you want to logout?",style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18
                  ),textAlign: TextAlign.center,),
                  SizedBox(height: 20,),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,

                      children: [
                        InkWell(
                          borderRadius: BorderRadius.circular(100.0),
                          onTap: (){
                            Navigator.of(context).pop('no');
                          },
                          child: Ink(
                            child: Card(
                              color: Colors.white,

                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(100.0),
                                  side: BorderSide(color: config.Colors().accentColor(1))
                              ),
                              child: Container(
                                width: 100,
                                padding: EdgeInsets.only(left: 20,right: 20),
                                height: 40,
                                child: Center(
                                  child: Text("No",style: TextStyle(
                                      fontSize: 16,
                                      color: config.Colors().accentColor(1),
                                      fontWeight: FontWeight.bold
                                  ),),
                                ),
                              ),
                            ),
                          ),
                        ),
                        InkWell(
                          borderRadius: BorderRadius.circular(100.0),
                          onTap: (){
                            Navigator.of(context).pop('yes');
                          },
                          child: Ink(
                            child: Card(
                              color: config.Colors().mainColor(1),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(100.0),
                              ),
                              child: Container(
                                width: 100,
                                padding: EdgeInsets.only(left: 20,right: 20),
                                height: 40,
                                child: Center(
                                  child: Text("Yes",style: TextStyle(
                                      fontSize: 15,
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold
                                  ),),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              )
          ),
        );
      },
    );
  }
}