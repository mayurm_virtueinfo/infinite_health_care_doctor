import 'package:flutter/material.dart';

class CustomStyles{
  /// custom text variable is make it easy a custom textStyle black font
  static var customTextStyleBlack = TextStyle(
      fontFamily: "Gotik",
      color: Colors.black,
      fontWeight: FontWeight.w700,
      fontSize: 15.0);

  /// Custom text blue in variable
  static var customTextStyleBlue = TextStyle(
      fontFamily: "Gotik",
      color: Color(0xFF6991C7),
      fontWeight: FontWeight.w700,
      fontSize: 15.0);


  /// Custom Text black
  static var customTextStyle = TextStyle(
    color: Colors.black,
    fontFamily: "Gotik",
    fontSize: 17.0,
    fontWeight: FontWeight.w800,
  );

  /// Custom Text for Header title
  static var subHeaderCustomStyle = TextStyle(
      color: Colors.black54,
      fontWeight: FontWeight.w700,
      fontFamily: "Gotik",
      fontSize: 16.0);

  /// Custom Text for Detail title
  static var detailText = TextStyle(
      fontFamily: "Gotik",
      color: Colors.black54,
      letterSpacing: 0.3,
      wordSpacing: 0.5);

   static TextStyle _txt = TextStyle(
    color: Colors.black,
    fontFamily: "Sans",
  );

  /// Get _txt and custom value of Variable for Name User
  static var txtName = _txt.copyWith(fontWeight: FontWeight.w700, fontSize: 17.0);

  /// Get _txt and custom value of Variable for Edit text
  static var txtEdit = _txt.copyWith(color: Colors.black26, fontSize: 15.0);

  static var txtError = _txt.copyWith(color: Colors.red, fontSize: 15.0);

  /// Get _txt and custom value of Variable for Category Text
  static var txtCategory = _txt.copyWith(
      fontSize: 14.5, color: Colors.black54, fontWeight: FontWeight.w500);

}