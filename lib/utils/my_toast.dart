import 'package:flutter/material.dart';
import 'package:infinite_health_care_doctor/config/app_config.dart' as config;
import 'package:toast/toast.dart';
class MyToast{
 /* static showToast(String message){
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: config.Colors().mainColor(1),
        textColor: Colors.white,
        fontSize: 16.0
    );
  }*/

  static showToast(String message,BuildContext context){
    Toast.show(message,context,
        duration: Toast.LENGTH_SHORT,
        gravity: Toast.BOTTOM,
        backgroundColor: config.Colors().mainColor(1),
        textColor: Colors.white,
    );
  }

}