import 'package:flutter/material.dart';

class MyColors{
  static Color colorConvert(String color) {
    color = color.replaceAll("#", "");
    if (color.length == 6) {
      return Color(int.parse("0xFF"+color));
    } else if (color.length == 8) {
      return Color(int.parse("0x"+color));
    }
  }

  static Color get getAppButtonColor => MyColors.colorConvert("#736ab7");
  static Color get backgroundColor => MyColors.colorConvert("#B1383D");
}