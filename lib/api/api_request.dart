import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:infinite_health_care_doctor/api/api.dart';
import 'package:infinite_health_care_doctor/utils/utility.dart';
import 'package:path/path.dart';

class ApiRequest {
  static final String serverKey = "key=AAAAFmUQbLA:APA91bECPxKBBlcwhRu0jHf_BPY1n3UNXP-wYTBHoyOJONLWYOrS3VVFWZ09ienikJX0VrI3YRFLoGPJMQ1SNw5vUCznmQX5m6TWKzeDZGgfzyvz3yDqJPforBnewvMum_YL8d1PCOkR";
  static Future<Map<String, dynamic>> getPatientList({String doctorId}) async {
    String apiStr = API.GET_PATIENTS.replaceAll('{doctor_id}',doctorId);
    debugPrint("App-API : GET_PATIENTS : ${apiStr}");
    var client = http.Client();
    final response = await client.get(Uri.parse(apiStr));

    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return json.decode(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load categories');
    }
  }

  static Future<Map<String, dynamic>> postStoreToken({String firebaseRegToken}) async {
    String apiStr = API.POST_STORE_TOKEN;
    print("API : POST_STORE_TOKEN : ${apiStr}");
    Map<String,dynamic> mData = Map();
    String strUdid = await Utility.getDeviceUUID();
    mData['udid'] = 'doctor_$strUdid';
    mData['firebase_token'] = firebaseRegToken;
    debugPrint("API : Params : ${mData.toString()}");
    FormData formData = FormData.fromMap(mData);
    try {
      Response response = await Dio().post(apiStr, data: formData);
      final jsonData = json.decode(response.toString());
      // if (response.statusCode == 201) {
      //   final jsonData = json.decode(response.toString());
      var map=Map<String, dynamic>.from(jsonData);
      return map;
      // } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      // throw Exception(jsonData.toString());
      // }
    } on DioError catch(e) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx and is also not 304.

      if(e.response !=null) {
        print("Data : ${e.response.data}");
        print("Headers : ${e.response.headers}");
        // print("Request : ${e.response.request}");
      } else{
        // Something happened in setting up or sending the request that triggered an Error
        // print("Request : ${e.request}");
        print("Messaage ${e.message}");
      }
    }catch(e){
      throw Exception(e.toString());
    }
  }
  static Future<Map<String, dynamic>> getDoctors({String type, int type_id}) async {
    String apiStr = API.GET_DOCTORS.replaceAll('{type}', type);
    apiStr = apiStr.replaceAll('{type_id}', "${type_id.toString()}");
    debugPrint("App-API : GET_DOCTORS : ${apiStr}");
    var client = http.Client();
    final response = await client.get(Uri.parse(apiStr));

    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return json.decode(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load categories');
    }
  }
  static Future<Map<String, dynamic>> getNotifications({String userID}) async {
    String apiStr = API.GET_NOTIFICATIONS.replaceAll('{user_id}', userID);
    debugPrint("App-API : GET_NOTIFICATIONS : $apiStr");
    var client = http.Client();
    final response = await client.get(Uri.parse(apiStr));

    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return json.decode(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to get my doctors');
    }
  }
  static Future<Map<String, dynamic>> getMyDoctors({String userID}) async {
    String apiStr = API.GET_MY_DOCTORS.replaceAll('{user_id}', userID);
    debugPrint("App-API : GET_MY_DOCTORS : $apiStr");
    var client = http.Client();
    final response = await client.get(Uri.parse(apiStr));

    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return json.decode(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to get my doctors');
    }
  }

  static Future<Map<String, dynamic>> getMyAppointments({String userID}) async {
    String apiStr = API.GET_MY_APPOINTMENTS.replaceAll('{user_id}', userID);
    debugPrint("App-API : GET_MY_APPOINTMENTS : $apiStr");
    var client = http.Client();
    final response = await client.get(Uri.parse(apiStr));

    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return json.decode(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to get my appointments');
    }
  }

  static Future<Map<String, dynamic>> getSingleCustomer({String id_customer}) async {
    String apiStr = API.GET_SINGLE_CUSTOMER.replaceAll('{id_customer}', id_customer);
    debugPrint("App-API : GET_SINGLE_CUSTOMER : $apiStr");
    var client = http.Client();
    final response = await client.get(Uri.parse(apiStr));

    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return json.decode(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to get my appointments');
    }
  }

  static Future<Map<String, dynamic>> getSingleDoctors({String id_doctors}) async {
    String apiStr = API.GET_SINGLE_DOCTORS.replaceAll('{id_doctors}', id_doctors);
    debugPrint("App-API : GET_SINGLE_DOCTORS : $apiStr");
    var client = http.Client();
    final response = await client.get(Uri.parse(apiStr));

    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return json.decode(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to get single doctors');
    }
  }

  static Future<Map<String, dynamic>> postDoctorsNearYou({String lat, String lng, String radius, String limit, String page}) async {
    String apiStr = API.POST_DOCTORS_NEAR_YOU;
    debugPrint("App-API : POST_DOCTORS_NEAR_YOU : $apiStr");

    Map<String, String> params = Map();
    params['lat'] = lat;
    params['lng'] = lng;
    params['radius'] = radius;
    params['limit'] = limit;
    params['page'] = page;

    print("API postDoctorsNearYou Params : ${params.toString()}");
//    FormData
    try {
      var client = http.Client();
      final response = await client.post(Uri.parse(apiStr), headers: <String, String>{"Content-Type": "application/x-www-form-urlencoded", "Content-type": "application/json"}, body: json.encode(params));
      if (response.statusCode == 202 || response.statusCode == 201) {
        Map<String, dynamic> mMap = jsonDecode(response.body.toString());
        debugPrint("Result from API POST_DOCTORS_NEAR_YOU : ${mMap}");
//      Map<dynamic,dynamic> mapResult = Map<String, dynamic>.from(json.decode(response.body));
        return mMap;
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to validate customer');
      }
    } on DioError catch(e){
      throw Exception(e.response);
    }
    catch (e) {
      throw Exception(e.toString());
    }
  }

  static Future<Map<String,dynamic>> postValidateDoctor({String id_firebase}) async {
    String apiStr = API.POST_VALIDATE_DOCTOR;
    debugPrint("App-API : POST_VALIDATE_DOCTOR : $apiStr");
    Map<String, String> params = Map();
    params['id_firebase'] = id_firebase;
    String strUdid = await Utility.getDeviceUUID();
    params['udid'] = 'doctor_$strUdid';
//    FormData
    print("API postValidateDoctor params : ${params.toString()}");

    var client = http.Client();
    final response = await client.post(Uri.parse(apiStr), headers: <String, String>{"Content-Type": "application/x-www-form-urlencoded", "Content-type": "application/json"}, body: jsonEncode(params));
    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      Map<String, dynamic> map = json.decode(response.body);
      print("result : ${map}");

      return Future.value(map);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to validate customer');
    }
  }

  static Future<Map<String, dynamic>> postAddNote({
    @required String doctor_id,
    @required String patient_id,
    @required String noteTile,
    @required String note,
    @required File image
  }) async {
    String apiStr = API.POST_ADD_NOTE;
    debugPrint("App-API : POST_ADD_NOTE : $apiStr");
    Map<String,dynamic> params = Map();
    params["doctor_id"]= doctor_id;
    params["patient_id"]= patient_id;
    params["note"]= note;
    params["note_title"]= noteTile;
    if(image!=null) {
     params["image"] = image!=null? await MultipartFile.fromFile(image.path,filename: basename(image.path) ):null;
    }
    debugPrint("App-API : POST_ADD_NOTE : Params : ${params}");
    FormData formData = FormData.fromMap(params);

//    FormData
    try {
      debugPrint("1");
      Response response = await Dio().post(apiStr, data: formData);
      debugPrint("2");
      debugPrint("dio only response : ${response}");
      debugPrint("dio toString response : ${response.toString()}");
      debugPrint("dio response : ${response.data}");
      debugPrint("dio status code : ${response.statusCode}");
      if (response.statusCode == 201) {
        final jsonData = json.decode(response.toString());
        var map=Map<String, dynamic>.from(jsonData);
        return map;
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to validate customer');
      }
    } on DioError catch(e){
      throw Exception(e.response);
    }catch (e) {
      debugPrint("3");
      throw Exception("Mayur ${e.toString()}");
    }
  }

  static Future<Map<String, dynamic>> postDoctorCreate({
      @required String fullname,
      String email,
      String password,
      String id_firebase,
      String mobile,
      String state,
      String city,
      String address,
      String zipcode,
      File image,
      List<String> category,
    List<String> degree,
    List<String> services,
    List<String> specialization,
    String fees,
    String description,
    String lat,
    String lng,
    String experience,
    }) async {
    String apiStr = API.POST_DOCTOR_CREATE;
    debugPrint("App-API : POST_DOCTOR_CREATE : $apiStr");
    debugPrint("5.2");
    String strCat = category.join(',');
    String strService = services.join(',');
    String strDegree = degree.join(',');
    String strSpecialization = specialization.join(',');


    Map<String,dynamic> params = {
      "fullname": fullname,
      "email": email,
      "mobile": mobile,
      "category":strCat,
      "degree":strDegree,
      "services":strService,
      "specialization":strSpecialization,
      "fees":fees,
      "id_firebase": id_firebase,
      "state": state,
      "city": city,
      "address": address,
      "zipcode": zipcode,
      "lat":lat,
      "lng":lng,
      'experience':experience,
      "base_image": image!=null? await MultipartFile.fromFile(image.path,filename: basename(image.path) ):null,
      // "cover_image": image!=null? await MultipartFile.fromFile(image.path,filename: basename(image.path) ):null,
      "description": description,
    };
    String strUdid = await Utility.getDeviceUUID();
    params['udid'] = 'doctor_$strUdid';
    debugPrint("App-API : POST_DOCTOR_CREATE : Params : ${params}");
    FormData formData = FormData.fromMap(params);

//    FormData
    try {
      debugPrint("1");
    Response response = await Dio().post(apiStr, data: formData);
      debugPrint("dio response postDoctorCreate : ${response.data}");
      debugPrint("dio status code postDoctorCreate : ${response.statusCode}");
      if (response.statusCode == 201) {
        final jsonData = json.decode(response.toString());
        var map=Map<String, dynamic>.from(jsonData);
        return map;
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to create customer');
      }
    } on DioError catch(e){
        throw Exception(e.response);
    }catch (e) {
      debugPrint("3");
      throw Exception("Mayur ${e.toString()}");
    }
  }
  static Future<Map<String, dynamic>> getMyPayments({String userID}) async {
    String apiStr = API.GET_MY_PAYMENTS.replaceAll('{user_id}', userID);
    debugPrint("App-API : GET_MY_PAYMENTS : $apiStr");
    var client = http.Client();
    final response = await client.get(Uri.parse(apiStr));

    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return json.decode(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to get my payments');
    }
  }
  static Future<Map<String, dynamic>> postDoctorUpdate({
    @required String id,
    @required String fullname,
    String email,
    String mobile,
    List<String> category,
    List<String> degree,
    List<String> service,
    List<String> specialization,
    String state,
    String city,
    String address,
    String zipcode,
    File image,
    File cover_image,
    String fees,
    String experience,
    String description,
    String lat,
    String lng
  }) async {
    String apiStr = API.POST_DOCTOR_UPDATE;
    debugPrint("API-App POST_DOCTOR_UPDATE : $apiStr");

    String strCat = category.join(',');
    String strService = service.join(',');
    String strDegree = degree.join(',');
    String strSpecialization = specialization.join(',');
    Map<String,dynamic> mapFormData = Map();
    mapFormData['id'] = /*mID*/id;
    mapFormData['fullname'] = fullname;
    mapFormData['email'] = email;
    mapFormData['mobile'] = mobile;
    mapFormData['category'] = strCat;
    mapFormData['degree'] = strDegree;
    mapFormData['specialization'] = strSpecialization;
    mapFormData['services'] = strService;
    mapFormData['state'] = state;
    mapFormData['city'] = city;
    mapFormData['address'] = address;
    mapFormData['zipcode'] = zipcode;
    mapFormData['fees'] = fees;
    mapFormData["experience"] = experience;
    mapFormData["lat"] = lat;
    mapFormData["lng"] = lng;
    mapFormData["description"] = description;
    mapFormData['base_image'] = image!=null? await MultipartFile.fromFile(image.path,filename: basename(image.path) ):null;
    mapFormData['cover_image'] = cover_image!=null? await MultipartFile.fromFile(cover_image.path,filename: basename(cover_image.path) ):null;
    debugPrint("API-App POST_DOCTOR_UPDATE : PARAMS : ${mapFormData}");

    FormData formData = FormData.fromMap(mapFormData);

//    FormData
    try {
      Response response = await Dio().post(apiStr, data: formData);
      debugPrint("dio only response : ${response}");
      debugPrint("dio status code : ${response.statusCode}");
      if (response.statusCode == 201) {
        final jsonData = json.decode(response.toString());
        var map=Map<String, dynamic>.from(jsonData);
        return map;
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to validate customer');
      }
    } on DioError catch(e){
      throw Exception(e.response);
    }catch (e) {
      throw Exception(e.toString());
    }
  }

  static Future<Map<String, dynamic>> getNoteListing({String doctor_id,String patient_id}) async {
    var client = http.Client();
    String apiStr = API.GET_NOTE_LISTING.replaceAll('{doctor_id}', doctor_id);
    apiStr = apiStr.replaceAll('{patient_id}', patient_id);
    final response = await client.get(Uri.parse(apiStr));
    debugPrint("API-App GET_NOTE_LISTING doctor_id : ${doctor_id}");
    debugPrint("API-App GET_NOTE_LISTING patient_id : ${patient_id}");
    debugPrint("API-App GET_NOTE_LISTING : $apiStr");

    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return json.decode(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to get note listing');
    }
  }

  static Future<Map<String, dynamic>> getLatestReview({String doctor_id}) async {
    var client = http.Client();
    String apiStr = API.GET_LATEST_REVIEW.replaceAll('{doctor_id}', doctor_id);
    final response = await client.get(Uri.parse(apiStr));
    debugPrint("API-App GET_LATEST_REVIEW doctor_id : ${doctor_id}");
    debugPrint("API-App GET_LATEST_REVIEW : $apiStr");

    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return json.decode(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to get note listing');
    }
  }

  static Future<Map<String, dynamic>> getBlogList({String doctor_id}) async {
    var client = http.Client();
    String apiStr = API.GET_BLOG_LIST.replaceAll('{doctor_id}', doctor_id);
    final response = await client.get(Uri.parse(apiStr));
    debugPrint("API-App GET_BLOG_LIST doctor_id : ${doctor_id}");
    debugPrint("API-App GET_BLOG_LIST : $apiStr");

    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return json.decode(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to get note listing');
    }
  }

  static Future<Map<String, dynamic>> getCategoryList() async {
    var client = http.Client();
    final response = await client.get(Uri.parse(API.GET_CATEGORY_LIST));
    debugPrint("API-App GET_CATEGORY_LIST : ${API.GET_CATEGORY_LIST}");

    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return json.decode(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load categories');
    }
  }

  static Future<Map<String, dynamic>> getDegreeList() async {
    var client = http.Client();
    final response = await client.get(Uri.parse(API.GET_DEGREE_LIST));
    debugPrint("API-App GET_DEGREE_LIST : ${API.GET_DEGREE_LIST}");

    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return json.decode(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load categories');
    }
  }

  static Future<Map<String, dynamic>> getServiceList() async {
    var client = http.Client();
    final response = await client.get(Uri.parse(API.GET_SERVICE_LIST));
    debugPrint("API-App GET_SERVICE_LIST : ${API.GET_SERVICE_LIST}");

    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return json.decode(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load categories');
    }
  }

  static Future<Map<String, dynamic>> getSpecializationList() async {
    var client = http.Client();
    final response = await client.get(Uri.parse(API.GET_SPECIALIZATION_LIST));
    debugPrint("API-App GET_SPECIALIZATION_LIST : ${API.GET_SPECIALIZATION_LIST}");

    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return json.decode(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load specialization');
    }
  }

  static Future<Map<String,dynamic>> postUpcomingAppointmentDoctor({String date_time,String doctor_id}) async {
    String apiStr = API.POST_UPCOMING_APPOINTMENT_DOCTOR;
    debugPrint("App-API : POST_UPCOMING_APPOINTMENT : $apiStr");
    Map<String, String> params = Map();
    params['date_time'] = date_time;
    params['doctor_id'] = doctor_id;
//    FormData
    print("API POST_UPCOMING_APPOINTMENT params : ${params.toString()}");

    var client = http.Client();
    final response = await client.post(Uri.parse(apiStr), headers: <String, String>{"Content-Type": "application/x-www-form-urlencoded", "Content-type": "application/json"}, body: jsonEncode(params));
    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      Map<String, dynamic> map = json.decode(response.body);
      print("result : ${map}");

      return Future.value(map);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to validate customer');
    }
  }
  static Future<Map<String, dynamic>> getDoctorTimings({String doctor_id}) async {
    var client = http.Client();
    String apiStr = API.GET_DOCTOR_TIMINGS.replaceAll('{doctor_id}', doctor_id);
    final response = await client.get(Uri.parse(apiStr));
    debugPrint("API-App GET_NOTE_LISTING : $apiStr");

    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return json.decode(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to get DoctorTimings');
    }
  }

  static Future<Map<String, dynamic>> getSearchPatient({String keyword}) async {
    var client = http.Client();
    String apiStr = API.GET_SEARCH_PATIENT.replaceAll('{keyword}', keyword);
    final response = await client.get(Uri.parse(apiStr));
    debugPrint("API- getSearchPatient : $apiStr");

    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return json.decode(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to get DoctorTimings');
    }
  }

  static Future<Map<String, dynamic>> postDoctorsTimingCreate({
    @required Map<String,dynamic> dataTimings
  }) async {
    String apiStr = API.POST_DOCTOR_TIMINGS;
    debugPrint("App-API : POST_DOCTOR_TIMINGS : $apiStr");

    Map<String,dynamic> params = dataTimings;
    debugPrint("App-API : POST_DOCTOR_TIMINGS : Params : ${params}");
    FormData formData = FormData.fromMap(params);

//    FormData
    try {
      debugPrint("1");
      Response response = await Dio().post(apiStr, data: formData);
      debugPrint("2");
      debugPrint("dio response : ${response.data}");
      if (response.statusCode == 201) {
        final jsonData = json.decode(response.toString());
        var map=Map<String, dynamic>.from(jsonData);
        return map;
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to validate customer');
      }
    } on DioError catch(e){
      throw Exception(e.response);
    }catch (e) {
      debugPrint("3");
      throw Exception("${e.toString()}");
    }
  }
  static Future<Map<String,dynamic>> postSendPush({String patient, List<String> registrationIds,String message,Map<String,String> data}) async {
    String apiStr = "https://fcm.googleapis.com/fcm/send";
    print("API postSendPush : $apiStr");
    Map<String, String> headers = Map();
    headers['Authorization'] =  ApiRequest.serverKey;
    headers['Content-type'] =  'application/json';

    Map<String,String> notification = Map();
    notification['title'] = "New Message from $patient";
    notification['body'] = message;



    Map<String, dynamic> params = Map();

    params['notification'] = notification;
    params['data'] = data;
    params['priority'] = 'high';
    params['registration_ids'] = registrationIds;

//    FormData
    print("API postSendPush headers : ${headers.toString()}");
    print("API postSendPush params : ${params.toString()}");

    var client = http.Client();
    final response = await client.post(Uri.parse(apiStr), headers: headers, body: jsonEncode(params));
    Map<String, dynamic> map = json.decode(response.body);
    print("result : ${map}");
    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      Map<String, dynamic> map = json.decode(response.body);
      return Future.value(map);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to validate customer');
    }
  }
}
