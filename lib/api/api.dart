class API{
//  http://work-updates.com/salon/public/api/get-categories
  /*static const String BASE = "https://reqres.in/";
  static const String LOGIN_USER = BASE+"api/login";*/

  static const String BASE = "https://work-updates.com/ihc/public/";
  static const String GET_CATEGORIES = BASE+"api/get-categories";
  static const String GET_TOP = BASE+"api/get-categories/top";
  static const String GET_NOTE_LISTING = BASE+"api/v1/note_listing?doctor_id={doctor_id}&patient_id={patient_id}";
  static const String GET_LATEST_REVIEW = BASE+"api/v1/latest_review/{doctor_id}";
  static const String GET_BLOG_LIST = BASE+"api/v1/blog_list/{doctor_id}";
  static const String GET_CATEGORY_LIST = BASE+"api/v1/category-list";
  static const String GET_DEGREE_LIST = BASE+"api/v1/degree-list";
  static const String GET_SERVICE_LIST = BASE+"api/v1/service-list";
  static const String GET_SPECIALIZATION_LIST = BASE+"api/v1/get-specialization";
  static const String GET_BANNER = BASE+"api/get-categories/future";
  static const String GET_DOCTORS = BASE+"api/get-doctors/{type}/{type_id}";
  static const String GET_PATIENTS = BASE+"api/v1/doctor_patient_list?doctor_id={doctor_id}";
  static const String GET_CONVERSATION = BASE+"api/get-doctors";
  static const String GET_MY_DOCTORS = BASE+"api/my-doctors/{user_id}";
  static const String GET_NOTIFICATIONS = BASE+"api/v1/notification_list/{user_id}";
  static const String GET_MY_APPOINTMENTS = BASE+"api/my-appointments/{user_id}";
  static const String GET_SINGLE_CUSTOMER = BASE+"api/single-customer/{id_customer}";
  static const String GET_SINGLE_DOCTORS = BASE+"api/single-doctors/{id_doctors}";
  static const String POST_VALIDATE_DOCTOR = BASE+"api/v1/doctor_validate";
  static const String POST_UPCOMING_APPOINTMENT_DOCTOR = BASE+"api/v1/upcoming-appointme-doctor";
  static const String POST_DOCTORS_NEAR_YOU = BASE+"api/get-doctors-by-distance";
  static const String POST_DOCTOR_CREATE = BASE+"api/v1/doctor_create";
  static const String POST_ADD_NOTE = BASE+"api/v1/add_notes";
  static const String POST_DOCTOR_TIMINGS = BASE+"api/doctor-timings";
  static const String GET_DOCTOR_TIMINGS = BASE+"api/v1/doctor_schedule_list/{doctor_id}";
  static const String GET_SEARCH_PATIENT = BASE+"api/v1/search_patient?keyword={keyword}";
  static const String POST_DOCTOR_UPDATE = BASE+"api/v1/doctor-update";
  static const String GET_MY_PAYMENTS = BASE+"api/v1/my_payment_doctor/{user_id}";
  static const String POST_STORE_TOKEN = BASE+"api/v1/store_token";

/*

  http://work-updates.com/ihc/public/api/get-doctors/category/7


  http://work-updates.com/ihc/public/api/get-categories


  GET_TOP : http://work-updates.com/ihc/public/api/get-categories/top

  GET_FUTURE : http://work-updates.com/ihc/public/api/get-categories/future
*/


/*  this is the final api for home page

  top category mate apde top vadi user karsu
  banner mate future vadi

  for the category*/

}
