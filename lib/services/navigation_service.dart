import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:infinite_health_care_doctor/main.dart';
import 'package:infinite_health_care_doctor/models/appointment_type.dart';
import 'package:infinite_health_care_doctor/models/schedule_conversation.dart';
import 'package:infinite_health_care_doctor/services/navigation_service.dart';
import 'package:infinite_health_care_doctor/ui/conversation/personal_chat.dart';
import 'package:infinite_health_care_doctor/ui/waiting_list_confirm_screen.dart';
import 'package:infinite_health_care_doctor/utils/my_toast.dart';

class NavigationService {
  final GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();

  Future<dynamic> navigateTo(String routeName,String id_booking) {
    return navigatorKey.currentState.pushNamedAndRemoveUntil(routeName,(route)=>false,arguments: [id_booking]);
  }
  Future<dynamic> navigateToSplash(String routeName) {
    return navigatorKey.currentState.pushNamedAndRemoveUntil(routeName,(route)=>false);
  }
}
