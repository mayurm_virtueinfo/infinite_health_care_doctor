import 'dart:convert';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:infinite_health_care_doctor/main.dart';
import 'package:infinite_health_care_doctor/utils/utility.dart';

class FirebaseNotificationHandler {
  FirebaseMessaging _firebaseMessaging;

  void setUpFirebase(Function onTokenReceived) {
    _firebaseMessaging = FirebaseMessaging.instance;
    firebaseCloudMessagingListeners(onTokenReceived);
  }

  void firebaseCloudMessagingListeners(onTokenReceived) async {
    // if (Platform.isIOS) iOS_Permission();

    NotificationSettings settings = await _firebaseMessaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );

    print('User granted permission: ${settings.authorizationStatus}');

    _firebaseMessaging.getToken().then((token) async {
      print('token : ${token}');
      try {
        onTokenReceived(token);
      } catch (e) {
        onTokenReceived(null);
        debugPrint("Error storing firebase token : ${e.toString()}");
      }
    });
    _firebaseMessaging.getInitialMessage().then((RemoteMessage message) {
      // Will be called when app is terminated
      debugPrint("pns-status-getInitialMessage : ${message.toString()}");
      if(message != null && message.data != null){
        if(message.data == null){
          return;
        }
        mapLaunchData = message.data;
      }
    });
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      // Will be called when app is in forground state
      debugPrint("pns-status-onMessage");
      RemoteNotification notification = message.notification;
      AndroidNotification android = message.notification?.android;

      // If `onMessage` is triggered with a notification, construct our own
      // local notification to show to users using the created channel.
      if (notification != null && android != null) {
        flutterLocalNotificationsPlugin.show(
            notification.hashCode,
            notification.title,
            notification.body,
            NotificationDetails(
              android: AndroidNotificationDetails(
                channel.id,
                channel.name,
                channel.description,
                icon: android?.smallIcon,
                // other properties...
              ),
            ),
          payload: jsonEncode(message.data)
        );
      }
    });
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      // Will be called when app is in background state
      debugPrint("pns-status-onMessageOpenedApp : ${message.notification}");
      debugPrint("pns-status-onMessageOpenedApp : ${message.data}");
      if(message.data == null){
        return;
      }
      Utility.checkForNotification(message.data);
    });
  }

/*void iOS_Permission() {
    _firebaseMessaging.requestNotificationPermissions(IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered.listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
  }*/
/*

  void fcmMsgOnResume(message) {
    try {
      final data = message['data'];
      String screen = data['screen'];

      if (screen == 'PersonalChat') {
        String appointmentType = data['appointmentType'];
        AppointmentType apType;
        if (appointmentType == AppointmentType.SCHEDULED_APPOINTMENT.toString()) {
          apType = AppointmentType.SCHEDULED_APPOINTMENT;
        } else {
          apType = AppointmentType.PAST_APPOINTMENT;
        }
        String userId = data['id_doctor'];
        Map<String, dynamic> patient = json.decode(data['patient']);
        String patientId = patient['id_doctor'].toString();
        if (patient['id_doctor'] is String) {
          String pId = patient['id_doctor'];
          patient['id_doctor'] = int.parse(pId);
        }
        ScheduledConversation scheduledConversation = ScheduledConversation.fromMapDynamic(patient);
        locator<NavigationService>().navigatorKey.currentState.pushNamed(PersonalChat.routeName, arguments: [userId, apType, scheduledConversation]);
      }
    } catch (e) {
      debugPrint("parsing error : ${e.toString()}");
    }
  }
*/
/*
  void fcmMsgOnLaunch(message) {
    try {
      final data = message['data'];
      String screen = data['screen'];

      if (screen == 'PersonalChat') {
        String appointmentType = data['appointmentType'];
        AppointmentType apType;
        if (appointmentType == AppointmentType.SCHEDULED_APPOINTMENT.toString()) {
          apType = AppointmentType.SCHEDULED_APPOINTMENT;
        } else {
          apType = AppointmentType.PAST_APPOINTMENT;
        }
        String userId = data['id_doctor'];
        Map<String, dynamic> patient = json.decode(data['patient']);
        String patientId = patient['id_doctor'].toString();
        if (patient['id_doctor'] is String) {
          String pId = patient['id_doctor'];
          patient['id_doctor'] = int.parse(pId);
        }
        ScheduledConversation scheduledConversation = ScheduledConversation.fromMapDynamic(patient);
        setOnLaunchData(routeName:PersonalChat.routeName,arguments:[userId, apType, scheduledConversation] );
        // locator<NavigationService>().navigatorKey.currentState.pushNamed(PersonalChat.routeName, arguments: [userId, apType, scheduledConversation]);
      }
    } catch (e) {
      debugPrint("parsing error : ${e.toString()}");
    }
  }
  */
}
