import 'package:carousel_slider/carousel_slider.dart';
import 'package:infinite_health_care_doctor/bloc/loading/loading_bloc_event.dart';
import 'package:infinite_health_care_doctor/bloc/loading/loading_bloc.dart';
import 'package:infinite_health_care_doctor/widgets/loading_widget.dart';
import 'package:infinite_health_care_doctor/main.dart';
import 'package:infinite_health_care_doctor/main.dart';
import 'package:flutter/material.dart';
import 'package:infinite_health_care_doctor/api/api_request.dart';
import 'package:infinite_health_care_doctor/models/blog_model.dart';
import 'package:infinite_health_care_doctor/models/user_model.dart';
import 'package:infinite_health_care_doctor/ui/home/blog_detail_screen.dart';
import 'package:infinite_health_care_doctor/ui/home_screen.dart';
class HealthTips extends StatefulWidget {
  static const String routeName = '/health';
  @override
  _HealthTipsState createState() => _HealthTipsState();
}

class _HealthTipsState extends State<HealthTips> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
       appBar: AppBar(
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(bottomLeft:Radius.circular(16.0),bottomRight: Radius.circular(16.0)),
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color:Theme.of(context).primaryColor )
              
             
         ,
          onPressed: (){
            Navigator.of(context).pop();
          },
        ),
        backgroundColor: Theme.of(context).accentColor,
        title: Text(
          'Health Tips',
          style: TextStyle(
            fontSize:22.0,
            fontWeight: FontWeight.bold,
            color: Theme.of(context).primaryColor,
          ),
        ),

      ),
       body: Center(
         child:Container(
            child:FutureBuilder(
              future: ApiRequest.getBlogList(doctor_id: appUserModel.id),
              builder: (context, snapshot) {
                if(snapshot.hasData){
                  locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
                  List<dynamic> pReviewList = snapshot.data['data'];
                  List<BlogModel> blogList = [];
                  pReviewList.forEach((element) {
                    BlogModel pReview = BlogModel.fromMap(element);
                    blogList.add(pReview);
                  });
                  debugPrint("blog length : ${blogList.length}");
                  return blogList.length>0?Container(
                    child: CarouselSlider(
                      options: CarouselOptions(
                        height: 400,
                      ),
                      items: blogList.asMap().entries.map((e) {
                        return Card(
                          elevation: 1,
                          color: Colors.grey,
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
                          child: Container(
                            padding: EdgeInsets.all(12),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      "${e.value.title}",
                                      style: TextStyle(
                                        fontSize: 12.0,
                                        color: Theme.of(context).primaryColor.withOpacity(0.8),
                                      ),
                                    ),
                                    Text(
                                      "${e.value.date}",
                                      style: TextStyle(
                                        fontSize: 12.0,
                                        color: Theme.of(context).primaryColor.withOpacity(0.8),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 80),
                                Container(
                                  child: Text(
                                    "${e.value.description}",
                                    style: TextStyle(fontSize: 28.0,  color: Theme.of(context).primaryColor, fontWeight: FontWeight.bold),
                                  ),
                                ),
                                SizedBox(height: 120),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      "",
                                      style: TextStyle(
                                        fontSize: 18.0,
                                        color: Theme.of(context).primaryColor.withOpacity(0.8),
                                      ),
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        Navigator.of(context).pushNamed(BlogDetailScreen.routeName, arguments: [e.value]);
                                      },
                                      child: Text(
                                        "Read more",
                                        style: TextStyle(
                                          fontSize: 18.0,
                                          color: Theme.of(context).primaryColor.withOpacity(0.8),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        );
                      }).toList(),

                    ),
                  ):Container(
                    margin: EdgeInsets.only(top: 30),
                    alignment: Alignment.center,
                    child: Text(
                      "No health tips found",
                      style: TextStyle(fontSize: 16),
                    ),
                  );
                }else if(snapshot.hasError){
                  locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
                  return Container(
                    child: Center(
                      child: Text(snapshot.error.toString()),
                    ),
                  );
                }else {
                  return LoadingWidget(initialData: true,);
                }
              },

            ),
          ),
      ),
     
    );
  }
  
}