import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:infinite_health_care_doctor/ui/home_screen.dart';
class CreateAcount extends StatefulWidget {
  static const String routeName = '/createAcount';
  @override
  _CreateAcountState createState() => _CreateAcountState();
}

class _CreateAcountState extends State<CreateAcount> {
  var _fullNameController = new TextEditingController();
  var _phoneNumberController = new TextEditingController();

  final GlobalKey<FormState> _fbKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Theme.of(context).accentColor,
        leading: IconButton(
          onPressed: (){
            Navigator.of(context).pop();
          },
          icon: Icon(Icons.arrow_back,color: Theme.of(context).primaryColor),
        ),
        title: Text(
          "Create An Acount",
          style:TextStyle(
            fontSize: 20.0,
            fontWeight: FontWeight.bold,
            color: Theme.of(context).primaryColor,
          ),
        ), 
      ),
      body: SingleChildScrollView(
        child:Container(
          height: double.maxFinite,
          color: Theme.of(context).primaryColor,
          child:Column(
            children: <Widget>[
              Container(
                padding:EdgeInsets.all(12.0),
                child:Column(
                  children: <Widget>[
                    Form(
                      key: _fbKey,
/*                      initialValue: {
                       
                      },*/
                      // autovalidate: true,
                      autovalidateMode: AutovalidateMode.always,
                      child: Column(
                        children: <Widget>[
                          Container(
                            height: 50.0,
                            margin:  const EdgeInsets.only(top: 12.0),
                            padding: const EdgeInsets.only(left: 12.0,right: 12.0),
                            decoration: BoxDecoration(
                              border: Border.all(width: 1.5,color: Colors.grey),
                              borderRadius: BorderRadius.circular(12.0),
                              color: Colors.grey.withOpacity(0.4)                          
                            ),
                            child:TextFormField(
                              // attribute: "Full Name",
                              // initialValue: 'raoufsmari',//for testing
                              controller: _fullNameController,
                              decoration: InputDecoration(
                                hintText: "Full Name",
                                hintStyle: TextStyle(


                                ),
                                border: InputBorder.none,

                              ),
                              validator: (value) {
                                if(value.length>70){
                                  return "Invalid length";
                                }
                                if(value.length==0){
                                  return "Required";
                                }
                                return null;
                              },


                            ), 
                          ),
                          Container(
                            height: 50.0,
                            margin:  const EdgeInsets.only(top: 12.0),
                            padding: const EdgeInsets.only(left: 12.0,right: 12.0),
                            decoration: BoxDecoration(
                              border: Border.all(width: 1.5,color: Colors.grey),
                              borderRadius: BorderRadius.circular(12.0),
                              color: Colors.grey.withOpacity(0.4)                           
                            ),
                            child:DropdownButtonFormField(
                              // initialValue: 'New York',//for testing
                              // attribute: "city",
                              decoration: InputDecoration(border: InputBorder.none,hintText: "City",hintStyle: TextStyle()),
                              hint: Text('Select your city',style:TextStyle()),
                              
                              items: ['New York', 'California', 'Los Angelos','las Vigas','Wachinton']
                                .map((country) => DropdownMenuItem(
                                   value: country,
                                   child: Text("$country")
                              )).toList(),
                            ),
                          ),
                          Container(
                            height: 50.0,
                            margin:  const EdgeInsets.only(top: 12.0),
                            padding: const EdgeInsets.only(left: 12.0,right: 12.0),
                            decoration: BoxDecoration(
                              border: Border.all(width: 1.5,color: Colors.grey),
                              borderRadius: BorderRadius.circular(12.0),
                              color: Colors.grey.withOpacity(0.4)                           
                            ),
                            child: TextFormField(
                              // attribute: "phone Number",
                              // initialValue: '213796113384',//for testing
                              controller: _phoneNumberController,
                              decoration: InputDecoration(border: InputBorder.none,hintText: "Phone Number",hintStyle: TextStyle(),prefixText: "+",),
                              keyboardType: TextInputType.number,
                              validator: (value) {

                                if(value.length==0){
                                  return "Required";
                                }
                                return null;
                              },


                            ),
                          ),
                          /*Container(
                            height: 50.0,
                            margin:  const EdgeInsets.only(top: 12.0),
                            padding: const EdgeInsets.only(left: 12.0,right: 12.0),
                            decoration: BoxDecoration(
                              border: Border.all(width: 1.5,color: Colors.grey),
                              borderRadius: BorderRadius.circular(12.0),
                              color: Colors.grey.withOpacity(0.4)                           
                            ),
                            child: DateTi(
                              attribute: "Birth Day",
                              inputType: InputType.date,
                              decoration:
                                  InputDecoration(border: InputBorder.none,hintText: "Birth Day",hintStyle: TextStyle()),
                            ),
                          ),*/
                          Container(
                            height: 50.0,
                            margin:  const EdgeInsets.only(top: 12.0),
                            padding: const EdgeInsets.only(left: 12.0,right: 12.0),
                            decoration: BoxDecoration(
                              border: Border.all(width: 1.5,color: Colors.grey),
                              borderRadius: BorderRadius.circular(12.0),
                              color: Colors.grey.withOpacity(0.4)                           
                            ),
                            child:DropdownButtonFormField(
                              // attribute: "gender",

                              decoration: InputDecoration(border: InputBorder.none,hintText: "Gender",hintStyle: TextStyle()),
                              // initialValue: 'Male',
                              hint: Text('Select Gender'),
                              items: ['Male', 'Female', 'Other']
                                .map((gender) => DropdownMenuItem(
                                   value: gender,
                                   child: Text("$gender")
                              )).toList(),
                            ),
                          ),
                          Container(
                            height: 50.0,
                            margin:  const EdgeInsets.only(top: 12.0),
                            padding: const EdgeInsets.only(left: 12.0,right: 12.0),
                            decoration: BoxDecoration(
                              border: Border.all(width: 1.5,color: Colors.grey),
                              borderRadius: BorderRadius.circular(12.0),
                              color: Colors.grey.withOpacity(0.4)                           
                            ),
                            child: TextFormField(
                              // attribute: "Weight",
                              initialValue: '25',//for testing
                              keyboardType: TextInputType.number,
                              decoration:InputDecoration(border: InputBorder.none,hintText: "Weight",hintStyle: TextStyle(),suffixText: 'KG'),
                              validator: (value) {
                                if(value.length>100){
                                  return "Invalid length";
                                }
                                if(value.length==0){
                                  return "Required";
                                }
                                return null;
                              },
                            ),
                          ),
                          Container(
                            height: 50.0,
                            margin:  const EdgeInsets.only(top: 12.0),
                            padding: const EdgeInsets.only(left: 12.0,right: 12.0),
                            decoration: BoxDecoration(
                              border: Border.all(width: 1.5,color: Colors.grey),
                              borderRadius: BorderRadius.circular(12.0),
                              color: Colors.grey.withOpacity(0.4)                           
                            ),
                            child: TextFormField(
                              // attribute: "Height",
                              initialValue: '62',//for testing
                              keyboardType: TextInputType.number,
                              decoration:InputDecoration(border: InputBorder.none,hintText: "Height",hintStyle: TextStyle(),suffixText: 'CM'),
                              validator: (value) {
                                if(value.length>1000){
                                  return "Invalid length";
                                }
                                if(value.length==0){
                                  return "Required";
                                }
                                return null;
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                )
              ), 
            ],
          ),
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        elevation: 0,
        child:Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              MaterialButton(
                child: Text(
                  "Submit",
                  style: TextStyle(
                    color:Theme.of(context).accentColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 24.0,
                    fontFamily: "Poppins"
                  ),
                ),
                onPressed: () {
                  if (_fbKey.currentState.validate()) {
                    // print(_fbKey.currentState.value);
                    Navigator.of(context).pushNamed(HomeScreen.routeName, arguments:[_fullNameController.text,_phoneNumberController.text.toString()]);
                  }
                },
              ),
              MaterialButton(
                child: Text(
                  "Reset",
                  style: TextStyle(
                    color:Colors.grey,
                    fontWeight: FontWeight.bold,
                    fontSize: 24.0,
                    fontFamily: "Poppins"
                  ),
                ),
                onPressed: () {
                  _fbKey.currentState.reset();
                },
              ),
            ],
          ),
        ),
      ),      
    );
  }
}