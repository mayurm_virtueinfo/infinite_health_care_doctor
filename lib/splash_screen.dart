import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:infinite_health_care_doctor/bloc/loading/loading_bloc.dart';
import 'package:infinite_health_care_doctor/bloc/loading/loading_bloc_event.dart';
import 'package:infinite_health_care_doctor/main.dart';
import 'package:infinite_health_care_doctor/services/navigation_service.dart';
import 'package:infinite_health_care_doctor/bloc/personal_chat_bloc.dart';
import 'package:infinite_health_care_doctor/ui/conversation/personal_chat.dart';
import 'package:infinite_health_care_doctor/routes_generator.dart';
import 'package:infinite_health_care_doctor/bloc/life_cycle_bloc.dart';
import 'package:infinite_health_care_doctor/main.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:infinite_health_care_doctor/api/api_request.dart';
import 'package:infinite_health_care_doctor/firebase_notification/firebase_notification_handler.dart';
import 'package:infinite_health_care_doctor/models/user_model.dart';
import 'package:infinite_health_care_doctor/ui/home_screen.dart';
import 'package:infinite_health_care_doctor/ui/intro/intro_screen.dart';
import 'package:infinite_health_care_doctor/ui/loginorsignup/patient_registration_screen.dart';
import 'package:infinite_health_care_doctor/ui/loginorsignup/signup.dart';
import 'package:infinite_health_care_doctor/bloc/user_model_bloc.dart';

import 'package:infinite_health_care_doctor/utils/app_preferences.dart';
import 'package:infinite_health_care_doctor/utils/utility.dart';
import 'package:shared_preferences/shared_preferences.dart';
class SplashScreen extends StatefulWidget {
  static const String routeName = "/";
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  MediaQueryData queryData;
  final FacebookLogin facebookSignIn = FacebookLogin();
  /// Setting duration in splash screen
  startTime() async {
    return new Timer(Duration(milliseconds: 2000), NavigatorPage);
  }

  /// To navigate layout change
  void NavigatorPage() async{

    /*UserModel userModel = UserModel(
      id: '123456789',
      // loginType: AppPreferences.EMAIL,
      email: 'mayurm.virtueinfo@gmail.com',
    );
    Navigator.pushNamedAndRemoveUntil(context, PatientRegistrationScreen.routeName, (route) => false, arguments: [userModel]);

    return;*/
    locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
    bool isAppOpened = appPreference.getBool(AppPreferences.is_app_opened)??false;
    // isAppOpened = false;
    if(!isAppOpened){
      await appPreference.setBool(AppPreferences.is_app_opened,true);
      Navigator.of(context).pushReplacementNamed(IntroScreen.routeName);
      return;
    }

    User user = await FirebaseAuth.instance.currentUser;
    bool isFbLogin = await facebookSignIn.isLoggedIn;
    if(user == null && (isFbLogin == null || !isFbLogin)){
      debugPrint("not logged in");
      Navigator.of(context).pushReplacementNamed(SignUp.routeName);
    }else{
      String userId = '';
      if(user != null){
        userId = user.uid;
      } else if(isFbLogin){
        final FacebookAccessToken accessToken = await facebookSignIn.currentAccessToken;

        final graphResponse = await http.get(Uri.parse(
            'https://graph.facebook.com/v2.12/me?fields=name,picture.width(800).height(800),first_name,last_name,email&access_token=${accessToken.token}'));
        final profile = jsonDecode(graphResponse.body);

        debugPrint("facebook user : $profile");
        debugPrint("accessToken userId : ${accessToken.userId}");
        userId = accessToken.userId;
      }
      /*debugPrint("firebase user : ${user.toString()}");
      debugPrint("facebook user : ${isFbLogin}");*/
      debugPrint("User Id : $userId");
      Map<String,dynamic> mapResult = await ApiRequest.postValidateDoctor(id_firebase: "${userId}");
      final data = mapResult['data'];
      if (data is Map<String, dynamic> && data['id']!=0) {
        UserModel updatedUser = UserModel.fromMap(data);
        locator<UserModelBloc>().userModelEventSink.add(updatedUser);
        // Navigator.pushNamedAndRemoveUntil(context, HomeScreen.routeName, (route) => false);
        if(mapLaunchData != null && mapLaunchData.length>0){
          Utility.checkForNotification(mapLaunchData);
        }else {
          debugPrint("-------5");
          Navigator.of(context).pushReplacementNamed(HomeScreen.routeName);
        }
      } else if (data is int && data == 0) {
        await Utility.logoutUser();
        Navigator.pushNamedAndRemoveUntil(context, SignUp.routeName,(route) => false);
      }
      return;
    }
    locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());

    /*
    User user = await FirebaseAuth.instance.currentUser;
    UserModel mUser = await UserModel.user();
    // mUser.id = "68";
    bool isAppOpened = await appPreference.get(AppPreferences.is_app_opened)??false;
    debugPrint("isAppOpened : $isAppOpened");

    if(!isAppOpened){
      await appPreference.setBool(AppPreferences.is_app_opened,true);
      Navigator.of(context).pushReplacementNamed(IntroScreen.routeName);
    }
    else{
      if(mUser.isAuthenticated){
        locator<UserModelBloc>().userModelEventSink.add(mUser);
        if(mapLaunchData != null && mapLaunchData.length>0){
          Utility.checkForNotification(mapLaunchData);
        }else {
          Navigator.of(context).pushReplacementNamed(HomeScreen.routeName);
        }
      }
      else{
        Navigator.of(context).pushReplacementNamed(SignUp.routeName);
      }
    }
    */
  }

  /// Declare startTime to InitState
  @override
  void initState() {
    super.initState();
    initSplashScreen();
    locator<LifeCycleBloc>().lifeCycleStream.listen((event) {
      debugPrint("---LifeCycleEvent : $event");
      // debugPrint("---LifeCycleEvent - Current route : ${RouteGenerator.currentRoute}");
      // if(RouteGenerator.currentRoute != null && RouteGenerator.currentRoute == PersonalChat.routeName){
        locator<PersonalChatBloc>().personalChatEventSink.add(event);
      // }else if(){
      //   locator<PersonalChatBloc>().personalChatEventSink.add(event);
      // }
    });
    Utility.requestForLocationPermission();
  }
  @override
  void dispose() {
    super.dispose();
  }
  void initSplashScreen() async{

    FirebaseNotificationHandler().setUpFirebase((fToken) async{
        fcmToken = fToken;
        await ApiRequest.postStoreToken(firebaseRegToken: fToken);
        startTime();
    });

  }
  @override
  Widget build(BuildContext context) {

    queryData = MediaQuery.of(context);
    double devicePixelRatio = queryData.devicePixelRatio;

    debugPrint("Device Pixel Ratio $devicePixelRatio");
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image:AssetImage('images/image-home.jpeg'),
            fit: BoxFit.cover,
            ),
        ),
        child: Container(
          decoration: BoxDecoration(
            color: Theme.of(context).accentColor.withOpacity(0.8),
          ),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.all(12.0),
                  child: Text(
                    'IHC-Doctor',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color:Theme.of(context).primaryColor.withOpacity(0.8),
                      fontWeight: FontWeight.bold,
                      fontSize: 30.0,
                      fontFamily: "Poppins"
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}