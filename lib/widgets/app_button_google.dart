import 'package:flutter/material.dart';

class AppButtonGoogle extends StatefulWidget {
  Function onTap;
  AppButtonGoogle({@required this.onTap});
  @override
  _AppButtonGoogleState createState() => _AppButtonGoogleState();
}

class _AppButtonGoogleState extends State<AppButtonGoogle> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 20.0, right: 50.0, left: 50.0),
      height: 40,
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
              color: Colors.grey.withOpacity(0.10),
              offset: Offset(0, 4),
              blurRadius: 10)
        ],
      ),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          elevation: 0.2,
          primary: Theme.of(context).primaryColor,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0),
          ),
        ),

        onPressed: widget.onTap/*() {
          Navigator.of(context).pushNamed(HomeScreen.routeName,
              arguments: [widget.name, widget.phoneNumber]);
        }*/,
        child: Container(
          child: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image(
                  image: AssetImage('images/google-fill.png'),
                ),
                Text(
                  ' Google',
                  style: TextStyle(
                    fontSize: 16.0,
                    color: Theme.of(context).focusColor,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
