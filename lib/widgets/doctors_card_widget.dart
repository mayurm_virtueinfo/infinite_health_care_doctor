import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:flutter/material.dart';
import 'package:infinite_health_care_doctor/config/app_config.dart' as config;
import 'package:infinite_health_care_doctor/config/const.dart';
import 'package:infinite_health_care_doctor/models/doctor_model.dart' as doctorModel;
import 'package:infinite_health_care_doctor/ui/home/doctor_profile.dart';
import 'package:infinite_health_care_doctor/utils/my_toast.dart';
import 'package:infinite_health_care_doctor/utils/strings.dart';
class DoctorsCardWidget extends StatefulWidget {
  final doctorModel.DoctorModel doctors;
  final title;
  const DoctorsCardWidget({Key key, this.title,this.doctors}) : super(key: key);
  
  @override
  _DoctorsCardWidgetState createState() => _DoctorsCardWidgetState();
}

class _DoctorsCardWidgetState extends State<DoctorsCardWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 150.0,
        padding: const EdgeInsets.all(6.0),
        child:TextButton(
          style: TextButton.styleFrom(
            primary: Theme.of(context).primaryColor,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(16.0),
            ),
          ),

          onPressed: (){
            Navigator.of(context).pushNamed(DoctorProfile.routeName,arguments: ['${widget.doctors.id}',widget.title]);

          },
          child: Card(
            elevation:0.2,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(16.0),
            ),
            child: Container(
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                      color: Colors.grey.withOpacity(0.1), offset: Offset(0,4), blurRadius: 10)
                ],
              ),
              padding: const EdgeInsets.only(top:12.0,bottom: 12.0,left: 12.0,right: 12.0),
                child:Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(
                      flex: 2,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          ball(widget.doctors.originalPath??Const.defaultProfileUrl,Colors.transparent),
                          Text(
                            widget.doctors.status=='0'?Strings.CLOSED_TODAY:Strings.OPEN_TODAY,
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontSize: 12.0,
                              color: widget.doctors.status=='0'?Colors.red:Colors.green,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 3,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                margin: const EdgeInsets.only(bottom: 6.0),
                                child: Text(
                                  '${widget.doctors.title}',
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    fontSize: 12.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                              (widget.doctors.degree != null || widget.doctors.description != null)?Container(
                                padding: const EdgeInsets.all(6.0),
                                decoration: BoxDecoration(
                                  border: Border.all(width: 1,color: Colors.grey.withOpacity(0.1)),
                                  borderRadius: BorderRadius.circular(12),
                                ),
                                child:Text(
                                  '${widget.doctors.degree??""} ${widget.doctors.description??""}',
                                  style: TextStyle(
                                    color:Colors.grey,
                                    fontSize: 10.0
                                  ),
                                ),
                              ):Container(),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                '${widget.doctors.timings??""}',
                                style: TextStyle(
                                  fontSize: 10.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),

                              Row(
                                children: <Widget>[
                                  Icon(Icons.star,color: Colors.yellow,),
                                  Text('${widget.doctors.rating??""}',style: TextStyle(),),
                                ],
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
    ),
    );
  }
  Widget ball(String image,Color color){
    return Container(
      height: 60,width: 60.0,
      child: ClipOval(
        child: FancyShimmerImage(
          imageUrl: image,
          shimmerBaseColor: Colors.white,
          shimmerHighlightColor: config.Colors().mainColor(1),
          shimmerBackColor: Colors.green,
          errorWidget: Image.network('https://i0.wp.com/www.dobitaobyte.com.br/wp-content/uploads/2016/02/no_image.png?ssl=1'),
        ),
      )
      ,
      
    );
  }
}