import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:flutter/material.dart';
import 'package:infinite_health_care_doctor/config/const.dart';
import 'package:infinite_health_care_doctor/config/app_config.dart' as config;

class ProfileHeader extends StatefulWidget {
  Function onTap;
  String imagePath;
  bool isImageUploading = false;

  ProfileHeader({this.imagePath, this.onTap, this.isImageUploading});

  @override
  _ProfileHeaderState createState() => _ProfileHeaderState();
}

class _ProfileHeaderState extends State<ProfileHeader> {


  String userEmail;
  String firstName;
  String lastName;
  String profileUrl;
  String coverUrl;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Stack(
      children: <Widget>[
        /*Image.network(
          "https://images.pexels.com/photos/396547/pexels-photo-396547.jpeg?auto=compress&cs=tinysrgb&h=350",
          fit: BoxFit.fitWidth,
        ),*/
        Stack(
          children: <Widget>[
            Align(
              alignment: Alignment.center,
              child: Container(
                width: 120,
                height: 120,
                padding: EdgeInsets.all(1),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: config.Colors().mainColor(1)
                ),
                child: widget.isImageUploading
                    ? Center(
                      child: SizedBox(
                          height: 50, width: 50, child: CircularProgressIndicator(
                  strokeWidth: 1,
                )),
                    )
                    : SizedBox(
                        width: 120,
                        height: 120,
                        child: ClipOval(
                          child: FancyShimmerImage(
                            boxFit: BoxFit.fill,
                            imageUrl: widget.imagePath ?? Const.defaultProfileUrl,
                            shimmerBaseColor: Colors.white,
                            shimmerHighlightColor: config.Colors().mainColor(1),
                            errorWidget: Image.network('https://i0.wp.com/www.dobitaobyte.com.br/wp-content/uploads/2016/02/no_image.png?ssl=1'),
                            //                          shimmerBackColor: Colors.blue,
                          ) /* Image.network(widget.imagePath!=null?widget.imagePath:defaultImageUrl,
                                        fit: BoxFit.fitWidth,
                                      )*/
                          ,
                        ),
                      ),
              ),
            ),
            Align(
              alignment: Alignment.center,
              child: GestureDetector(
                onTap: widget
                    .onTap /*() {
                  _settingModalBottomSheet(context);
                }*/
                ,
                child: SizedBox(
                  width: 120,
                  height: 120,
                  child: Container(
                    child: Align(
                      alignment: Alignment.bottomRight,
                      child: Container(
                        width: 40,
                        height: 40,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: config.Colors().mainColor(1)),
                        child: Icon(
                          Icons.photo_camera,
                          size: 25,
                          color: Colors.white,

                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),

          ],
        )
      ],
    );
  }
}
