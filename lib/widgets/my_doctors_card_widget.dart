import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:infinite_health_care_doctor/models/doctor_model.dart' as doctoreModel;
import 'package:infinite_health_care_doctor/config/app_config.dart' as config;
import 'package:flutter/material.dart';
import 'package:infinite_health_care_doctor/config/const.dart';
import 'package:infinite_health_care_doctor/models/appointment_type.dart';
import 'package:infinite_health_care_doctor/models/my_doctor_model.dart' as myDoctorModel;
import 'package:infinite_health_care_doctor/ui/conversation/personal_chat.dart';
import 'package:infinite_health_care_doctor/ui/home/doctor_profile.dart';
class MyDoctorsCardWidget extends StatefulWidget {

  final myDoctorModel.MyDoctorModel doctors;
  final AppointmentType appointmentType;
  const MyDoctorsCardWidget({Key key, this.doctors,this.appointmentType}) : super(key: key);
  
  @override
  _MyDoctorsCardWidgetState createState() => _MyDoctorsCardWidgetState();
}

class _MyDoctorsCardWidgetState extends State<MyDoctorsCardWidget> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        Navigator.of(context).pushNamed(DoctorProfile.routeName,arguments: ['${widget.doctors.id_doctor}',widget.doctors.doctor_name]);
      },
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(16.0),
        ),
        child: Container(
          /*decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.1), offset: Offset(0,4), blurRadius: 10)
            ],
          ),*/
          padding: const EdgeInsets.only(top:20.0,bottom: 20.0,left: 12.0,right: 12.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Container(
                    margin: const EdgeInsets.only(right: 25.0),
                    child:ball(this.widget.doctors.avatar_image),
                  ),
                  Container(
                    width: 150,
                    child:Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          '${widget.doctors.doctor_name}',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 14.0,
                          ),
                        ),
                        SizedBox(height: 12,),
                        Text(
                          '${widget.doctors.description??""}',
                          textWidthBasis: TextWidthBasis.longestLine,

                          style: TextStyle(

                            color: Colors.grey,
                            fontSize: 12.0,
                          ),
                        ),
                      ],
                    )

                  ),
                ],
              ),
              Container(
                child: IconButton(
                  padding: EdgeInsets.all(0),
                  onPressed: (){
                   Navigator.pushNamed(context, PersonalChat.routeName,arguments: ['${widget.doctors.id_doctor}',widget.appointmentType,widget.doctors]);
                  },
                  icon: Icon(Icons.chat_bubble_outline),
                  iconSize: 20,
                  color: Theme.of(context).accentColor,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
  Widget ball(String image){
    return Container(
      height: 60,width: 60.0,
      child: ClipOval(
        child: FancyShimmerImage(
          imageUrl: image??Const.defaultProfileUrl,
          shimmerBaseColor: Colors.white,
          shimmerHighlightColor: config.Colors().mainColor(1),
          shimmerBackColor: Colors.green,
          errorWidget: Image.network('https://i0.wp.com/www.dobitaobyte.com.br/wp-content/uploads/2016/02/no_image.png?ssl=1'),
        ),
      )
      ,

    );
  }
}