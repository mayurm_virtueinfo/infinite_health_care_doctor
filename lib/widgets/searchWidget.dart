import 'package:flutter/material.dart';
import 'package:infinite_health_care_doctor/ui/home/patient_list.dart';
import 'package:infinite_health_care_doctor/ui/home/home_tab.dart';

class SearchBarWidget extends StatelessWidget {
  String routeName = "SearchBarWidget";
  Function onTap;
  bool enabled;
  SearchBarWidget({
    Key key,
    this.enabled,
    this.onTap
  }) : super(key: key);
  FocusNode myFocusNode;

  @override
  Widget build(BuildContext context) {
    /*if(isTapped && fromScreen == DoctorsList.routeName){
      myFocusNode.requestFocus();
    }*/
    return GestureDetector(
      onTap: this.onTap,
      child: Container(
        decoration: BoxDecoration(
          color: Theme.of(context).primaryColor,
          borderRadius: BorderRadius.circular(16),
          boxShadow: [
            BoxShadow(color: Theme.of(context).hintColor.withOpacity(0.10), offset: Offset(0, 4), blurRadius: 10)
          ],
        ),
        child: Stack(
          alignment: Alignment.centerRight,
          children: <Widget>[
            TextField(
              enabled: this.enabled,
              onTap: this.onTap,
              decoration: InputDecoration(
                contentPadding: EdgeInsets.all(12),
                hintText: 'Search',
                hintStyle: TextStyle(color: Theme.of(context).hintColor,),
                prefixIcon: Icon(Icons.search, size: 20, color: Theme.of(context).hintColor),
                border: UnderlineInputBorder(borderSide: BorderSide.none),
                enabledBorder: UnderlineInputBorder(borderSide: BorderSide.none),
                focusedBorder: UnderlineInputBorder(borderSide: BorderSide.none),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
