import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:infinite_health_care_doctor/config/app_config.dart' as config;
import 'package:flutter/material.dart';
import 'package:infinite_health_care_doctor/config/const.dart';
import 'package:infinite_health_care_doctor/models/my_appointment_model.dart' as myAppointmentModel;
import 'package:infinite_health_care_doctor/models/appointment_type.dart';
import 'package:infinite_health_care_doctor/ui/home/doctor_profile.dart';
class AppointmentsWidget extends StatefulWidget {
  final myAppointmentModel.MyAppointmentModel appointment;
  final AppointmentType appointmentType;
  const AppointmentsWidget({Key key, this.appointment,this.appointmentType}) : super(key: key);
  
  @override
  _AppointmentsWidgetState createState() => _AppointmentsWidgetState();
}

class _AppointmentsWidgetState extends State<AppointmentsWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child:Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextButton(
            onPressed: (){
              debugPrint("${widget.appointment.id_doctor}");
              Navigator.of(context).pushNamed(DoctorProfile.routeName,arguments: ['${widget.appointment.id_doctor}',widget.appointment.doctor_name]);
            },
            style: TextButton.styleFrom(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(16.0),
              ),
            ),

            child: Card(
              elevation:0.2,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(16.0),
              ),
              child: Container(
                decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.1), offset: Offset(0,4), blurRadius: 10)
                  ],
                ),
                padding: const EdgeInsets.only(top:20.0,bottom: 20.0,left: 12.0,right: 12.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Container(
                          margin: const EdgeInsets.only(right: 25.0),
                          child: ball(this.widget.appointment.avatar_image),
                          
                        ),
                        Container(
                          child:Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                            '${widget.appointment.doctor_name}',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 14.0,
                            ),
                          ),
                              SizedBox(height: 12,),
                              Text(
                            '${widget.appointment.appointment_date??""}',
                            style: TextStyle(
                              color: Colors.grey,
                              fontSize: 12.0,
                            ),
                          ),
                            ],
                          )
                        ),
                      ],
                    ),
                    
                    Container(
                      child: IconButton(  
                        padding: EdgeInsets.all(0),
                        onPressed: (){
                          //Navigator.of(context).pushNamed(ChatWidget.routeName);
                        },
                        icon: Icon(Icons.calendar_today),
                        iconSize: 20,
                        color: Theme.of(context).accentColor,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          //SizedBox(height: 20.0,child: Center(child: Container(height: 2.0,color: Colors.grey.withOpacity(0.1),),),),

        ],
      )
      
    );
  }
  Widget ball(String image){
    return Container(
      height: 60,width: 60.0,
      child: ClipOval(
        child: FancyShimmerImage(
          imageUrl: image??Const.defaultProfileUrl,
          shimmerBaseColor: Colors.white,
          shimmerHighlightColor: config.Colors().mainColor(1),
          shimmerBackColor: Colors.green,
          errorWidget: Image.network('https://i0.wp.com/www.dobitaobyte.com.br/wp-content/uploads/2016/02/no_image.png?ssl=1'),
        ),
      )
      ,

    );
  }
}