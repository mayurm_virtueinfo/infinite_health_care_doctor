import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:infinite_health_care_doctor/config/app_config.dart' as config;
import 'package:flutter/material.dart';
import 'package:infinite_health_care_doctor/config/const.dart';
import 'package:infinite_health_care_doctor/models/appointment_type.dart';
import 'package:infinite_health_care_doctor/models/notifications_model.dart' as myNotificationModel;
import 'package:infinite_health_care_doctor/ui/conversation/personal_chat.dart';
import 'package:infinite_health_care_doctor/ui/home/doctor_profile.dart';
class ItemNotification extends StatefulWidget {

  final myNotificationModel.NotificationModel notification;
  const ItemNotification({Key key, this.notification}) : super(key: key);
  
  @override
  _ItemNotificationState createState() => _ItemNotificationState();
}

class _ItemNotificationState extends State<ItemNotification> {
  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16.0),
      ),
      child: Container(
        padding: const EdgeInsets.only(top:20.0,bottom: 20.0,left: 12.0,right: 12.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Row(
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.only(right: 25.0),
                  child:ball(this.widget.notification.patient_image),
                ),
                Container(
                  width: 150,
                  child:Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        '${widget.notification.patient_firstname} ${widget.notification.patient_lastname}',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 14.0,
                        ),
                      ),
                      SizedBox(height: 12,),
                      Text(
                        '${widget.notification.message??""}',
                        textWidthBasis: TextWidthBasis.longestLine,

                        style: TextStyle(

                          color: Colors.grey,
                          fontSize: 12.0,
                        ),
                      ),
                    ],
                  )

                ),
              ],
            ),
            /*Container(
              child: IconButton(
                padding: EdgeInsets.all(0),
                onPressed: (){
                 Navigator.pushNamed(context, PersonalChat.routeName,arguments: ['${widget.doctors.id_doctor}',widget.appointmentType,widget.doctors]);
                },
                icon: Icon(Icons.chat_bubble_outline),
                iconSize: 20,
                color: Theme.of(context).accentColor,
              ),
            ),*/
          ],
        ),
      ),
    );
  }
  Widget ball(String image){
    return Container(
      height: 70,width: 70.0,
      child: ClipOval(
        child: FancyShimmerImage(
          imageUrl: image??Const.defaultProfileUrl,
          shimmerBaseColor: Colors.white,
          shimmerHighlightColor: config.Colors().mainColor(1),
          shimmerBackColor: Colors.green,
          errorWidget: Image.network('https://i0.wp.com/www.dobitaobyte.com.br/wp-content/uploads/2016/02/no_image.png?ssl=1'),
        ),
      )
      ,

    );
  }
}