import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:infinite_health_care_doctor/config/app_config.dart' as config;
import 'package:infinite_health_care_doctor/utils/custom_styles.dart';
import 'package:infinite_health_care_doctor/widgets/button_black_bottom.dart';
//import 'package:keyboard_visibility/keyboard_visibility.dart';

class LabelTextarea extends StatefulWidget {
  IconData leadingIcon;
  String label;
  String labelValue;
  IconData actionIcon;
  Function onClickEdit;
  LabelTextarea({this.leadingIcon, this.label, this.labelValue, this.actionIcon,this.onClickEdit});

  @override
  _LabelTextareaState createState() => _LabelTextareaState();
}

class _LabelTextareaState extends State<LabelTextarea> {
  /*TODO Keyboard Code
  KeyboardVisibilityNotification _keyboardVisibility = new KeyboardVisibilityNotification();
  int _keyboardVisibilitySubscriberId;
  bool _keyboardState;
  double tempHeight;*/
  @override
  void initState() {
    super.initState();

  }
  @override
  void dispose() {
    super.dispose();
    /*
    TODO Keyboard Code
    _keyboardVisibility.removeListener(_keyboardVisibilitySubscriberId);
    */
  }
  /*TODO Keyboard code
  updateHeight(_keyboardState){
    if(_keyboardState){
      tempHeight = MediaQuery.of(context).size.height / 2 +
          MediaQuery.of(context).viewInsets.bottom;
    }else{
      tempHeight = 180;
    }
  }*/
  @override
  Widget build(BuildContext context) {
    /*
    TODO Keyboard Code
    _keyboardState = _keyboardVisibility.isKeyboardVisible;

    updateHeight(_keyboardState);
    _keyboardVisibilitySubscriberId = _keyboardVisibility.addNewListener(
      onChange: (bool visible) {
        setState(() {
          _keyboardState = visible;
          print("Keyboard from label_text : $_keyboardState");
          updateHeight(_keyboardState);
          *//*_keyboardState?(MediaQuery.of(context).size.height / 2 +
              MediaQuery.of(context).viewInsets.bottom):MediaQuery.of(context).size.height;*//*
        });
      },
    );*/
    return Row(children: <Widget>[
      Container(
          height: 50,
          width: 50,
//          decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.grey),
          child: Icon(widget.leadingIcon,color: config.Colors().mainColor(1)),),
      Expanded(
          child: Wrap(direction: Axis.vertical, children: <Widget>[
        Text(
          (widget.label==null)?'':widget.label,
          style: CustomStyles.txtCategory,
        ),
        SizedBox(height: 5,),
        Container(
          child: (widget.labelValue == null)?Text(
            "Enter ${widget.label}",style: CustomStyles.txtError,
          ):Text(widget.labelValue,
            overflow: TextOverflow.ellipsis,
            maxLines: 20,
            style: CustomStyles.txtEdit,
          ),
        ),
      ])),
      GestureDetector(
        onTap: widget.onClickEdit,
        child: Container(
            margin: EdgeInsets.all(20),
            height: 50,
            width: 50,
//            decoration:
//                BoxDecoration(shape: BoxShape.circle, color: Colors.grey),
            child: Icon(widget.actionIcon,color: config.Colors().mainColor(1),)),
      ),
    ]);
  }


  showMyTextareaModalBottomSheet() {
    TextEditingController _firstNameController = TextEditingController();
    _firstNameController.text = widget.labelValue;
    showModalBottomSheet(
        isScrollControlled: true,
        isDismissible: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(10),
            topRight: Radius.circular(10),
          ),
        ),
        context: context,
        builder: (BuildContext bc) {
          return Container(
            margin: EdgeInsets.only(top: 20),
            /*TODO Keyboard Code
            height: tempHeight,*/
            child: new Wrap(
              direction: Axis.horizontal,
              children: <Widget>[
                Container(
                  child: TextField(
                    maxLines: null,
                    keyboardType: TextInputType.multiline,
                    controller: _firstNameController,
                    decoration: InputDecoration(
                      prefixIcon: Icon(
                        widget.leadingIcon,
                        color: config.Colors().mainColor(1),
                      ),
                      hintText: "enter ${widget.label}",
                      labelStyle: TextStyle(color: config.Colors().mainColor(1)),
                      labelText: "Enter ${widget.label}",
                      border: OutlineInputBorder(),
                    ),
                  ),
                  padding: EdgeInsets.all(20),
                ),
                SizedBox(
                  width: double.infinity,
                  child: InkWell(
                    child: ButtonBlackBottom(),
                    onTap: (){
                      Navigator.pop(context);
                      setState(() {
                        // widget.onChanged(_firstNameController.text);
                        widget.labelValue = _firstNameController.text;
                      });
                    },
                  ),
                )
              ],
            ),
          );
        });
  }
}

