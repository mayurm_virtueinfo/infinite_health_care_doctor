import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:infinite_health_care_doctor/config/app_config.dart' as config;
import'package:flutter/material.dart';
import 'package:infinite_health_care_doctor/models/appointment_type.dart';
import 'package:infinite_health_care_doctor/models/past_conversation.dart' as pastModel;
import 'package:infinite_health_care_doctor/ui/conversation/personal_chat.dart';

class ConversationItemWidget extends StatefulWidget {
  final String peerId;
  final pastModel.PastConversation conversation;
  final AppointmentType appointmentType;
  const ConversationItemWidget({Key key,this.peerId, this.conversation,this.appointmentType}) : super(key: key);
  @override
  _ConversationItemWidgetState createState() => _ConversationItemWidgetState();
}

class _ConversationItemWidgetState extends State<ConversationItemWidget> {
  @override
  Widget build(BuildContext context){
    debugPrint("CW 2: ${widget.appointmentType}");
    return Container(
      child: TextButton(
        onPressed: (){
          debugPrint("click aType ${widget.appointmentType}");
            Navigator.pushNamed(context, PersonalChat.routeName,arguments: [widget.peerId,widget.appointmentType,widget.conversation]);
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Row(
              children: <Widget>[
                Container(
                margin: const EdgeInsets.only(right: 25.0),
                child:ball(this.widget.conversation.avatar_image),
              ),
                Container(
                child: Text(
                  '${widget.conversation.patient_name}',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 12.0,
                  ),
                ),
              ),
              ],
            ),
            Container(
            height: 10,width: 10.0,
            decoration: BoxDecoration(
              color: Colors.green,
              borderRadius: BorderRadius.circular(100.0),
            ),
          ),
          ],
        ),
      ),
    );
  }
  Widget ball(String image){
    return Container(
      height: 60,width: 60.0,
      child: ClipOval(
        child: FancyShimmerImage(
          imageUrl: image,
          shimmerBaseColor: Colors.white,
          shimmerHighlightColor: config.Colors().mainColor(1),
          shimmerBackColor: Colors.green,
          errorWidget: Image.network('https://i0.wp.com/www.dobitaobyte.com.br/wp-content/uploads/2016/02/no_image.png?ssl=1'),
        ),
      )
      ,

    );
  }
}