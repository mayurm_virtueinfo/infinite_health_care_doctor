
import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:flutter/material.dart';
import 'package:infinite_health_care_doctor/config/const.dart';

class ProfileHeaderRegister extends StatefulWidget {
  Function onTap;
  final imagePath;
  bool isImageUploading = false;
  bool showCameraIcon = true;

  ProfileHeaderRegister({this.imagePath, this.onTap, this.isImageUploading,this.showCameraIcon});

  @override
  _ProfileHeaderRegisterState createState() => _ProfileHeaderRegisterState();
}

class _ProfileHeaderRegisterState extends State<ProfileHeaderRegister> {


  String userEmail;
  String firstName;
  String lastName;
  String profileUrl;
  String coverUrl;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Stack(
      children: <Widget>[
        /*Image.network(
          "https://images.pexels.com/photos/396547/pexels-photo-396547.jpeg?auto=compress&cs=tinysrgb&h=350",
          fit: BoxFit.fitWidth,
        ),*/
        Stack(
          children: <Widget>[
            Align(
              alignment: Alignment.center,
              child: Container(
                width: 120,
                height: 120,
                padding: EdgeInsets.all(1),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Theme.of(context).accentColor
                ),
                child: widget.isImageUploading
                    ? Center(
                      child: SizedBox(
                          height: 50, width: 50, child: CircularProgressIndicator(
                  strokeWidth: 1,
                )),
                    )
                    : SizedBox(
                        width: 120,
                        height: 120,
                        child: ClipOval(
                          child: (widget.imagePath == null )?FancyShimmerImage(
                            boxFit: BoxFit.fill,
                            imageUrl: Const.defaultProfileUrl,
                            shimmerBaseColor: Colors.white,
                            shimmerHighlightColor: Theme.of(context).accentColor,
                            errorWidget: Image.network('https://i0.wp.com/www.dobitaobyte.com.br/wp-content/uploads/2016/02/no_image.png?ssl=1'),
                            //                          shimmerBackColor: Colors.blue,
                          ) : Image.file(widget.imagePath) /* Image.network(widget.imagePath!=null?widget.imagePath:defaultImageUrl,
                                        fit: BoxFit.fitWidth,
                                      )*/
                          ,
                        ),
                      ),
              ),
            ),
            Align(
              alignment: Alignment.center,
              child: GestureDetector(
                onTap: widget
                    .onTap /*() {
                  _settingModalBottomSheet(context);
                }*/
                ,
                child: widget.showCameraIcon?SizedBox(
                  width: 120,
                  height: 120,
                  child: Container(
                    child: Align(
                      alignment: Alignment.bottomRight,
                      child: Container(
                        width: 40,
                        height: 40,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: Theme.of(context).accentColor),
                        child: Icon(
                          Icons.photo_camera,
                          size: 25,
                          color: Colors.white,

                        ),
                      ),
                    ),
                  ),
                ):Container(),
              ),
            ),

          ],
        )
      ],
    );
  }
}
