import 'package:flutter/material.dart';
import 'package:infinite_health_care_doctor/ui/home_screen.dart';

class AppButtonCircularProgress extends StatefulWidget {
  String name,phoneNumber;
  AppButtonCircularProgress({this.name,this.phoneNumber});
  @override
  _AppButtonCircularProgressState createState() => _AppButtonCircularProgressState();
}

class _AppButtonCircularProgressState extends State<AppButtonCircularProgress> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 20.0, right: 50.0, left: 50.0),
      height: 40,
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
              color: Colors.grey.withOpacity(0.10),
              offset: Offset(0, 4),
              blurRadius: 10)
        ],
      ),
      child: ElevatedButton(

        style: ElevatedButton.styleFrom(
          elevation: 0.2,
          primary: Theme.of(context).primaryColor,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0),
          ),
        ),
        onPressed: () {
          Navigator.of(context).pushNamed(HomeScreen.routeName,
              arguments: [widget.name, widget.phoneNumber]);
        },
        child: Container(
          child: Center(
            child: Padding(
              padding: EdgeInsets.all(1),
              child: CircularProgressIndicator()
            ),
          ),
        ),
      ),
    );
  }
}
