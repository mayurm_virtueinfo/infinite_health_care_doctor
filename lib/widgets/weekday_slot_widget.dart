import 'package:flutter/material.dart';
import 'package:infinite_health_care_doctor/utils/app_style.dart';
import 'package:intl/intl.dart';

class WeekdaySlotWidget extends StatefulWidget {
  Function onChange;
  String weekDayTitle;
  Function onSelectStartTime;
  Function onSelectSecondStartTime;
  Function onSelectSecondEndTime;
  Function onSelectEndTime;
  Function isValidTime;
  Function isValidSecondTime;
  Map<String,dynamic> initialData;
  WeekdaySlotWidget({
    this.initialData,
    @required this.weekDayTitle,
    @required this.onSelectEndTime,
    @required this.onSelectStartTime,
    @required this.onChange,
    @required this.isValidTime,
    @required this.onSelectSecondStartTime,
    @required this.onSelectSecondEndTime,
    @required this.isValidSecondTime
  });
  @override
  _WeekdaySlotWidgetState createState() => _WeekdaySlotWidgetState();
}

class _WeekdaySlotWidgetState extends State<WeekdaySlotWidget> {
  bool addSecondTime = false;

  bool validTime = true;
  bool validSecondTime = true;
  int startTimeHour=0;
  int startTimeMinute=0;
  int endTimeHour=0;
  int endTimeMinute=0;



  bool chkSwitchValue = false;
  String selTextStart = '00:00';
  String selTextEnd = '00:00';

  int secondStartTimeHour=0;
  int secondStartTimeMinute=0;
  int secondEndTimeHour=0;
  int secondEndTimeMinute=0;

  String selSecondTextStart = '00:00';
  String selSecondTextEnd = '00:00';
  final double erroHeight = 20;
  final double noErroHeight = 10;

  BoxDecoration addRemoveDecoration () => BoxDecoration(
  shape: BoxShape.circle,
  color: chkSwitchValue?Theme.of(context).accentColor:Colors.grey
  );




  @override
  void initState() {

    super.initState();
    // TODO: implement initState

    if(widget.initialData != null && widget.initialData.isNotEmpty){
      // debugPrint('-- entered');
      String status = widget.initialData['status'];
      // debugPrint('-- status : $status');
      if(status == '1'){
        chkSwitchValue = true;
        widget.onChange(chkSwitchValue);
      }
      else{
        chkSwitchValue = false;
        widget.onChange(chkSwitchValue);
      }
      String firstStartTime = widget.initialData['first_start_time'];
      String firstEndTime = widget.initialData['first_end_time'];
      String secondStartTime = widget.initialData['second_start_time'];
      String secondEndTime = widget.initialData['second_end_time'];



      DateFormat format= DateFormat("HH:mm");
      DateTime dtFirstStartTime = format.parse(firstStartTime);
      // debugPrint("dtFirstStartTime : $dtFirstStartTime");

      DateTime dtFirstEndTime = format.parse(firstEndTime);
      // debugPrint("dtFirstEndTime : $dtFirstStartTime");

      DateTime dtSecondStartTime = format.parse(secondStartTime);
      // debugPrint("dtSecondStartTime : $dtFirstStartTime");

      DateTime dtSecondEndTime = format.parse(secondEndTime);
      // debugPrint("dtSecondEndTime : $dtFirstStartTime");

      startTimeHour=dtFirstStartTime.hour;
      startTimeMinute=dtFirstStartTime.minute;
      endTimeHour=dtFirstEndTime.hour;
      endTimeMinute=dtFirstEndTime.minute;

      String tmpStartTimeHour;
      String tmpStartTimeMinute;
      if(startTimeHour < 10){
        tmpStartTimeHour = '0$startTimeHour';
      }else{
        tmpStartTimeHour = '$startTimeHour';
      }

      if(startTimeMinute < 10){
        tmpStartTimeMinute = '0$startTimeMinute';
      }else{
        tmpStartTimeMinute   = '$startTimeMinute';
      }
      selTextStart = '$tmpStartTimeHour:$tmpStartTimeMinute';
      widget.onSelectStartTime(selTextStart);

      String tmpEndTimeHour;
      String tmpEndTimeMinute;
      if(endTimeHour < 10){
        tmpEndTimeHour = '0$endTimeHour';
      }else{
        tmpEndTimeHour = '$endTimeHour';
      }

      if(endTimeMinute < 10){
        tmpEndTimeMinute = '0$endTimeMinute';
      }else{
        tmpEndTimeMinute   = '$endTimeMinute';
      }
      selTextEnd = '$tmpEndTimeHour:$tmpEndTimeMinute';
      widget.onSelectEndTime(selTextEnd);

      secondStartTimeHour=dtSecondStartTime.hour;
      secondStartTimeMinute=dtSecondStartTime.minute;

      String tmpSecondStartTimeHour;
      String tmpSecondStartTimeMinute;
      if(secondStartTimeHour < 10){
        tmpSecondStartTimeHour = '0$secondStartTimeHour';
      }else{
        tmpSecondStartTimeHour = '$secondStartTimeHour';
      }

      if(secondStartTimeMinute < 10){
        tmpSecondStartTimeMinute = '0$secondStartTimeMinute';
      }else{
        tmpSecondStartTimeMinute   = '$secondStartTimeMinute';
      }

      selSecondTextStart = '$tmpSecondStartTimeHour:$tmpSecondStartTimeMinute';
      widget.onSelectSecondStartTime(selSecondTextStart);

      secondEndTimeHour=dtSecondEndTime.hour;
      secondEndTimeMinute=dtSecondEndTime.minute;

      String tmpSecondEndTimeHour;
      String tmpSecondEndTimeMinute;
      if(secondEndTimeHour < 10){
        tmpSecondEndTimeHour = '0$secondEndTimeHour';
      }else{
        tmpSecondEndTimeHour = '$secondEndTimeHour';
      }

      if(secondEndTimeMinute < 10){
        tmpSecondEndTimeMinute = '0$secondEndTimeMinute';
      }else{
        tmpSecondEndTimeMinute   = '$secondEndTimeMinute';
      }

      selSecondTextEnd = '$tmpSecondEndTimeHour:$tmpSecondEndTimeMinute';
      widget.onSelectSecondEndTime(selSecondTextEnd);

      if((secondStartTimeHour == 0 && secondStartTimeMinute == 0) && (secondEndTimeHour == 0 && secondEndTimeMinute == 0)){
        addSecondTime = false;
      }else{
        addSecondTime = true;
      }

      widget.isValidTime(true);
      widget.isValidSecondTime(true);

    }
    setState(() {

    });
  }
  @override
  Widget build(BuildContext context) {
    BoxDecoration enableDecoration =  BoxDecoration(
        color: Theme.of(context).accentColor.withAlpha(70),
        borderRadius: BorderRadius.all(Radius.circular(10)));

    BoxConstraints slotsConstraints = BoxConstraints(
        minWidth: MediaQuery.of(context).size.width, minHeight: 50);

    BoxDecoration slotsDecoration = BoxDecoration(
        color: Theme.of(context).primaryColor,
        borderRadius: BorderRadius.all(Radius.circular(10)));

    BoxDecoration disableDecoration = BoxDecoration(
        color: Colors.grey.withAlpha(70),
        borderRadius: BorderRadius.all(Radius.circular(10)));

    BoxDecoration disableDecorationStartEnd = BoxDecoration(
        color: Colors.grey.withAlpha(70),
        border: Border.all(
            color: Colors.grey.withAlpha(70),
            width: 0.5),
        borderRadius:
        BorderRadius.all(Radius.circular(5)));

    return Container(
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.only(left: 10,right: 10),
      constraints: BoxConstraints(
          minWidth: MediaQuery.of(context).size.width,
          minHeight: 50),
      decoration: chkSwitchValue?enableDecoration:disableDecoration,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            flex: 2,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  child: Text(widget.weekDayTitle,
                      style: TextStyle(
                          color: chkSwitchValue?Theme.of(context).accentColor:Colors.black,
                          fontSize: 12,
                          fontWeight: chkSwitchValue?FontWeight.bold:FontWeight.normal)),
                ),
                Switch(
                  value: chkSwitchValue,
                  onChanged: _onChangedMonday,
                  activeTrackColor: Colors.grey,
                  activeColor: Theme.of(context).accentColor,
                ),

              ],
            ),
          ),
          //s-monday
          //Monday StartTime
          Expanded(
            flex: 6,
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      flex: 3,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          /*Padding(
                            padding: const EdgeInsets.only(left: 10,bottom: 5),
                            child: Text("Start Time"),
                          ),*/
                          GestureDetector(
                            onTap: () async{
                              if(!chkSwitchValue){
                                return;
                              }
                              Future<TimeOfDay> selectedTime = showTimePicker(

                                initialTime: TimeOfDay.now(),
                                context: context,
                              );
                              TimeOfDay tOf = await selectedTime;
                              // checkIsValidStart(tOf);
                              startTimeHour = tOf.hour;
                              startTimeMinute = tOf.minute;
                              selTextStart = await getTimeString(selectedTime);
                              // debugPrint("$selTextStart");
                              widget.onSelectStartTime(selTextStart);
                              setState(() {

                              });

                            },
                            child: Container(
                              padding: EdgeInsets.only(left: 10, right: 10),
                              margin: EdgeInsets.only(left: 10, right: 10),
                              constraints: BoxConstraints(
                                  minWidth: MediaQuery.of(context).size.width,
                                  minHeight: 40),
                              decoration: chkSwitchValue?BoxDecoration(
                                  color: Theme.of(context).primaryColor,
                                  border: Border.all(
                                      color: Theme.of(context).accentColor,
                                      width: 0.5),
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(5))):disableDecorationStartEnd,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    selTextStart,
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 14),
                                  ),
                                  Icon(
                                    Icons.keyboard_arrow_down,
                                    color: Colors.grey,
                                    size: 20,
                                  )
                                ],
                              ),
                            ),
                          ),
                          (!validTime)?Container(
                            height: erroHeight,
                          ):Container(
                            height: noErroHeight,
                          ),

                        ],
                      ),
                    ),
                    SizedBox(height: 10,),
                    Expanded(
                      flex: 3,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          /*Padding(
                            padding: const EdgeInsets.only(left: 10,bottom: 5),
                            child: Text("End Time"),
                          ),*/
                          GestureDetector(
                            onTap: () async{
                              if(!chkSwitchValue){
                                return;
                              }
                              Future<TimeOfDay> selectedTime = showTimePicker(

                                initialTime: TimeOfDay.now(),
                                context: context,
                              );
                              TimeOfDay tOf = await selectedTime;
                              checkIsValidEnd(tOf);

                              selTextEnd = await getTimeString(selectedTime);
                              // debugPrint("$selTextEnd");
                              widget.onSelectEndTime(selTextEnd);

                              setState(() {

                              });

                            },
                            child: Container(
                              padding: EdgeInsets.only(left: 10, right: 10),
                              margin: EdgeInsets.only(left: 10, right: 10),
                              constraints: BoxConstraints(
                                  minWidth: MediaQuery.of(context).size.width,
                                  minHeight: 40),
                              decoration: chkSwitchValue?BoxDecoration(
                                  color: Theme.of(context).primaryColor,
                                  border: Border.all(
                                      color: Theme.of(context).accentColor,
                                      width: 0.5),
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(5))):disableDecorationStartEnd,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    selTextEnd,
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 14),
                                  ),
                                  Icon(
                                    Icons.keyboard_arrow_down,
                                    color: Colors.grey,
                                    size: 20,
                                  )
                                ],
                              ),
                            ),
                          ),
                          (!validTime)?Container(
                            padding: EdgeInsets.only(left: 10),
                            height: erroHeight,
                            child: Text("Invalid End Time",style: TextStyle(color: Colors.red),),
                          ):Container(
                            height: noErroHeight,
                          ),
                        ],
                      ),
                    ),
                    addSecondTime?Container(
                      width: 35,
                      height: 30,
                    ):GestureDetector(
                      onTap: (){
                        if(!chkSwitchValue){
                          return;
                        }
                        setState(() {
                          addSecondTime= !addSecondTime;
                        });
                      },
                      child: Container(
                          decoration: addRemoveDecoration(),
                          width: 35,
                          height: 30,
                          child: Icon(Icons.add,color: Colors.white,)
                      ),
                    )

                  ],
                ),
                addSecondTime?Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      flex: 3,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                         /* Padding(
                            padding: const EdgeInsets.only(left: 10,bottom: 5),
                            child: Text("Second Start Time"),
                          ),*/
                          GestureDetector(
                            onTap: () async{
                              if(!chkSwitchValue){
                                return;
                              }
                              Future<TimeOfDay> selectedTime = showTimePicker(

                                initialTime: TimeOfDay.now(),
                                context: context,
                              );
                              TimeOfDay tOf = await selectedTime;
                              checkIsValidSecond(tOf);
                              secondStartTimeHour = tOf.hour;
                              secondStartTimeMinute = tOf.minute;
                              selSecondTextStart = await getTimeString(selectedTime);
                              // debugPrint("$selSecondTextStart");
                              widget.onSelectSecondStartTime(selSecondTextStart);
                              setState(() {

                              });

                            },
                            child: Container(
                              padding: EdgeInsets.only(left: 10, right: 10),
                              margin: EdgeInsets.only(left: 10, right: 10),
                              constraints: BoxConstraints(
                                  minWidth: MediaQuery.of(context).size.width,
                                  minHeight: 40),
                              decoration: chkSwitchValue?BoxDecoration(
                                  color: Theme.of(context).primaryColor,
                                  border: Border.all(
                                      color: Theme.of(context).accentColor,
                                      width: 0.5),
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(5))):disableDecorationStartEnd,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    selSecondTextStart,
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 14),
                                  ),
                                  Icon(
                                    Icons.keyboard_arrow_down,
                                    color: Colors.grey,
                                    size: 20,
                                  )
                                ],
                              ),
                            ),
                          ),

                        ],
                      ),
                    ),


                    Expanded(
                      flex: 3,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          /*Padding(
                            padding: const EdgeInsets.only(left: 10,bottom: 5),
                            child: Text("Second End Time"),
                          ),*/
                          GestureDetector(
                            onTap: () async{
                              if(!chkSwitchValue){
                                return;
                              }
                              Future<TimeOfDay> selectedTime = showTimePicker(

                                initialTime: TimeOfDay.now(),
                                context: context,
                              );
                              TimeOfDay tOf = await selectedTime;
                              checkIsValidSecond(tOf);

                              selSecondTextEnd = await getTimeString(selectedTime);
                              // debugPrint("$selSecondTextEnd");
                              widget.onSelectSecondEndTime(selSecondTextEnd);

                              setState(() {

                              });

                            },
                            child: Container(
                              padding: EdgeInsets.only(left: 10, right: 10),
                              margin: EdgeInsets.only(left: 10, right: 10),
                              constraints: BoxConstraints(
                                  minWidth: MediaQuery.of(context).size.width,
                                  minHeight: 40),
                              decoration: chkSwitchValue?BoxDecoration(
                                  color: Theme.of(context).primaryColor,
                                  border: Border.all(
                                      color: Theme.of(context).accentColor,
                                      width: 0.5),
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(5))):disableDecorationStartEnd,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    selSecondTextEnd,
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 14),
                                  ),
                                  Icon(
                                    Icons.keyboard_arrow_down,
                                    color: Colors.grey,
                                    size: 20,
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    addSecondTime?GestureDetector(
                      onTap: (){
                        if(!chkSwitchValue){
                          return;
                        }
                        setState(() {
                          addSecondTime= !addSecondTime;
                          // debugPrint("yes ${addSecondTime}");

                          secondStartTimeHour=0;
                          secondStartTimeMinute=0;
                          selSecondTextStart = '00:00';
                          // debugPrint("$selSecondTextStart");
                          widget.onSelectSecondStartTime(selSecondTextStart);


                          secondEndTimeHour=0;
                          secondEndTimeMinute=0;
                          selSecondTextEnd = '00:00';
                          // debugPrint("$selSecondTextStart");
                          widget.onSelectSecondEndTime(selSecondTextEnd);
                          setState(() {

                          });
                        });
                      },
                      child: Container(
                        decoration: addRemoveDecoration(),
                        width: 35,
                        height: 30,
                        child: Icon(Icons.remove,color: Colors.white,)
                      ),
                    ):Container(
                      height: 30,
                      width: 35,
                    )
                  ],
                ):Container()
              ],
            ),
          ),


        ],
      ),
    );
  }
  /*void checkIsValidStart(TimeOfDay tOf){
    endTimeHour = tOf.hour;
    endTimeMinute = tOf.minute;

    if(endTimeHour>startTimeHour){
      validTime = true;
    }else if(endTimeHour == startTimeHour && endTimeMinute > startTimeMinute){
      validTime = true;
    }
    else {
      validTime = false;
    }
    widget.isValidTime(validTime);
  }*/
  void checkIsValidEnd(TimeOfDay tOf){
    endTimeHour = tOf.hour;
    endTimeMinute = tOf.minute;

    if(endTimeHour>startTimeHour){
      validTime = true;
    }else if(endTimeHour == startTimeHour && endTimeMinute > startTimeMinute){
      validTime = true;
    }
    else {
      validTime = false;
    }
    widget.isValidTime(validTime);
  }
  void checkIsValidSecond(TimeOfDay tOf){
    secondEndTimeHour = tOf.hour;
    secondEndTimeMinute = tOf.minute;

    if(secondEndTimeHour>secondEndTimeMinute){
      validSecondTime = true;
    }else if(secondEndTimeHour == secondStartTimeHour && secondEndTimeMinute > secondStartTimeMinute){
      validSecondTime = true;
    }
    else {
      validSecondTime = false;
    }
    widget.isValidSecondTime(validSecondTime);
  }

  Future<String> getTimeString(Future<TimeOfDay> selectedTime)async{
    TimeOfDay tOD = await selectedTime;
    String strHour='';
    if(tOD.hour<10){
      strHour='0${tOD.hour}';
    }else{
      strHour='${tOD.hour}';
    }

    String strMinute='';
    if(tOD.minute<10){
      strMinute='0${tOD.minute}';
    }else{
      strMinute='${tOD.minute}';
    }
    String timeString = "$strHour:$strMinute";
    return timeString;
  }
  void _onChangedMonday(bool newValue) => setState(() {
    chkSwitchValue = newValue;
    widget.onChange(chkSwitchValue);
  });
}
