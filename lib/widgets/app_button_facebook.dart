import 'package:flutter/material.dart';

class AppButtonFacebook extends StatefulWidget {
  Function onTap;

  AppButtonFacebook({@required this.onTap});
  @override
  _AppButtonFacebookState createState() => _AppButtonFacebookState();
}

class _AppButtonFacebookState extends State<AppButtonFacebook> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 20.0, right: 50.0, left: 50.0),
      height: 40,
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
              color: Theme.of(context).focusColor.withOpacity(0.10),
              offset: Offset(0, 4),
              blurRadius: 20)
        ],
      ),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          elevation: 0.2,
          primary: Theme.of(context).primaryColor,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0),
          ),
        ),

        onPressed: widget.onTap/*() {
          Navigator.of(context).pushNamed(HomeScreen.routeName,
              arguments: [currentUser.name, currentUser.phoneNumber]);
        }*/,

        child: Container(
          child: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image(
                  image: AssetImage('images/facebook-fill.png'),
                ),
                Text(
                  ' Facebook',
                  style: TextStyle(
                    fontSize: 16.0,
                    color: Theme.of(context).focusColor,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
