import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:flutter/material.dart';
import 'package:infinite_health_care_doctor/config/app_config.dart' as config;
import 'package:infinite_health_care_doctor/config/const.dart';
import 'package:infinite_health_care_doctor/models/patient_model.dart';
import 'package:infinite_health_care_doctor/utils/utility.dart';
class WidgetSearchPatientsItem extends StatefulWidget {
  final PatientModel patientModel;
  final title;
  const WidgetSearchPatientsItem({Key key, this.title,this.patientModel}) : super(key: key);
  
  @override
  _DoctorsCardWidgetState createState() => _DoctorsCardWidgetState();
}

class _DoctorsCardWidgetState extends State<WidgetSearchPatientsItem> {
  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16.0),
      ),
      child: Container(
        padding: const EdgeInsets.only(top:12.0,bottom: 12.0,left: 12.0,right: 12.0),
          child:Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                margin: const EdgeInsets.only(right: 20.0),
                child: ball(widget.patientModel.patient_image??Const.defaultProfileUrl,Colors.transparent)
              ),
              Expanded(
                flex: 1,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          margin: const EdgeInsets.only(bottom: 6.0),
                          child: Text(
                            '${widget.patientModel.patient_firstname} ${widget.patientModel.patient_lastname}',
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontSize: 12.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        Utility.getProfileString(widget.patientModel.patient_address, widget.patientModel.patient_city, widget.patientModel.patient_state, null)==''?Container():Container(
                          padding: const EdgeInsets.all(6.0),
                          decoration: BoxDecoration(
                            border: Border.all(width: 1,color: Colors.grey.withOpacity(0.1)),
                            borderRadius: BorderRadius.circular(5),
                          ),
                          child:Text(
                            'Address : ${Utility.getProfileString(widget.patientModel.patient_address, widget.patientModel.patient_city, widget.patientModel.patient_state, null)}',
                            style: TextStyle(
                              color:Colors.grey,
                              fontSize: 10.0,

                            ),
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 10,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          'Mobile : ${widget.patientModel.patient_mobile??""}',
                          style: TextStyle(
                            fontSize: 10.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),

                       /* Row(
                          children: <Widget>[
                            // Icon(Icons.star,color: Colors.yellow,),
                            Text('${widget.patientModel.date_of_birth??""}',style: TextStyle(),),
                          ],
                        )*/
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      );
  }
  Widget ball(String image,Color color){
    return Container(
      height: 80,width: 80.0,
      child: ClipOval(
        child: FancyShimmerImage(
          imageUrl: image,
          shimmerBaseColor: Colors.white,
          shimmerHighlightColor: config.Colors().mainColor(1),
          shimmerBackColor: Colors.green,
          errorWidget: Image.network('https://i0.wp.com/www.dobitaobyte.com.br/wp-content/uploads/2016/02/no_image.png?ssl=1'),
        ),
      )
      ,
      
    );
  }
}