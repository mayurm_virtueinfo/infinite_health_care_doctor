import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:infinite_health_care_doctor/config/app_config.dart' as config;
import 'package:flutter/material.dart';
import 'package:infinite_health_care_doctor/config/const.dart';
import 'package:infinite_health_care_doctor/models/appointment_type.dart';
import 'package:infinite_health_care_doctor/models/past_conversation.dart' as pastModel;
import 'package:infinite_health_care_doctor/ui/conversation/personal_chat.dart';
import 'package:infinite_health_care_doctor/models/doctor_model.dart' as doctoreModel;
import 'package:infinite_health_care_doctor/utils/utility.dart';

class PastConversationItemWidget extends StatefulWidget {
  final String peerId;
  final pastModel.PastConversation conversation;
  final AppointmentType appointmentType;
  final Map<String, dynamic> mapApptStruct;

  const PastConversationItemWidget({Key key, this.peerId, this.conversation, this.appointmentType, this.mapApptStruct}) : super(key: key);

  @override
  _PastConversationItemWidgetState createState() => _PastConversationItemWidgetState();
}

class _PastConversationItemWidgetState extends State<PastConversationItemWidget> {
  @override
  Widget build(BuildContext context) {
    debugPrint("CW 1: ${widget.appointmentType}");
    return GestureDetector(
      onTap: (){
        Navigator.pushNamed(context, PersonalChat.routeName, arguments: ["${widget.conversation.id_user}", widget.appointmentType, widget.conversation]);
      },
      child: Container(
        margin: EdgeInsets.only(left: 10,right: 10),
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(16.0),
          ),
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.only(right: 25.0),
                  child: ball(this.widget.conversation.avatar_image),
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          '${widget.conversation.patient_name}',
                          style: TextStyle(fontSize: 16.0, color: Colors.black,fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: widget.mapApptStruct.entries.map((e) {
                            return Container(

                              margin: EdgeInsets.only(top: 5),
                              padding: EdgeInsets.only(left: 5,right: 5,top: 2,bottom: 2),
                             /* decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(color: Colors.black,width: 1)
                              ),*/
                              child: Wrap(
                                crossAxisAlignment: WrapCrossAlignment.center,
                                direction: Axis.horizontal,
                                children: [
                                  Container(
                                    padding: EdgeInsets.all(5),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(topRight: Radius.circular(100),bottomRight: Radius.circular(100)),
                                        // color: Theme.of(context).accentColor,
                                        gradient: LinearGradient(
                                          colors: [Colors.red[200],Colors.blue[200]],
                                          begin: const FractionalOffset(0.0, 0.0),
                                          end: const FractionalOffset(0.5, 0.0),
                                          stops: [0.0,1.0],
                                          tileMode: TileMode.clamp,
                                        )
                                    ),
                                    child: Text(
                                      '${e.key}',
                                      style: TextStyle(
                                          fontSize: 12.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.white

                                      ),
                                    ),
                                  ),
                                  /*Text('${e.value.join(', ')}',style: TextStyle(
                                    fontSize: 14.0,
                                  ),),*/
                                  Wrap(
                                    direction: Axis.horizontal,
                                    children: (e.value as List<dynamic>).asMap().entries.map((eV) {
                                      return Container(
                                          padding: EdgeInsets.only(left: 5, right: 5),
                                          margin: EdgeInsets.only(left: 2, right: 2,top: 2,bottom: 2),
                                          decoration: BoxDecoration(borderRadius: BorderRadius.circular(100), color: Colors.red),
                                          child: Text(
                                            eV.value,
                                            style: TextStyle(color: Colors.white),
                                          ));
                                    }).toList(),
                                  )
                                ],
                              ),
                            );
                          }).toList(),
                        )
                      ],
                    ),
                  ),
                ) /*,
                Container(
                height: 10,width: 10.0,
                decoration: BoxDecoration(
                  color: Colors.green,
                  borderRadius: BorderRadius.circular(100.0),
                ),
              ),*/
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget ball(String image) {
    return Container(
      height: 60,
      width: 60.0,
      child: ClipOval(
        child: FancyShimmerImage(
          imageUrl: image ?? Const.defaultProfileUrl,
          shimmerBaseColor: Colors.white,
          shimmerHighlightColor: config.Colors().mainColor(1),
          shimmerBackColor: Colors.green,
          errorWidget: Image.network('https://i0.wp.com/www.dobitaobyte.com.br/wp-content/uploads/2016/02/no_image.png?ssl=1'),
        ),
      ),
    );
  }
}
