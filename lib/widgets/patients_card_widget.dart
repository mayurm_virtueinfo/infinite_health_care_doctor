import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:flutter/material.dart';
import 'package:infinite_health_care_doctor/config/app_config.dart' as config;
import 'package:infinite_health_care_doctor/config/const.dart';
import 'package:infinite_health_care_doctor/models/patient_model.dart' as patientModel;
import 'package:infinite_health_care_doctor/ui/home/patient_detail.dart';
import 'package:infinite_health_care_doctor/utils/strings.dart';
import 'package:intl/intl.dart';
import 'package:infinite_health_care_doctor/utils/utility.dart';
class PatientsCardWidget extends StatefulWidget {
  final patientModel.PatientModel patient;
  const PatientsCardWidget({Key key, this.patient}) : super(key: key);
  
  @override
  _PatientsCardWidgetState createState() => _PatientsCardWidgetState();
}

class _PatientsCardWidgetState extends State<PatientsCardWidget> {
  String date='';
  String time='';
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        Navigator.of(context).pushNamed(PatientDetail.routeName,arguments: [widget.patient]);
      },
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(16.0),
        ),
        child: Container(
          padding: const EdgeInsets.only(top:12.0,bottom: 12.0,left: 12.0,right: 12.0),
            child:Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 10,top: 10,bottom: 10,right: 20),
                  child: ball(widget.patient.patient_image??Const.defaultProfileUrl,Colors.transparent),
                ),
                Expanded(
                  flex: 1,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            margin: const EdgeInsets.only(bottom: 6.0),
                            child: Text(
                              '${widget.patient.patient_firstname} ${widget.patient.patient_lastname}',
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                fontSize: 14.0,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          SizedBox(height: 10,),
                          Container(
                            /*padding: const EdgeInsets.all(6.0),
                            decoration: BoxDecoration(
                              border: Border.all(width: 1,color: Colors.grey.withOpacity(0.1)),
                              borderRadius: BorderRadius.circular(12),
                            ),*/
                            child:Row(
                              children: [
                                Icon(Icons.phone_android_outlined,color: Theme.of(context).accentColor,size: 20,),
                                SizedBox(width: 5,),
                                Text(
                                  '${widget.patient.patient_mobile}',
                                  style: TextStyle(
                                    color:Colors.grey,
                                    fontSize: 14.0
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: 10,),
                          Container(

                            child:Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Icon(
                                  Icons.location_on,
                                  color: Theme.of(context).accentColor,
                                  size: 20,
                                ),
                                SizedBox(width: 5,),
                                Flexible(
                                  child: Text(
                                    // '${widget.patient.patient_address??''} ${widget.patient.patient_city??''} ${widget.patient.patient_state??''}',
                                    '${Utility.getProfileString(widget.patient.patient_address,widget.patient.patient_city, widget.patient.patient_state, null)}',
                                    style: TextStyle(
                                        color:Colors.grey,
                                        fontSize: 14.0
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            '${date}',
                            style: TextStyle(
                              fontSize: 10.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(width: 5,),
                          Text(
                            '${time}',
                            style: TextStyle(
                              fontSize: 10.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
    );
  }
  Widget ball(String image,Color color){
    return Container(
      height: 80,width: 80.0,
      child: ClipOval(
        child: FancyShimmerImage(
          imageUrl: image,
          shimmerBaseColor: Colors.white,
          shimmerHighlightColor: config.Colors().mainColor(1),
          shimmerBackColor: Colors.green,
          errorWidget: Image.network('https://i0.wp.com/www.dobitaobyte.com.br/wp-content/uploads/2016/02/no_image.png?ssl=1'),
        ),
      )
      ,
      
    );
  }
}