import 'package:flutter/material.dart';
import 'package:infinite_health_care_doctor/bloc/loading/loading_bloc.dart';
import 'package:infinite_health_care_doctor/config/app_config.dart' as config;
import 'package:infinite_health_care_doctor/main.dart';

class LoadingWidget extends StatelessWidget {
  bool initialData;
  LoadingWidget({this.initialData});
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      initialData: this.initialData,
      stream: locator<LoadingBloc>().loadingStream,
      builder: (context, snapshot) {
        if (snapshot.hasData && snapshot.data) {
          return Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              color: Colors.white.withOpacity(0.7),
              child: Center(
                child: SizedBox(
                  width: 40,
                  height: 40,
                  child: CircularProgressIndicator(
                    strokeWidth: 1.5,
                    valueColor: AlwaysStoppedAnimation<Color>(config.Colors().mainColor(1),),
                  ),
                ),
              ));
        }
        return Container();
      },
    );
  }
}
