import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class DoctorNearYouModel {
 /* "id": 6,
  "title": "Dr. Pooja Ajmera",
  "lat": "23.0320",
  "lng": "72.5258",
  "address": "Ghatlodia",
  "zipcode": "380063",
  "avatar_image": null,
  "cover_image": null,
  "distance": 194.71596213831955,
  "originalPath": "",
  "coverPath": ""*/
  int id;
  String title;
  String lat;
  String lng;
  String address;
  String zipcode;
  String avatar_image;
  String cover_image;
  double distance;
  String originalPath;
  String coverPath;

  DoctorNearYouModel.init();

  DoctorNearYouModel(
    this.id,
    this.title,
    this.lat,
    this.lng,
    this.address,
    this.zipcode,
    this.avatar_image,
    this.cover_image,
    this.distance,
    this.originalPath,
    this.coverPath
  );


  DoctorNearYouModel.fromMap(map) :
    this.id = map['id'],
    this.title = map['title'],
    this.lat = map['lat'],
    this.lng = map['lng'],
    this.address = map['address'],
    this.zipcode = map['zipcode'],
    this.avatar_image = map['avatar_image'],
    this.cover_image = map['cover_image'],
    this.distance = map['distance'],
    this.originalPath = map['originalPath'],
    this.coverPath = map['coverPath'];
}

class DoctorNearYouModelList {
  List<DoctorNearYouModel> _doctorsList = [];


  DoctorNearYouModelList.fromSnapshot(snapshot) {
    List<dynamic> data = snapshot.data['data'];
    debugPrint("data : ${data}");
//    data = mDoctorList;
    data.forEach((item) {

      DoctorNearYouModel doctor = DoctorNearYouModel.fromMap(item);
      _doctorsList.add(doctor);
    });
  }


  List<DoctorNearYouModel> get doctorsNearYou => _doctorsList;
}
