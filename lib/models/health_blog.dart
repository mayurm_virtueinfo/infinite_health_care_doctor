import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class HealthBlog {
  String id = UniqueKey().toString();
  String image;
  String description;
  String date;
  String title;
  List<String> tags;
  List<String> categories;
  String color;
  HealthBlog({this.image, this.description, this.date, this.tags, this.title,this.color,this.categories});
}

  class HealthBlogList{
  List<HealthBlog> _healthBlogList;
  HealthBlogList(){
    this._healthBlogList =[
      HealthBlog(
        date: 'Wednesday 28th April',
        description: 'Brush up on hygiene',
        image: 'https://image.shutterstock.com/image-photo/bloggingblog-concepts-ideas-white-worktable-600w-1029506242.jpg',
        tags: ['tag1','tag2','tag3','tag4','tag5','tag6'],
        categories: ['Category 1','Category 2','Category 3','Category 4','Category 5','Category 6'],
        title: 'Title1',
        color: '#DCC3BD'
      ),
      HealthBlog(
          date: 'Wednesday 28th April',
          description: 'Don\'t skip your breakfast',
          image: 'https://image.shutterstock.com/image-photo/cup-coffee-next-learn-english-600w-1519258124.jpg',
          tags: ['tag1','tag2','tag3','tag4','tag5','tag6'],
          categories: ['Category 1','Category 2','Category 3','Category 4','Category 5','Category 6'],
          title: 'Title2',
          color: '#FF5733'
      ),
      HealthBlog(
          date: 'Wednesday 28th April',
          description: 'Neurobics for your mind',
          image: 'https://image.shutterstock.com/image-photo/business-concept-top-view-2020-600w-1572748462.jpg',
          tags: ['tag1','tag2','tag3','tag4','tag5','tag6'],
          categories: ['Category 1','Category 2','Category 3','Category 4','Category 5','Category 6'],
          title: 'Title3',
          color: '#13B925'
      ),

    ];
  }
  List<HealthBlog> get healthBlogs => _healthBlogList;
}