import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class PatientReview {
  int id ;
  int customer_id;
  String first_name;
  String last_name;
  int doctors_id;
  String doctor_name;
  String rating;
  String description;
  String patient_image;
  PatientReview({this.id, this.customer_id, this.first_name, this.last_name, this.doctors_id, this.doctor_name, this.rating, this.description,this.patient_image});

  PatientReview.fromMap(Map<String,dynamic> data):
      this.id = data['id'],
      this.customer_id = data['customer_id'],
      this.first_name = data['first_name'],
      this.last_name = data['last_name'],
      this.doctors_id = data['doctors_id'],
      this.doctor_name = data['doctor_name'],
      this.rating = data['rating'],
      this.description = data['description'],
      this.patient_image = data['patient_image'];

}

