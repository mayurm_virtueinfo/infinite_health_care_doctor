import 'package:flutter/material.dart';
import 'package:infinite_health_care_doctor/models/doctor_model.dart';

class MyAppointmentModel {
  int id_doctor;
  String doctor_name;
  String avatar_image;
  String degree;
  int rating;
  String type;
  String appointment_date;

  MyAppointmentModel(this.id_doctor, this.doctor_name, this.avatar_image, this.degree, this.rating,
      this.type, this.appointment_date);

  MyAppointmentModel.fromMap(map):
        this.id_doctor = map['id_doctor'],
        this.doctor_name = map['doctor_name'],
        this.avatar_image = map['avatar_image'],
        this.degree = map['degree'],
        this.rating = map['rating'],
        this.type = map['type'],
        this.appointment_date = map['appointment_date']    ;

}

class MyApointmentListModel {
  DoctorModel currentDoctor = new DoctorModel.init().getCurrentDoctor();
  List<MyAppointmentModel> _appointmentList = [];

  /*List<Map<String, dynamic>> mMyAppointmentModelList = [
    {
      'name': "Dr.Vipul Bariya",
      'image': "https://www.iconbunny.com/icons/media/catalog/product/2/1/2131.10-doctor-icon-iconbunny.jpg",
      'degree': "BHMS",
      'rating': 0,
      'type': "doctor",
      'appointment_date': "10 Novembre 2019",
    },
    {
      'id': "",
      'name': "Dr.Rajesh Kumbhani",
      'image': "https://www.iconbunny.com/icons/media/catalog/product/2/1/2131.10-doctor-icon-iconbunny.jpg",
      'degree': "BHMS",
      'rating': 1,
      'type': "doctor",
      'appointment_date': "14 Decembre 2019",
    }
  ];*/
  MyApointmentListModel.fromSnapshot(snapshot) {
    List<dynamic> data = snapshot.data['data'];
//    data = mMyAppointmentModelList;
    data.forEach((item) {
      MyAppointmentModel appointment = MyAppointmentModel.fromMap(item);
      _appointmentList.add(appointment);
    });
  }

 /* ApointmentListModel() {
    this._appointmentList = [
      new MyAppointmentModel("14 Decembre 2019", currentDoctor),
      new MyAppointmentModel("10 Novembre 2019", currentDoctor),
      new MyAppointmentModel("12 Octobre 2019", currentDoctor),
    ];
  }*/

  List<MyAppointmentModel> get myAppointment => _appointmentList;

}