

class ScheduledConversation{
  int id_user ;
  String patient_name ;
  String avatar_image ;
  List<dynamic> appointment;
  String firebase_token;

 ScheduledConversation.fromMap(map):
       this.id_user = map['id_user']??null,
       this.patient_name = map['patient_name']??null,
       this.avatar_image = map['avatar_image']??null,
       this.appointment = map['appointment']??null,
       this.firebase_token = map['patient_firebase_token']??null;

  ScheduledConversation.fromMapDynamic(map):
        this.id_user = map['id_user']??null,
        this.patient_name = map['patient_name']??null,
        this.avatar_image = map['avatar_image']??null,
        this.firebase_token = map['firebase_token']??null;



  Map<String,dynamic> toJson(){
    return {
      'id_user': this.id_user,
      'patient_name': this.patient_name,
      'avatar_image': this.avatar_image,
      'appointment': this.appointment,
      'firebase_token': this.firebase_token
    };
  }

}

class ScheduledConversationListModel{
  List<ScheduledConversation> _conversationList = [];

  ScheduledConversationListModel.fromSnapshot(snapshot) {
    List<dynamic> data = snapshot.data['data']['upcoming_appointment'];

    /*ScheduledConversation appointment = ScheduledConversation.fromMap(data[0]);
    _conversationList.add(appointment);*/

    data.forEach((item) {
      ScheduledConversation appointment = ScheduledConversation.fromMap(item);
      List<dynamic> alist = appointment.appointment;
      // appointment.appointment.addAll(alist);
      _conversationList.add(appointment);
    });
  }

  List<ScheduledConversation> get scheduledConversation => _conversationList;



}