import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:infinite_health_care_doctor/config/const.dart';

class FutureBanner {
  int id ;
  String title;
  String image;
  String ImagePath;
  String IconPath;

  FutureBanner.init();

  FutureBanner(this.id,this.title, this.image);

  FutureBanner.fromMap(map)
      : this.id = map['id'],
        this.title = map['title'],
        this.image = map['image'],
        this.ImagePath = map['ImagePath'],
        this.IconPath = map['IconPath'];
}

class FutureBannerListModel {
  List<FutureBanner> _futureBannerList = [];

  FutureBannerListModel.fromSnapshot(snapshot) {
    List<dynamic> data = snapshot.data['data'];
    data.forEach((item) {
      FutureBanner top = FutureBanner.fromMap(item);
      _futureBannerList.add(top);
    });
  }

  List<FutureBanner> get futureBannerList => _futureBannerList;
}
