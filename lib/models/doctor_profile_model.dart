import 'package:flutter/material.dart';
import 'package:infinite_health_care_doctor/models/past_conversation.dart';

class DoctorProfileModel{
  int id;//: 5,
  String id_category;//: "a:1:{i:0;s:1:"7";}",
  String id_disease;//: "a:1:{i:0;s:1:"1";}",
  String id_degree;//: "a:1:{i:0;s:1:"1";}",
  String title;//: "Dr. Kiran Patel",
  String description;//: "<p>test</p>",
  String city;//: "Ahmedabad",
  String address;//: "iskon",
  String zipcode;//: "380052",
  String email;//: "kiranpatel2875@gmail.com",
  String mobile;//: "7434012352",
  String rating;//: "4.5",
  String gtimings;//: "9:30 AM - 8:00 PM",
  String avatar_image;//: null,
  String cover_image;//: null,
  String gstatus;//: "1",
  String status;//: "1",
  String created_at;//: "2020-07-12T17:44:34.000000Z",
  String updated_at;//: "2020-07-12T14:17:35.000000Z",
  int last_by;//: 0,
  String last_ip;//: "0"
  List album_images;


  DoctorProfileModel.fromPastConversation(PastConversation pastConversation){
    this.id = pastConversation.id_user;
    this.avatar_image = pastConversation.avatar_image;
    this.title = pastConversation.patient_name;

  }

  DoctorProfileModel.fromMap(Map<String,dynamic> mData){
    this.id = mData['id'] ;
    this.id_category = mData['id_category'] ;
    this.id_disease = mData['id_disease'] ;
    this.id_degree = mData['id_degree'] ;
    this.title = mData['title'] ;
    this.description = mData['description'] ;
    this.city = mData['city'] ;
    this.address = mData['address'] ;
    this.zipcode = mData['zipcode'] ;
    this.email = mData['email'] ;
    this.mobile = mData['mobile'] ;
    this.rating = mData['rating'] ;
    this.gtimings = mData['gtimings'] ;
    this.avatar_image = mData['avatar_image'] ;
    this.cover_image = mData['cover_image'] ;
    this.gstatus = mData['gstatus'] ;
    this.status = mData['status'] ;
    this.created_at = mData['created_at'] ;
    this.updated_at = mData['updated_at'] ;
    this.last_by = mData['last_by'] ;
    this.last_ip = mData['last_ip'] ;
    this.album_images = mData['album_images'] ;


  }

  DoctorProfileModel.fromSnapshot(AsyncSnapshot snapshot){
    Map<String,dynamic> mData = snapshot.data['data'];
    this.id = mData['id'] ;
    this.id_category = mData['id_category'] ;
    this.id_disease = mData['id_disease'] ;
    this.id_degree = mData['id_degree'] ;
    this.title = mData['title'] ;
    this.description = mData['description'] ;
    this.city = mData['city'] ;
    this.address = mData['address'] ;
    this.zipcode = mData['zipcode'] ;
    this.email = mData['email'] ;
    this.mobile = mData['mobile'] ;
    this.rating = mData['rating'] ;
    this.gtimings = mData['gtimings'] ;
    this.avatar_image = mData['avatar_image'] ;
    this.cover_image = mData['cover_image'] ;
    this.gstatus = mData['gstatus'] ;
    this.status = mData['status'] ;
    this.created_at = mData['created_at'] ;
    this.updated_at = mData['updated_at'] ;
    this.last_by = mData['last_by'] ;
    this.last_ip = mData['last_ip'] ;
    this.album_images = mData['album_images'] ;


  }


}
