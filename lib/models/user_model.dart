import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:infinite_health_care_doctor/utils/app_preferences.dart';
import 'package:infinite_health_care_doctor/main.dart';

class UserModel {
  String id;
  String fullname;
  String fees;
  String email;
  String mobile;
  String rating;
  String gtimings;
  String state;
  String city;
  String address;
  String zipcode;
  String avatar_image;
  String cover_image;
  String status;
  String last_login;
  String ip;
  // String loginType;
  String description;
  String experience;
  List<dynamic> category_data;
  List<dynamic> degree_data;
  List<dynamic> dieses_data;
  List<dynamic> specialization;
  String lat;
  String lng;
  // bool isAuthenticated;

  UserModel(
      {this.id,
      // this.loginType,
      this.email,
      this.fullname,
        this.fees,
      this.category_data,
      this.degree_data,
      this.dieses_data,
      this.specialization,
      this.state,
      this.zipcode,
      this.address,
      this.avatar_image,
      this.city,
      this.cover_image,
      this.mobile,
      this.status,
      this.rating,
      this.gtimings,
      this.last_login,
      this.ip,
      this.description,
      this.experience,
      this.lat,
      this.lng,
      // this.isAuthenticated
    });

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['id'] = this.id;
    // data['loginType'] = this.loginType;
    data['fullname'] = this.fullname;
    data['fees'] = this.fees;
    data['category_data'] = this.category_data;
    data['degree_data'] = this.degree_data;
    data['dieses_data'] = this.dieses_data;
    data['email'] = this.email;
    data['mobile'] = this.mobile;
    data['state'] = this.state;
    data['city'] = this.city;
    data['address'] = this.address;
    data['zipcode'] = this.zipcode;
    data['avatar_image'] = this.avatar_image;
    data['cover_image'] = this.cover_image;
    data['rating'] = this.rating;
    data['gtimings'] = this.gtimings;
    data['status'] = this.status;
    data['last_login'] = this.last_login;
    data['ip'] = this.ip;
    // data['isAuthenticated'] = this.isAuthenticated;
    data['description'] = this.description;
    data['lat'] = this.lat;
    data['specialization'] = this.specialization;
    data['experience'] = this.experience;
    data['lng'] = this.lng;
    return data;
  }

  UserModel.fromMap(Map<String,dynamic> data) :
    this.id = "${data['id']}",
    this.fullname = data['fullname'],
    this.fees = "${data['fees']}",
    this.category_data = data['category_data'],
    this.degree_data = data['degree_data'],
    this.dieses_data = data['dieses_data'],
    this.email = data['email'],
    this.mobile = data['mobile'],
    this.state = data['state'],
    this.gtimings = data['gtimings'],
    this.status = data['status'],
    this.rating = data['rating'],
    this.city = data['city'],
    this.address = data['address'],
    this.zipcode = data['zipcode'],
    this.avatar_image = data['avatar_image'],
    this.cover_image = data['cover_image'],
    this.last_login = data['last_login'],
    this.ip = data['ip'],
    this.description = data['description'],
    this.lat = data['lat'],
    this.lng = data['lng'],
    this.experience = data['experience'],
    this.specialization = data['specialization']
    // this.isAuthenticated = data['isAuthenticated'],
    // this.loginType = data['loginType']
  ;

  /*Future<void> saveUser(UserModel user) async {
    await appPreference.setString(AppPreferences.user, jsonEncode(user.toJson()));
    return;
  }

  static Future<UserModel> user() async {
    String strUser = appPreference.getString(AppPreferences.user);
    if(strUser == null){
      UserModel mUser = UserModel(isAuthenticated: false);
      return Future.value(mUser);
    }
    Map user = jsonDecode(strUser);

    UserModel mUser ;
    debugPrint("check map : ${user.toString()}");
    if(user!=null) {
      if(user['isAuthenticated'] == null){
        user['isAuthenticated'] = false;
      }
      mUser = UserModel.fromMap(user);
    }else{
      user = {
        "isAuthenticated":true
      };
      mUser = UserModel(isAuthenticated: false);
    }
    return Future.value(mUser);
  }*/
}
