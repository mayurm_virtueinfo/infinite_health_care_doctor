import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class PatientModel {
  int id_user ;
  String patient_firstname;
  String patient_mobile;
  String patient_lastname;
  String patient_address;
  String patient_state;
  String patient_city;
  String firebase_token;
  // String avatar_image;
  List<dynamic> appointment;
  // String timings;
  String patient_image;

  String date_of_birth;
 /* int doctor_id ;
  String doctor_name;
  String doctor_image;
  int patient_id;
  String patient_name;
  String patient_image;*/

  PatientModel.init();

  PatientModel(this.date_of_birth,this.firebase_token,this.id_user,this.patient_firstname,this.patient_mobile,this.patient_lastname, this.patient_address, this.patient_state,
      this.patient_city, /*this.avatar_image,*/ this.appointment/*,this.timings*//*,this.doctor_image*/,this.patient_image);

  /*PatientModel(this.doctor_id,this.doctor_name,this.doctor_image,this.patient_id, this.patient_name, this.patient_image);*/


  PatientModel.fromMap(map)
    :
    this.id_user = map['id_user'],
    this.patient_firstname = map['patient_firstname'],
    this.patient_mobile = map['patient_mobile'],
    this.patient_lastname = map['patient_lastname'],
    this.patient_address = map['patient_address'],
    this.patient_state = map['patient_state'],
    this.patient_city = map['patient_city'],
    this.appointment = map['appointment'],
    // this.timings = map['timings'],
    this.patient_image = map['patient_image'],
    this.date_of_birth = map['date_of_birth'],
    this.firebase_token = map['firebase_token'];

}

class PatientModelList {
  List<PatientModel> _patientsList = [];

  PatientModelList.fromSnapshot(snapshot) {
    List<dynamic> data = snapshot.data['data'];
    debugPrint("data : ${data}");
    data.forEach((item) {

      PatientModel doctor = PatientModel.fromMap(item);
      _patientsList.add(doctor);
    });
  }


  List<PatientModel> get patients => _patientsList;
}
