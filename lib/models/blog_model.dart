import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class BlogModel {
  int id ;
  String title;
  String date;
  String description;
  String blog_image;
  List<dynamic> category;
  List<dynamic> tags;
  BlogModel({this.id, this.title, this.date, this.blog_image, this.category, this.tags, this.description});

  BlogModel.fromMap(Map<String,dynamic> data):
      this.id  = data['id '],
      this.title = data['title'],
      this.date = data['date'],
      this.description = data['description'],
      this.blog_image = data['blog_image'],
      this.category = data['category'],
      this.tags = data['tags']
      ;

}

