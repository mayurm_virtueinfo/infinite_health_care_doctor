import 'package:flutter/material.dart';

class MyPaymentModel {
  int id;//": 40,
  int id_patient;//": 63,
  String patient_firstname;//": "Mayur K",
  String patient_lastname;//": "Chudasama",
  String amount;//": "500",
  String patient_email;//": "mayurkchudasama@gmail.com",
  String patient_mobile;//": "+918866700337",
  String payment_id;//": null,
  String appointment_date;//": "2020-12-08",
  String appointment_time;//": "23:00:00",
  String transaction_datetime;//": "2020-12-08 14:07:31",
  String patient_image;//": "http://work-updates.com/ihc/public/uploads/patient/no_image.png"
  MyPaymentModel(
    this.id_patient,
    this.patient_firstname,
    this.patient_lastname,
    this.amount,
    this.patient_email,
    this.patient_mobile,
    this.payment_id,
    this.appointment_date,
    this.appointment_time,
    this.patient_image,
    this.transaction_datetime
  );

  MyPaymentModel.fromMap(Map<dynamic,dynamic> map):
        this.id_patient = map['id_patient'],
        this.patient_firstname = map['patient_firstname'],
        this.patient_lastname = map['patient_lastname'],
        this.amount = map['amount'],
        this.patient_email = map['patient_email'],
        this.patient_mobile = map['patient_mobile'],
        this.payment_id = map['payment_id'],
        this.appointment_date = map['appointment_date'],
        this.appointment_time = map['appointment_time'],
        this.patient_image = map['patient_image'],
        this.transaction_datetime = map['transaction_datetime']
  ;

}

