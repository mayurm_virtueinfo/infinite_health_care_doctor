import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class NotificationModel {
  int patient_id;//": 64,
  int doctor_id;//": 59,
  String patient_firstname;//": "P. Mayur",
  String patient_lastname;//": "Mobile",
  String patient_email;//": "mayurchudasama77@gmail.com",
  String gender;//": null,
  String patient_state;//": "gujarat",
  String patient_city;//": "rajkot",
  String patient_address;//": "shramjivi society",
  String patient_zipcode;//": "360001",
  String patient_mobile;//": "+918866770652",
  String appointment_date;//": "2021-01-04",
  String appointment_time;//": "22:00:00",
  String message;//": "Book Appointment",
  String patient_image;//": "http://work-updates.com/ihc/public/uploads/patient/1608107144_image_cropper_1608107124757.jpg"



  NotificationModel(
    this.patient_id,
    this.doctor_id,
    this.patient_firstname,
    this.patient_lastname,
    this.patient_email,
    this.gender,
    this.patient_state,
    this.patient_city,
    this.patient_address,
    this.patient_zipcode,
    this.patient_mobile,
    this.appointment_date,
    this.appointment_time,
    this.message,
    this.patient_image,
  );



  NotificationModel.fromMap(map) :
        this.patient_id = map['patient_id'],
        this.doctor_id = map['doctor_id'],
        this.patient_firstname = map['patient_firstname'],
        this.patient_lastname = map['patient_lastname'],
        this.patient_email = map['patient_email'],
        this.gender = map['gender'],
        this.patient_state = map['patient_state'],
        this.patient_city = map['patient_city'],
        this.patient_address = map['patient_address'],
        this.patient_zipcode = map['patient_zipcode'],
        this.patient_mobile = map['patient_mobile'],
        this.appointment_date = map['appointment_date'],
        this.appointment_time = map['appointment_time'],
        this.message = map['message'],
        this.patient_image = map['patient_image'] ;
}

class NotificationModelList {
  List<NotificationModel> _notificationList = [];

  NotificationModelList.fromSnapshot(snapshot) {
    List<dynamic> data = snapshot.data['data'];
    debugPrint("data : ${data}");
//    data = mDoctorList;
    data.forEach((item) {

      NotificationModel doctor = NotificationModel.fromMap(item);
      _notificationList.add(doctor);
    });
  }


  List<NotificationModel> get notificationList => _notificationList;
}
