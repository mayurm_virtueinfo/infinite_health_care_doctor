import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class MyDoctorModel {
  int id_doctor;
  String doctor_name;
  String avatar_image;
  String degree;
  String description;
  String status;
  String timings;
  String rating;
  String type;


  MyDoctorModel.init();

  MyDoctorModel(this.id_doctor,this.doctor_name,this.avatar_image,this.degree, this.description, this.status,
      this.timings, this.rating, this.type);

  MyDoctorModel getCurrentMyDoctorModel() {
    return MyDoctorModel(1,"Dr.Vipul Bariya", "images/asset-1.png", "BHMS", "Homeopathy",
        "0", "9:30 AM - 8:00 PM", "4.2", "MyDoctorModel");
//        MyDoctorModel(1,"Dr. Vipul Bariay","mages/asset-1.png");
  }

  MyDoctorModel.fromMap(map)
    : this.id_doctor = map['id_doctors'],
    this.doctor_name = map['doctor_name'],
    this.avatar_image = map['avatar_image'],
    /*this.id = map['id'],
    this.title = map['title'],
    this.image = map['image'],*/
    this.degree = map['degree'],
    this.description = map['description'],
    this.status = map['status'],
    this.timings = map['timings'],
    this.rating = map['rating'],
    this.type = map['type'];
}

class MyDoctorModelList {
  List<MyDoctorModel> _doctorsList = [];
  /*List<Map<String, dynamic>> mMyDoctorModelList = [
    {
      'title': "Dr.Vipul Bariya",
      'image': "https://www.iconbunny.com/icons/media/catalog/product/2/1/2131.10-doctor-icon-iconbunny.jpg",
      'degree': "BHMS",
      'description': "Homeopathy",
      'status': "0",
      'slot': "9:30 AM - 8:00 PM",
      'rating': "4.5",
      'type': "Doctor"
    },
    {
      'title': "Dr.Rajesh Kumbhani",
      'image': "https://www.iconbunny.com/icons/media/catalog/product/2/1/2131.10-doctor-icon-iconbunny.jpg",
      'degree': "BHMS",
      'description': "Homeopathy",
      'status': "1",
      'slot': "10:30 AM - 7:30 PM",
      'rating': "4.2",
      'type': "Doctor"
    }
  ];*/

  MyDoctorModelList.fromSnapshot(snapshot) {
    List<dynamic> data = snapshot.data['data'];
    debugPrint("data : ${data}");
//    data = mDoctorList;
    data.forEach((item) {

      MyDoctorModel doctor = MyDoctorModel.fromMap(item);
      _doctorsList.add(doctor);
    });
  }


  List<MyDoctorModel> get myDoctorList => _doctorsList;
}
