
class PastConversation{
  int id_user ;
  String patient_name ;
  String avatar_image ;
  List<dynamic> appointment;
  String firebase_token;
 PastConversation.fromMap(map):
       this.id_user = map['id_user'],
       this.patient_name = map['patient_name'],
       this.avatar_image = map['avatar_image'],
       this.appointment = map['appointment']??null,
       this.firebase_token = map['patient_firebase_token'];

}
class PastConversationListModel{
  List<PastConversation> _conversationList = [];

  PastConversationListModel.fromSnapshot(snapshot) {
    List<dynamic> data = snapshot.data['data']['past_appointment'];

    data.forEach((item) {
      PastConversation appointment = PastConversation.fromMap(item);
      _conversationList.add(appointment);
    });
  }

  List<PastConversation> get pastConversation => _conversationList;

}