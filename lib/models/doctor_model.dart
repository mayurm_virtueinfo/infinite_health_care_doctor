import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class DoctorModel {
  int id ;
  String title;
  String image;
  String degree;
  String description;
  String status;
  String timings;
  String rating;
  String type;
  String originalPath;
  String coverPath;


  DoctorModel.init();

  DoctorModel(this.id,this.title,this.image,this.degree, this.description, this.status,
      this.timings, this.rating, this.type,this.originalPath,this.coverPath);

  DoctorModel getCurrentDoctor() {
    return DoctorModel(1,"Dr.Vipul Bariya", "images/asset-1.png", "BHMS", "Homeopathy",
        "0", "9:30 AM - 8:00 PM", "4.2", "Doctor","","");
//        DoctorModel(1,"Dr. Vipul Bariay","mages/asset-1.png");
  }

  DoctorModel.fromMap(map)
    : this.id = map['id'],
    this.title = map['title'],
    this.image = map['image'],
    this.degree = map['degree'],
    this.description = map['description'],
    this.status = map['status'],
    this.timings = map['timings'],
    this.rating = map['rating'],
    this.type = map['type'],
    this.originalPath = map['originalPath'],
    this.coverPath = map['coverPath'];
}

class DoctorModelList {
  List<DoctorModel> _doctorsList = [];
  /*List<Map<String, dynamic>> mDoctorList = [
    {
      'title': "Dr.Vipul Bariya",
      'image': "https://www.iconbunny.com/icons/media/catalog/product/2/1/2131.10-doctor-icon-iconbunny.jpg",
      'degree': "BHMS",
      'description': "Homeopathy",
      'status': "0",
      'slot': "9:30 AM - 8:00 PM",
      'rating': "4.5",
      'type': "Doctor"
    },
    {
      'title': "Dr.Rajesh Kumbhani",
      'image': "https://www.iconbunny.com/icons/media/catalog/product/2/1/2131.10-doctor-icon-iconbunny.jpg",
      'degree': "BHMS",
      'description': "Homeopathy",
      'status': "1",
      'slot': "10:30 AM - 7:30 PM",
      'rating': "4.2",
      'type': "Doctor"
    }
  ];*/

  DoctorModelList.fromSnapshot(snapshot) {
    List<dynamic> data = snapshot.data['data'];
    debugPrint("data : ${data}");
//    data = mDoctorList;
    data.forEach((item) {

      DoctorModel doctor = DoctorModel.fromMap(item);
      _doctorsList.add(doctor);
    });
  }


  List<DoctorModel> get doctors => _doctorsList;
}
