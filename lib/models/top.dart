import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:infinite_health_care_doctor/config/const.dart';

class Top {
  int id ;
  String title;
  String icon;
  String ImagePath;
  String IconPath;

  Top.init();

  Top(this.id,this.title, this.icon);

  Top.fromMap(map)
      : this.id = map['id'],
        this.title = map['title'],
        this.icon = map['icon'],
        this.ImagePath = map['ImagePath'],
        this.IconPath = map['IconPath'];
}

class TopListModel {
  List<Top> _topList = [];

  TopListModel.fromSnapshot(snapshot) {
    List<dynamic> data = snapshot.data['data'];
    data.forEach((item) {
//      item['icon'] = Const.defaultProfileUrl;
      Top top = Top.fromMap(item);
      _topList.add(top);
    });
  }

  List<Top> get topList => _topList;
}
