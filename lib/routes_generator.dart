import 'package:flutter/material.dart';
import 'package:infinite_health_care_doctor/pages/book-test-online1.dart';
import 'package:infinite_health_care_doctor/pages/book-test-online2.dart';
import 'package:infinite_health_care_doctor/pages/book-test-online3.dart';
import 'package:infinite_health_care_doctor/pages/createAccount.dart';
import 'package:infinite_health_care_doctor/pages/health.dart';
import 'package:infinite_health_care_doctor/pages/medeciens-2.dart';
import 'package:infinite_health_care_doctor/pages/medecines.dart';
import 'package:infinite_health_care_doctor/pages/offers.dart';
import 'package:infinite_health_care_doctor/splash_screen.dart';
import 'package:infinite_health_care_doctor/ui/account/my_appointments.dart';
import 'package:infinite_health_care_doctor/ui/account/my_patients.dart';
import 'package:infinite_health_care_doctor/ui/account/my_payments.dart';
import 'package:infinite_health_care_doctor/ui/account/notification_list.dart';
import 'package:infinite_health_care_doctor/ui/account/user_profile.dart';
import 'package:infinite_health_care_doctor/ui/conversation/personal_chat.dart';
import 'package:infinite_health_care_doctor/ui/home/add_notes_screen.dart';
import 'package:infinite_health_care_doctor/ui/home/blog_detail_screen.dart';
import 'package:infinite_health_care_doctor/ui/home/consultancy_booked.dart';
import 'package:infinite_health_care_doctor/ui/home/credit_card_payment.dart';
import 'package:infinite_health_care_doctor/ui/home/doctor_album_images.dart';
import 'package:infinite_health_care_doctor/ui/home/doctor_book_first_step.dart';
import 'package:infinite_health_care_doctor/ui/home/doctor_book_second_step.dart';
import 'package:infinite_health_care_doctor/ui/home/doctor_profile.dart';
import 'package:infinite_health_care_doctor/ui/home/doctors_near_you.dart';
import 'package:infinite_health_care_doctor/ui/home/patient_detail.dart';
import 'package:infinite_health_care_doctor/ui/home/patient_list.dart';
import 'package:infinite_health_care_doctor/ui/home/review_list_screen.dart';
import 'package:infinite_health_care_doctor/ui/home/schedule_screen.dart';
import 'package:infinite_health_care_doctor/ui/home/search_patients_list.dart';
import 'package:infinite_health_care_doctor/ui/home/send_feedback.dart';
import 'package:infinite_health_care_doctor/ui/home/upcoming_appointment_list.dart';
import 'package:infinite_health_care_doctor/ui/home_screen.dart';
import 'package:infinite_health_care_doctor/ui/intro/intro_screen.dart';
import 'package:infinite_health_care_doctor/ui/loginorsignup/enter_valid_mobile_number.dart';
import 'package:infinite_health_care_doctor/ui/loginorsignup/my_home_page.dart';
import 'package:infinite_health_care_doctor/ui/loginorsignup/patient_registration_screen.dart';
import 'package:infinite_health_care_doctor/ui/loginorsignup/signup.dart';
import 'package:infinite_health_care_doctor/ui/loginorsignup/verification_number.dart';

class RouteGenerator {
  // static String currentRoute;

  static Route<dynamic> generateRoute(RouteSettings settings) {
    // Getting arguments passed in while calling Navigator.pushNamed
    final args = settings.arguments;
    switch(settings.name){
      case SplashScreen.routeName :
        return MaterialPageRoute(builder: (_) => SplashScreen());
      case IntroScreen.routeName :
        return MaterialPageRoute(builder: (_) => IntroScreen());
      case SignUp.routeName :
        return MaterialPageRoute(builder: (_) => SignUp());
      case EnterValidMobileNumber.routeName:
        return MaterialPageRoute(builder: (_) => EnterValidMobileNumber());
      case VerificationNumber.routeName:
        List<dynamic> lstArgs = args;
        return MaterialPageRoute(builder: (_) => VerificationNumber(mobileNumber: lstArgs[0],verificationId: lstArgs[1],forceResendingToken: lstArgs[2],));
      case CreateAcount.routeName:
        return MaterialPageRoute(builder: (_) => CreateAcount());  
      case HomeScreen.routeName:
        List<dynamic> lstArgs = args;
        return MaterialPageRoute(builder: (_) => HomeScreen());
      case PatientList.routeName:
        List<dynamic> lstArgs = args;
        return MaterialPageRoute(builder: (_) => PatientList(doctorId: lstArgs[0],title: lstArgs[1],));
      case DoctorProfile.routeName:
        List<dynamic> lstArgs = args;
        return MaterialPageRoute(builder: (_) => DoctorProfile(id_doctor: lstArgs[0],title: lstArgs[1],));
      case DoctorBookFirstStep.routeName:
        List<dynamic> lstArgs = args;
        return MaterialPageRoute(builder: (_) => DoctorBookFirstStep(doctorProfileModel: lstArgs[0],));
      case DoctorBookSecondeStep.routeName:
        List<dynamic> lstArgs = args;
        return MaterialPageRoute(builder: (_) => DoctorBookSecondeStep(doctorProfileModel: lstArgs[0],));
      case ScheduleScreen.routeName:
        List<dynamic> lstArgs = args;
        return MaterialPageRoute(builder: (_) => ScheduleScreen(doctor_id: lstArgs[0],));
      case SendFeedback.routeName:
        return MaterialPageRoute(builder: (_) => SendFeedback());
      case OffersList.routeName:
        return MaterialPageRoute(builder: (_) => OffersList());
      case BookTestsOnline.routeName:
        return MaterialPageRoute(builder: (_) => BookTestsOnline());
      case BookTestsOnlineSecondeStep.routeName:
        return MaterialPageRoute(builder: (_) => BookTestsOnlineSecondeStep());
      case BookTestsOnlineThirdStep.routeName:
        return MaterialPageRoute(builder: (_) => BookTestsOnlineThirdStep());
      case Medecines.routeName:
        return MaterialPageRoute(builder: (_) => Medecines());
      case MedecinesSlected.routeName:
        return MaterialPageRoute(builder: (_) => MedecinesSlected());
      case MyPatients.routeName:
        return MaterialPageRoute(builder: (_) => MyPatients());
      case NotificationList.routeName:
        return MaterialPageRoute(builder: (_) => NotificationList());
      case MyAppointments.routeName:
        return MaterialPageRoute(builder: (_) => MyAppointments());
      case MyPayments.routeName:
        return MaterialPageRoute(builder: (_) => MyPayments());
      case HealthTips.routeName:
        return MaterialPageRoute(builder: (_) => HealthTips());
      case UserProfile.routeName:
        return MaterialPageRoute(builder: (_) => UserProfile());
      case CreditCardPayment.routeName:
        List<dynamic> lstArgs = args;
        return MaterialPageRoute(builder: (_) => CreditCardPayment(doctorProfileModel: lstArgs[0],));
      case ConsultancyBooked.routeName:
        return MaterialPageRoute(builder: (_) => ConsultancyBooked());
      case SearchPatientsList.routeName:
        return MaterialPageRoute(builder: (_) => SearchPatientsList());
      case PatientRegistrationScreen.routeName:
        List<dynamic> lstArgs = args;
        return MaterialPageRoute(builder: (_) => PatientRegistrationScreen(user: lstArgs[0],),);
      case PersonalChat.routeName:
        List<dynamic> lstArgs = args;
        return MaterialPageRoute(builder: (_) => PersonalChat(peerId: lstArgs[0],appointmentType: lstArgs[1],doctor: lstArgs[2],));
      case DoctorsNearYou.routeName:
        List<dynamic> lstArgs = args;
        return MaterialPageRoute(builder: (_) => DoctorsNearYou(latitude: lstArgs[0],longitude: lstArgs[1],));
      case DoctorAlbumImages.routeName:
        List<dynamic> lstArgs = args;
        return MaterialPageRoute(builder: (_) => DoctorAlbumImages(index: lstArgs[0],album_images: lstArgs[1],));
      case MyHomePage.routeName:
        return MaterialPageRoute(builder: (_) => MyHomePage());
      case SendFeedback.routeName:
        return MaterialPageRoute(builder: (_) => SendFeedback());
      case BlogDetailScreen.routeName:
        List<dynamic> lstArgs = args;
        return MaterialPageRoute(builder: (_) => BlogDetailScreen(blogModel: lstArgs[0],));
      case PatientDetail.routeName:
        List<dynamic> lstArgs  = args;
        return MaterialPageRoute(builder: (_) => PatientDetail(patient: lstArgs[0],));
      case AddNotesScreen.routeName:
        List<dynamic> lstArgs = args;
        return MaterialPageRoute(builder: (_) => AddNotesScreen(singleCustomer: lstArgs[0],));
      case ReviewListScreen.routeName:
        List<dynamic> lstArgs = args;
        return MaterialPageRoute(builder: (_) => ReviewListScreen(doctorId: lstArgs[0],));
      case UpcomingAppointmentList.routeName:
        return MaterialPageRoute(builder: (_) => UpcomingAppointmentList());
      default:
        // If there is no such named route in the switch statement, e.g. /third
        return _errorRoute();
    }
  }
    static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: Text('Error'),
        ),
        body: Center(
          child: Text('ERROR'),
        ),
      );
    });
  }


}