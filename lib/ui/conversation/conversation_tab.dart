import 'package:flutter/material.dart';
import 'package:infinite_health_care_doctor/bloc/loading/loading_bloc_event.dart';
import 'package:infinite_health_care_doctor/bloc/loading/loading_bloc.dart';
import 'package:infinite_health_care_doctor/widgets/loading_widget.dart';
import 'package:infinite_health_care_doctor/main.dart';
import 'package:infinite_health_care_doctor/api/api_request.dart';
import 'package:infinite_health_care_doctor/models/appointment_type.dart';
import 'package:infinite_health_care_doctor/models/past_conversation.dart' as pastModel;
import 'package:infinite_health_care_doctor/models/schedule_conversation.dart' as scheduledModel;
import 'package:infinite_health_care_doctor/models/user_model.dart';
import 'package:infinite_health_care_doctor/main.dart';
import 'package:infinite_health_care_doctor/utils/utility.dart';
import 'package:infinite_health_care_doctor/widgets/past_conversation_item_widget.dart';
import 'package:infinite_health_care_doctor/widgets/scheduled_conversation_item_widget.dart';
import 'package:infinite_health_care_doctor/widgets/searchWidget.dart';
import 'package:intl/intl.dart';

class ConversationTab extends StatefulWidget {
  @override
  _ConversationTabState createState() => _ConversationTabState();
}

class _ConversationTabState extends State<ConversationTab> {
  @override
  void initState() {
    super.initState();
  }

  var editingController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                  height: AppBar().preferredSize.height+MediaQuery.of(context).padding.top,
                  padding: const EdgeInsets.only(left: 20.0, right: 20.0,top: 25),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(bottomLeft: Radius.circular(25.0), bottomRight: Radius.circular(25.0)),
                    color: Theme.of(context).accentColor,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            'Messages',
                            style: TextStyle(
                              fontSize: 22.0,

                              fontWeight: FontWeight.bold,
                              color: Theme.of(context).primaryColor,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                /*Padding(
                  padding: const EdgeInsets.only(top: 100, left: 12.0, right: 12.0),
                  child: SearchBarWidget(),
                ),*/
              ],
            ),
            FutureBuilder(
              future: ApiRequest.postUpcomingAppointmentDoctor(date_time: Utility.getFormatedCurrentTime(), doctor_id: appUserModel.id),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
                  AppointmentType saType = AppointmentType.SCHEDULED_APPOINTMENT;
                  scheduledModel.ScheduledConversationListModel sAList = scheduledModel.ScheduledConversationListModel.fromSnapshot(snapshot);

                  AppointmentType aType = AppointmentType.PAST_APPOINTMENT;
                  pastModel.PastConversationListModel paList = pastModel.PastConversationListModel.fromSnapshot(snapshot);
                  return Column(
                    children: [
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Container(
                          width: 250,
                          margin: EdgeInsets.only(top: 15, right: 15),
                          padding: EdgeInsets.only(left: 20,bottom: 10,top: 10,right: 20),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(topRight: Radius.circular(100),bottomRight: Radius.circular(100)),
                              //color: Colors.grey,
                              gradient: LinearGradient(
                                colors: [Colors.red[200],Colors.blue[200]],
                                begin: const FractionalOffset(0.0, 0.0),
                                end: const FractionalOffset(0.5, 0.0),
                                stops: [0.0,1.0],
                                tileMode: TileMode.clamp,
                              )
                          ),
                          child: Text(
                            "Scheduled Appointments",
                            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                          ),
                        ),
                      ),
                      sAList.scheduledConversation.length>0?ListView.separated(
                        shrinkWrap: true,
                        primary: false,
                        itemCount: sAList.scheduledConversation.length,
                        separatorBuilder: (context, index) {
                          return SizedBox(height: 20.0);
                        },
                        itemBuilder: (context, index) {
                          List<dynamic> appt = sAList.scheduledConversation.elementAt(index).appointment;
                          Set<String> dtSet = Set();
                          appt.forEach((element) {
                            dtSet.add(element['appointment_date']);
                          });
                          Map<String,dynamic> mapApptStruct = Map();
                          dtSet.forEach((elementSet) {
                            List<dynamic> srchdDt = appt.where((element) => element['appointment_date'] == elementSet).toList();
                            if(srchdDt.length>0){
                              List<String> lstTime = [];
                              srchdDt.forEach((elementSrchDt) {
                                String time = elementSrchDt['time'];
                                DateFormat parseFormat= DateFormat("dd-MMM-yyyy HH:mm:ss");
                                DateTime  dtTime = parseFormat.parse('$elementSet $time');

                                DateFormat showFormat= DateFormat("hh:mm a");
                                String strTime = showFormat.format(dtTime);

                                lstTime.add(strTime);
                              });
                              mapApptStruct[elementSet]=lstTime;
                            }
                          });
                          debugPrint(mapApptStruct.toString());
                          return ScheduledConversationItemWidget(
                            conversation: sAList.scheduledConversation.elementAt(index),
                            appointmentType: saType,
                            mapApptStruct: mapApptStruct,
                          );
                        },
                      ):Container(
                        margin: EdgeInsets.only(top: 30),
                        alignment: Alignment.center,
                        child: Text(
                          "No scheduled appointments found",
                          style: TextStyle(fontSize: 16),
                        ),
                      ),
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Container(
                          width: 250,
                          margin: EdgeInsets.only(top: 35, right: 15),
                          padding: EdgeInsets.only(left: 20,bottom: 10,top: 10,right: 20),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(topRight: Radius.circular(100),bottomRight: Radius.circular(100)),
                              //color: Colors.grey,
                              gradient: LinearGradient(
                                colors: [Colors.red[200],Colors.blue[200]],
                                begin: const FractionalOffset(0.0, 0.0),
                                end: const FractionalOffset(0.5, 0.0),
                                stops: [0.0,1.0],
                                tileMode: TileMode.clamp,
                              )
                          ),
                          child: Text(
                            "Past Appointments",
                            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                          ),
                        ),
                      ),
                      paList.pastConversation.length>0?ListView.separated(
                        shrinkWrap: true,
                        primary: false,
                        itemCount: paList.pastConversation.length,
                        separatorBuilder: (context, index) {
                          return SizedBox(height: 20.0);
                        },
                        itemBuilder: (context, index) {
                          List<dynamic> appt = paList.pastConversation.elementAt(index).appointment;
                          Set<String> dtSet = Set();
                          appt.forEach((element) {
                            dtSet.add(element['appointment_date']);
                          });
                          Map<String,dynamic> mapApptStruct = Map();
                          dtSet.forEach((elementSet) {
                            List<dynamic> srchdDt = appt.where((element) => element['appointment_date'] == elementSet).toList();
                            if(srchdDt.length>0){
                              List<String> lstTime = [];
                              srchdDt.forEach((elementSrchDt) {
                                String time = elementSrchDt['time'];
                                DateFormat parseFormat= DateFormat("dd-MMM-yyyy HH:mm:ss");
                                DateTime  dtTime = parseFormat.parse('$elementSet $time');

                                DateFormat showFormat= DateFormat("hh:mm a");
                                String strTime = showFormat.format(dtTime);

                                lstTime.add(strTime);
                              });
                              mapApptStruct[elementSet]=lstTime;
                            }
                          });
                          debugPrint(mapApptStruct.toString());
                          return PastConversationItemWidget(
                            conversation: paList.pastConversation.elementAt(index),
                            appointmentType: aType,
                            mapApptStruct: mapApptStruct,
                          );
                        },
                      ):Container(
                        margin: EdgeInsets.only(top: 30),
                        alignment: Alignment.center,
                        child: Text(
                          "No past appointments found",
                          style: TextStyle(fontSize: 16),
                        ),
                      ),
                    ],
                  );
                } else {
                  return LoadingWidget(initialData: true,);
                }
              },
            ),
            SizedBox(height: 30,)

          ],
        ),
      ),
    );
  }
}
