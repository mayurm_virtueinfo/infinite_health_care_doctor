import 'package:infinite_health_care_doctor/routes_generator.dart';
import 'package:infinite_health_care_doctor/ui/home/patient_detail.dart';
import 'package:infinite_health_care_doctor/models/patient_model.dart';
import 'package:infinite_health_care_doctor/bloc/loading/loading_bloc_event.dart';
import 'package:infinite_health_care_doctor/bloc/loading/loading_bloc.dart';
import 'package:infinite_health_care_doctor/widgets/loading_widget.dart';
import 'package:infinite_health_care_doctor/widgets/loading_widget.dart';
import 'dart:convert';
import 'dart:async';
import 'package:infinite_health_care_doctor/bloc/personal_chat_bloc.dart';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:infinite_health_care_doctor/api/api_request.dart';
import 'package:infinite_health_care_doctor/bloc/life_cycle_bloc.dart';
import 'package:infinite_health_care_doctor/bloc/onlineoffline/online_offline_bloc.dart';
import 'package:infinite_health_care_doctor/bloc/typingbloc/typing_bloc.dart';
import 'package:infinite_health_care_doctor/main.dart';
import 'package:infinite_health_care_doctor/models/appointment_type.dart';
import 'package:infinite_health_care_doctor/models/doctor_profile_model.dart';
import 'package:infinite_health_care_doctor/models/user_model.dart';
import 'package:infinite_health_care_doctor/services/life_cycle_mgr.dart';
import 'package:infinite_health_care_doctor/ui/home/doctor_book_first_step.dart';
import 'package:infinite_health_care_doctor/utils/const.dart';
import 'package:infinite_health_care_doctor/utils/my_toast.dart';
import 'package:infinite_health_care_doctor/utils/utility.dart';
import 'package:infinite_health_care_doctor/widgets/full_photo.dart';
import 'package:infinite_health_care_doctor/widgets/loading.dart';
import 'package:intl/intl.dart';
import 'package:infinite_health_care_doctor/ui/home_screen.dart';
import 'package:infinite_health_care_doctor/services/navigation_service.dart';

class PersonalChat extends StatefulWidget {
  static const routeName = "PersonalChat";
  final String peerId;

//  final doctorModel.DoctorModel doctor;
  final doctor;
  AppointmentType appointmentType;

  PersonalChat({this.peerId, this.appointmentType, this.doctor});

  @override
  _PersonalChatState createState() => _PersonalChatState();
}

class _PersonalChatState extends State<PersonalChat> {
  Map<String,dynamic> mapLaunchGoBackData=Map();

  TypingBloc typingBloc = TypingBloc();

  bool isLoadingSingleDoctor = false;
  String peerId;

  // String id;

  var listMessage;
  String groupChatId;

  File imageFile;
  bool isLoading;
  bool isShowSticker;
  String imageUrl;

  final TextEditingController textEditingController = TextEditingController();
  final ScrollController listScrollController = ScrollController();
  final FocusNode focusNode = FocusNode();
  String chatSenderId='';
  @override
  void initState() {
    mapLaunchGoBackData = Map.from(mapLaunchData);
    chatSenderId = getChatSenderId();
    super.initState();
    mapLaunchData = Map();
    locator<PersonalChatBloc>().personalChatStream.listen((event) {
      if (event == '${AppLifecycleState.resumed.toString()}') {
        userOnlineStatus(status: EnumOnlineStatus.ONLINE);
      } else if (event == '${AppLifecycleState.inactive.toString()}' || event == '${AppLifecycleState.paused.toString()}') {
        userOnlineStatus(status: EnumOnlineStatus.OFFLINE);
      }
    });
    focusNode.addListener(onFocusChange);

    peerId = widget.peerId;
    debugPrint("peerId : ${widget.peerId} ");
    groupChatId = '';

    isLoading = false;
    isShowSticker = false;
    imageUrl = '';

    readLocal();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    debugPrint("---LifeCycleEvent : personal_chat : dispose : offline");
    // RouteGenerator.currentRoute = null;
    userOnlineStatus(status: EnumOnlineStatus.OFFLINE);
  }

  void onFocusChange() {
    if (focusNode.hasFocus) {
      // Hide sticker when keyboard appear
      setState(() {
        isShowSticker = false;
      });
    }
  }


  readLocal() async {

    debugPrint("User ID : ${appUserModel.id}");
    debugPrint("peerID : ${widget.peerId}");
    groupChatId = '${widget.peerId}-${appUserModel.id}';
    debugPrint("Chat Group : ${groupChatId}");

    DocumentSnapshot mSnap = await FirebaseFirestore.instance.collection('messages').doc(groupChatId).get();

    Map<String, dynamic> mDataStart = mSnap.data();
    if (mDataStart == null) {
      valOnlineStatusPeer = EnumOnlineStatus.OFFLINE;
    } else {
      debugPrint("My Data online ${peerId}: ${mDataStart}");
      String keyStatus = 'patient_$peerId';
      valOnlineStatusPeer = mDataStart[keyStatus][onlineStatus];
      debugPrint("My Data online ${keyStatus}: ${valOnlineStatusPeer}");
    }
    blocOnlineOffline.onlineOfflineEventSink.add(valOnlineStatusPeer);

    await userOnlineStatus(status: EnumOnlineStatus.ONLINE);
    registerUserOnlinEvent();

    userTypingStatus(status: false);
    registerUserTypingEvent();

    setState(() {});
  }

  String onlineStatus = "online_status";
  String typingStatus = "typing_status";
  String valOnlineStatusPeer = '';
  bool valTypingStatusPeer = false;
  String valOnlineStatusUser = '';
  bool valTypingStatusUser = false;
  OnlineOfflineBloc blocOnlineOffline = OnlineOfflineBloc();

  Future<void> userOnlineStatus({String status}) async {
    valOnlineStatusUser = status;
    DocumentSnapshot mSnap = await FirebaseFirestore.instance.collection('messages').doc(groupChatId).get();
    Map<String, dynamic> mDataOnlineStatus = mSnap.data();
    debugPrint("mDataOnlineStatus : ${mDataOnlineStatus}");
    if (mDataOnlineStatus == null) {
      await FirebaseFirestore.instance.collection('messages').doc(groupChatId).set({
        'doctor_${appUserModel.id}': {onlineStatus: valOnlineStatusUser, typingStatus: valTypingStatusUser},
        'patient_$peerId': {typingStatus: valTypingStatusPeer, onlineStatus: valOnlineStatusPeer}
      });
    } else {
      await FirebaseFirestore.instance.collection('messages').doc(groupChatId).update({
        'doctor_${appUserModel.id}': {onlineStatus: valOnlineStatusUser, typingStatus: valTypingStatusUser},
        'patient_$peerId': {typingStatus: valTypingStatusPeer, onlineStatus: valOnlineStatusPeer}
      });
    }
  }

  void userTypingStatus({bool status}) async {
    valTypingStatusUser = status;
    DocumentSnapshot mSnap = await FirebaseFirestore.instance.collection('messages').doc(groupChatId).get();
    Map<String, dynamic> mDataUserTypingstatus = mSnap.data();
    debugPrint("mDataUserTypingstatus : ${mDataUserTypingstatus}");
    if (mDataUserTypingstatus == null) {
      FirebaseFirestore.instance.collection('messages').doc(groupChatId).set({
        'doctor_${appUserModel.id}': {
          typingStatus: valTypingStatusUser,
          onlineStatus: valOnlineStatusUser,
        },
        'patient_$peerId': {typingStatus: valTypingStatusPeer, onlineStatus: valOnlineStatusPeer}
      });
    } else {
      FirebaseFirestore.instance.collection('messages').doc(groupChatId).update({
        'doctor_${appUserModel.id}': {
          typingStatus: valTypingStatusUser,
          onlineStatus: valOnlineStatusUser,
        },
        'patient_$peerId': {typingStatus: valTypingStatusPeer, onlineStatus: valOnlineStatusPeer}
      });
    }
  }

  void registerUserOnlinEvent() async {
    FirebaseFirestore.instance.collection('messages').doc(groupChatId).snapshots(includeMetadataChanges: true).listen((event) {
      debugPrint('event received');
      debugPrint('event ${event.data()}');
      Map<String, dynamic> eventData = event.data();
      if (eventData == null) {
        valOnlineStatusPeer = EnumOnlineStatus.OFFLINE;
      } else {
        String keyStatus = 'patient_$peerId';
        String status = eventData[keyStatus][onlineStatus];
        valOnlineStatusPeer = status;
      }
      debugPrint("event online status : ${valOnlineStatusPeer}");
      blocOnlineOffline.onlineOfflineEventSink.add(valOnlineStatusPeer);
    });
  }

  void registerUserTypingEvent() {
    FirebaseFirestore.instance.collection('messages').doc(groupChatId).snapshots(includeMetadataChanges: true).listen((event) {
      debugPrint('event received');
      debugPrint('event ${event.data()}');
      Map<String, dynamic> eventData = event.data();
      if (eventData == null) {
        valTypingStatusPeer = false;
      } else {
        String keyStatus = 'patient_$peerId';
        bool status = eventData[keyStatus][typingStatus];
        valTypingStatusPeer = status;
      }
      debugPrint("event Status : ${valTypingStatusPeer}");
      typingBloc.typingEventSink.add(valTypingStatusPeer);
    });
  }

  Future getImage() async {
    PickedFile pickedFile = await ImagePicker().getImage(source: ImageSource.gallery);

    if (pickedFile != null) {
      imageFile = File(pickedFile.path);
    } else {
      imageFile = null;
    }

    if (imageFile != null) {
      setState(() {
        isLoading = true;
      });
      imageFile = await Utility.cropImage(imageFile);
      uploadFile();
    }
  }

  void getSticker() {
    // Hide keyboard when sticker appear
    focusNode.unfocus();
    setState(() {
      isShowSticker = !isShowSticker;
    });
  }

  Future uploadFile() async {
    debugPrint("uploading file : $imageFile");
    String fileName = DateTime.now().millisecondsSinceEpoch.toString();
    debugPrint("uploading file name : $fileName");
    Reference reference = FirebaseStorage.instance.ref().child(fileName);
    final metadata = SettableMetadata(contentType: 'image/jpeg', customMetadata: {'picked-file-path': imageFile.path});
    UploadTask uploadTask = reference.putFile(imageFile, metadata);

    try {
      TaskSnapshot storageTaskSnapshot = await uploadTask.whenComplete(() {
        return uploadTask.snapshot;
      });
      String urlPath = await storageTaskSnapshot.ref.getDownloadURL();
      debugPrint('urlPath : ${urlPath}');
      imageUrl = urlPath;
      setState(() {
        isLoading = false;
        onSendMessage(imageUrl, 1);
      });
    } catch (e) {
      debugPrint('image upload error : ${e.toString()}');
      setState(() {
        isLoading = false;
      });
      MyToast.showToast('Something went wrong uploading image', context);
    }
  }
  String getChatSenderId(){
    return 'doctor_${appUserModel.id}';
  }
  void onSendMessage(String content, int type) async {
    /*DocumentSnapshot snapshot = await FirebaseFirestore.instance.collection('messages').doc(groupChatId).get();
    final snapData = snapshot.data();
    debugPrint("snapData : ${snapData}");*/

    // return;
    // type: 0 = text, 1 = image, 2 = sticker
    if (content.trim() != '') {
      textEditingController.clear();

      var documentReference = FirebaseFirestore.instance.collection('messages').doc(groupChatId).collection(groupChatId).doc(DateTime.now().millisecondsSinceEpoch.toString());

      FirebaseFirestore.instance.runTransaction((transaction) async {
        await transaction.set(
          documentReference,
          {'sender': chatSenderId, 'receiver': peerId, 'timestamp': DateTime.now().millisecondsSinceEpoch.toString(), 'content': content, 'type': type},
        );
      });
      listScrollController.animateTo(0.0, duration: Duration(milliseconds: 300), curve: Curves.easeOut);

      debugPrint("dr status : ");
      if (valOnlineStatusPeer == EnumOnlineStatus.OFFLINE) {
        debugPrint("-----offline-----");
        debugPrint("-----token-----patient : ${widget.doctor.firebase_token}");
        debugPrint("dr status : offline");
        List<String> listRegistretionIds = [];
        // listRegistretionIds.add('dWeEcxfSS2GVDcPLtjrGPq:APA91bHNhawdAob_zXx2Y-IJBL3mAaa9Qx9tLoXTVxiHxUWfzgKq1j-If-xqAQnwLbmxZk1JyPD-tmuVW7PUY161mktpV2fFlp735i8L1KC9JcZtSRPCKy2hzhAvRkYTFhsIW1wVSl0e');
        listRegistretionIds.add(widget.doctor.firebase_token);

        Map<String, String> data = Map();
        /*data['click_action'] = "FLUTTER_NOTIFICATION_CLICK";
        data['timestamp'] = Utility.getFormatedCurrentTime();
        data['content'] = content;
        data['screen'] = PersonalChat.routeName;
        data['doctorId'] = widget.peerId;
        data['patientId'] = user.id;
        data['type'] = '0';


        Map<String,dynamic> data= Map();*/
        data['click_action'] = "FLUTTER_NOTIFICATION_CLICK";
        data['timestamp'] = Utility.getFormatedCurrentTime();
        data['content'] = content;
        data['screen'] = 'PersonalChat';
        Map<String,dynamic> userData=Map();
        userData['id_doctor']=appUserModel.id;
        userData['doctor_name']='${appUserModel.fullname}';
        userData['avatar_image']=appUserModel.avatar_image;
        userData['firebase_token']=fcmToken;
        data['appointmentType'] = widget.appointmentType.toString();
        data['id_doctor'] = appUserModel.id;
        // data['patient'] = userData;
        data['patient'] = json.encode(userData) ;
        debugPrint("notification data : ${data}");


        final sentResult = await ApiRequest.postSendPush(patient: appUserModel.fullname, registrationIds: listRegistretionIds.toList(), message: content, data: data);
        debugPrint("message sent : result : ${sentResult.toString()}");
      } else {
        debugPrint("dr status : online");
      }
    } else {
      MyToast.showToast('Nothing to send', context);
    }
  }

  Widget buildItem(int index, DocumentSnapshot document) {
    if (document['sender'] == chatSenderId) {
      // Right (my message)
      return Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              document['type'] == 0
              // Text
                  ? Flexible(
                child: Align(
                  alignment: Alignment.centerRight,
                  child: Container(
                    decoration: BoxDecoration(color: Theme.of(context).accentColor.withOpacity(0.8), borderRadius: BorderRadius.only(topLeft: Radius.circular(15), bottomLeft: Radius.circular(15), bottomRight: Radius.circular(15))),
                    padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                    child: new Text(
                      document['content'],
                      style: TextStyle(color: Theme.of(context).primaryColor),
                    ),
                  ),
                ),
              )
                  : document['type'] == 1
              // Image
                  ? Container(
                padding: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                decoration: BoxDecoration(color: Theme.of(context).accentColor.withOpacity(0.8), borderRadius: BorderRadius.only(topLeft: Radius.circular(15), bottomLeft: Radius.circular(15), bottomRight: Radius.circular(15))),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    TextButton(
                      child: Material(
                        child: CachedNetworkImage(
                          placeholder: (context, url) => Container(
                            child: Center(
                              child: LoadingWidget(initialData: true,),
                            ),
                            width: 200.0,
                            height: 200.0,
//                                  padding: EdgeInsets.all(70.0),
                            decoration: BoxDecoration(
                              color: greyColor2,
                              borderRadius: BorderRadius.all(
                                Radius.circular(8.0),
                              ),
                            ),
                          ),
                          errorWidget: (context, url, error) => Material(
                            child: Image.asset(
                              'images/img_not_available.jpeg',
                              width: 200.0,
                              height: 200.0,
                              fit: BoxFit.cover,
                            ),
                            borderRadius: BorderRadius.all(
                              Radius.circular(8.0),
                            ),
                            clipBehavior: Clip.hardEdge,
                          ),
                          imageUrl: document['content'],
                          width: 200.0,
                          height: 200.0,
                          fit: BoxFit.cover,
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(8.0)),
                        clipBehavior: Clip.hardEdge,
                      ),
                      onPressed: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => FullPhoto(url: document['content'])));
                      },
                      style: TextButton.styleFrom(
                        padding: EdgeInsets.all(0),
                      ),

                    ),
                  ],
                ),
                margin: EdgeInsets.only(bottom: isLastMessageRight(index) ? 20.0 : 10.0, right: 10.0),
              )
              // Sticker
                  : Container(
                padding: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                decoration: BoxDecoration(color: Theme.of(context).accentColor.withOpacity(0.8), borderRadius: BorderRadius.only(topRight: Radius.circular(15), bottomLeft: Radius.circular(15), bottomRight: Radius.circular(15))),
                child: Image.asset(
                  'images/${document['content']}.gif',
                  width: 100.0,
                  height: 100.0,
                  fit: BoxFit.cover,
                ),
                margin: EdgeInsets.only(bottom: isLastMessageRight(index) ? 20.0 : 10.0, right: 10.0),
              ),
            ],
            mainAxisAlignment: MainAxisAlignment.end,
          ),
          Container(
            alignment: Alignment.centerRight,
            child: Text(
              DateFormat('dd MMM hh:mm a').format(DateTime.fromMillisecondsSinceEpoch(int.parse(document['timestamp']))),
              style: TextStyle(color: greyColor, fontSize: 10.0, fontStyle: FontStyle.italic),
            ),
            margin: EdgeInsets.only(left: 5.0, bottom: 5.0,top: 5.0),
          )
        ],
      );
    } else {
      // Left (peer message)
      return Container(
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                document['type'] == 0
                //Text
                    ? Flexible(
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Container(
                      decoration: BoxDecoration(color: Theme.of(context).backgroundColor.withOpacity(0.8), borderRadius: BorderRadius.only(topLeft: Radius.circular(15), bottomLeft: Radius.circular(15), bottomRight: Radius.circular(15))),
                      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                      child: new Text(
                        document['content'],
                        style: TextStyle(color: Theme.of(context).primaryColor),
                      ),
                    ),
                  ),
                )
                    : document['type'] == 1
                //Image
                    ? Container(
                  decoration: BoxDecoration(color: Theme.of(context).backgroundColor.withOpacity(0.8), borderRadius: BorderRadius.only(topRight: Radius.circular(15), bottomLeft: Radius.circular(15), bottomRight: Radius.circular(15))),
                  padding: EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                  child: TextButton(
                    child: Material(
                      child: CachedNetworkImage(
                        placeholder: (context, url) => Container(
                          child: Center(
                            child: LoadingWidget(initialData: true,),
                          ),
                          width: 200.0,
                          height: 200.0,
//                                  padding: EdgeInsets.all(70.0),
                          decoration: BoxDecoration(
                            color: greyColor2,
                            borderRadius: BorderRadius.all(
                              Radius.circular(8.0),
                            ),
                          ),
                        ),
                        errorWidget: (context, url, error) => Material(
                          child: Image.asset(
                            'images/img_not_available.jpeg',
                            width: 200.0,
                            height: 200.0,
                            fit: BoxFit.cover,
                          ),
                          borderRadius: BorderRadius.all(
                            Radius.circular(8.0),
                          ),
                          clipBehavior: Clip.hardEdge,
                        ),
                        imageUrl: document['content'],
                        width: 200.0,
                        height: 200.0,
                        fit: BoxFit.cover,
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(8.0)),
                      clipBehavior: Clip.hardEdge,
                    ),
                    onPressed: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context) => FullPhoto(url: document['content'])));
                    },
                    style: TextButton.styleFrom(
                      padding: EdgeInsets.all(0),
                    ),

                  ),
                  margin: EdgeInsets.only(left: 10.0),
                )
                // Sticker
                    : Container(
                  decoration: BoxDecoration(color: Theme.of(context).backgroundColor.withOpacity(0.8), borderRadius: BorderRadius.only(topRight: Radius.circular(15), bottomLeft: Radius.circular(15), bottomRight: Radius.circular(15))),
                  padding: EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                  child: Image.asset(
                    'images/${document['content']}.gif',
                    width: 100.0,
                    height: 100.0,
                    fit: BoxFit.cover,
                  ),
                  margin: EdgeInsets.only(bottom: isLastMessageRight(index) ? 20.0 : 10.0, right: 10.0),
                ),
              ],
            ),

            // Time
            Container(

              child: Text(
                DateFormat('dd MMM hh:mm a').format(DateTime.fromMillisecondsSinceEpoch(int.parse(document['timestamp']))),
                style: TextStyle(color: greyColor, fontSize: 10.0, fontStyle: FontStyle.italic),
              ),
              margin: EdgeInsets.only(left: 5.0, bottom: 5.0,top: 5.0),
            )
            /*isLastMessageLeft(index)
                ? Container(
                    child: Text(
                      DateFormat('dd MMM kk:mm')
                          .format(DateTime.fromMillisecondsSinceEpoch(int.parse(document['timestamp']))),
                      style: TextStyle(color: greyColor, fontSize: 10.0, fontStyle: FontStyle.italic),
                    ),
                    margin: EdgeInsets.only(left: 5.0, bottom: 5.0),
                  )
                : Container()*/
          ],
          crossAxisAlignment: CrossAxisAlignment.start,
        ),
        margin: EdgeInsets.only(bottom: 10.0),
      );
    }
  }

  bool isLastMessageLeft(int index) {
    if ((index > 0 && listMessage != null && listMessage[index - 1]['sender'] == chatSenderId) || index == 0) {
      return true;
    } else {
      return false;
    }
  }

  bool isLastMessageRight(int index) {
    if ((index > 0 && listMessage != null && listMessage[index - 1]['sender'] != chatSenderId) || index == 0) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> onBackPress() {
    debugPrint('onLaunch Back pressed 1');
    if (isShowSticker) {
      debugPrint('onLaunch Back pressed 2');
      setState(() {
        isShowSticker = false;
      });
    } else {
      debugPrint('onLaunch Back pressed 3');
      // Firestore.instance.collection('users').document(user.id).updateData({'chattingWith': null});
      Navigator.pop(context);
    }

    return Future.value(false);
  }



  Widget buildSticker() {
    return Container(
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              TextButton(
                onPressed: () => onSendMessage('mimi1', 2),
                child: Image.asset(
                  'images/mimi1.gif',
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
              ),
              TextButton(
                onPressed: () => onSendMessage('mimi2', 2),
                child: Image.asset(
                  'images/mimi2.gif',
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
              ),
              TextButton(
                onPressed: () => onSendMessage('mimi3', 2),
                child: Image.asset(
                  'images/mimi3.gif',
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
              )
            ],
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          ),
          Row(
            children: <Widget>[
              TextButton(
                onPressed: () => onSendMessage('mimi4', 2),
                child: Image.asset(
                  'images/mimi4.gif',
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
              ),
              TextButton(
                onPressed: () => onSendMessage('mimi5', 2),
                child: Image.asset(
                  'images/mimi5.gif',
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
              ),
              TextButton(
                onPressed: () => onSendMessage('mimi6', 2),
                child: Image.asset(
                  'images/mimi6.gif',
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
              )
            ],
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          ),
          Row(
            children: <Widget>[
              TextButton(
                onPressed: () => onSendMessage('mimi7', 2),
                child: Image.asset(
                  'images/mimi7.gif',
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
              ),
              TextButton(
                onPressed: () => onSendMessage('mimi8', 2),
                child: Image.asset(
                  'images/mimi8.gif',
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
              ),
              TextButton(
                onPressed: () => onSendMessage('mimi9', 2),
                child: Image.asset(
                  'images/mimi9.gif',
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
              )
            ],
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          )
        ],
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      ),
      decoration: BoxDecoration(border: Border(top: BorderSide(color: greyColor2, width: 0.5)), color: Colors.white),
      padding: EdgeInsets.all(5.0),
      height: 180.0,
    );
  }

  Widget buildLoading() {
    return Positioned(
      child: isLoading ? const Loading() : Container(),
    );
  }

  Widget buildInput() {
    return Container(
      child: Row(
        children: <Widget>[
          // Button send image
          Container(
            child: IconButton(
              icon: Icon(Icons.image),
              onPressed: getImage,
              color: Theme.of(context).accentColor,
            ),
          ),
          /* Material(
            child: Container(
              child: IconButton(
                icon: Icon(Icons.face),
                onPressed: getSticker,
                color: Theme.of(context).accentColor,
              ),
            ),
          ),*/
          // Edit text
          Flexible(
            child: Container(
              child: TextField(
                minLines: 1,
                maxLines: 3,
                textAlign: TextAlign.start,
                keyboardType: TextInputType.multiline,
                onChanged: (text) {
                  debugPrint(text);
                  if (mainTimer != null) {
                    mainTimer.cancel();
                  }
                  starTypingTimer();
                  userTypingStatus(status: true);
                },
                style: TextStyle(color: Theme.of(context).accentColor, fontSize: 15.0),
                controller: textEditingController,
                decoration: InputDecoration.collapsed(
                  hintText: 'Type your message...',
                  hintStyle: TextStyle(color: greyColor),
                ),
                focusNode: focusNode,
              ),
            ),
          ),

          // Button send message
          Container(
            margin: EdgeInsets.symmetric(horizontal: 8.0),
            child: IconButton(
              icon: Icon(
                Icons.send,
                color: Theme.of(context).accentColor,
              ),
              onPressed: () => onSendMessage(textEditingController.text, 0),
              color: primaryColor,
            ),
          ),
        ],
      ),
      width: double.infinity,
      /* decoration: BoxDecoration(border: Border(top: BorderSide(color: greyColor2, width: 0.5)), color: Colors.white),*/
      margin: const EdgeInsets.only(bottom: 12.0, right: 6.0, left: 6.0),
      decoration: BoxDecoration(
        border: Border.all(width: 1.0, color: Colors.grey.withOpacity(0.6)),
        color: Theme.of(context).primaryColor,
        borderRadius: BorderRadius.circular(20.0),
        boxShadow: [
          //BoxShadow(color: Theme.of(context).hintColor.withOpacity(0.10), offset: Offset(0,-4), blurRadius: 10)
        ],
      ),
    );
  }

  Timer mainTimer;

  Future<Timer> starTypingTimer() async {
    mainTimer = Timer(Duration(seconds: 2), typingTimerComplete);
    return mainTimer;
  }

  Future<void> typingTimerComplete() async {
    // widget.typingBloc.typingEventSink.add(false);
    userTypingStatus(status: false);
  }

  Widget buildListMessage() {
    return Flexible(
      child: groupChatId == ''
          ? Center(child: LoadingWidget(initialData: true,))
          : StreamBuilder(
        stream: FirebaseFirestore.instance.collection('messages').doc(groupChatId).collection(groupChatId).orderBy('timestamp', descending: true).snapshots(),
        builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData) {
            return Center(child: LoadingWidget(initialData: true,));
          } else {
            listMessage = snapshot.data.docs;
            return ListView.builder(
              padding: EdgeInsets.all(10.0),
              itemBuilder: (context, index) => buildItem(index, snapshot.data.docs[index]),
              itemCount: snapshot.data.docs.length,
              reverse: true,
              controller: listScrollController,
            );
          }
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return LifeCycleMgr(
      child: WillPopScope(
        onWillPop: () {
          if(mapLaunchGoBackData.length>0){
            locator<NavigationService>().navigatorKey.currentState.pushNamedAndRemoveUntil(HomeScreen.routeName,(route)=>false);
            return Future.value(false);
          }
          return Future.value(true);
        },
        child: Scaffold(
          appBar: AppBar(
            leading: GestureDetector(
                onTap: () {
                  if(mapLaunchGoBackData.length>0){
                    locator<NavigationService>().navigatorKey.currentState.pushNamedAndRemoveUntil(HomeScreen.routeName,(route)=>false);
                    return;
                  }
                  Navigator.of(context).pop();
                },
                child: Icon(
                  Icons.arrow_back,
                  color: Theme.of(context).primaryColor,
                )),
            elevation: 0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(bottomLeft: Radius.circular(16.0), bottomRight: Radius.circular(16.0)),
            ),
            backgroundColor: Theme.of(context).accentColor,
            title: GestureDetector(
              onTap: () async{
                locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
                Map<String, dynamic> mapResult = await ApiRequest.getPatientList(doctorId: appUserModel.id);
                locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
                debugPrint("resut : ${mapResult}");
                debugPrint("patient id : ${widget.peerId}");
                debugPrint("doctor id : ${appUserModel.id}");
                int idPatient = widget.doctor.id_user;
                List<dynamic> lstData = mapResult['data'];
                List<dynamic> lstResult = lstData.where((element) => element['id_user'] == idPatient).toList();
                if(lstResult.length>0){
                    PatientModel patientModel = PatientModel.fromMap(lstResult[0]);
                    Navigator.of(context).pushNamed(PatientDetail.routeName,arguments: [patientModel]);

                }


              },
              child: Text(
                '${widget.doctor.patient_name}',
                style: TextStyle(
                  fontSize: 22.0,
                  color: Theme.of(context).primaryColor,
                ),
              ),
            ),
          ),
          body: Stack(
            children: <Widget>[
              Column(
                children: <Widget>[
                  // List of messages
                  buildListMessage(),
                  StreamBuilder(
                    stream: typingBloc.typingStream,
                    builder: (context, snapshot) {
                      debugPrint("yes");
                      if (snapshot.hasData && snapshot.data == true) {
                        return Container(
                          padding: EdgeInsets.only(left: 20,bottom: 10),
                          child: Row(
                            children: [
                              Text('Typing...',
                                style: TextStyle(
                                  fontSize: 10.0,
                                  color: Theme.of(context).accentColor,
                                ),
                              ),
                            ],
                          ),
                        );
                      } else {
                        return Container();
                      }
                    },
                  ),
                  // Sticker
                  (isShowSticker ? buildSticker() : Container()),

                  // Input content

                  widget.appointmentType == AppointmentType.PAST_APPOINTMENT
                      ? isLoadingSingleDoctor
                      ? Container(
                    margin: EdgeInsets.all(12),
                    child: Container(margin: EdgeInsets.only(left: 55.0, right: 55.0, top: 12, bottom: 12), child: LoadingWidget(initialData: true,)),
                  )
                      : Container()
                      : buildInput(),
                ],
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                child: StreamBuilder(
                  stream: blocOnlineOffline.onlineOfflineStream,
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return Container(
                          margin: EdgeInsets.only(top: 10, bottom: 10),
                          color: Colors.transparent,
                          alignment: Alignment.center,
                          height: 30,
                          child: snapshot.data == EnumOnlineStatus.ONLINE
                              ? Container(
                              decoration: BoxDecoration(color: Colors.green, borderRadius: BorderRadius.all(Radius.circular(100))),
                              alignment: Alignment.center,
                              width: 100,
                              height: double.infinity,
                              child: Text(
                                'Online',
                                style: TextStyle(color: Colors.white),
                              ))
                              : Container(
                              decoration: BoxDecoration(color: Colors.grey, borderRadius: BorderRadius.all(Radius.circular(100))),
                              alignment: Alignment.center,
                              width: 100,
                              height: double.infinity,
                              child: Text(
                                'Offline',
                                style: TextStyle(color: Colors.white),
                              )));
                    } else {
                      return Container();
                    }
                  },
                ),
              ),
              // Loading
              buildLoading(),
              LoadingWidget()
            ],
          ),
        ),
      ),
    );
  }
}


class EnumOnlineStatus {
  static String ONLINE = 'online', OFFLINE = 'offline';
}
