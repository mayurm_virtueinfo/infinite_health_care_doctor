import 'dart:io';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:geolocator/geolocator.dart';
import 'package:infinite_health_care_doctor/api/api_request.dart';
import 'package:infinite_health_care_doctor/config/app_config.dart' as config;
import 'package:infinite_health_care_doctor/config/const.dart';
import 'package:infinite_health_care_doctor/models/blog_model.dart';
import 'package:infinite_health_care_doctor/models/patient_review.dart';
import 'package:infinite_health_care_doctor/models/schedule_conversation.dart' as scheduledModel;
import 'package:infinite_health_care_doctor/models/user_model.dart';
import 'package:infinite_health_care_doctor/ui/home/blog_detail_screen.dart';
import 'package:infinite_health_care_doctor/ui/home/doctor_profile.dart';
import 'package:infinite_health_care_doctor/ui/home/patient_list.dart';
import 'package:infinite_health_care_doctor/ui/home/review_list_screen.dart';
import 'package:infinite_health_care_doctor/ui/home/schedule_screen.dart';
import 'package:infinite_health_care_doctor/ui/home/search_patients_list.dart';
import 'package:infinite_health_care_doctor/ui/home/upcoming_appointment_list.dart';
import 'package:infinite_health_care_doctor/main.dart';
import 'package:infinite_health_care_doctor/utils/utility.dart';
import 'package:infinite_health_care_doctor/widgets/searchWidget.dart';
import 'package:intl/intl.dart';

class HomeTab extends StatefulWidget {
  static final String routeName = "HomeTab";

  const HomeTab({Key key}) : super(key: key);

//  final bool androidFusedLocation;

  @override
  _HomeTabState createState() => _HomeTabState();
}

class _HomeTabState extends State<HomeTab> {
  String latitude = '';
  String longitude = '';
  Position _lastKnownPosition;
  Position _currentPosition;
  bool androidFusedLocation = true;

  @override
  void initState() {
    super.initState();
    _initLastKnownLocation();
    _initCurrentLocation();
    // getUpcomingAppointment(context);
  }

  @override
  void didUpdateWidget(Widget oldWidget) {
    super.didUpdateWidget(oldWidget);

    setState(() {
      _lastKnownPosition = null;
      _currentPosition = null;
    });

    _initLastKnownLocation().then((_) => _initCurrentLocation());
  }



  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> _initLastKnownLocation() async {
    Position position;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      position = await Geolocator.getLastKnownPosition(forceAndroidLocationManager:!androidFusedLocation);
    } on PlatformException {
      position = null;
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return;
    }

    setState(() {
      _lastKnownPosition = position;
    });
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  _initCurrentLocation() async {
    Position position = await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    if (!mounted) {
      return;
    }
    setState(() {
      _currentPosition = position;
    });
  }

  @override
  void dispose() {
    // Clean up the focus node when the Form is disposed.

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                  height: 120,
                  padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(bottomLeft: Radius.circular(25.0), bottomRight: Radius.circular(25.0)),
                    color: Theme.of(context).accentColor,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            'IHC',
                            style: TextStyle(
                              fontSize: 22.0,
                              fontWeight: FontWeight.bold,
                              color: Theme.of(context).primaryColor.withOpacity(0.8),
                            ),
                          ),
                          Text(
                            "${appUserModel.fullname}",
                            style: TextStyle(
                              fontSize: 16.0,
                              color: Theme.of(context).primaryColor,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                // Top
                Container(
                  margin: const EdgeInsets.only(top: 80.0),
//                  padding: const EdgeInsets.only(left: 30.0, right: 30.0),
                  width: MediaQuery.of(context).size.width,
                  height: 120,
                  child: Row(
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width / 3,
                        child: Column(
                          children: <Widget>[
                            TextButton(
                              style: TextButton.styleFrom(
                                padding: EdgeInsets.all(0),
                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(150)),
                              ),
                              onPressed: () {
                                Navigator.of(context).pushNamed(PatientList.routeName, arguments: [appUserModel.id, 'My Patient']);
                              },
                              child: ballFromAssets('images/icon_patient.png', Theme.of(context).scaffoldBackgroundColor),
                            ),
                            Text(
                              'Patient',
                              style: TextStyle(
                                fontSize: 14.0,
                                fontWeight: FontWeight.bold,
                                color: Theme.of(context).focusColor,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width / 3,
                        child: Column(
                          children: <Widget>[
                            TextButton(
                              style: TextButton.styleFrom(
                                padding: EdgeInsets.all(0),
                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(150)),
                              ),

                              onPressed: () {
                                Navigator.of(context).pushNamed(ScheduleScreen.routeName,arguments: [appUserModel.id]);
                              },
                              child: ballFromAssets('images/icon_schedule.png', Theme.of(context).scaffoldBackgroundColor),
                            ),
                            Text(
                              'Schedule',
                              style: TextStyle(
                                fontSize: 14.0,
                                fontWeight: FontWeight.bold,
                                color: Theme.of(context).focusColor,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width / 3,
                        child: Column(
                          children: <Widget>[
                            TextButton(
                              style: TextButton.styleFrom(
                                padding: EdgeInsets.all(0),
                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(150)),
                              ),

                              onPressed: () {
                                Navigator.of(context).pushNamed(ReviewListScreen.routeName,arguments: [appUserModel.id]);
                              },
                              child: ballFromAssets('images/icon_feedback.png', Theme.of(context).scaffoldBackgroundColor),
                            ),
                            Text(
                              'Feedback',
                              style: TextStyle(
                                fontSize: 14.0,
                                fontWeight: FontWeight.bold,
                                color: Theme.of(context).focusColor,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            // Search Bar
            Padding(
              padding: const EdgeInsets.all(12),
              child: SearchBarWidget(
                enabled: false,
                onTap: () {
                  debugPrint("yes");
                  sleep(const Duration(seconds: 2));

                 /* // Use FirebaseCrashlytics to throw an error. Use this for
                  // confirmation that errors are being correctly reported.
                  FirebaseCrashlytics.instance.crash();
                  return;*/
                  // List<String> list = List.generate(10, (index) => "Text $index");
                  // List<dynamic> list = [];
                  // showSearch(context: context, delegate: Search(list));
                  Navigator.of(context).push(
                    PageRouteBuilder(
                      transitionDuration: Duration(milliseconds: 500),
                      pageBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation) {
                        return SearchPatientsList();
                      },
                      transitionsBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
                        return Align(
                          child: FadeTransition(
                            opacity: animation,
                            child: child,
                          ),
                        );
                      },
                    ),
                  );
                },
              ),
            ),

            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  TextButton(
                    onPressed: () {},
                    child: Text(
                      'Upcoming Appointments',
                      style: TextStyle(fontSize: 16.0,fontWeight: FontWeight.bold, color: Theme.of(context).focusColor),
                    ),
                  ),
                  TextButton(
                    onPressed: () {
                      Navigator.of(context).pushNamed(UpcomingAppointmentList.routeName);
                    },
                    child: Text(
                      'See All',
                      style: TextStyle(
                        fontSize: 12.0,
                        fontWeight: FontWeight.bold,
                        color: Theme.of(context).accentColor,
                      ),
                    ),
                  )
                ],
              ),
            ),
            //mytest
            FutureBuilder(
              future: ApiRequest.postUpcomingAppointmentDoctor(date_time: Utility.getFormatedCurrentTime(), doctor_id: appUserModel.id),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  scheduledModel.ScheduledConversationListModel sAList = scheduledModel.ScheduledConversationListModel.fromSnapshot(snapshot);
                  // sAList.scheduledConversation.addAll(tAList.scheduledConversation);
                  debugPrint("m length : ${sAList.scheduledConversation.length}");
                  ScrollPhysics mScrollPhysics = sAList.scheduledConversation.length>1?ScrollPhysics():NeverScrollableScrollPhysics();
                  bool mAutoPlay = sAList.scheduledConversation.length>1?true:false;
                  double viewPortFraction = sAList.scheduledConversation.length>1?0.8:1;
                  double mMergin = sAList.scheduledConversation.length==1?20:5;


                  double fHeight = 110;
                  sAList.scheduledConversation.forEach((element) {
                    List<dynamic> mAppointments = element.appointment;
                    Set<String> dtSet = Set();
                    mAppointments.forEach((element) {
                      dtSet.add(element['appointment_date']);
                    });
                    Map<String,dynamic> mapApptStruct = Map();
                    dtSet.forEach((elementSet) {
                      List<dynamic> srchdDt = mAppointments.where((element) => element['appointment_date'] == elementSet).toList();
                      if(srchdDt.length>0){
                        List<String> lstTime = [];
                        srchdDt.forEach((elementSrchDt) {
                          String time = elementSrchDt['time'];
                          DateFormat parseFormat= DateFormat("dd-MMM-yyyy HH:mm:ss");
                          DateTime  dtTime = parseFormat.parse('$elementSet $time');

                          DateFormat showFormat= DateFormat("hh:mm a");
                          String strTime = showFormat.format(dtTime);

                          lstTime.add(strTime);
                        });
                        mapApptStruct[elementSet]=lstTime;
                      }
                    });
                    debugPrint("MapAppStruct : ${mapApptStruct.length}");
                    debugPrint("MapAppStruct : ${mapApptStruct}");
                    double height = 110;
                    if(mapApptStruct.length>1){
                      int orgLn = mapApptStruct.length-1;
                      int orgHeight = 35 * orgLn;
                      height+=orgHeight;
                    }
                    mapApptStruct.forEach((key, value) {
                      int valueLength = value.length;
                      if(valueLength>=4 && valueLength<9){
                        height+=30;
                      }else if(valueLength>=9 && valueLength<13){
                        height+=60;
                      } else if(valueLength>=13 && valueLength<17){
                        height+=90;
                      }
                      debugPrint("key : $key");
                      debugPrint("valueLength : $valueLength");
                    });
                    if(height>fHeight){
                      fHeight = height;
                    }
                  });
                  sAList.scheduledConversation.forEach((element) {
                    List<dynamic> mAppointments = element.appointment;
                    double mCount = mAppointments.length/4;
                    debugPrint("length : ${mAppointments.length}");
                    debugPrint("length : ${mAppointments.length}");
                    debugPrint("mCount : $mCount");
                    /*if(mAppointments.length>=4){
                      fHeight = (20+10);
                    }*/
                  });



                  return Column(
                    children: [
                      sAList.scheduledConversation.length>0?CarouselSlider(
                        options: CarouselOptions(
                          height: fHeight,
                          autoPlayInterval: Duration(seconds: 3),
                          autoPlay: mAutoPlay,
                          scrollPhysics: mScrollPhysics,
                          viewportFraction: viewPortFraction,
                          autoPlayAnimationDuration: Duration(milliseconds: 800),
                        ),
                        items: sAList.scheduledConversation.asMap().entries.map((e) {
                          debugPrint("Appointments : ${e.value.appointment}");
                          List<dynamic> tAppointments = e.value.appointment;
                          List<dynamic> mAppointments = e.value.appointment;

                          /*
                           */
                          Set<String> dtSet = Set();
                          mAppointments.forEach((element) {
                            dtSet.add(element['appointment_date']);
                          });
                          Map<String,dynamic> mapApptStruct = Map();
                          dtSet.forEach((elementSet) {
                            List<dynamic> srchdDt = mAppointments.where((element) => element['appointment_date'] == elementSet).toList();
                            if(srchdDt.length>0){
                              List<String> lstTime = [];
                              srchdDt.forEach((elementSrchDt) {
                                String time = elementSrchDt['time'];
                                DateFormat parseFormat= DateFormat("dd-MMM-yyyy HH:mm:ss");
                                DateTime  dtTime = parseFormat.parse('$elementSet $time');

                                DateFormat showFormat= DateFormat("hh:mm a");
                                String strTime = showFormat.format(dtTime);

                                lstTime.add(strTime);
                              });
                              mapApptStruct[elementSet]=lstTime;
                            }
                          });


                          // mAppointments.addAll(tAppointments);
                          return Card(
                            margin: EdgeInsets.only(left: mMergin,right: mMergin,bottom: mMergin/2),
                            elevation: 1,
                            // color: MyColors.colorConvert(e.value.color),
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
                            child: Container(
                              // padding: EdgeInsets.all(12),
                              child: Row(
                                children: [
                                  Container(
                                    padding: EdgeInsets.all(15),
                                    alignment: Alignment.topCenter,
                                    child: Container(
                                      width: 60,
                                      height: 60,
                                      child: ClipOval(
                                        child: FancyShimmerImage(
                                          imageUrl: e.value.avatar_image ?? Const.defaultProfileUrl,
                                          shimmerBaseColor: Colors.white,
                                          shimmerHighlightColor: config.Colors().mainColor(1),
                                          shimmerBackColor: Colors.green,
                                          errorWidget: Image.network('https://i0.wp.com/www.dobitaobyte.com.br/wp-content/uploads/2016/02/no_image.png?ssl=1'),
                                          boxFit: BoxFit.cover,
                                        ),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                      flex: 1,
                                      child: Container(
                                        padding: EdgeInsets.only(top: 20, bottom: 10),
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              '${e.value.patient_name}',
                                              style: TextStyle(
                                                fontSize: 14.0,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.black,
                                              ),
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),

                                            Flexible(
                                              child: Wrap(
                                                direction: Axis.horizontal,
                                                children: mapApptStruct.entries.map((e) {
                                                  return Container(
                                                    margin: EdgeInsets.only(top: 5),
                                                    padding: EdgeInsets.only(left: 5,right: 5,top: 2,bottom: 2),
                                                    child: Wrap(
                                                      crossAxisAlignment: WrapCrossAlignment.center,
                                                      direction: Axis.horizontal,
                                                      children: [
                                                        Container(
                                                          padding: EdgeInsets.all(5),
                                                          decoration: BoxDecoration(
                                                              borderRadius: BorderRadius.only(topRight: Radius.circular(100),bottomRight: Radius.circular(100)),
                                                              // color: Theme.of(context).accentColor,
                                                              gradient: LinearGradient(
                                                                colors: [Colors.red[200],Colors.blue[200]],
                                                                begin: const FractionalOffset(0.0, 0.0),
                                                                end: const FractionalOffset(0.5, 0.0),
                                                                stops: [0.0,1.0],
                                                                tileMode: TileMode.clamp,
                                                              )
                                                          ),
                                                          child: Text(
                                                            '${e.key}',
                                                            style: TextStyle(
                                                                fontSize: 12.0,
                                                                fontWeight: FontWeight.bold,
                                                                color: Colors.white

                                                            ),
                                                          ),
                                                        ),
                                                        Wrap(
                                                          direction: Axis.horizontal,
                                                          children: (e.value as List<dynamic>).asMap().entries.map((eV) {
                                                            return Container(
                                                                padding: EdgeInsets.only(left: 5, right: 5),
                                                                margin: EdgeInsets.only(left: 2, right: 2,top: 2,bottom: 2),
                                                                decoration: BoxDecoration(borderRadius: BorderRadius.circular(100), color: Colors.green),
                                                                child: Text(
                                                                  eV.value,
                                                                  style: TextStyle(color: Colors.white),
                                                                ));
                                                          }).toList(),
                                                        )
                                                      ],
                                                    ),
                                                  );
                                                }).toList(),/*mAppointments.asMap().entries.map((e) {
                                                  return Container(
                                                    padding: EdgeInsets.all(5),
                                                    margin: EdgeInsets.all(2),
                                                    decoration: BoxDecoration(
                                                      borderRadius: BorderRadius.all(Radius.circular(10)),
                                                      color: Colors.grey,

                                                    ),
                                                    child: Text(Utility.getFormattedDateSingleHome(e.value['appointment_date'],e.value['time']),
                                                      style: TextStyle(
                                                          color: Colors.white,
                                                          fontSize: 10.0,
                                                      ),
                                                    ),
                                                  );
                                                }).toList(),*/
                                              ),
                                            ),
                                          ],
                                        ),
                                      ))
                                ],
                              ),
                            ),
                          );
                        }).toList(),

                      ):Container(
                        margin: EdgeInsets.only(top: 30),
                        alignment: Alignment.center,
                        child: Text(
                          "No upcoming appointments found",
                          style: TextStyle(fontSize: 16),
                        ),
                      ),
                    ],
                  );
                } else {
                  return SizedBox(
                    width: 40,
                    height: 40,
                    child: CircularProgressIndicator(
                      strokeWidth: 1.5,
                      valueColor: AlwaysStoppedAnimation<Color>(config.Colors().mainColor(1),),
                    ),
                  );
                }
              },
            ),

            SizedBox(
              height: 30,
            ),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  TextButton(
                    onPressed: () {},
                    child: Text(
                      'Health Tips',
                      style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold, color: Theme.of(context).focusColor),
                    ),
                  ),
                ],
              ),
            ),


            FutureBuilder(
              future: ApiRequest.getBlogList(doctor_id: appUserModel.id),
              builder: (context, snapshot) {
                if(snapshot.hasData){

                  List<dynamic> pReviewList = snapshot.data['data'];
                  List<BlogModel> blogList = [];
                  pReviewList.forEach((element) {
                    BlogModel pReview = BlogModel.fromMap(element);
                    blogList.add(pReview);
                  });
                      debugPrint("blog length : ${blogList.length}");
                  return blogList.length>0?Container(
                    child: CarouselSlider(
                      options: CarouselOptions(
                        height: 400,
                      ),
                      items: blogList.asMap().entries.map((e) {
                        return Card(
                          elevation: 1,
                          color: Colors.grey,
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
                          child: Container(
                            padding: EdgeInsets.all(12),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      "${e.value.title}",
                                      style: TextStyle(
                                        fontSize: 12.0,
                                        color: Theme.of(context).primaryColor.withOpacity(0.8),
                                      ),
                                    ),
                                    Text(
                                      "${e.value.date}",
                                      style: TextStyle(
                                        fontSize: 12.0,
                                        color: Theme.of(context).primaryColor.withOpacity(0.8),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 80),
                                Container(
                                  child: Text(
                                    "${e.value.description}",
                                    style: TextStyle(fontSize: 28.0, color: Theme.of(context).primaryColor, fontWeight: FontWeight.bold),
                                  ),
                                ),
                                SizedBox(height: 120),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      "",
                                      style: TextStyle(
                                        fontSize: 18.0,
                                        color: Theme.of(context).primaryColor.withOpacity(0.8),
                                      ),
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        Navigator.of(context).pushNamed(BlogDetailScreen.routeName, arguments: [e.value]);
                                      },
                                      child: Text(
                                        "Read more",
                                        style: TextStyle(
                                          fontSize: 18.0,
                                          color: Theme.of(context).primaryColor.withOpacity(0.8),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        );
                      }).toList(),

                    ),
                  ):Container(
                    margin: EdgeInsets.only(top: 30),
                    alignment: Alignment.center,
                    child: Text(
                      "No health tips found",
                      style: TextStyle(fontSize: 16),
                    ),
                  );
                }else if(snapshot.hasError){
                  return Container(
                    child: Center(
                      child: Text(snapshot.error.toString()),
                    ),
                  );
                }else {
                  return SizedBox(
                    width: 40,
                    height: 40,
                    child: CircularProgressIndicator(
                      strokeWidth: 1.5,
                      valueColor: AlwaysStoppedAnimation<Color>(config.Colors().mainColor(1),),
                    ),
                  );
                }

              },

            ),


            SizedBox(
              height: 30,
            ),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  TextButton(
                    onPressed: () {},
                    child: Text(
                      'Latest Reviews',
                      style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold, color: Theme.of(context).focusColor),
                    ),
                  ),
                  TextButton(
                    onPressed: () {
                      Navigator.of(context).pushNamed(ReviewListScreen.routeName,arguments: [appUserModel.id]);
                    },
                    child: Text(
                      'See All',
                      style: TextStyle(
                        fontSize: 12.0,
                        fontWeight: FontWeight.bold,
                        color: Theme.of(context).accentColor,
                      ),
                    ),
                  )
                ],
              ),
            ),
            FutureBuilder(
              future: ApiRequest.getLatestReview(doctor_id: appUserModel.id),
              builder: (context, snapshot) {
                if(snapshot.hasData){
                  List<dynamic> pReviewList = snapshot.data['data'];
                  List<PatientReview> patientReviewList = [];
                  pReviewList.forEach((element) {
                    PatientReview pReview = PatientReview.fromMap(element);
                      patientReviewList.add(pReview);
                    if(patientReviewList.length<1){

                    }
                  });
                  ScrollPhysics mScrollPhysics = patientReviewList.length>1?ScrollPhysics():NeverScrollableScrollPhysics();
                  bool mAutoPlay = patientReviewList.length>1?true:false;
                  double viewPortFraction = patientReviewList.length>1?0.8:1;
                  double mMergin = 5;//patientReviewList.length==1?10:10;
                  return patientReviewList.length>0?Container(
                    child: CarouselSlider(
                      options: CarouselOptions(
                        height: 110,
                        autoPlayInterval: Duration(seconds: 3),
                        autoPlay: mAutoPlay,
                        scrollPhysics: mScrollPhysics,
                        viewportFraction: viewPortFraction,
                        autoPlayAnimationDuration: Duration(milliseconds: 800),
                      ),
                      items: patientReviewList.asMap().entries.map((e) {
                        return Card(
                          margin: EdgeInsets.only(left: mMergin,right: mMergin,bottom: mMergin/2),
                          elevation: 1,
                          // color: MyColors.colorConvert(e.value.color),
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
                          child: Container(
                            // padding: EdgeInsets.all(12),
                            child: Row(
                              children: [
                                Expanded(
                                  flex: 2,
                                  child: Container(
                                    alignment: Alignment.center,
                                    child: Container(
                                      width: 50,
                                      height: 50,
                                      child: ClipOval(
                                        child: FancyShimmerImage(
                                          imageUrl:e.value.patient_image ?? Const.defaultProfileUrl,
                                          shimmerBaseColor: Colors.white,
                                          shimmerHighlightColor: config.Colors().mainColor(1),
                                          shimmerBackColor: Colors.green,
                                          errorWidget: Image.network('https://i0.wp.com/www.dobitaobyte.com.br/wp-content/uploads/2016/02/no_image.png?ssl=1'),
                                          boxFit: BoxFit.cover,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                Expanded(
                                    flex: 4,
                                    child: Container(
                                      padding: EdgeInsets.only(top: 20, bottom: 10),
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            '${e.value.first_name} ${e.value.last_name}',
                                            style: TextStyle(
                                              fontSize: 12.0,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.black,
                                            ),
                                          ),
                                          Container(
                                            child: RatingBarIndicator(
                                              itemPadding: EdgeInsets.only(right: 5),
                                              rating: double.parse(e.value.rating),
                                              itemBuilder: (context, index) => Container(
                                                height: 20,
                                                width: 20,
                                                child: Icon(
                                                  Icons.star,
                                                  color: Colors.amber,
                                                ),
                                              ),
                                              itemCount: 5,
                                              itemSize: 20.0,
                                              unratedColor: Colors.amber.withAlpha(50),
                                              direction: Axis.horizontal,
                                            ),
                                          ),
                                          SizedBox(
                                            height: 10,
                                          ),
                                          Flexible(
                                            child: Wrap(
                                              direction: Axis.horizontal ,
                                              children: [
                                                Text(

                                                  e.value.description,
                                                  overflow: TextOverflow.ellipsis,
                                                  style: TextStyle(fontSize: 10.0,  color: Colors.grey, fontStyle: FontStyle.italic),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ))
                              ],
                            ),
                          ),
                        );
                      }).toList(),

                    ),
                  ):Container(
                    margin: EdgeInsets.only(top: 30),
                    alignment: Alignment.center,
                    child: Text(
                      "No reviews found",
                      style: TextStyle(fontSize: 16),
                    ),
                  );
                }else if(snapshot.hasError){
                  return Container(
                    child: Center(
                      child: Text(snapshot.error.toString()),
                    ),
                  );
                }else {
                  return SizedBox(
                    width: 40,
                    height: 40,
                    child: CircularProgressIndicator(
                      strokeWidth: 1.5,
                      valueColor: AlwaysStoppedAnimation<Color>(config.Colors().mainColor(1),),
                    ),
                  );
                }

              },

            ),
            SizedBox(
              height: 20,
            )
          ],
        ),
      ),
    );
  }

  String _fusedLocationNote() {
    if (androidFusedLocation) {
      return 'Geolocator is using the Android FusedLocationProvider. This requires Google Play Services to be installed on the target device.';
    }

    return 'Geolocator is using the raw location manager classes shipped with the operating system.';
  }

  List<Widget> getCarouselSliderList(dList) {
    List<Widget> wList = [];
    dList.forEach((Banner banner) {
      wList.add(Container(
        height: 200.0,
        margin: const EdgeInsets.only(left: 12.0),
        decoration: BoxDecoration(
          border: Border.all(width: 1.0, color: Colors.grey.withOpacity(0.2)),
          borderRadius: BorderRadius.circular(16.0),
          image: DecorationImage(
            image: AssetImage('images/doctor-productivity.jpg'),
            fit: BoxFit.cover,
          ),
        ),
      ));
    });
    return wList; //Future.value(wList);
  }

  Widget ballFromAssets(String image, Color color) {
    return Container(
      height: 90,
      width: 90,
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(100.0),
      ),
      child: Container(

        child: Center(
          child: Image.asset(
            image,
            fit: BoxFit.contain,
          ),
        ),
      ),
    );
  }

  Widget ball(String image, Color color) {
    return Container(
      height: 80,
      width: 80.0,
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(100.0),
      ),
      child: Center(
        child: image == ""
            ? Image(
                image: AssetImage("images/nurse.png"), //----1
                fit: BoxFit.cover,
              )
            : ClipOval(
                child: FancyShimmerImage(
                  imageUrl: image,
                  shimmerBaseColor: Colors.white,
                  shimmerHighlightColor: config.Colors().mainColor(1),
                  shimmerBackColor: Colors.green,
                  errorWidget: Image.network('https://i0.wp.com/www.dobitaobyte.com.br/wp-content/uploads/2016/02/no_image.png?ssl=1'),
                ),
              ),
      ),
    );
  }

  Widget ballcard(String image, Color color) {
    print("----image -111: $image");
    return Container(
      height: 60,
      width: 60.0,
      child: ClipOval(
        child: FancyShimmerImage(
          imageUrl: image ?? Const.defaultProfileUrl,
          shimmerBaseColor: Colors.white,
          shimmerHighlightColor: config.Colors().mainColor(1),
          shimmerBackColor: Colors.green,
          errorWidget: Image.network('https://i0.wp.com/www.dobitaobyte.com.br/wp-content/uploads/2016/02/no_image.png?ssl=1'),
        ),
      ),
    );
  }

  Widget card(int id, String image, String title, String subTitle, String rank) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).pushNamed(DoctorProfile.routeName, arguments: ["${id}", title]);
      },
      child: Stack(
        children: <Widget>[
          Container(
            margin: const EdgeInsets.only(top: 20.0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8.0),
              boxShadow: [BoxShadow(color: Theme.of(context).primaryColor.withOpacity(0.1), offset: Offset(0, 4), blurRadius: 10)],
            ),
            width: 140.0,
            height: 140.0,
            child: Card(
              elevation: 0.2,
              child: Wrap(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 0.0, vertical: 40.0),
                    child: ListTile(
                      title: Text(
                        title,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontSize: 10.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      subtitle: Column(
                        children: <Widget>[
                          Text(
                            subTitle ?? "",
                            textAlign: TextAlign.left,
                            style: TextStyle(fontSize: 10.0),
                          ),
                          Row(
                            children: <Widget>[
                              Icon(
                                Icons.star,
                                color: Colors.yellow,
                              ),
                              Text(
                                rank ?? "",
                                style: TextStyle(
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            width: 140,
            height: 60,
            alignment: Alignment.center,
            child: ballcard(image, Colors.transparent),
          ),
        ],
      ),
    );
  }
}
 