import 'dart:io';
import 'package:infinite_health_care_doctor/main.dart';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:infinite_health_care_doctor/models/user_model.dart';
import 'package:infinite_health_care_doctor/utils/app_style.dart';
import 'package:infinite_health_care_doctor/utils/my_colors.dart';
import 'package:infinite_health_care_doctor/utils/utility.dart';
import 'package:infinite_health_care_doctor/utils/width_sizes.dart';
import 'package:infinite_health_care_doctor/widgets/app_button.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SendFeedback extends StatefulWidget {
  static const String routeName = "/SendFeedback";


  @override
  _SendFeedbackState createState() => _SendFeedbackState();
}


class _SendFeedbackState extends State<SendFeedback> {

  GlobalKey<FormState> _formState= GlobalKey<FormState>();

  final _messageController = TextEditingController();
  final _mobileController  = TextEditingController();
  final _nameController  = TextEditingController();

  @override


  Timestamp sender_message_time;
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  SharedPreferences prefs;
  String userid = '';
  String name = '';
  String mobile = '';
  String photo = '';
  String message_id = UniqueKey().toString();
  bool isSendingMessage = false;
  List<String> adminIdList= [];


  void initState() {
    super.initState();


  }
  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    _nameController.text = appUserModel.fullname;
    _mobileController.text = appUserModel.mobile;
    super.didChangeDependencies();
  }
  @override
  Widget build(BuildContext context) {


    double devicePixelRatio = Utility.getDevicePixelRatio(context);
    final img_bapasitaram_width = WidthSizes.splash_screen_img_bapasitaram_width*devicePixelRatio;
    final img_savealife_width = WidthSizes.splash_screen_img_savealife_width*devicePixelRatio;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Theme
              .of(context)
              .primaryColor),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text(
          "Send Feedback",
          style: TextStyle(
            fontSize: 22.0,
            fontWeight: FontWeight.bold,
            color: Theme
                .of(context)
                .primaryColor,
          ),
        ),
        backgroundColor: Theme.of(context).accentColor,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[

            Stack(
              children: <Widget>[
                Container(
                  height: 150,
                  padding: const EdgeInsets.only(top: 40),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(25.0),
                        bottomRight: Radius.circular(25.0)),
                    color: Theme.of(context).accentColor,
                  ),
                ),
                Stack(
                  children: <Widget>[
                    Form(
                      key: _formState,
                      child: Container(
                        width: double.infinity,
                        margin: EdgeInsets.only(left: 14.0, right: 14.0,top: 50),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20.0),
                          color: Theme
                              .of(context)
                              .primaryColor,
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            /*Container(
                              width: img_bapasitaram_width- (devicePixelRatio*30),
                              child: Center(
                                child: Image.asset(
                                  "assets/icon_bapasitaram.png",
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),*/

                            SizedBox(height: 20,),
//                        name
                            Container(
                              child: TextFormField(
                                validator: (text){
                                  if(text.length == 0){
                                    return "Name is required";
                                  }

                                  return null;
                                },
                                controller: _nameController,
                                decoration: InputDecoration(
                                    errorStyle: AppStyles.errorStyle,
                                    focusedBorder: AppStyles.focusBorder,
                                    prefixIcon: Icon(
                                      Icons.account_circle,
                                      color: Theme.of(context).accentColor,

                                    ),
                                    labelStyle: TextStyle(color: Theme.of(context).accentColor),
                                    border: OutlineInputBorder(),
                                    labelText: "Enter Name"
                                ),
                              ),
                              padding: EdgeInsets.all(20),
                            ),

//                        mobile_number
                            Container(
                                margin: EdgeInsets.only(
                                    left: 5, right: 5),
                                child: Divider(
                                  height: 1,
                                  color: Theme.of(context).accentColor,
                                )),

                            Container(
                              child: TextFormField(
                                validator: (text){
                                  if(text.length == 0){
                                    return "Mobile Number is required";
                                  }else if(text.length < 10){
                                    return "Invalid Mobile Number";
                                  }
                                  return null;
                                },
                                controller: _mobileController,
                                keyboardType: TextInputType.number,
                                maxLength: 10,
                                decoration: InputDecoration(
                                  errorStyle: AppStyles.errorStyle,
                                  focusedBorder: AppStyles.focusBorder,
                                  prefixIcon: Icon(
                                    Icons.call,
                                    color: Theme.of(context).accentColor,

                                  ),
                                  labelStyle: TextStyle(color: Theme.of(context).accentColor),
                                  border: OutlineInputBorder(),
                                  labelText: "Enter Mobile Number",

                                ),
                              ),
                              padding: EdgeInsets.all(20),
                            ),
                            Container(
                                margin: EdgeInsets.only(
                                    left: 5, right: 5),
                                child: Divider(
                                  height: 1,
                                  color: Theme.of(context).accentColor,
                                )),
//                              address
                            Container(
                              child: TextFormField(
                                controller: _messageController,
                                validator: (text){
                                  if(text.length == 0){
                                    return "Message is required";
                                  }
                                  return null;
                                },
                                maxLines: 5,
                                minLines: 5,
                                keyboardType: TextInputType.multiline,
                                decoration: InputDecoration(
                                  errorStyle: AppStyles.errorStyle,
                                  focusedBorder: AppStyles.focusBorder,
                                  alignLabelWithHint: true,
                                  /*prefixIcon: Icon(
                                    Icons.location_on,
                                    color: Theme.of(context).accentColor,

                                  ),*/
                                  labelStyle: TextStyle(color: Theme.of(context).accentColor),
                                  border: OutlineInputBorder(),
                                  labelText: "Enter Message",

                                ),
                              ),
                              padding: EdgeInsets.all(20),
                            ),
                            /*
                               ,photo, userid,
                              */
                            InkWell(
                                onTap: () async {

                                  if(_formState.currentState.validate()){
                                    _saveProfile();
                                  }
                                  setState(() {});
                                },
                                child: isSendingMessage
                                    ? Padding(
                                    padding: EdgeInsets.all(30.0),
                                    child: Container(
                                        height: 55.0,
                                        width: 600.0,
                                        child: Center(
                                          child: CircularProgressIndicator(
                                            valueColor:
                                            AlwaysStoppedAnimation<Color>(
                                                Theme.of(context).accentColor),
                                          ),
                                        )))
                                    : AppButton(text: "Send Message",)),
                            SizedBox(height: 10,),

//                          photo
                            SizedBox(height: 30,),
                          ],
                        ),
                      ),
                    ),
//                        Center(child:ball(currentDoctor.avatar, Theme.of(context).primaryColor,)),
                  ],
                ),
              ],
            ),

          ],
        ),
      ),
    );
  }

  void _saveProfile() async {/*

    String name = _nameController.text;
    String mobile = _mobileController.text;
    String message = _messageController.text;
    sender_message_time = Timestamp.fromDate(DateTime.now().toUtc());


    try {
      setState(() {
        isSendingMessage = true;
      });

      Map<String,dynamic> contact_us_data = Map();
      contact_us_data[ConstContactUs.message_id] = message_id;
      contact_us_data[ConstContactUs.sender_id] = userid;
      contact_us_data[ConstContactUs.sender_name] = name;
      contact_us_data[ConstContactUs.sender_mobile] = mobile;
      contact_us_data[ConstContactUs.sender_message] = message;
      contact_us_data[ConstContactUs.sender_photo] = photo;
      contact_us_data[ConstContactUs.sender_message_time] = sender_message_time;

      adminIdList.forEach((element) {
        contact_us_data["${ConstContactUs.admin}_${element}"] = element;
      });
      debugPrint("Contact us data : ${contact_us_data.toString()}");

      await Firestore.instance
          .collection(ConstCollection.contact_us)
          .document(message_id)
          .setData(contact_us_data);

      setState(() {
        isSendingMessage = false;
        MyToast.showToast("Thanks for contacting us. We will reach you soon.");
        Navigator.pop(context);
      });
    } catch (e) {
      debugPrint("Error saving prifile${e.message}");
      MyToast.showToast("Error updating profile ${e.toString()}");
      setState(() {
        isSendingMessage = false;
      });
    }
  */}



  @override
  void dispose() {
    super.dispose();
  }
}





