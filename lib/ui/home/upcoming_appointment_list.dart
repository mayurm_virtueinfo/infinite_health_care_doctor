import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:infinite_health_care_doctor/bloc/loading/loading_bloc_event.dart';
import 'package:infinite_health_care_doctor/bloc/loading/loading_bloc.dart';
import 'package:infinite_health_care_doctor/widgets/loading_widget.dart';
import 'package:infinite_health_care_doctor/main.dart';
import 'package:flutter/material.dart';
import 'package:infinite_health_care_doctor/api/api_request.dart';
import 'package:infinite_health_care_doctor/config/app_config.dart' as config;
import 'package:infinite_health_care_doctor/config/const.dart';
import 'package:infinite_health_care_doctor/models/schedule_conversation.dart' as scheduledModel;
import 'package:infinite_health_care_doctor/models/past_conversation.dart' as pastModel;
import 'package:infinite_health_care_doctor/models/schedule_conversation.dart';
import 'package:infinite_health_care_doctor/main.dart';
import 'package:infinite_health_care_doctor/utils/my_toast.dart';
import 'package:infinite_health_care_doctor/utils/utility.dart';
import 'package:infinite_health_care_doctor/widgets/searchWidget.dart';
import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:infinite_health_care_doctor/main.dart';
import 'package:infinite_health_care_doctor/models/patient_model.dart' as patientModel;
import 'package:infinite_health_care_doctor/ui/home/patient_detail.dart';
import 'package:infinite_health_care_doctor/models/appointment_type.dart';
import 'package:infinite_health_care_doctor/ui/conversation/personal_chat.dart';
class UpcomingAppointmentList extends StatefulWidget {
  static const String routeName = '/UpcomingAppointmentList';
  UpcomingAppointmentList();
  @override
  _UpcomingAppointmentListState createState() => _UpcomingAppointmentListState();
}

class _UpcomingAppointmentListState extends State<UpcomingAppointmentList> {
  @override
  void initState() {
    super.initState();
  }
  @override
  void dispose() {
    // Clean up the focus node when the Form is disposed.
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          elevation: 0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(bottomLeft:Radius.circular(16.0),bottomRight: Radius.circular(16.0)),
          ),
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color:Theme.of(context).primaryColor ),
            onPressed: (){
//            Navigator.of(context).pushNamed(HomeScreen.routeName, arguments:[widget.currentUser.name,widget.currentUser.phoneNumber]);
              Navigator.of(context).pop(context);
            },
          ),
          backgroundColor: Theme.of(context).accentColor,
          title: Text(
            "Appointments",
            style: TextStyle(
              fontSize:22.0,
              fontWeight: FontWeight.bold,
              color: Theme.of(context).primaryColor,
            ),
          ),
          bottom: TabBar(

            labelColor: Theme.of(context).primaryColor,
            // indicatorColor: Colors.red,
            indicatorWeight: 2,
            indicatorSize:TabBarIndicatorSize.label ,
            // indicatorPadding: EdgeInsets.all(20),
            indicator: BoxDecoration(
              borderRadius: BorderRadius.circular(16.0),
              color: Colors.transparent
            ),
            tabs: [
              Container(
                height: 40,
                alignment: Alignment.center,
                child: Text('Scheduled Appointments',style: TextStyle(
                fontSize: 14,fontWeight: FontWeight.bold
                ))),
              Container(
                height: 40,
                alignment: Alignment.center,
                child: Text('Past Appointments',style: TextStyle(
                  fontSize: 14,fontWeight: FontWeight.bold
                ),)
              )
            ],
          ),
        ),
        body: Stack(
          children: [
            TabBarView(
            children: [
              FutureBuilder(
                future: ApiRequest.postUpcomingAppointmentDoctor(date_time: Utility.getFormatedCurrentTime(), doctor_id: appUserModel.id),
                builder: (context, snapshot) {

                  if(snapshot.hasData) {
                    locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
                    scheduledModel.ScheduledConversationListModel sAList = scheduledModel.ScheduledConversationListModel.fromSnapshot(snapshot);
                    Set<String> setDateList=Set();
                    sAList.scheduledConversation.forEach((element) {
                      element.appointment.forEach((elementAppt) {
                        setDateList.add(elementAppt['appointment_date']);
                      });
                    });

                    debugPrint("appointments : ${setDateList.toString()}");
                    List<String> lstDate = setDateList.toList();
                    List<DateTime> tmpLstDtTime = [];
                    lstDate.forEach((element) {
                      DateFormat dtFormat = DateFormat('dd-MMM-yyyy');
                      DateTime dtTime = dtFormat.parse(element);
                      tmpLstDtTime.add(dtTime);
                    });
                    tmpLstDtTime.sort((a,b) => b.compareTo(a));
                    lstDate.clear();
                    tmpLstDtTime.forEach((element) {
                      DateFormat dtFormat = DateFormat('dd-MMM-yyyy');
                      String strDestDate = dtFormat.format(element);
                      lstDate.add(strDestDate);
                    });
                    debugPrint("lst appointments : ${lstDate.toString()}");
                    Map<String,dynamic> newMapping = Map();;
                    lstDate.forEach((elementLstDate) {
                      List<Map<String,dynamic>> lstUsers = [];
                      sAList.scheduledConversation.forEach((elementPatient) {
                        sAList.scheduledConversation.forEach((element) {
                          if(elementPatient.id_user==element.id_user){
                            Map<String,dynamic> mapUser = Map();
                            mapUser['id_user']=elementPatient.id_user;
                            mapUser['patient_name'] = elementPatient.patient_name;
                            mapUser['patient_firebase_token']=elementPatient.firebase_token;
                            mapUser['avatar_image'] = elementPatient.avatar_image;

                            List<String> lstDateSlot = [];
                            element.appointment.forEach((elementAppt) {
                              String apptDate = elementAppt['appointment_date'];
                              String apptTime = elementAppt['time'];
                              if(elementLstDate == apptDate ) {
                                lstDateSlot.add(apptTime);
                              }
                            });
                            mapUser['slots'] = lstDateSlot;
                            if(lstDateSlot.length>0) {
                              lstUsers.add(mapUser);
                            }
                          }
                        });
                      });
                      newMapping[elementLstDate]=lstUsers;
                    });
                    JsonEncoder  encoder = new JsonEncoder.withIndent('    ');
                    String prettyprint = encoder.convert(newMapping);
                    debugPrint("NewMapping : ${prettyprint}");
                    debugPrint("NewMapping length : ${newMapping.length}");
                    if(newMapping.length==0){
                      return Container(
                        margin: EdgeInsets.only(top: 30),
                        alignment: Alignment.center,
                        child: Text(
                          "No scheduled appointments found",
                          style: TextStyle(fontSize: 16),
                        ),
                      );
                    }else{
                      return SingleChildScrollView(
                        controller: ScrollController(),
                        child: Container(
                          padding: EdgeInsets.all(10),
                          child: Column(
                            children: newMapping.entries.map((e) {
                              List<dynamic> lstValue = e.value;
                              return Container(
                                margin: EdgeInsets.only(bottom: 10),
                                child: Column(
                                  children: [
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Container(
                                        padding: EdgeInsets.all(7),
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.only(topRight: Radius.circular(100),bottomRight: Radius.circular(100)),
                                            // color: Theme.of(context).accentColor,
                                            gradient: LinearGradient(
                                              colors: [Colors.red[200],Colors.blue[200]],
                                              begin: const FractionalOffset(0.0, 0.0),
                                              end: const FractionalOffset(0.5, 0.0),
                                              stops: [0.0,1.0],
                                              tileMode: TileMode.clamp,
                                            )
                                        ),
                                        child: Text(
                                          '${e.key}',
                                          style: TextStyle(
                                              fontSize: 14.0,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white

                                          ),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.only(top:10),
                                      /*decoration: BoxDecoration(
                                          borderRadius: BorderRadius.only(bottomRight: Radius.circular(10),bottomLeft: Radius.circular(10)),
                                          border: Border.all(color: Theme.of(context).accentColor)
                                      ),*/
                                      child: Column(
                                        children: lstValue.asMap().entries.map((eValue) {
                                          Map<String,dynamic> mapDataValue = eValue.value;
                                          List<dynamic> listMapDataSlots = mapDataValue['slots'];
                                          return Card(
                                            margin: EdgeInsets.only(left: 10,right: 10,bottom: 10),
                                            elevation: 2,
                                            // shadowColor: Theme.of(context).accentColor,
                                            child: Container(
                                              padding: const EdgeInsets.only(top:12.0,bottom: 12.0,left: 12.0,right: 12.0),
                                              child:Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: <Widget>[
                                                  Container(
                                                    margin: const EdgeInsets.only(left: 10,top: 10,bottom: 10,right: 20),
                                                    height: 70,width: 70.0,
                                                    child: ClipOval(
                                                      child: FancyShimmerImage(
                                                        imageUrl: mapDataValue['avatar_image']??Const.defaultProfileUrl,
                                                        shimmerBaseColor: Colors.white,
                                                        shimmerHighlightColor: config.Colors().mainColor(1),
                                                        shimmerBackColor: Colors.green,
                                                        errorWidget: Image.network('https://i0.wp.com/www.dobitaobyte.com.br/wp-content/uploads/2016/02/no_image.png?ssl=1'),
                                                      ),
                                                    )
                                                    ,

                                                  ),
                                                  Expanded(
                                                    flex: 1,
                                                    child: Column(
                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                      children: <Widget>[
                                                        Column(
                                                          crossAxisAlignment: CrossAxisAlignment.start,
                                                          children: <Widget>[
                                                            Container(
                                                              margin: const EdgeInsets.only(bottom: 6.0),
                                                              child: Text(
                                                                '${mapDataValue['patient_name']}',
                                                                textAlign: TextAlign.left,
                                                                style: TextStyle(
                                                                  fontSize: 14.0,
                                                                  fontWeight: FontWeight.bold,
                                                                ),
                                                              ),
                                                            ),
                                                            listMapDataSlots.length>0?Container(
                                                              child:Wrap(
                                                                direction: Axis.horizontal,
                                                                children: listMapDataSlots.asMap().entries.map((e){
                                                                  DateFormat dtFormat = DateFormat('HH:mm:ss');
                                                                  DateTime dtTime = dtFormat.parse(e.value);
                                                                  DateFormat dtFormatD = DateFormat('hh:mm a');
                                                                  String formattedSlot = dtFormatD.format(dtTime);
                                                                  return Container(
                                                                    margin: EdgeInsets.only(left: 3,right: 3,top: 3,bottom: 3),
                                                                    padding: EdgeInsets.only(top: 5,bottom: 5,right: 10,left: 10),
                                                                    decoration: BoxDecoration(
                                                                      border: Border.all(width: 1,color: Colors.grey.withOpacity(0.1)),
                                                                      borderRadius: BorderRadius.circular(100),
                                                                      color: Colors.green
                                                                    ),
                                                                    child: Text('${formattedSlot}',style: TextStyle(
                                                                      color: Theme.of(context).primaryColor
                                                                    ),),
                                                                  );
                                                                }).toList(),
                                                              ),
                                                            ):Container(),
                                                            Row(
                                                              mainAxisAlignment: MainAxisAlignment.end,
                                                              children: [
                                                                ElevatedButton(
                                                                  style: ElevatedButton.styleFrom(
                                                                    padding: EdgeInsets.all(0),
                                                                    primary: Theme.of(context).accentColor,
                                                                    shape: RoundedRectangleBorder(
                                                                      borderRadius: BorderRadius.circular(100),
                                                                    ),
                                                                  ),

                                                                  onPressed: ()async{
                                                                    locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
                                                                    Map<String,dynamic> mapResult = await ApiRequest.getPatientList(doctorId: appUserModel.id);
                                                                    locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
                                                                    debugPrint("mapResult : ${mapResult}");
                                                                    List<dynamic> listData = mapResult['data'];
                                                                    List<dynamic> lstPModel = listData.where((element) => element['id_user'] == mapDataValue['id_user'] ).toList();
                                                                    if(lstPModel.length>0){
                                                                      Map<String,dynamic> mapPatient = lstPModel[0];
                                                                      patientModel.PatientModel patient = patientModel.PatientModel.fromMap(mapPatient);
                                                                      Navigator.of(context).pushNamed(PatientDetail.routeName,arguments: [patient]);
                                                                    }
                                                                  },
                                                                  child: Text('View',style: TextStyle(
                                                                      color: Theme.of(context).primaryColor
                                                                  )),
                                                                ),
                                                                SizedBox(width: 10,),
                                                                ElevatedButton(
                                                                  style: ElevatedButton.styleFrom(
                                                                    primary: Theme.of(context).accentColor,
                                                                    shape: RoundedRectangleBorder(
                                                                      borderRadius: BorderRadius.circular(100),
                                                                    ),
                                                                  ),

                                                                  onPressed: ()async{

                                                                    locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
                                                                    Map<String,dynamic> mapResult = await ApiRequest.getPatientList(doctorId: appUserModel.id);
                                                                    locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
                                                                    debugPrint("mapResult : ${mapResult}");
                                                                    List<dynamic> listData = mapResult['data'];
                                                                    List<dynamic> lstPModel = listData.where((element) => element['id_user'] == mapDataValue['id_user'] ).toList();
                                                                    if(lstPModel.length>0){
                                                                      Map<String,dynamic> mapPatient = lstPModel[0];
                                                                      patientModel.PatientModel patient = patientModel.PatientModel.fromMap(mapPatient);
                                                                      // Navigator.of(context).pushNamed(PatientDetail.routeName,arguments: [patient]);

                                                                      Map<String,dynamic> mapSchedule = Map();
                                                                      mapSchedule['id_user']= patient.id_user;
                                                                      mapSchedule['patient_name'] = '${patient.patient_firstname} ${patient.patient_lastname}';
                                                                      mapSchedule['avatar_image'] = patient.patient_image;
                                                                      mapSchedule['patient_firebase_token'] = patient.firebase_token;
                                                                      mapSchedule['appointment'] = [];
                                                                      ScheduledConversation scheduleConversation = ScheduledConversation.fromMap(mapSchedule);
                                                                      // map['patient_firebase_token']=widget.patient.
                                                                      AppointmentType aType = AppointmentType.SCHEDULED_APPOINTMENT;
                                                                      Navigator.pushNamed(context, PersonalChat.routeName,arguments: ["${patient.id_user}",aType,scheduleConversation]);

                                                                    }




                                                                  },
                                                                  child: Text('Chat',style: TextStyle(
                                                                    color: Theme.of(context).primaryColor
                                                                  ),),
                                                                )
                                                              ],
                                                            )
                                                          ],
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          );
                                        }).toList(),
                                      ),
                                    )
                                  ],
                                ),
                              );
                            }).toList(),
                          ),
                        ),
                      );

                    }

                  } else if(snapshot.hasError){
                    locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
                    MyToast.showToast(snapshot.error.toString(),context);
                    return Container();
                  } else{
                    return LoadingWidget(initialData: true,);
                  }
                }
                ,
              ),
              FutureBuilder(
                future: ApiRequest.postUpcomingAppointmentDoctor(date_time: Utility.getFormatedCurrentTime(), doctor_id: appUserModel.id),
                builder: (context, snapshot) {

                  if(snapshot.hasData) {
                    locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
                    pastModel.PastConversationListModel paList = pastModel.PastConversationListModel.fromSnapshot(snapshot);

                    Set<String> setDateList=Set();
                    paList.pastConversation.forEach((element) {
                      element.appointment.forEach((elementAppt) {
                        setDateList.add(elementAppt['appointment_date']);
                      });
                    });

                    debugPrint("appointments : ${setDateList.toString()}");
                    List<String> lstDate = setDateList.toList();
                    List<DateTime> tmpLstDtTime = [];
                    lstDate.forEach((element) {
                      DateFormat dtFormat = DateFormat('dd-MMM-yyyy');
                      DateTime dtTime = dtFormat.parse(element);
                      tmpLstDtTime.add(dtTime);
                    });
                    tmpLstDtTime.sort((a,b) => b.compareTo(a));
                    lstDate.clear();
                    tmpLstDtTime.forEach((element) {
                      DateFormat dtFormat = DateFormat('dd-MMM-yyyy');
                      String strDestDate = dtFormat.format(element);
                      lstDate.add(strDestDate);
                    });
                    debugPrint("lst appointments : ${lstDate.toString()}");
                    Map<String,dynamic> newMapping = Map();;
                    lstDate.forEach((elementLstDate) {
                      List<Map<String,dynamic>> lstUsers = [];
                      paList.pastConversation.forEach((elementPatient) {
                        paList.pastConversation.forEach((element) {
                          if(elementPatient.id_user==element.id_user){
                            Map<String,dynamic> mapUser = Map();
                            mapUser['id_user']=elementPatient.id_user;
                            mapUser['patient_name'] = elementPatient.patient_name;
                            mapUser['patient_firebase_token']=elementPatient.firebase_token;
                            mapUser['avatar_image'] = elementPatient.avatar_image;

                            List<String> lstDateSlot = [];
                            element.appointment.forEach((elementAppt) {
                              String apptDate = elementAppt['appointment_date'];
                              String apptTime = elementAppt['time'];
                              if(elementLstDate == apptDate ) {
                                lstDateSlot.add(apptTime);
                              }
                            });
                            mapUser['slots'] = lstDateSlot;
                            if(lstDateSlot.length>0) {
                              lstUsers.add(mapUser);
                            }
                          }
                        });
                      });
                      newMapping[elementLstDate]=lstUsers;
                    });
                    JsonEncoder  encoder = new JsonEncoder.withIndent('    ');
                    String prettyprint = encoder.convert(newMapping);
                    debugPrint("NewMapping : ${prettyprint}");
                    debugPrint("NewMapping length : ${newMapping.length}");
                    if(newMapping.length==0){
                      return Container(
                        margin: EdgeInsets.only(top: 30),
                        alignment: Alignment.center,
                        child: Text(
                          "No past appointments found",
                          style: TextStyle(fontSize: 16),
                        ),
                      );
                    }else{
                      return SingleChildScrollView(
                        controller: ScrollController(),
                        child: Container(
                          padding: EdgeInsets.all(10),
                          child: Column(
                            children: newMapping.entries.map((e) {
                              List<dynamic> lstValue = e.value;
                              return Container(
                                margin: EdgeInsets.only(bottom: 10),
                                child: Column(
                                  children: [
                                    /*Container(
                                      padding: EdgeInsets.only(left: 10),
                                      alignment: Alignment.centerLeft,
                                      width: MediaQuery.of(context).size.width,
                                      height: 35,
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.only(topLeft: Radius.circular(10),topRight: Radius.circular(10)),
                                          color: Theme.of(context).accentColor
                                      ),
                                      child: Text(e.key,style: TextStyle(
                                          color: Theme.of(context).primaryColor,
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold
                                      ),),
                                    ),*/
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Container(
                                        padding: EdgeInsets.all(7),
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.only(topRight: Radius.circular(100),bottomRight: Radius.circular(100)),
                                            // color: Theme.of(context).accentColor,
                                            gradient: LinearGradient(
                                              colors: [Colors.red[200],Colors.blue[200]],
                                              begin: const FractionalOffset(0.0, 0.0),
                                              end: const FractionalOffset(0.5, 0.0),
                                              stops: [0.0,1.0],
                                              tileMode: TileMode.clamp,
                                            )
                                        ),
                                        child: Text(
                                          '${e.key}',
                                          style: TextStyle(
                                              fontSize: 14.0,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white

                                          ),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.only(top:10),
                                      /*decoration: BoxDecoration(
                                          borderRadius: BorderRadius.only(bottomRight: Radius.circular(10),bottomLeft: Radius.circular(10)),
                                          border: Border.all(color: Theme.of(context).accentColor)
                                      ),*/
                                      child: Column(
                                        children: lstValue.asMap().entries.map((eValue) {
                                          Map<String,dynamic> mapDataValue = eValue.value;
                                          List<dynamic> listMapDataSlots = mapDataValue['slots'];
                                          return Card(
                                            margin: EdgeInsets.only(left: 10,right: 10,bottom: 10),
                                            elevation: 2,
                                            // shadowColor: Theme.of(context).accentColor,
                                              child: Container(
                                              padding: const EdgeInsets.only(top:12.0,bottom: 12.0,left: 12.0,right: 12.0),
                                              child:Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: <Widget>[
                                                  Container(
                                                    margin: const EdgeInsets.only(left: 10,top: 10,bottom: 10,right: 20),
                                                    height: 70,width: 70.0,
                                                    child: ClipOval(
                                                      child: FancyShimmerImage(
                                                        imageUrl: mapDataValue['avatar_image']??Const.defaultProfileUrl,
                                                        shimmerBaseColor: Colors.white,
                                                        shimmerHighlightColor: config.Colors().mainColor(1),
                                                        shimmerBackColor: Colors.green,
                                                        errorWidget: Image.network('https://i0.wp.com/www.dobitaobyte.com.br/wp-content/uploads/2016/02/no_image.png?ssl=1'),
                                                      ),
                                                    )
                                                    ,

                                                  ),
                                                  Expanded(
                                                    flex: 1,
                                                    child: Column(
                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                      children: <Widget>[
                                                        Column(
                                                          crossAxisAlignment: CrossAxisAlignment.start,
                                                          children: <Widget>[
                                                            Container(
                                                              margin: const EdgeInsets.only(bottom: 6.0),
                                                              child: Text(
                                                                '${mapDataValue['patient_name']}',
                                                                textAlign: TextAlign.left,
                                                                style: TextStyle(
                                                                  fontSize: 14.0,
                                                                  fontWeight: FontWeight.bold,
                                                                ),
                                                              ),
                                                            ),
                                                            listMapDataSlots.length>0?Container(
                                                              child:Wrap(
                                                                direction: Axis.horizontal,
                                                                children: listMapDataSlots.asMap().entries.map((e){
                                                                  DateFormat dtFormat = DateFormat('HH:mm:ss');
                                                                  DateTime dtTime = dtFormat.parse(e.value);
                                                                  DateFormat dtFormatD = DateFormat('hh:mm a');
                                                                  String formattedSlot = dtFormatD.format(dtTime);
                                                                  return Container(
                                                                    margin: EdgeInsets.all(3),
                                                                    padding: EdgeInsets.only(top: 5,bottom: 5,right: 10,left: 10),
                                                                    decoration: BoxDecoration(
                                                                      border: Border.all(width: 1,color: Colors.grey.withOpacity(0.1)),
                                                                      borderRadius: BorderRadius.circular(100),
                                                                      color: Colors.red
                                                                    ),
                                                                    child: Text('${formattedSlot}',style:TextStyle(
                                                                      color: Theme.of(context).primaryColor
                                                                    )),
                                                                  );
                                                                }).toList(),
                                                              ),
                                                            ):Container(),
                                                          ],
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          );
                                        }).toList(),
                                      ),
                                    )
                                  ],
                                ),
                              );
                            }).toList(),
                          ),
                        ),
                      );

                    }
/*
                    return paList.pastConversation.length>0?ListView.builder(
                      shrinkWrap: true,
                      primary: false,
                      itemCount: paList.pastConversation.length,
                      itemBuilder: (context, index) {
                        pastModel.PastConversation pastConversation = paList.pastConversation.elementAt(index);
                        // List<dynamic> appt = paList.pastConversation.elementAt(index).appointment;
                        return Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(16.0),
                          ),
                          child: Container(
                            padding: const EdgeInsets.only(top:12.0,bottom: 12.0,left: 12.0,right: 12.0),
                            child:Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Container(
                                  margin: const EdgeInsets.only(left: 10,top: 10,bottom: 10,right: 20),
                                  height: 80,width: 80.0,
                                  child: ClipOval(
                                    child: FancyShimmerImage(
                                      imageUrl: pastConversation.avatar_image??Const.defaultProfileUrl,
                                      shimmerBaseColor: Colors.white,
                                      shimmerHighlightColor: config.Colors().mainColor(1),
                                      shimmerBackColor: Colors.green,
                                      errorWidget: Image.network('https://i0.wp.com/www.dobitaobyte.com.br/wp-content/uploads/2016/02/no_image.png?ssl=1'),
                                    ),
                                  )
                                  ,

                                ),
                                Expanded(
                                  flex: 1,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Container(
                                            margin: const EdgeInsets.only(bottom: 6.0),
                                            child: Text(
                                              '${pastConversation.patient_name}',
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                fontSize: 12.0,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                          ),
                                          pastConversation.appointment.length>0?Container(
                                            child:Wrap(
                                              direction: Axis.horizontal,
                                              children: pastConversation.appointment.asMap().entries.map((e){
                                                return Container(
                                                  padding: EdgeInsets.only(top: 5,bottom: 5,right: 10,left: 10),
                                                  decoration: BoxDecoration(
                                                    border: Border.all(width: 1,color: Colors.grey.withOpacity(0.1)),
                                                    borderRadius: BorderRadius.circular(100),
                                                  ),
                                                  child: Text('${e.value['appointment_date']} ${e.value['time']}'),
                                                );
                                              }).toList(),
                                            ),
                                          ):Container(),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    ):Container(
                      margin: EdgeInsets.only(top: 30),
                      alignment: Alignment.center,
                      child: Text(
                        "No past appointments found",
                        style: TextStyle(fontSize: 16),
                      ),
                    );*/
                  } else if(snapshot.hasError){
                    locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
                    MyToast.showToast(snapshot.error.toString(),context);
                    return Container();
                  } else{
                    return LoadingWidget(initialData: true,);
                  }
                }
                ,
              )
            ],
            ),
            LoadingWidget()
          ],
        ),
      ),
    );
  }

}
