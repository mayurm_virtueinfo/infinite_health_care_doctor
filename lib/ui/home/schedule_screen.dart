import 'package:flutter/material.dart';
import 'package:infinite_health_care_doctor/main.dart';
import 'package:infinite_health_care_doctor/api/api_request.dart';
import 'package:infinite_health_care_doctor/bloc/duration/duration_bloc.dart';
import 'package:infinite_health_care_doctor/bloc/duration/duration_bloc_event.dart';
import 'package:infinite_health_care_doctor/bloc/loading/loading_bloc.dart';
import 'package:infinite_health_care_doctor/bloc/loading/loading_bloc_event.dart';
import 'package:infinite_health_care_doctor/bloc/weekdata/week_data_bloc.dart';
import 'package:infinite_health_care_doctor/bloc/weekdata/week_data_event.dart';
import 'package:infinite_health_care_doctor/models/user_model.dart';
import 'package:infinite_health_care_doctor/utils/app_style.dart';
import 'package:infinite_health_care_doctor/utils/my_toast.dart';
import 'package:infinite_health_care_doctor/widgets/loading_widget.dart';
import 'package:infinite_health_care_doctor/widgets/weekday_slot_widget.dart';

class ScheduleScreen extends StatefulWidget {
  static const String routeName = '/ScheduleScreen';
  String doctor_id;
  ScheduleScreen({this.doctor_id});
  @override
  _ScheduleScreenState createState() => _ScheduleScreenState();
}

class _ScheduleScreenState extends State<ScheduleScreen> {

  WeekDataBloc weekDataBloc = WeekDataBloc();
  DurationBloc durationBloc = DurationBloc();
  // LoadingBloc loadingBloc = LoadingBloc();

  bool isValidTimeMonday = false;
  bool isValidTimeTuesday = false;
  bool isValidTimeWednesday = false;
  bool isValidTimeThursday = false;
  bool isValidTimeFriday = false;
  bool isValidTimeSaturday = false;
  bool isValidTimeSunday = false;

  bool isValidSecondTimeMonday = false;
  bool isValidSecondTimeTuesday = false;
  bool isValidSecondTimeWednesday = false;
  bool isValidSecondTimeThursday = false;
  bool isValidSecondTimeFriday = false;
  bool isValidSecondTimeSaturday = false;
  bool isValidSecondTimeSunday = false;


  String defaultVal = '00:00';

  bool chkValMonday = false;
  bool chkValTueday = false;
  bool chkValWednesday = false;
  bool chkValThursday = false;
  bool chkValFriday = false;
  bool chkValSaturday = false;
  bool chkValSunday = false;
  String selTextStartMonday = '00:00';
  String selTextStartTuesday = '00:00';
  String selTextStartWednesday = '00:00';
  String selTextStartThursday = '00:00';
  String selTextStartFriday = '00:00';
  String selTextStartSaturday = '00:00';
  String selTextStartSunday = '00:00';
  String selTextEndMonday = '00:00';
  String selTextEndTuesday = '00:00';
  String selTextEndWednesday = '00:00';
  String selTextEndThursday = '00:00';
  String selTextEndFriday = '00:00';
  String selTextEndSaturday = '00:00';
  String selTextEndSunday = '00:00';

  String selTextSecondStartMonday = '00:00';
  String selTextSecondStartTuesday = '00:00';
  String selTextSecondStartWednesday = '00:00';
  String selTextSecondStartThursday = '00:00';
  String selTextSecondStartFriday = '00:00';
  String selTextSecondStartSaturday = '00:00';
  String selTextSecondStartSunday = '00:00';
  String selTextSecondEndMonday = '00:00';
  String selTextSecondEndTuesday = '00:00';
  String selTextSecondEndWednesday = '00:00';
  String selTextSecondEndThursday = '00:00';
  String selTextSecondEndFriday = '00:00';
  String selTextSecondEndSaturday = '00:00';
  String selTextSecondEndSunday = '00:00';

  String selTextDuration = '00';
  @override
  void initState() {

    getDoctorTimings();
    super.initState();
  }

  Map<String,dynamic>  mapMonday = Map();
  Map<String,dynamic> mapTuesday = Map();
  Map<String,dynamic> mapWednesday = Map();
  Map<String,dynamic> mapThursday = Map();
  Map<String,dynamic> mapFriday = Map();
  Map<String,dynamic> mapSaturday = Map();
  Map<String,dynamic> mapSunday = Map();

  List<dynamic> listWeeklyData;
  void getDoctorTimings()async{
    locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
    try {
      final result = await ApiRequest.getDoctorTimings(doctor_id: widget.doctor_id);
      List data = result['data'];
      if (data != null) {
        weekDataBloc.weekDataEventSink.add(WeekDataEvent(weekData: data));
      }
      locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());

    }catch(e){
      locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
      MyToast.showToast('Error getting slots : ${e.toString()}',context);
    }
  }
  @override
  void dispose() {
    // Clean up the focus node when the Form is disposed.
    super.dispose();
  }


  final String firstStartTime = 'first_start_time';
  final String firstEndTime = 'first_end_time';

  final String secondStartTime = 'second_start_time';
  final String secondEndTime = 'second_end_time';

  final String monday = 'monday';
  final String tuesday = 'tuesday';
  final String wednesday = 'wednesday';
  final String thursday = 'thursday';
  final String friday = 'friday';
  final String saturday = 'saturday';
  final String sunday = 'sunday';
  final String status = 'status';

  @override
  Widget build(BuildContext context) {
    debugPrint("---refreshed");
    BoxDecoration enableDecoration =  BoxDecoration(
        color: Theme.of(context).primaryColor,
        borderRadius: BorderRadius.all(Radius.circular(10)));

    BoxConstraints slotsConstraints = BoxConstraints(
        minWidth: MediaQuery.of(context).size.width, minHeight: 50);

    BoxDecoration slotsDecoration = BoxDecoration(
        color: Theme.of(context).primaryColor,
        borderRadius: BorderRadius.all(Radius.circular(10)));

    BoxDecoration disableDecoration = BoxDecoration(
        color: Colors.grey,
        borderRadius: BorderRadius.all(Radius.circular(10)));

    return Scaffold(
      bottomNavigationBar: BottomAppBar(
        color: Colors.transparent,
        elevation: 0,
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: ConstrainedBox(
            constraints: BoxConstraints(
                minWidth: MediaQuery.of(context).size.width),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                shape: AppStyles.roundedRectangleBorder,
                primary: Theme.of(context).accentColor,
                padding: EdgeInsets.all(12),
              ),

              child: Text(
                'Submit',
                style: TextStyle(fontSize: 16,color: Colors.white),
              ),
              onPressed: () async{

                if(selTextDuration == "00"){
                  MyToast.showToast("Please select duration",context);
                  return;
                }
                Map<String,dynamic> mapSchedule = Map();
                mapSchedule['duration'] = selTextDuration;
                bool isError = false;
                List<String> lstString = [];
                // debugPrint("${lstString.join(', ')}");


                bool isSlotSelected = false;
                if(chkValMonday || chkValTueday || chkValWednesday || chkValThursday || chkValFriday || chkValFriday || chkValSaturday || chkValSunday){
                  isSlotSelected = true;
                }

                if( chkValMonday && isValidTimeMonday && (selTextStartMonday != defaultVal) && selTextEndMonday != defaultVal ) {
                  Map<String, dynamic> mapMonday = Map();
                  mapMonday[firstStartTime] = selTextStartMonday;
                  mapMonday[firstEndTime] = selTextEndMonday;
                  mapMonday[secondStartTime] = selTextSecondStartMonday;
                  mapMonday[secondEndTime] = selTextSecondEndMonday;

                  mapMonday[status] = '1';
                  mapSchedule[monday] = mapMonday;
                }
                else{
                  if(chkValMonday) {
                    if(selTextStartMonday == defaultVal){
                      lstString.add('Monday (Select Start Time)');
                    }
                    else if(selTextEndMonday == defaultVal){
                      lstString.add('Monday (Select End Time)');
                    }
                    else {
                      lstString.add('Monday');
                    }
                      isError= true;
                  }else{
                    Map<String, dynamic> mapMonday = Map();
                    mapMonday[firstStartTime] = defaultVal;
                    mapMonday[firstEndTime] = defaultVal;
                    mapMonday[secondStartTime] = defaultVal;
                    mapMonday[secondEndTime] = defaultVal;
                    mapMonday[status] = '0';
                    mapSchedule[monday] = mapMonday;
                  }
                }

                if(chkValTueday && isValidTimeTuesday && (selTextStartTuesday != defaultVal) && selTextEndTuesday != defaultVal ) {
                  Map<String, dynamic> mapTuesday = Map();
                  mapTuesday[firstStartTime] = selTextStartTuesday;
                  mapTuesday[firstEndTime] = selTextEndTuesday;
                  mapTuesday[secondStartTime] = selTextSecondStartTuesday;
                  mapTuesday[secondEndTime] = selTextSecondEndTuesday;

                  mapTuesday[status] = '1';
                  mapSchedule[tuesday] = mapTuesday;
                }
                else{
                  if(chkValTueday) {
                    if(selTextStartTuesday == defaultVal){
                      lstString.add('Tuesday (Select Start Time)');
                    }
                    else if(selTextEndTuesday == defaultVal){
                      lstString.add('Tuesday (Select End Time)');
                    }
                    else {
                      lstString.add('Tuesday');
                    }
                    isError = true;
                  }else{
                    Map<String, dynamic> mapTuesday = Map();
                    mapTuesday[firstStartTime] = defaultVal;
                    mapTuesday[firstEndTime] = defaultVal;
                    mapTuesday[secondStartTime] = defaultVal;
                    mapTuesday[secondEndTime] = defaultVal;

                    mapTuesday[status] = '0';
                    mapSchedule[tuesday] = mapTuesday;
                  }
                }

                if(chkValWednesday && isValidTimeWednesday && (selTextStartWednesday != defaultVal) && selTextEndWednesday != defaultVal ) {
                  Map<String, dynamic> mapWednesday = Map();
                  mapWednesday[firstStartTime] = selTextStartWednesday;
                  mapWednesday[firstEndTime] = selTextEndWednesday;
                  mapWednesday[secondStartTime] = selTextSecondStartWednesday;
                  mapWednesday[secondEndTime] = selTextSecondEndWednesday;

                  mapWednesday[status] = '1';
                  mapSchedule[wednesday] = mapWednesday;
                }
                else{
                  if(chkValWednesday) {

                    if(selTextStartWednesday == defaultVal){
                      lstString.add('Wednesday (Select Start Time)');
                    }
                    else if(selTextEndWednesday == defaultVal){
                      lstString.add('Wednesday (Select End Time)');
                    }
                    else {
                      lstString.add('Wednesday');
                    }
                    isError = true;
                  }else{
                    Map<String, dynamic> mapWednesday = Map();
                    mapWednesday[firstStartTime] = defaultVal;
                    mapWednesday[firstEndTime] = defaultVal;
                    mapWednesday[secondStartTime] = defaultVal;
                    mapWednesday[secondEndTime] = defaultVal;

                    mapWednesday[status] = '0';
                    mapSchedule[wednesday] = mapWednesday;
                  }
                }

                if(chkValThursday && isValidTimeThursday && (selTextStartTuesday != defaultVal) && selTextEndTuesday != defaultVal ) {
                  Map<String, dynamic> mapThursday = Map();
                  mapThursday[firstStartTime] = selTextStartThursday;
                  mapThursday[firstEndTime] = selTextEndThursday;
                  mapThursday[secondStartTime] = selTextSecondStartThursday;
                  mapThursday[secondEndTime] = selTextSecondEndThursday;

                  mapThursday[status] = '1';
                  mapSchedule[thursday] = mapThursday;
                }
                else{
                  if(chkValThursday) {

                    if(selTextStartThursday == defaultVal){
                      lstString.add('Thursday (Select Start Time)');
                    }
                    else if(selTextEndThursday == defaultVal){
                      lstString.add('Thursday (Select End Time)');
                    }
                    else {
                      lstString.add('Thursday');
                    }
                    isError = true;
                  }else{
                    Map<String, dynamic> mapThursday = Map();
                    mapThursday[firstStartTime] = defaultVal;
                    mapThursday[firstEndTime] = defaultVal;
                    mapThursday[secondStartTime] = defaultVal;
                    mapThursday[secondEndTime] = defaultVal;

                    mapThursday[status] = '0';
                    mapSchedule[thursday] = mapThursday;
                  }
                }

                if(chkValFriday && isValidTimeFriday && (selTextStartFriday != defaultVal) && selTextEndFriday != defaultVal ) {
                  Map<String, dynamic> mapFriday = Map();
                  mapFriday[firstStartTime] = selTextStartFriday;
                  mapFriday[firstEndTime] = selTextEndFriday;
                  mapFriday[secondStartTime] = selTextSecondStartFriday;
                  mapFriday[secondEndTime] = selTextSecondEndFriday;

                  mapFriday[status] = '1';
                  mapSchedule[friday] = mapFriday;
                }
                else{
                  if(chkValFriday) {

                    if(selTextStartFriday == defaultVal){
                      lstString.add('Friday (Select Start Time)');
                    }
                    else if(selTextEndFriday == defaultVal){
                      lstString.add('Friday (Select End Time)');
                    }
                    else {
                      lstString.add('Friday');
                    }
                    isError = true;
                  }else{
                    Map<String, dynamic> mapFriday = Map();
                    mapFriday[firstStartTime] = selTextStartFriday;
                    mapFriday[firstEndTime] = selTextEndFriday;
                    mapFriday[secondStartTime] = selTextSecondStartFriday;
                    mapFriday[secondEndTime] = selTextSecondEndFriday;

                    mapFriday[status] = '0';
                    mapSchedule[friday] = mapFriday;
                  }
                }


                if(chkValSaturday && isValidTimeSaturday && (selTextStartSaturday != defaultVal) && selTextEndSaturday != defaultVal ) {
                  Map<String, dynamic> mapSaturday = Map();
                  mapSaturday[firstStartTime] = selTextStartSaturday;
                  mapSaturday[firstEndTime] = selTextEndSaturday;
                  mapSaturday[secondStartTime] = selTextSecondStartSaturday;
                  mapSaturday[secondEndTime] = selTextSecondEndSaturday;

                  mapSaturday[status] = '1';
                  mapSchedule[saturday] = mapSaturday;
                }
                else{
                  if(chkValSaturday) {

                    if(selTextStartSaturday == defaultVal){
                      lstString.add('Saturday (Select Start Time)');
                    }
                    else if(selTextEndSaturday == defaultVal){
                      lstString.add('Saturday (Select End Time)');
                    }
                    else {
                      lstString.add('Saturday');
                    }
                    isError = true;
                  }else{
                    Map<String, dynamic> mapSaturday = Map();
                    mapSaturday[firstStartTime] = defaultVal;
                    mapSaturday[firstEndTime] = defaultVal;
                    mapSaturday[secondStartTime] = defaultVal;
                    mapSaturday[secondEndTime] = defaultVal;

                    mapSaturday[status] = '0';
                    mapSchedule[saturday] = mapSaturday;
                  }
                }
                if(chkValSunday && isValidTimeSunday && (selTextStartSunday != defaultVal) && selTextEndSunday != defaultVal ) {
                  Map<String, dynamic> mapSunday = Map();
                  mapSunday[firstStartTime] = selTextStartSunday;
                  mapSunday[firstEndTime] = selTextEndSunday;
                  mapSunday[secondStartTime] = selTextSecondStartSunday;
                  mapSunday[secondEndTime] = selTextSecondEndSunday;

                  mapSunday[status] = '1';
                  mapSchedule[sunday] = mapSunday;
                }
                else{
                  if(chkValSunday) {
                    if(selTextStartSunday == defaultVal){
                      lstString.add('Sunday (Select Start Time)');
                    }
                    else if(selTextEndSunday == defaultVal){
                      lstString.add('Sunday (Select End Time)');
                    }
                    else {
                      lstString.add('Sunday');
                    }
                    isError = true;
                  }else{
                    Map<String, dynamic> mapSunday = Map();
                    mapSunday[firstStartTime] = defaultVal;
                    mapSunday[firstEndTime] = defaultVal;
                    mapSunday[secondStartTime] = defaultVal;
                    mapSunday[secondEndTime] = defaultVal;

                    mapSunday[status] = '0';
                    mapSchedule[sunday] = mapSunday;
                  }
                }

                if(!isSlotSelected){
                  MyToast.showToast("Please select weekday",context);
                  return;
                }
                if(isError){
                  String strError = 'Invalid data for : ${lstString.join(', ')} ';
                  MyToast.showToast(strError,context);
                  return;
                }
                //
                // MyToast.showToast('Valid Data');
                Map<String,dynamic> myData=Map.from(mapSchedule);
                myData['doctor_id'] = appUserModel.id;
                debugPrint("My Data : ${myData}");

                locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());

                try{
                  final result = await ApiRequest.postDoctorsTimingCreate(dataTimings: myData);
                  final data = result['data'];
                  if(data !=null){
                    getDoctorTimings();
                    MyToast.showToast('Timings created successfully',context);
                  }
                }catch(e){
                  MyToast.showToast('Error creating doctors timings',context);
                  debugPrint("Error : ${e.toString()}");
                  locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
                }

              },
            ),
          ),
        ),
      ),
      appBar: AppBar(
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Theme.of(context).primaryColor),
          onPressed: () {
            Navigator.of(context).pop(context);
          },
        ),
        backgroundColor: Theme.of(context).accentColor,
        title: Text(
          'Schedule',
          style: TextStyle(color: Theme.of(context).primaryColor),
        ),
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: ConstrainedBox(
          constraints: BoxConstraints(
              minWidth: MediaQuery.of(context).size.width,
              minHeight: MediaQuery.of(context).size.height),
          child: Stack(
            children: [
              StreamBuilder(
                stream: weekDataBloc.weekDataStream,
                builder: (context, snapshot) {
                  if(snapshot.hasData){
                    final result = snapshot.data;
                    listWeeklyData = result as List;
                    debugPrint("stream result : ${listWeeklyData}");
                    listWeeklyData.forEach((element) {
                      if(element['day'] == 'monday' || element['day'] == 'Monday'){
                        mapMonday = element;
                        selTextDuration = '${mapMonday['duration']}';

                      }else if(element['day'] == 'tuesday' || element['day'] == 'Tuesday'){
                        mapTuesday = element;
                        selTextDuration = '${mapTuesday['duration']}';
                      }else if(element['day'] == 'wednesday' || element['day'] == 'Wednesday'){
                        mapWednesday = element;
                        selTextDuration = '${mapWednesday['duration']}';
                      }else if(element['day'] == 'thursday' || element['day'] == 'Thursday'){
                        mapThursday = element;
                        selTextDuration = '${mapThursday['duration']}';
                      }else if(element['day'] == 'friday' || element['day'] == 'Friday'){
                        mapFriday = element;
                        selTextDuration = '${mapFriday['duration']}';
                      }else if(element['day'] == 'saturday' || element['day'] == 'Saturday'){
                        mapSaturday = element;
                        selTextDuration = '${mapSaturday['duration']}';
                      }else if(element['day'] == 'sunday' || element['day'] == 'Sunday'){
                        mapSunday = element;
                        selTextDuration = '${mapSunday['duration']}';
                      }
                      durationBloc.durationEventSink.add(DurationBlocEvent(duration: selTextDuration));
                    });
                    return Container(
                      child: Column(
                        children: <Widget>[
                          Column(
                            children: <Widget>[
                              Container(
                                height: 20,
                                padding: const EdgeInsets.only(
                                    top: 0, left: 12.0, right: 12.0),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                      bottomLeft: Radius.circular(25.0),
                                      bottomRight: Radius.circular(25.0)),
                                  color: Theme.of(context).accentColor,
                                ),
                              ),
                              // header section duration
                              Container(
                                padding: EdgeInsets.all(10),
                                margin: EdgeInsets.all(10),
                                constraints: BoxConstraints(
                                    minWidth: MediaQuery.of(context).size.width,
                                    minHeight: 50),
                                decoration: BoxDecoration(
                                    color: Theme.of(context).primaryColor,
                                    borderRadius: BorderRadius.all(Radius.circular(10))),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Expanded(
                                      flex: 2,
                                      child: Container(
                                        alignment: Alignment.topLeft,
                                        child: Text(
                                          "Duration",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              color: Theme.of(context).accentColor,
                                              fontSize: 12,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 3,
                                      child: GestureDetector(
                                        onTap: (){
                                          selectDuration(onSelect: (text){
                                              selTextDuration = text;
                                              debugPrint('$selTextDuration');
                                              durationBloc.durationEventSink.add(DurationBlocEvent(duration: selTextDuration));
                                          });
                                        },
                                        child: Container(
                                          alignment: Alignment.center,
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Container(
                                                padding: EdgeInsets.only(left: 10, right: 10),
                                                constraints: BoxConstraints(
                                                    minWidth: MediaQuery.of(context).size.width,
                                                    minHeight: 40),
                                                decoration: BoxDecoration(
                                                    color: Theme.of(context).primaryColor,
                                                    border: Border.all(
                                                        color: Theme.of(context).accentColor,
                                                        width: 0.5),
                                                    borderRadius:
                                                    BorderRadius.all(Radius.circular(5))),
                                                child: Column(
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  children: [

                                                    Row(
                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                      children: [
                                                        StreamBuilder(
                                                          stream: durationBloc.durationStream,
                                                          builder:  (context, snapshot) {
                                                            if(snapshot.hasData){
                                                              return Text(
                                                                '${snapshot.data} MIN',
                                                                style: TextStyle(
                                                                    color: Colors.black, fontSize: 14),
                                                              );
                                                            }else{
                                                              return Text(
                                                                '0 MIN',
                                                                style: TextStyle(
                                                                    color: Colors.black, fontSize: 14),
                                                              );
                                                            }
                                                          },
                                                        ),
                                                        Icon(
                                                          Icons.keyboard_arrow_down,
                                                          color: Colors.grey,
                                                          size: 20,
                                                        )
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 2,
                                      child: Container(
                                        alignment: Alignment.center,
                                        child: Container(),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              // header section
                              Container(
                                padding: EdgeInsets.all(10),
                                margin: EdgeInsets.all(10),
                                constraints: BoxConstraints(
                                    minWidth: MediaQuery.of(context).size.width,
                                    minHeight: 50),
                                decoration: BoxDecoration(
                                    color: Theme.of(context).primaryColor,
                                    borderRadius: BorderRadius.all(Radius.circular(10))),
                                child: Row(
                                  children: [
                                    Expanded(
                                      flex: 2,
                                      child: Container(
                                        alignment: Alignment.centerLeft,
                                        child: Text(
                                          "Week Day",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              color: Theme.of(context).accentColor,
                                              fontSize: 12,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 3,
                                      child: Container(
                                        margin: EdgeInsets.only(left: 10),
                                        alignment: Alignment.centerLeft,
                                        child: Text(
                                          "Start Time",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              color: Theme.of(context).accentColor,
                                              fontSize: 12,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 3,
                                      child: Container(
                                        alignment: Alignment.centerLeft,
                                        child: Text(
                                          "End Time",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              color: Theme.of(context).accentColor,
                                              fontSize: 12,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          // Monday section
                          WeekdaySlotWidget(
                            initialData: mapMonday,
                            isValidTime: (value){
                              isValidTimeMonday = value;
                              debugPrint("isValidTime Monday : ${value}");
                            },

                            weekDayTitle: "Monday",

                            onSelectEndTime:(text){
                              debugPrint("onSelectEndTime Monday : ${text}");
                              selTextEndMonday = text;
                            } ,
                            onSelectStartTime:(text){
                              debugPrint("onSelectStartTime Monday : ${text}");
                              selTextStartMonday = text;
                            },
                            onChange: (value){
                              debugPrint("onChange Monday : ${value}");
                              chkValMonday = value;
                            } ,
                            isValidSecondTime: (value){
                              isValidSecondTimeMonday = value;
                              debugPrint("Second : isValidTime Monday : ${value}");
                            },
                            onSelectSecondEndTime:(text){
                              debugPrint("Second : onSelectEndTime Monday : ${text}");
                              selTextSecondEndMonday = text;
                            } ,
                            onSelectSecondStartTime:(text){
                              debugPrint("Second : onSelectStartTime Monday : ${text}");
                              selTextSecondStartMonday = text;
                            },
                          ),
                          SizedBox(height: 10,),
                          // Tuesday section
                          WeekdaySlotWidget(
                            initialData: mapTuesday,
                            isValidTime: (value){
                              isValidTimeTuesday = value;
                              debugPrint("isValidTime Tuesday : ${value}");
                            },
                            weekDayTitle: "Tuesday",

                            onSelectEndTime:(text){
                              debugPrint("onSelectEndTime Tuesday : ${text}");
                              selTextEndTuesday = text;
                            } ,
                            onSelectStartTime:(text){
                              debugPrint("onSelectStartTime Tuesday : ${text}");
                              selTextStartTuesday = text;
                            },
                            onChange: (value){
                              debugPrint("onChange Tuesday : ${value}");
                              chkValTueday = value;
                            } ,

                            isValidSecondTime: (value){
                              isValidSecondTimeTuesday = value;
                              debugPrint("Second : isValidTime Monday : ${value}");
                            },
                            onSelectSecondEndTime:(text){
                              debugPrint("Second : onSelectEndTime Monday : ${text}");
                              selTextSecondEndTuesday = text;
                            } ,
                            onSelectSecondStartTime:(text){
                              debugPrint("Second : onSelectStartTime Monday : ${text}");
                              selTextSecondStartTuesday = text;
                            },
                          ),
                          SizedBox(height: 10,),
                          // Wednesday section
                          WeekdaySlotWidget(
                            initialData: mapWednesday,
                            isValidTime: (value){
                              isValidTimeWednesday = value;
                              debugPrint("isValidTime Wednesday : ${value}");
                            },
                            weekDayTitle: "Wednesday",

                            onSelectEndTime:(text){
                              debugPrint("onSelectEndTime Wednesday : ${text}");
                              selTextEndWednesday = text;
                            } ,
                            onSelectStartTime:(text){
                              debugPrint("onSelectStartTime Wednesday : ${text}");
                              selTextStartWednesday = text;
                            },
                            onChange: (value){
                              debugPrint("onChange Wednesday : ${value}");
                              chkValWednesday = value;
                            } ,
                            isValidSecondTime: (value){
                              isValidSecondTimeWednesday = value;
                              debugPrint("Second : isValidTime Monday : ${value}");
                            },
                            onSelectSecondEndTime:(text){
                              debugPrint("Second : onSelectEndTime Monday : ${text}");
                              selTextSecondEndWednesday = text;
                            } ,
                            onSelectSecondStartTime:(text){
                              debugPrint("Second : onSelectStartTime Monday : ${text}");
                              selTextSecondStartWednesday = text;
                            },
                          ),
                          SizedBox(height: 10,),
                          // Thursday section
                          WeekdaySlotWidget(
                            initialData: mapThursday,
                            isValidTime: (value){
                              isValidTimeThursday = value;
                              debugPrint("isValidTime Thursday : ${value}");
                            },
                            weekDayTitle: "Thursday",

                            onSelectEndTime:(text){
                              debugPrint("onSelectEndTime Thursday : ${text}");
                              selTextEndThursday = text;
                            } ,
                            onSelectStartTime:(text){
                              debugPrint("onSelectStartTime Thursday : ${text}");
                              selTextStartThursday = text;
                            },
                            onChange: (value){
                              debugPrint("onChange Thursday : ${value}");
                              chkValThursday = value;
                            } ,
                            isValidSecondTime: (value){
                              isValidSecondTimeThursday = value;
                              debugPrint("Second : isValidTime Monday : ${value}");
                            },
                            onSelectSecondEndTime:(text){
                              debugPrint("Second : onSelectEndTime Monday : ${text}");
                              selTextSecondEndThursday = text;
                            } ,
                            onSelectSecondStartTime:(text){
                              debugPrint("Second : onSelectStartTime Monday : ${text}");
                              selTextSecondStartThursday = text;
                            },
                          ),
                          SizedBox(height: 10,),
                          // Friday section
                          WeekdaySlotWidget(
                            initialData: mapFriday,
                            isValidTime: (value){
                              isValidTimeFriday = value;
                              debugPrint("isValidTime Friday : ${value}");
                            },
                            weekDayTitle: "Friday",

                            onSelectEndTime:(text){
                              debugPrint("onSelectEndTime Friday : ${text}");
                              selTextEndFriday = text;
                            } ,
                            onSelectStartTime:(text){
                              debugPrint("onSelectStartTime Friday : ${text}");
                              selTextStartFriday = text;
                            },
                            onChange: (value){
                              debugPrint("onChange Friday : ${value}");
                              chkValFriday = value;
                            } ,
                            isValidSecondTime: (value){
                              isValidSecondTimeFriday = value;
                              debugPrint("Second : isValidTime Monday : ${value}");
                            },
                            onSelectSecondEndTime:(text){
                              debugPrint("Second : onSelectEndTime Monday : ${text}");
                              selTextSecondEndFriday = text;
                            } ,
                            onSelectSecondStartTime:(text){
                              debugPrint("Second : onSelectStartTime Monday : ${text}");
                              selTextSecondStartFriday = text;
                            },
                          ),
                          SizedBox(height: 10,),
                          // Saturday section
                          WeekdaySlotWidget(
                            initialData: mapSaturday,
                            isValidTime: (value){
                              isValidTimeSaturday = value;
                              debugPrint("isValidTime Saturday : ${value}");
                            },
                            weekDayTitle: "Saturday",

                            onSelectEndTime:(text){
                              debugPrint("onSelectEndTime Saturday : ${text}");
                              selTextEndSaturday = text;
                            } ,
                            onSelectStartTime:(text){
                              debugPrint("onSelectStartTime Saturday : ${text}");
                              selTextStartSaturday = text;
                            },
                            onChange: (value){
                              debugPrint("onChange Saturday : ${value}");
                              chkValSaturday = value;
                            } ,
                            isValidSecondTime: (value){
                              isValidSecondTimeSaturday = value;
                              debugPrint("Second : isValidTime Monday : ${value}");
                            },
                            onSelectSecondEndTime:(text){
                              debugPrint("Second : onSelectEndTime Monday : ${text}");
                              selTextSecondEndSaturday = text;
                            } ,
                            onSelectSecondStartTime:(text){
                              debugPrint("Second : onSelectStartTime Monday : ${text}");
                              selTextSecondStartSaturday = text;
                            },
                          ),
                          SizedBox(height: 10,),
                          // Sunday section
                          WeekdaySlotWidget(
                            initialData: mapSunday,
                            isValidTime: (value){
                              isValidTimeSunday = value;
                              debugPrint("isValidTime Sunday : ${isValidTimeSunday}");
                            },
                            weekDayTitle: "Sunday",

                            onSelectEndTime:(text){
                              selTextEndSunday = text;
                              debugPrint("onSelectEndTime : ${text}");
                            } ,
                            onSelectStartTime:(text){
                              selTextStartSunday = text;
                              debugPrint("onSelectStartTime : ${text}");
                            },
                            onChange: (value){
                              chkValSunday = value;
                              debugPrint("onChange : ${value}");
                            } ,

                            isValidSecondTime: (value){
                              isValidSecondTimeSunday = value;
                              debugPrint("Second : isValidTime Monday : ${value}");
                            },
                            onSelectSecondEndTime:(text){
                              debugPrint("Second : onSelectEndTime Monday : ${text}");
                              selTextSecondEndSunday = text;
                            } ,
                            onSelectSecondStartTime:(text){
                              debugPrint("Second : onSelectStartTime Monday : ${text}");
                              selTextSecondStartSunday = text;
                            },
                          ),
                          SizedBox(height: 20,),

                          SizedBox(height: 20,),
                        ],
                      ),
                    );
                  }else{
                    return Container();
                  }
                },
              ),
              LoadingWidget()
            ],
          ),
        ),
      ),
    );
  }
  void selectDuration({Function onSelect}){
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Container(
              width: double.maxFinite,
              height: 400,
              child: ListView(
                  children: <Widget>[
                    Container(
                        alignment: Alignment.center,
                        child: Text("Select Duration",style: TextStyle(
                            fontSize: 20,
                            color: Theme.of(context).accentColor

                        ),)
                    ),
                    SizedBox(height: 20,),
                    ConstrainedBox(
                      constraints: BoxConstraints(
                          minWidth: MediaQuery.of(context).size.width),
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          shape: AppStyles.roundedRectangleBorder,
                          primary: Theme.of(context).accentColor,
                          padding: EdgeInsets.all(12),
                        ),

                        child: Text(
                          '15 MIN',
                          style: TextStyle(fontSize: 16,color: Colors.white),
                        ),
                        onPressed: () async{
                          onSelect("15");
                          Navigator.of(context).pop();
                        },
                      ),
                    ),
                    SizedBox(height: 10,),
                    ConstrainedBox(
                      constraints: BoxConstraints(
                          minWidth: MediaQuery.of(context).size.width),
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          shape: AppStyles.roundedRectangleBorder,
                          primary: Theme.of(context).accentColor,
                          padding: EdgeInsets.all(12),
                        ),

                        child: Text(
                          '30 MIN',
                          style: TextStyle(fontSize: 16,color: Colors.white),
                        ),
                        onPressed: () async{
                          onSelect("30");
                          Navigator.of(context).pop();
                        },
                      ),
                    ),
                    SizedBox(height: 10,),
                    ConstrainedBox(
                      constraints: BoxConstraints(
                          minWidth: MediaQuery.of(context).size.width),
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          shape: AppStyles.roundedRectangleBorder,
                          primary: Theme.of(context).accentColor,
                          padding: EdgeInsets.all(12),
                        ),

                        child: Text(
                          '45 MIN',
                          style: TextStyle(fontSize: 16,color: Colors.white),
                        ),
                        onPressed: () async{
                          onSelect("45");
                          Navigator.of(context).pop();
                        },
                      ),
                    ),
                    SizedBox(height: 10,),
                    ConstrainedBox(
                      constraints: BoxConstraints(
                          minWidth: MediaQuery.of(context).size.width),
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          shape: AppStyles.roundedRectangleBorder,
                          primary: Theme.of(context).accentColor,
                          padding: EdgeInsets.all(12),
                        ),
                        child: Text(
                          '60 MIN',
                          style: TextStyle(fontSize: 16,color: Colors.white),
                        ),
                        onPressed: () async{
                          onSelect("60");
                          Navigator.of(context).pop();
                        },
                      ),
                    ),
                    SizedBox(height: 10,),
                    ConstrainedBox(
                      constraints: BoxConstraints(
                          minWidth: MediaQuery.of(context).size.width),
                      child: ElevatedButton(

                        style: ElevatedButton.styleFrom(
                          shape: AppStyles.roundedRectangleBorder,
                          primary: Theme.of(context).accentColor,
                          padding: EdgeInsets.all(12),
                        ),
                        child: Text(
                          '90 MIN',
                          style: TextStyle(fontSize: 16,color: Colors.white),
                        ),
                        onPressed: () async{
                          onSelect("90");
                          Navigator.of(context).pop();
                        },
                      ),
                    ),
                    SizedBox(height: 10,),
                    ConstrainedBox(
                      constraints: BoxConstraints(
                          minWidth: MediaQuery.of(context).size.width),
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          shape: AppStyles.roundedRectangleBorder,
                          primary: Theme.of(context).accentColor,
                          padding: EdgeInsets.all(12),
                        ),

                        child: Text(
                          '120 MIN',
                          style: TextStyle(fontSize: 16,color: Colors.white),
                        ),
                        onPressed: () async{
                          onSelect("120");
                          Navigator.of(context).pop();
                        },
                      ),
                    ),
                  ]
              ),
            ),
          );
        }
    );

  }
//before remove
}
