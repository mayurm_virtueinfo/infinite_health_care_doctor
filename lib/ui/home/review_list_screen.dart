import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:infinite_health_care_doctor/api/api_request.dart';
import 'package:infinite_health_care_doctor/config/const.dart';
import 'package:infinite_health_care_doctor/models/patient_model.dart' as patientModel;
import 'package:infinite_health_care_doctor/models/patient_review.dart';
import 'package:infinite_health_care_doctor/ui/home/patient_detail.dart';
import 'package:infinite_health_care_doctor/config/app_config.dart' as config;
import 'package:infinite_health_care_doctor/utils/my_toast.dart';
import 'package:infinite_health_care_doctor/widgets/doctors_card_widget.dart';
import 'package:infinite_health_care_doctor/widgets/loading_widget.dart';
import 'package:infinite_health_care_doctor/widgets/patients_card_widget.dart';
import 'package:infinite_health_care_doctor/widgets/searchWidget.dart';
class ReviewListScreen extends StatefulWidget {
  static const String routeName = '/ReviewListScreen';
  final String doctorId;
  ReviewListScreen({this.doctorId});
  @override
  _ReviewListScreenState createState() => _ReviewListScreenState();
}

class _ReviewListScreenState extends State<ReviewListScreen> {
  @override
  void initState() {
    super.initState();
  }
  @override
  void dispose() {
    // Clean up the focus node when the Form is disposed.
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color:Theme.of(context).primaryColor ),
          onPressed: (){
//            Navigator.of(context).pushNamed(HomeScreen.routeName, arguments:[widget.currentUser.name,widget.currentUser.phoneNumber]);
            Navigator.of(context).pop(context);
          },
        ),
        backgroundColor: Theme.of(context).accentColor,
        title: Text(
          "Latest Review",
          style: TextStyle(
            fontSize:22.0,
            fontWeight: FontWeight.bold,
            color: Theme.of(context).primaryColor,
          ),
        ),

      ),
      body: SingleChildScrollView(
        child:ConstrainedBox(
          constraints: BoxConstraints(
            minHeight: MediaQuery.of(context).size.height,
            minWidth: MediaQuery.of(context).size.width
          ),
          child: Container(
            decoration: BoxDecoration(
             color: Colors.transparent,
            ),
            child: FutureBuilder(
              future: ApiRequest.getLatestReview(doctor_id:widget.doctorId),
              builder: (context, snapshot) {

                if(snapshot.hasData) {
                  List<dynamic> pReviewList = snapshot.data['data'];
                  List<PatientReview> patientReviewList = [];
                  pReviewList.forEach((element) {
                    PatientReview pReview = PatientReview.fromMap(element);
                    patientReviewList.add(pReview);
                  });
                  return patientReviewList.length>0?ListView.separated(
                    shrinkWrap: true,
                    primary: false,
                    itemCount: patientReviewList.length,
                    separatorBuilder: (context, index) {
                      return SizedBox(height: 4.0);
                    },
                    itemBuilder: (context, index) {
                      PatientReview pReview=patientReviewList[index];
                      return Card(
                        margin: EdgeInsets.all(16),
                        elevation: 1,
                        // color: MyColors.colorConvert(e.value.color),
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
                        child: Container(
                          margin: EdgeInsets.all(16),
                          height: 120,
                          // padding: EdgeInsets.all(12),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                flex: 1,
                                child: Container(
                                  alignment: Alignment.center,
                                  child: Container(
                                    width: 70,
                                    height: 70,
                                    child: ClipOval(
                                      child: FancyShimmerImage(
                                        imageUrl: null/*e.value.image*/ ?? Const.defaultProfileUrl,
                                        shimmerBaseColor: Colors.white,
                                        shimmerHighlightColor: config.Colors().mainColor(1),
                                        shimmerBackColor: Colors.green,
                                        errorWidget: Image.network('https://i0.wp.com/www.dobitaobyte.com.br/wp-content/uploads/2016/02/no_image.png?ssl=1'),
                                        boxFit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                  flex: 4,
                                  child: Container(
                                    padding: EdgeInsets.only(top: 20, bottom: 10),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          '${pReview.first_name} ${pReview.last_name}',
                                          style: TextStyle(
                                            fontSize: 12.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black,
                                          ),
                                        ),
                                        Container(
                                          child: RatingBarIndicator(
                                            itemPadding: EdgeInsets.only(right: 5),
                                            rating: double.parse(pReview.rating),
                                            itemBuilder: (context, index) => Container(
                                              height: 20,
                                              width: 20,
                                              child: Icon(
                                                Icons.star,
                                                color: Colors.amber,
                                              ),
                                            ),
                                            itemCount: 5,
                                            itemSize: 20.0,
                                            unratedColor: Colors.amber.withAlpha(50),
                                            direction: Axis.horizontal,
                                          ),
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Flexible(
                                          child: Wrap(
                                            direction: Axis.horizontal ,
                                            children: [
                                              Text(
                                                pReview.description,
                                                // overflow: TextOverflow.ellipsis,
                                                style: TextStyle(fontSize: 10.0, color: Colors.grey, fontStyle: FontStyle.italic),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ))
                            ],
                          ),
                        ),
                      );
                    },
                  ):Container(
                    margin: EdgeInsets.only(top: 30),
                    alignment: Alignment.center,
                    child: Text(
                      "No reviews found",
                      style: TextStyle(fontSize: 16),
                    ),
                  );
                } else if(snapshot.hasError){
                  MyToast.showToast(snapshot.error.toString(),context);
                  return Container();
                } else{
                  return Center(
                    child:LoadingWidget(initialData: true,) ,
                  );
                }
              }
              ,
            ),
          ),
        ),
      ),
    );
  }

}
