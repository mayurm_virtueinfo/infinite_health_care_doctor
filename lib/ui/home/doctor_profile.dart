import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_html/style.dart';
import 'package:infinite_health_care_doctor/api/api_request.dart';
import 'package:infinite_health_care_doctor/config/app_config.dart' as config;
import 'package:infinite_health_care_doctor/config/const.dart';
import 'package:infinite_health_care_doctor/models/doctor_profile_model.dart';
import 'package:infinite_health_care_doctor/ui/home/doctor_album_images.dart';
import 'package:infinite_health_care_doctor/ui/home/doctor_book_first_step.dart';
import 'package:infinite_health_care_doctor/ui/home/send_feedback.dart';
import 'package:infinite_health_care_doctor/utils/strings.dart';
class DoctorProfile extends StatefulWidget {
  static const String routeName = '/DoctorProfile';
  String title;
  String id_doctor;
  DoctorProfile({Key key,this.id_doctor,this.title}) : super(key: key);
  @override
  _DoctorProfileState createState() => _DoctorProfileState();
}
class _DoctorProfileState extends State<DoctorProfile> {
  DoctorProfileModel doctorProfileModel;
  @override
  Widget build(BuildContext context) {


    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        leading: IconButton(
          icon: Icon(
              Icons.arrow_back,
              color:Theme.of(context).primaryColor
          ),
          onPressed: (){
            Navigator.pop(context);
          },
        ),
        title: Text(
          widget.title,
          style: TextStyle(
            fontSize:22.0,
            fontWeight: FontWeight.bold,
            color: Theme.of(context).primaryColor,
          ),
        ),
        backgroundColor: Theme.of(context).accentColor,
      ),
      body: SingleChildScrollView(
        child: Container(
          constraints: BoxConstraints(
            minHeight: MediaQuery.of(context).size.height,
            minWidth: MediaQuery.of(context).size.width,
          ),
          child: FutureBuilder(
            future: ApiRequest.getSingleDoctors(id_doctors: widget.id_doctor),
            builder: (context, snapshot) {
              if(snapshot.hasData){
                doctorProfileModel = DoctorProfileModel.fromSnapshot(snapshot);
                return Column(
                  children: <Widget>[
                    Stack(
                      children: <Widget>[
                        Container(
                          height: 150,
                          padding: const EdgeInsets.only(top:40,left:12.0,right: 12.0),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(bottomLeft:Radius.circular(25.0),bottomRight: Radius.circular(25.0)),
                            color: Theme.of(context).accentColor,
                          ),
                        ),
                        Stack(
                          children: <Widget>[
                            Container(

                              padding:EdgeInsets.all(26.0),
                              margin:EdgeInsets.only(top: 33.0,left: 14.0,right: 14.0),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20.0),
                                boxShadow: [BoxShadow( color: Colors.grey .withOpacity(0.4), offset: Offset(2,4), blurRadius: 10)],
                                color: Theme.of(context).primaryColor,
                              ),
                              child:Column(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Text(
                                        "Prime",
                                        style: TextStyle(
                                          fontSize: 14.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.green,
                                        ),
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Icon(Icons.star,color: Colors.yellow,),
                                          Text("${doctorProfileModel.rating}",style: TextStyle(color: Colors.grey,),),
                                        ],
                                      ),
                                    ],
                                  ),
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        "${doctorProfileModel.title}",
                                        style: TextStyle(
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.bold,
                                          color:Theme.of(context).hintColor,
                                        ),
                                      ),
                                      Html(
                                        data:doctorProfileModel.description ,
                                        shrinkWrap: true,
                                        style: {
                                          'p':Style(
                                            textAlign: TextAlign.center,
                                          )
                                        },
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: 30,),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Text(
                                        "6 yrs.Experience",
                                        style: TextStyle(
                                          fontSize: 14.0,
                                          color:Theme.of(context).hintColor,
                                        ),
                                      ),
                                      Text(
                                        "89% ( 4356 votes )",
                                        style: TextStyle(
                                          fontSize: 14.0,
                                          color:Theme.of(context).hintColor,
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: 30,),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: doctorProfileModel.album_images.asMap().entries.map((entry) {
                                      if(entry.key == 3){
                                        return GestureDetector(
                                          onTap: (){
                                            Navigator.of(context).pushNamed(DoctorAlbumImages.routeName,arguments: [0,doctorProfileModel.album_images]);
                                          },
                                          child: Container(
                                            margin: EdgeInsets.only(right: 7,left: 7),
                                            height: (MediaQuery
                                                .of(context)
                                                .size
                                                .width / 4) - (MediaQuery
                                                .of(context)
                                                .devicePixelRatio * 20),
                                            width: (MediaQuery
                                                .of(context)
                                                .size
                                                .width / 4) - (MediaQuery
                                                .of(context)
                                                .devicePixelRatio * 20),

                                            child: Container(

                                              child: Stack(
                                                children: [

                                                  FancyShimmerImage(
                                                    boxFit: BoxFit.fill,
                                                    imageUrl: entry.value['album_images'],
                                                    shimmerBaseColor: Colors.white,
                                                    shimmerHighlightColor: Theme
                                                        .of(context)
                                                        .accentColor,
                                                    errorWidget: Image.network('https://i0.wp.com/www.dobitaobyte.com.br/wp-content/uploads/2016/02/no_image.png?ssl=1'),
                                                    //                          shimmerBackColor: Colors.blue,
                                                  ),
                                                  Container(
                                                    decoration: BoxDecoration(
                                                      color: Theme.of(context).accentColor.withOpacity(0.4),
                                                    ),
                                                    child: Center(
                                                      child: Text(
                                                        "+${doctorProfileModel.album_images.length-4}",
                                                        style: TextStyle(
                                                          fontSize: 22.0,
                                                          fontWeight: FontWeight.bold,
                                                          color: Theme.of(context).primaryColor,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        );
                                      }
                                      else if(entry.key<4) {
                                        return GestureDetector(
                                          onTap: (){
                                            Navigator.of(context).pushNamed(DoctorAlbumImages.routeName,arguments: [entry.key,doctorProfileModel.album_images]);
                                          },
                                          child: Container(
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.circular(10.0),
                                              shape: BoxShape.rectangle,
                                            ),
                                            height: (MediaQuery
                                                .of(context)
                                                .size
                                                .width / 4) - (MediaQuery
                                                .of(context)
                                                .devicePixelRatio * 20),
                                            width: (MediaQuery
                                                .of(context)
                                                .size
                                                .width / 4) - (MediaQuery
                                                .of(context)
                                                .devicePixelRatio * 20),
                                            margin: EdgeInsets.only(right: 7,left: 7),

                                            child: FancyShimmerImage(
                                              boxFit: BoxFit.fill,
                                              imageUrl: entry.value['album_images'],
                                              shimmerBaseColor: Colors.white,
                                              shimmerHighlightColor: Theme
                                                  .of(context)
                                                  .accentColor,
                                              errorWidget: Image.network('https://i0.wp.com/www.dobitaobyte.com.br/wp-content/uploads/2016/02/no_image.png?ssl=1'),
                                              //                          shimmerBackColor: Colors.blue,
                                            ),
                                          ),
                                        );
                                      }else{
                                        return Container();
                                      }
                                    }).toList()/*<Widget>[
                                      SizedBox(height: 60,width: 60,child: Container(decoration: BoxDecoration(borderRadius: BorderRadius.circular(10.0),image:DecorationImage(image:AssetImage('images/Image 11.png'),
                                        fit: BoxFit.cover,)),),),
                                      SizedBox(height: 60,width: 60,child: Container(decoration: BoxDecoration(borderRadius: BorderRadius.circular(10.0),image:DecorationImage(image:AssetImage('images/Image 12.png'),
                                        fit: BoxFit.cover,)),),),
                                      SizedBox(height: 60,width: 60,child: Container(decoration: BoxDecoration(borderRadius: BorderRadius.circular(10.0),image:DecorationImage(image:AssetImage('images/Image 19.png'),
                                        fit: BoxFit.cover,)),),),
                                      SizedBox(
                                        height: 60,
                                        width: 60,
                                        child: Container(
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(10.0),
                                            image:DecorationImage(
                                              image:AssetImage('images/index.jpeg'),
                                              fit: BoxFit.cover,
                                            ),
                                          ),
                                          child: Container(
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.circular(10.0),
                                              color: Theme.of(context).accentColor.withOpacity(0.6),
                                            ),
                                            child: Center(
                                              child: Text(
                                                "+7",
                                                style: TextStyle(
                                                  ,
                                                  fontSize: 22.0,
                                                  fontWeight: FontWeight.bold,
                                                  color: Theme.of(context).primaryColor,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ]*/,
                                  ),
                                ],
                              ),
                            ),
                            Center(child:ball(doctorProfileModel.avatar_image, Theme.of(context).primaryColor,)),
                          ],
                        ),
                      ],
                    ),
                    Container(
                      padding:EdgeInsets.all(26.0),
                      width: double.maxFinite,
                      margin:EdgeInsets.only(top: 30.0,left: 14.0,right: 14.0,bottom: 30),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20.0),
                        boxShadow: [BoxShadow( color: Colors.grey.withOpacity(0.4), offset: Offset(2,4), blurRadius: 10)],
                        color: Theme.of(context).primaryColor,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                "Fees : \₹ 10",
                                style: TextStyle(
                                  fontSize: 12.0,
                                  fontWeight: FontWeight.bold,
                                  color:Theme.of(context).hintColor,
                                ),
                              ),
                              Container(
                                decoration: BoxDecoration(
                                    border: Border.all(width: 1.5,color: Theme.of(context).accentColor),
                                    borderRadius: BorderRadius.circular(20.0)
                                ),
                                child:TextButton(
                                  style: TextButton.styleFrom(
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(20.0)
                                    ),
                                  ),

                                  onPressed: (){
                                    if(doctorProfileModel != null) {
                                      Navigator.of(context).pushNamed(DoctorBookFirstStep.routeName, arguments: [doctorProfileModel]);
                                    }
                                  },
                                  child: Text(
                                    'Book',
                                    style: TextStyle(
                                      fontSize: 14.0,
                                      fontWeight: FontWeight.bold,
                                      color: Theme.of(context).accentColor,
                                    ),
                                  ),
                                ),
                              ),

                            ],
                          ),
                          SizedBox(height: 30.0,child: Center(child: Container(height: 1.0,color: Colors.grey[400].withOpacity(0.1),),)),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text("${doctorProfileModel.status=='0'?Strings.CLOSED_TODAY:Strings.OPEN_TODAY}",
                                style: TextStyle(
                                  fontSize: 10.0,
                                  fontWeight: FontWeight.bold,
                                  color: doctorProfileModel.status=='0'?Colors.red:Colors.green,
                                ),
                              ),
                              Text(
                                doctorProfileModel.gtimings??"",
                                style: TextStyle(
                                  fontSize: 10.0,
                                  fontWeight: FontWeight.bold,
                                  color:Theme.of(context).hintColor,
                                ),
                              ),
                              Text(
                                'All Timing',
                                style: TextStyle(
                                    fontSize: 10.0,
                                    fontWeight: FontWeight.bold,
                                    color: Theme.of(context).accentColor
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 30.0,child: Center(child: Container(height: 1.0,color: Colors.grey[400].withOpacity(0.1),),)),
                          Row(
                            children: <Widget>[
                              Icon(Icons.location_on,color:Theme.of(context).hintColor.withOpacity(0.5),),
                              Text(
                                '${doctorProfileModel.address}, ${doctorProfileModel.city}, ${doctorProfileModel.zipcode}',
                                style: TextStyle(
                                  fontSize: 12.0,
                                  color: Colors.grey,

                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 6.0,),
                          Container(
                            height: 100,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(16.0),
                                image:DecorationImage(
                                  image:AssetImage('images/gps.png'),
                                  fit: BoxFit.cover,
                                )
                            ),
                            /*child: GoogleMap(
                              mapType: MapType.hybrid,
                              initialCameraPosition: _kGooglePlex,
                              onMapCreated: (GoogleMapController controller) {
                                _controller.complete(controller);
                              },
                            ),*/
                          ),
                          SizedBox(height: 30.0,child: Center(child: Container(height: 1.0,color: Colors.grey[400].withOpacity(0.1),),)),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                'FEEDBACK',
                                style: TextStyle(
                                  fontSize: 14.0,
                                  color: Colors.grey,

                                ),
                              ),
                              Text(
                                'Very good,courteous and efficient staff',
                                style: TextStyle(
                                  fontSize: 12.0,
                                  //fontWeight: FontWeight.bold,
                                  color:Theme.of(context).hintColor,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 30.0,child: Center(child: Container(height: 1.0,color: Colors.grey[400].withOpacity(0.1),),)),
                          Text(
                            'SPECIALIZATION',
                            style: TextStyle(
                              fontSize: 14.0,
                              color: Colors.grey,
                            ),
                          ),
                          SizedBox(height: 6.0,),
                          Text(
                            'lorem ipsum',
                            style: TextStyle(
                              fontSize: 12.0,
                              color:Theme.of(context).hintColor,
                            ),
                          ),
                          SizedBox(height: 6.0,),
                          Text(
                            'lorem ipsum',
                            style: TextStyle(
                              fontSize: 12.0,
                              color:Theme.of(context).hintColor,
                            ),
                          ),
                          SizedBox(height: 6.0,),
                          Text(
                            'lorem ipsum',
                            style: TextStyle(
                              fontSize: 12.0,
                              color:Theme.of(context).hintColor,
                            ),
                          ),
                          SizedBox(height: 30.0,child: Center(child: Container(height: 1.0,color: Colors.grey[400].withOpacity(0.1),),)),
                          Text(
                            'ALSO PRACTICES AT',
                            style: TextStyle(
                              fontSize: 14.0,
                              color: Colors.grey,
                            ),
                          ),
                          SizedBox(height: 6.0,),
                          Column(
                            children: <Widget>[
                              card("images/asset-2.png","Dr.Vipul Bariya","BHMS Homeopathy","4.2"),
                              SizedBox(height: 30.0,child: Center(child: Container(height: 1.0,color: Colors.grey[350].withOpacity(0.1),),)),
                              card("images/asset-3.png","Dr.Vipul Bariya","BHMS Homeopathy","3.6"),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                );
              }
              else if(snapshot.hasError){
                return Center(
                  child: Text(snapshot.error.toString()),
                );
              }
              else{
                return Center(
                  child:CircularProgressIndicator() ,
                );
              }
            },

          ),
        ),
        
      ),
      bottomNavigationBar: BottomAppBar(
        elevation: 0,
        color: Colors.white,
        child: Container(
            margin:EdgeInsets.all(12.0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30.0),
              border: Border.all(width: 1,color: Colors.grey.withOpacity(0.6)),
            ),
            child: Row(
              // mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    elevation: 0,
                    // materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0)
                    ),
                    primary: Colors.white,
                  ),

                  onPressed: (){
                    Navigator.of(context).pushNamed(SendFeedback.routeName);
                  },
                  child:Container(
                    alignment: Alignment.center,
                    height: 50,
                    width: 150,
                    child:Text(
                      'GIVE FEEDBACK',
                      style: TextStyle(
                          fontSize: 12.0,
                          color: Theme.of(context).accentColor,
                          fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                ),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    elevation: 0,
                    // materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0)
                    ),
                    primary: Theme.of(context).accentColor,
                  ),

                  onPressed: (){
                    if(doctorProfileModel != null) {
                      Navigator.of(context).pushNamed(DoctorBookFirstStep.routeName, arguments: [doctorProfileModel]);
                    }
                  },
                  child:Container(
                    alignment: Alignment.center,
                    height: 50,
                    width: 150,
                    child:Text(
                      'Book',
                      style: TextStyle(
                          fontSize: 12.0,
                          color: Theme.of(context).primaryColor,
                          fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                )
              ],
            )
        ),
      ),
      
    );
  }
  Widget ball(String image,Color color,){
    return Container(
      height: 60,width: 60.0,
      child: ClipOval(
        child: FancyShimmerImage(
          imageUrl: image??Const.defaultProfileUrl,
          shimmerBaseColor: Colors.white,
          shimmerHighlightColor: config.Colors().mainColor(1),
          shimmerBackColor: Colors.green,
          errorWidget: Image.network('https://i0.wp.com/www.dobitaobyte.com.br/wp-content/uploads/2016/02/no_image.png?ssl=1'),
        ),
      )
      ,

    );
  }
  Widget card(String image,String title,String subTitle,String rank){
    return Container(
    child:Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        ball(image, Colors.transparent),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
                title,
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 12.0
                ),
              ),
            Text(
                subTitle,
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontSize: 10.0
                ),
              ),
            Text(
                "\₹ 50",
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontSize: 10.0
                ),
              ), 
          ],
        ),
        Row(
          children: <Widget>[
            Icon(Icons.star,color: Colors.yellow,),
            Text(rank,style: TextStyle(),),
          ],
        ),
      ],
    ),);
  }
}