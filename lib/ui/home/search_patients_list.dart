import 'dart:async';

import 'package:flutter/material.dart';
import 'package:infinite_health_care_doctor/api/api_request.dart';
import 'package:infinite_health_care_doctor/bloc/loading/loading_bloc.dart';
import 'package:infinite_health_care_doctor/bloc/loading/loading_bloc_event.dart';
import 'package:infinite_health_care_doctor/bloc/searchpatient/search_patient_bloc.dart';
import 'package:infinite_health_care_doctor/bloc/searchpatient/search_patient_bloc_event.dart';
import 'package:infinite_health_care_doctor/main.dart';
import 'package:infinite_health_care_doctor/models/doctor_model.dart' as doctorModel;
import 'package:infinite_health_care_doctor/models/patient_model.dart';
import 'package:infinite_health_care_doctor/utils/my_toast.dart';
import 'package:infinite_health_care_doctor/widgets/doctors_card_widget.dart';
import 'package:infinite_health_care_doctor/widgets/loading_widget.dart';
import 'package:infinite_health_care_doctor/widgets/searchWidget.dart';
import 'package:infinite_health_care_doctor/widgets/widget_search_patients_item.dart';
class SearchPatientsList extends StatefulWidget {
  static const String routeName = '/SearchPatientsList';
  @override
  _SearchPatientsListState createState() => _SearchPatientsListState();
}

class _SearchPatientsListState extends State<SearchPatientsList> {
  TextEditingController searchController = TextEditingController() ;
  @override
  void initState() {
    super.initState();
    searchController.addListener(searchListener);
  }
  void searchListener(){
    print("controller change : ${searchController.text}");
    stopTime();
    startTime();
  }

  Timer mainTimer;
  startTime() async {
    mainTimer = Timer(Duration(seconds:1), requestTimeOut);
    return mainTimer;
  }

  stopTime(){
    if(mainTimer!=null) {
      mainTimer.cancel();
    }
  }

  void requestTimeOut() {
    if(searchController.text.length>0) {
      debugPrint("requested");
      searchDoctorList();
    }
  }
  SearchPatientBloc searchDoctorBloc = SearchPatientBloc();
  void searchDoctorList() async{
    locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
    try {
      final result = await ApiRequest.getSearchPatient(keyword: searchController.text);
      List<dynamic> data = result['data'];
      debugPrint("result : ${data}");
      if (data != null) {
        List<PatientModel> listSearchDoctor=[];

        data.forEach((element) {
          PatientModel dModel = PatientModel.fromMap(element);
          listSearchDoctor.add(dModel);
        });
        locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
        searchDoctorBloc.searchDoctorEventSink.add(SearchPatientBlocEvent(listSearchDoctor: listSearchDoctor));
        // FocusScope.of(context).requestFocus(FocusNode());
        // FocusScope.of(context).unfocus();
      }



    }catch(e){
      locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
      MyToast.showToast('Error getting slots : ${e.toString()}',context);
      // FocusScope.of(context).requestFocus(FocusNode());
      // FocusScope.of(context).unfocus();
    }
  }

  @override
  void dispose() {
    // Clean up the focus node when the Form is disposed.
    searchController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Theme.of(context).accentColor,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          color: Colors.white,
          onPressed: (){
            Navigator.of(context).pop();
          },
        ),
        actions: [
          IconButton(
            onPressed: (){
                searchController.text = '';
                searchDoctorBloc.searchDoctorEventSink.add(SearchPatientBlocEvent(listSearchDoctor: []));
            },
            icon: Icon(Icons.close,size: 30,color: Theme.of(context).primaryColor,),
          )
        ],
        backgroundColor: Theme.of(context).accentColor,
        automaticallyImplyLeading: true,
        titleSpacing: 0,
        title: Container(
          margin: EdgeInsets.only(right: 5),
          decoration: BoxDecoration(
            color: Theme.of(context).primaryColor,
            borderRadius: BorderRadius.circular(100),
            boxShadow: [
              BoxShadow(color: Theme.of(context).hintColor.withOpacity(0.10), offset: Offset(0, 4), blurRadius: 10)
            ],
          ),
          child: TextField(
            controller: searchController,
            textAlignVertical: TextAlignVertical.center,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(12),
              hintText: 'Search Patients',
              hintStyle: TextStyle(color: Colors.grey,),
              prefixIcon: Icon(Icons.search, size: 25, color: Theme.of(context).accentColor),
              border: UnderlineInputBorder(borderSide: BorderSide.none),
              enabledBorder: UnderlineInputBorder(borderSide: BorderSide.none),
              focusedBorder: UnderlineInputBorder(borderSide: BorderSide.none),
            ),
          ),
        ),
      ),
      body: Container(
        constraints: BoxConstraints(
            minHeight: MediaQuery.of(context).size.height,
            minWidth: MediaQuery.of(context).size.width),
        child: Stack(
          children: <Widget>[
            SingleChildScrollView(
            child: Container(
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.all(20),
                    decoration: BoxDecoration(
                      color: Colors.transparent,
                    ),
                    child: StreamBuilder(
                      stream: searchDoctorBloc.searchDoctorStream,
                      builder: (context, snapshot) {

                        if(snapshot.hasData) {
                          List<dynamic> listSrcData = snapshot.data;
                          if(listSrcData.length==0){
                            return Container(
                              width: MediaQuery.of(context).size.width,
                              height: MediaQuery.of(context).size.height-(AppBar().preferredSize.height+MediaQuery.of(context).padding.top+50),
                              child: Center(
                                child: Text('No patients found',
                                  style: TextStyle(fontSize: 16),
                                ),
                              ),
                            );
                          }
                          else{
                            List<PatientModel> listSearchDoctor = snapshot.data as List<PatientModel>;
                            return ListView.builder(
                              shrinkWrap: true,
                              primary: false,
                              itemCount: listSearchDoctor.length,

                              itemBuilder: (context, index) {
                                return WidgetSearchPatientsItem(
                                  patientModel: listSearchDoctor.elementAt(index),
                                );
                              },
                            );
                          }
                        } else {
                          return Container();
                        }
                      }
                      ,
                    ),
                  ),
                ],
              ),
            ),
          ),

            LoadingWidget()
          ],
        ),
      ),
    );
  }

}
