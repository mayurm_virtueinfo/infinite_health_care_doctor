import 'dart:io';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:infinite_health_care_doctor/api/api_request.dart';
import 'package:infinite_health_care_doctor/config/app_config.dart' as config;
import 'package:infinite_health_care_doctor/config/const.dart';
import 'package:infinite_health_care_doctor/main.dart';
import 'package:infinite_health_care_doctor/models/appointment_type.dart';
import 'package:infinite_health_care_doctor/models/health_blog.dart' as healthBlog;
import 'package:infinite_health_care_doctor/models/past_conversation.dart' as pastModel;
import 'package:infinite_health_care_doctor/models/patient_model.dart' as patientModel;
import 'package:infinite_health_care_doctor/models/schedule_conversation.dart';
import 'package:infinite_health_care_doctor/models/schedule_conversation.dart' as scheduledModel;
import 'package:infinite_health_care_doctor/models/single_customer.dart';
import 'package:infinite_health_care_doctor/models/user_model.dart';
import 'package:infinite_health_care_doctor/ui/conversation/personal_chat.dart';
import 'package:infinite_health_care_doctor/ui/home/add_notes_screen.dart';
import 'package:infinite_health_care_doctor/utils/my_colors.dart';
import 'package:infinite_health_care_doctor/utils/utility.dart';
import 'package:infinite_health_care_doctor/widgets/loading_widget.dart';
import 'package:intl/intl.dart';

class PatientDetail extends StatefulWidget {
  static const String routeName = "/PatientDetail";
  final patientModel.PatientModel patient;

  PatientDetail({this.patient});

  @override
  _PatientDetailState createState() => _PatientDetailState();
}

class _PatientDetailState extends State<PatientDetail> {
  Map<String, dynamic> mapApptStruct = Map();

  @override
  void initState() {
    super.initState();

    debugPrint('Appointment : ${widget.patient.appointment}');
    debugPrint('AppUserModel : ${appUserModel.id}');
    int doctorId = int.parse(appUserModel.id);
    List<dynamic> appt = widget.patient.appointment.where((element) => element['doctor_id'] == doctorId).toList();
    Set<String> dtSet = Set();
    Set<String> tmpDtSet = Set();
    appt.forEach((element) {
      String appointmentDate = element['appointment_date'];
      DateFormat dtFormat = DateFormat('yyyy-MM-dd');
      DateTime destDate = dtFormat.parse(appointmentDate);
      DateTime now = DateFormat('yyyy-MM-dd').parse(DateFormat('yyyy-MM-dd').format(DateTime.now()));
      /*if(destDate.isBefore(now)) {
        dtSet.add(element['appointment_date']);
      }*/
      tmpDtSet.add(element['appointment_date']);
    });
    List<DateTime> dtTmpList = [];
    tmpDtSet.forEach((element) {
      String appointmentDate = element;
      DateFormat dtFormat = DateFormat('yyyy-MM-dd');
      DateTime destDate = dtFormat.parse(appointmentDate);
      dtTmpList.add(destDate);
    });
    dtTmpList.sort((a, b) => b.compareTo(a));
    dtTmpList.forEach((element) {
      DateFormat dtFormat = DateFormat('yyyy-MM-dd');
      String strDestDate = dtFormat.format(element);
      dtSet.add(strDestDate);
    });

    mapApptStruct = Map();
    dtSet.forEach((elementSet) {
      List<dynamic> srchdDt = appt.where((element) => element['appointment_date'] == elementSet).toList();
      if (srchdDt.length > 0) {
        List<String> lstTime = [];
        srchdDt.forEach((elementSrchDt) {
          String time = elementSrchDt['time'];
          DateFormat parseFormat = DateFormat("yyyy-MM-dd HH:mm:ss");
          DateTime dtTime = parseFormat.parse('$elementSet $time');

          DateFormat showFormat = DateFormat("hh:mm a");
          String strTime = showFormat.format(dtTime);

          lstTime.add(strTime);
        });

        DateFormat parseFormat = DateFormat("yyyy-MM-dd");
        DateTime dtTime = parseFormat.parse(elementSet);

        DateFormat showFormat = DateFormat("dd-MM-yyyy");
        String strTime = showFormat.format(dtTime);
        mapApptStruct[strTime] = lstTime;
      }
    });

    setState(() {});
  }

  double profileImageSize = 125;

  @override
  Widget build(BuildContext context) {
    double deviceWidth = MediaQuery.of(context).size.width;
    double deviceHeight = MediaQuery.of(context).size.height;
    double headerSize = (deviceHeight / 3.75);
    double devicePixelRatio = MediaQuery.of(context).devicePixelRatio;
    double coverHeight = Utility.getCoverHeight(context);
    double profilePicSize = Utility.getProfilePicSize(context);
    double topPadding = coverHeight - (profilePicSize / 2);
    return Scaffold(
      /*appBar: AppBar(
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Theme.of(context).primaryColor),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text(
          "Update Profile",
          style: TextStyle(
            fontSize: 22.0,
            ,
            fontWeight: FontWeight.bold,
            color: Theme.of(context).primaryColor,
          ),
        ),
        backgroundColor: Theme.of(context).accentColor,
      ),*/
      extendBodyBehindAppBar: true,
      floatingActionButton: GestureDetector(
        onTap: () {
          Map<String, dynamic> mapSchedule = Map();
          mapSchedule['id_user'] = widget.patient.id_user;
          mapSchedule['patient_name'] = '${widget.patient.patient_firstname} ${widget.patient.patient_lastname}';
          mapSchedule['avatar_image'] = widget.patient.patient_image;
          mapSchedule['patient_firebase_token'] = widget.patient.firebase_token;
          mapSchedule['appointment'] = [];
          ScheduledConversation scheduleConversation = ScheduledConversation.fromMap(mapSchedule);
          // map['patient_firebase_token']=widget.patient.
          AppointmentType aType = AppointmentType.SCHEDULED_APPOINTMENT;
          Navigator.pushNamed(context, PersonalChat.routeName, arguments: ["${widget.patient.id_user}", aType, scheduleConversation]);
        },
        child: Container(
          width: 60,
          height: 60,
          alignment: Alignment.center,
          decoration: BoxDecoration(shape: BoxShape.circle, color: Theme.of(context).accentColor),
          child: Icon(
            Icons.chat,
            color: Colors.white,
          ),
        ),
      ),
//      backgroundColor: Theme.of(context).accentColor,
      appBar: AppBar(
//        backgroundColor: Colors.transparent,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Theme.of(context).primaryColor),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        backgroundColor: Color(0x44000000),
        elevation: 0,
        title: Text(
          "${widget.patient.patient_firstname} ${widget.patient.patient_lastname}",
          style: TextStyle(
            fontSize: 22.0,
            fontWeight: FontWeight.bold,
            color: Theme.of(context).primaryColor,
          ),
        ),
      ),
      body: SingleChildScrollView(
          child: FutureBuilder(
        future: ApiRequest.getSingleCustomer(id_customer: '${widget.patient.id_user}'),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            debugPrint("result : ${snapshot.data}");
            List<String> lstAddress = [];

            // List<String> lstDiseases = ['Autoimmune Diseases', 'COVID19 & Infectious Diseases', 'Allergies & Asthma', 'Cancer', 'Celiac Disease', 'Heart Disease', 'Liver Disease'];

            SingleCustomer mapData = SingleCustomer.fromMap(snapshot.data['data']);
            List<dynamic> listDisease = [];
            if (mapData.disease is List) {
              mapData.disease.forEach((element) {
                listDisease.add(element);
              });
            }
            // mapData.disease =
            if (mapData.address != null && mapData.address != '') {
              lstAddress.add(mapData.address);
            }
            if (mapData.city != null && mapData.city != '') {
              lstAddress.add(mapData.city);
            }
            if (mapData.state != null && mapData.state != '') {
              lstAddress.add(mapData.state);
            }
            return Container(
              constraints: BoxConstraints(
                minWidth: MediaQuery.of(context).size.width,
                minHeight: MediaQuery.of(context).size.height,
              ),
              child: Column(
                children: [
                  Stack(
                    children: [
                      Container(
                        constraints: BoxConstraints(
                          minWidth: MediaQuery.of(context).size.width,
                          minHeight: coverHeight,
                        ),
                        child: Stack(
                          alignment: Alignment.bottomRight,
                          children: [
                            Container(
                              height: coverHeight,
                              width: MediaQuery.of(context).size.width,
                              child: FancyShimmerImage(
                                boxFit: BoxFit.cover,
                                imageUrl: mapData.cover_image ?? Const.defaultCoverUrl,
                                shimmerBaseColor: Colors.white,
                                shimmerHighlightColor: Theme.of(context).accentColor,
                                //                          shimmerBackColor: Colors.blue,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: coverHeight - (profileImageSize / 2), left: (MediaQuery.of(context).size.width / 2) - (profileImageSize / 2)),
                        height: profileImageSize,
                        width: profileImageSize,
                        decoration: new BoxDecoration(
                          color: const Color(0xff7c94b6),
                          borderRadius: new BorderRadius.all(new Radius.circular(100.0)),
                          border: new Border.all(
                            color: Theme.of(context).accentColor,
                            width: 1.0,
                          ),
                        ),
                        child: ClipOval(
                          child: FancyShimmerImage(
                            boxFit: BoxFit.cover,
                            imageUrl: mapData.avatar_image ?? Const.defaultProfileUrl,
                            shimmerBaseColor: Colors.white,
                            shimmerHighlightColor: Theme.of(context).accentColor,
                            //                          shimmerBackColor: Colors.blue,
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  lstAddress.length > 0
                      ? Text(
                          "${lstAddress.join(', ')}",
                          style: TextStyle(fontSize: 14),
                        )
                      : Container(),
                  SizedBox(
                    height: 5,
                  ),
                  mapData.mobile != null
                      ? Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.phone_android_outlined,
                              color: Theme.of(context).accentColor,
                            ),
                            SizedBox(width:10),
                            Text(
                              "${mapData.mobile}",
                              style: TextStyle(fontSize: 14),
                            ),
                          ],
                        )
                      : Container(),
                  Container(
                    // padding: EdgeInsets.all(5),
                    margin: EdgeInsets.all(10),
                    child: Column(
                      children: [
                        Container(
                          alignment: Alignment.centerLeft,
                          width: MediaQuery.of(context).size.width,
                          height: 40,
                          padding: EdgeInsets.only(left: 20),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(100)),
                            color: Colors.grey.withOpacity(0.1),
                          ),
                          child: Text(
                            "Appointments",
                            style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Container(
                          constraints: BoxConstraints(
                            minWidth: MediaQuery.of(context).size.width,
                            // minHeight: MediaQuery.of(context).size.height,
                          ),
                          padding: mapApptStruct.length == 0 ? EdgeInsets.only(left: 50, right: 50, top: 10, bottom: 10) : EdgeInsets.all(10),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(mapApptStruct.length == 0 ? 100 : 10)),
                            color: mapApptStruct.length == 0 ? Colors.red.withOpacity(0.1) : Colors.grey.withOpacity(0.1),
                          ),
                          child: mapApptStruct.length == 0
                              ? Container(
                                  child: Text(
                                    'You have no scheduled appointment with this patient',
                                    style: TextStyle(
                                      fontSize: 18,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                )
                              : Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: mapApptStruct.entries.map((e) {
                                    /* debugPrint("e.key : ${e.key}");
                                String appointmentDate = e.key;
                                DateFormat dtFormat = DateFormat('dd-MM-yyyy');
                                DateTime destDate = dtFormat.parse(appointmentDate);
                                DateTime now=DateFormat('dd-MM-yyyy').parse(DateFormat('dd-MM-yyyy').format(DateTime.now()));
                                Color aptColor=Colors.red;
                                if(destDate.isBefore(now)) {
                                  aptColor = Colors.red;
                                }else if(destDate.isAfter(now)){
                                  aptColor = Colors.green;
                                }*/
                                    return Container(
                                      margin: EdgeInsets.only(top: 5),
                                      padding: EdgeInsets.only(left: 5, right: 5, top: 2, bottom: 2),
                                      /*decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5),
                                      border: Border.all(color: Colors.black,width: 1)
                                  ),*/
                                      child: Wrap(
                                        crossAxisAlignment: WrapCrossAlignment.center,
                                        direction: Axis.horizontal,
                                        children: [
                                          Container(
                                            padding: EdgeInsets.all(5),
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.only(topRight: Radius.circular(100), bottomRight: Radius.circular(100)),
                                                // color: Theme.of(context).accentColor,
                                                gradient: LinearGradient(
                                                  colors: [Colors.red[200], Colors.blue[200]],
                                                  begin: const FractionalOffset(0.0, 0.0),
                                                  end: const FractionalOffset(0.5, 0.0),
                                                  stops: [0.0, 1.0],
                                                  tileMode: TileMode.clamp,
                                                )),
                                            child: Text(
                                              '${e.key}',
                                              style: TextStyle(fontSize: 12.0, fontWeight: FontWeight.bold, color: Colors.white),
                                            ),
                                          ),
                                          SizedBox(
                                            width: 5,
                                          ),
                                          Wrap(
                                            direction: Axis.horizontal,
                                            children: (e.value as List<dynamic>).asMap().entries.map((eV) {
                                              DateFormat dtFormat = DateFormat('dd-MM-yyyy hh:mm a');
                                              DateTime destDate = dtFormat.parse('${e.key} ${eV.value}');
                                              DateTime now = DateTime.now();
                                              Color aptColor = Colors.red;
                                              if (destDate.isBefore(now)) {
                                                aptColor = Colors.red;
                                              } else if (destDate.isAfter(now)) {
                                                aptColor = Colors.green;
                                              }
                                              return Container(
                                                  padding: EdgeInsets.only(left: 5, right: 5),
                                                  margin: EdgeInsets.only(left: 2, right: 2, top: 2, bottom: 2),
                                                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(100), color: aptColor),
                                                  child: Text(
                                                    eV.value,
                                                    style: TextStyle(color: Colors.white),
                                                  ));
                                            }).toList(),
                                          )
                                        ],
                                      ),
                                    );
                                  }).toList(),
                                ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Container(
                          height: 40,
                          padding: EdgeInsets.only(left: 20, top: 5, bottom: 5),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(100)),
                            color: Colors.grey.withOpacity(0.1),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "Notes",
                                style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                              ),
                              Container(
                                width: 40,
                                height: 40,
                                alignment: Alignment.center,
                                decoration: BoxDecoration(shape: BoxShape.circle, color: Theme.of(context).accentColor),
                                child: GestureDetector(
                                  onTap: () async {
                                    final result = await Navigator.of(context).pushNamed(AddNotesScreen.routeName, arguments: [mapData]);
                                    if (result) {
                                      setState(() {});
                                    }
                                  },
                                  child: Icon(
                                    Icons.note_add,
                                    color: Colors.white,
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Container(
                          alignment: Alignment.topLeft,
                          padding: EdgeInsets.only(left: 10, right: 10),
                          child: FutureBuilder(
                            future: ApiRequest.getNoteListing(doctor_id: '${appUserModel.id}', patient_id: '${widget.patient.id_user}'),
                            builder: (context, snapshot) {
                              if (snapshot.hasData) {
                                List<dynamic> lstNotes = snapshot.data['data'];
                                debugPrint("lstNotes api : ${lstNotes}");
                                return lstNotes.length == 0
                                    ? Container(
                                        constraints: BoxConstraints(
                                          minWidth: MediaQuery.of(context).size.width,
                                          // minHeight: MediaQuery.of(context).size.height,
                                        ),
                                        padding: EdgeInsets.only(left: 50, right: 50, top: 10, bottom: 10),
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(Radius.circular(100)),
                                          color: Colors.red.withOpacity(0.1),
                                        ),
                                        child: Text(
                                          'You have not added any notes to this patient. Please add the appropriate note for this patient',
                                          style: TextStyle(
                                            fontSize: 18,
                                          ),
                                          textAlign: TextAlign.center,
                                        ))
                                    : Wrap(
                                        alignment: WrapAlignment.start,
                                        direction: Axis.horizontal,
                                        children: lstNotes.asMap().entries.map((e) {
                                          return GestureDetector(
                                            onTap: () {
                                              showNoteDialog(context, devicePixelRatio: devicePixelRatio, title: e.value['note_title'], image: e.value['noteImagePath'], description: e.value['note']);
                                            },
                                            child: Container(
                                              decoration: BoxDecoration(shape: BoxShape.circle),
                                              constraints: BoxConstraints(maxWidth: 150),
                                              margin: EdgeInsets.only(right: 10),
                                              child: Chip(
                                                avatar: CircleAvatar(
                                                  backgroundColor: Colors.grey.shade800,
                                                  child: ClipOval(
                                                    child: FancyShimmerImage(
                                                      imageUrl: e.value['noteImagePath'] ?? Const.defaultProfileUrl,
                                                      shimmerBaseColor: Colors.white,
                                                      shimmerHighlightColor: config.Colors().mainColor(1),
                                                      shimmerBackColor: Colors.green,
                                                      boxFit: BoxFit.cover,
                                                      errorWidget: Image.network('https://i0.wp.com/www.dobitaobyte.com.br/wp-content/uploads/2016/02/no_image.png?ssl=1'),
                                                    ),
                                                  ),
                                                ),
                                                label: Text(e.value['note_title']),
                                              ),
                                            ),
                                          );
                                        }).toList(),
                                      );
                              } else {
                                return SizedBox(
                                    height: 100,
                                    child: LoadingWidget(
                                      initialData: true,
                                    ));
                              }
                            },
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Container(
                          height: 40,
                          padding: EdgeInsets.only(left: 20, top: 5, bottom: 5, right: 20),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(100)),
                            color: Colors.grey.withOpacity(0.1),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "Diseases",
                                style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        listDisease.length == 0
                            ? Container(
                                constraints: BoxConstraints(
                                  minWidth: MediaQuery.of(context).size.width,
                                  // minHeight: MediaQuery.of(context).size.height,
                                ),
                                padding: EdgeInsets.only(left: 50, right: 50, top: 10, bottom: 10),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(100)),
                                  color: Colors.red.withOpacity(0.1),
                                ),
                                child: Text(
                                  'This patient does not have any diseases',
                                  style: TextStyle(
                                    fontSize: 18,
                                  ),
                                  textAlign: TextAlign.center,
                                ))
                            : Wrap(
                                direction: Axis.horizontal,
                                children: listDisease.asMap().entries.map((e) {
                                  return Container(
                                    margin: EdgeInsets.only(right: 10),
                                    child: Chip(
                                      /* avatar: CircleAvatar(
                                    backgroundColor: Colors.grey.shade800,
                                    child: Text('AB'),
                                  ),*/
                                      label: Text(e.value['dieases_title']),
                                    ),
                                  );
                                }).toList(),
                              ),
                        SizedBox(
                          height: 10,
                        ),
                      ],
                    ),
                  )
                ],
              ),
            );
          } else if (snapshot.hasError) {
            return Container(
              child: Center(
                child: Text(snapshot.error.toString()),
              ),
            );
          } else {
            return LoadingWidget(
              initialData: true,
            );
          }
        },
      )),
    );
  }

  void showNoteDialog(BuildContext context, {double devicePixelRatio, String image, title, description}) async {
    return showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text(title),
        content: SingleChildScrollView(
          child: Container(
            color: Colors.white,
            constraints: BoxConstraints(
              minHeight: MediaQuery.of(context).size.height,
              minWidth: MediaQuery.of(context).size.width,
            ),
            child: Column(
              children: [
                Container(
                  width: double.infinity,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: FancyShimmerImage(
                      boxFit: BoxFit.fill,
                      imageUrl: image,
                      shimmerBaseColor: Colors.white,
                      shimmerHighlightColor: MyColors.backgroundColor,
                      shimmerBackColor: Colors.green,
                    ),
                  ),
                ),
                Text(description),
              ],
            ),
          ),
        ),
        actions: <Widget>[
          ElevatedButton(
            onPressed: () {
              Navigator.of(context).pop(false);
            },
            child: Text("Cancel"),
            style: ElevatedButton.styleFrom(
              primary: Theme.of(context).accentColor,
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget card(int id, String image, String title, String subTitle, String rank) {
    return GestureDetector(
      onTap: () {
        // Navigator.of(context).pushNamed(DoctorProfile.routeName,arguments: ["${id}",title]);
      },
      child: Stack(
        children: <Widget>[
          Container(
            margin: const EdgeInsets.only(top: 20.0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8.0),
              boxShadow: [BoxShadow(color: Theme.of(context).primaryColor.withOpacity(0.1), offset: Offset(0, 4), blurRadius: 10)],
            ),
            width: 140.0,
            height: 140.0,
            child: Card(
              elevation: 0.2,
              child: Wrap(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 0.0, vertical: 40.0),
                    child: ListTile(
                      title: Text(
                        title,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontSize: 10.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      subtitle: Column(
                        children: <Widget>[
                          Text(
                            subTitle ?? "",
                            textAlign: TextAlign.left,
                            style: TextStyle(fontSize: 10.0),
                          ),
                          Row(
                            children: <Widget>[
                              Icon(
                                Icons.star,
                                color: Colors.yellow,
                              ),
                              Text(
                                rank ?? "",
                                style: TextStyle(),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            width: 140,
            height: 60,
            alignment: Alignment.center,
            child: ballcard(image, Colors.transparent),
          ),
        ],
      ),
    );
  }

  Widget ballcard(String image, Color color) {
    print("----image -111: $image");
    return Container(
      height: 60,
      width: 60.0,
      child: ClipOval(
        child: FancyShimmerImage(
          imageUrl: image ?? Const.defaultProfileUrl,
          shimmerBaseColor: Colors.white,
          shimmerHighlightColor: config.Colors().mainColor(1),
          shimmerBackColor: Colors.green,
          errorWidget: Image.network('https://i0.wp.com/www.dobitaobyte.com.br/wp-content/uploads/2016/02/no_image.png?ssl=1'),
        ),
      ),
    );
  }
}
