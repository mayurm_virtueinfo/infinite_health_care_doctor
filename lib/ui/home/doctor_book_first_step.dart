import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_html/style.dart';
import 'package:infinite_health_care_doctor/config/const.dart';
import 'package:infinite_health_care_doctor/models/doctor_model.dart';
import 'package:infinite_health_care_doctor/config/app_config.dart' as config;
import 'package:infinite_health_care_doctor/models/doctor_profile_model.dart';
import 'package:infinite_health_care_doctor/ui/home/doctor_book_second_step.dart';
import 'package:infinite_health_care_doctor/ui/home/doctor_profile.dart';
import 'package:infinite_health_care_doctor/utils/my_toast.dart';
class DoctorBookFirstStep extends StatefulWidget {
  DoctorProfileModel doctorProfileModel;
  static const String routeName = '/DoctorBookFirstStep';

  DoctorBookFirstStep({this.doctorProfileModel});
  @override
  _DoctorBookFirstStepState createState() => _DoctorBookFirstStepState();
}

class _DoctorBookFirstStepState extends State<DoctorBookFirstStep> {
  List<String> morningList=[];
  List<String> afternoonList=[];
  List<String> nightList=[];
  String selectedChoice = "";
  @override
  Widget build(BuildContext context) {

   return Scaffold(
     appBar: AppBar(
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color:Theme.of(context).primaryColor ),
          onPressed: (){
            Navigator.of(context).pop();
          },
        ),
        backgroundColor: Theme.of(context).accentColor,
        title: Text(
          'Select a time slot',
          style: TextStyle(
            fontSize:20.0,
            fontWeight: FontWeight.bold,
            color: Theme.of(context).primaryColor,
          ),
        ),

      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                    height: 40,
                    padding: const EdgeInsets.only(left:0.0,right: 0.0,bottom: 8.0),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(bottomLeft:Radius.circular(25.0),bottomRight: Radius.circular(25.0)),
                      color: Theme.of(context).accentColor,
                    ),
                  ),
                Container(
                  padding:EdgeInsets.only(top:12.0,right: 12.0,left: 12.0,bottom: 12.0),
                  margin:EdgeInsets.only(right: 12.0,left: 12.0,bottom: 12.0,top: 0),
                  width: double.maxFinite,
                  decoration: BoxDecoration(            
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          ball(widget.doctorProfileModel.avatar_image, Colors.transparent),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                widget.doctorProfileModel.title,
                                style: TextStyle(
                                  fontSize: 14.0,

                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Container(
                                width: 200,
                                child:Html(
                                  data:widget.doctorProfileModel.description ,
                                  shrinkWrap: true,
                                  style: {
                                    'p':Style(
                                      textAlign: TextAlign.left,
                                    )
                                  },
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                      SizedBox(height: 15.0,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            child: IconButton(
                              onPressed: (){},
                              icon: Icon(Icons.chevron_left),
                            ),
                          ),
                          Container(
                            child: Text(
                              'Tomorrow,9 Dec',
                              style: TextStyle(
                                  fontSize: 12.0,
                                  fontWeight: FontWeight.bold,
                                ),
                            ),
                          ),
                          Container(
                            child: IconButton(
                              onPressed: (){},
                              icon: Icon(Icons.chevron_right),
                            ),
                          ),
                      
                        ],
                      ),
                      SizedBox(height: 15.0,),
                      Container(
                        child: Stack(
                          children: <Widget>[
                            Container(
                              padding: const EdgeInsets.all(12),
                              margin: const EdgeInsets.only(top: 14),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(16),
                                border: Border.all(width: 1,color: Colors.grey.withOpacity(0.8)),
                                color: Colors.grey[100].withOpacity(0.4),
                              ),
                              child: Wrap(
                                children: _buildChoiceList(morningList),
                              ),
                            ),
                            Container(
                              padding: const EdgeInsets.only(top: 6,bottom: 6,left: 16,right: 16,),
                              margin: const EdgeInsets.only(left: 30),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                //color: Colors.grey,
                                gradient: LinearGradient(
                                  colors: [Colors.red[200],Colors.blue[200]],
                                  begin: const FractionalOffset(0.0, 0.0),
                                  end: const FractionalOffset(0.5, 0.0),
                                  stops: [0.0,1.0],
                                  tileMode: TileMode.clamp,
                                )
                              ),
                              child: Text(
                                "Morning",
                                style: TextStyle(
                                  fontSize: 10.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 14),
                        child: Stack(
                          children: <Widget>[
                            Container(
                              padding: const EdgeInsets.all(12),
                              margin: const EdgeInsets.only(top: 14),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(16),
                                border: Border.all(width: 1,color: Colors.grey.withOpacity(0.8)),
                                color: Colors.grey[100].withOpacity(0.4),
                              ),

                              child:Wrap(
                                children: _buildChoiceList(afternoonList),
                              ), 
                            ),
                            Container(
                              padding: const EdgeInsets.only(top: 6,bottom: 6,left: 16,right: 16,),
                              margin: const EdgeInsets.only(left: 30),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                //color: Colors.grey,
                                gradient: LinearGradient(
                                  colors: [Colors.lightBlue[200],Colors.lightGreen[200]],
                                  begin: const FractionalOffset(0.0, 0.0),
                                  end: const FractionalOffset(0.5, 0.0),
                                  stops: [0.0,1.0],
                                  tileMode: TileMode.clamp,
                                )
                              ),
                              child: Text(
                                "Afternoon",
                                style: TextStyle(
                                  fontSize: 10.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 14),
                        child: Stack(
                          children: <Widget>[
                            Container(
                              padding: const EdgeInsets.all(12),
                              margin: const EdgeInsets.only(top: 14),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(16),
                                border: Border.all(width: 1,color: Colors.grey.withOpacity(0.8)),
                                color: Colors.grey[100].withOpacity(0.4),
                              ),
                              child:Wrap(
                                children: _buildChoiceList(nightList)
                                  
                              ) 
                            ),
                            Container(
                              padding: const EdgeInsets.only(top: 6,bottom: 6,left: 16,right: 16,),
                              margin: const EdgeInsets.only(left: 30),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                //color: Colors.grey,
                                gradient: LinearGradient(
                                  colors: [Colors.yellow[100],Colors.green[200]],
                                  begin: const FractionalOffset(0.0, 0.0),
                                  end: const FractionalOffset(0.5, 0.0),
                                  stops: [0.0,1.0],
                                  tileMode: TileMode.clamp,
                                )
                              ),
                              child: Text(
                                "Evening & Night",
                                style: TextStyle(
                                  fontSize: 10.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),    
                    ],
                  ),
                ),
              ],
            ),

          ],
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        elevation: 0,
        color: Colors.transparent,
        child: Container(
              padding:EdgeInsets.only(right: 0.0,left: 50.0,bottom: 0.0,top: 0),
              margin:EdgeInsets.all(12.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20.0),
                border: Border.all(width: 1,color: Colors.grey.withOpacity(0.6)),
              ),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text(
                        'Give Feedback',
                        style: TextStyle(
                          fontSize: 12.0,
                          color: Colors.grey
                        ),
                      ),
                    ],
                  ),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      elevation: 0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20.0)
                      ),
                      primary: Theme.of(context).accentColor,
                      // materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    ),

                    onPressed: (){
                      debugPrint("Selected Choice : $selectedChoice");
                      if(selectedChoice == ""){
                        MyToast.showToast("Please selecte time slot",context);
                        return;
                      }
//                      Navigator.of(context).pushNamed(DoctorBookSecondeStep.routeName);
                      Navigator.of(context).pushNamed(DoctorBookSecondeStep.routeName, arguments: [widget.doctorProfileModel]);
                    },

                    child:Container(
                      margin: EdgeInsets.only(left: 55.0,right: 55.0,top: 12,bottom: 12),
                      child:Text(
                        'Book',
                        style: TextStyle(
                          fontSize: 12.0,
                          color: Theme.of(context).primaryColor,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                  )
                ],
              )
            ),
      ),
    );
  }
  Widget ball(String image,Color color){
    return Container(
      height: 60,width: 60.0,
      child: ClipOval(
        child: FancyShimmerImage(
          imageUrl: image??Const.defaultProfileUrl,
          shimmerBaseColor: Colors.white,
          shimmerHighlightColor: config.Colors().mainColor(1),
          shimmerBackColor: Colors.green,
          errorWidget: Image.network('https://i0.wp.com/www.dobitaobyte.com.br/wp-content/uploads/2016/02/no_image.png?ssl=1'),
        ),
      )
      ,

    );
  }
  _buildChoiceList(List<String> list) {
    List<Widget> choices = [];
    list.forEach((item) {
      choices.add(
        Container(
          padding: const EdgeInsets.all(2.0),
          child: ChoiceChip(
            label: Text(item),
            selectedColor: config.Colors().mainColor(1),
            selected: selectedChoice == item,
            onSelected: (selected) {
              setState(() {
                selectedChoice = item;
              });
            },
          ),
        )
      );
    }
    );return choices;
  }
}