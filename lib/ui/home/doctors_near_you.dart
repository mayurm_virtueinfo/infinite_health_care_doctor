import 'package:flutter/material.dart';
import 'package:infinite_health_care_doctor/api/api_request.dart';
import 'package:infinite_health_care_doctor/ui/home_screen.dart';
import 'package:infinite_health_care_doctor/utils/my_toast.dart';
import 'package:infinite_health_care_doctor/widgets/app_button_circular_progress.dart';
import 'package:infinite_health_care_doctor/widgets/doctors_card_widget.dart';
import 'package:infinite_health_care_doctor/widgets/doctors_near_you_widget.dart';
import 'package:infinite_health_care_doctor/widgets/searchWidget.dart';
import 'package:infinite_health_care_doctor/models/doctor_near_you_model.dart' as doctorNearYouModel;
class DoctorsNearYou extends StatefulWidget {
  static const String routeName = '/DoctorsNearYou';
  final String latitude;
  final String longitude;
  DoctorsNearYou({this.latitude,this.longitude});
  @override
  _DoctorsNearYouState createState() => _DoctorsNearYouState();
}

class _DoctorsNearYouState extends State<DoctorsNearYou> {
  @override
  void initState() {
    super.initState();
  }
  @override
  void dispose() {
    // Clean up the focus node when the Form is disposed.
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color:Theme.of(context).primaryColor ),
          onPressed: (){
//            Navigator.of(context).pushNamed(HomeScreen.routeName, arguments:[widget.currentUser.name,widget.currentUser.phoneNumber]);
            Navigator.of(context).pop(context);
          },
        ),
        backgroundColor: Theme.of(context).accentColor,
        title: Text("Doctors Near You",
          style: TextStyle(
            fontSize:22.0,
            fontWeight: FontWeight.bold,
            color: Theme.of(context).primaryColor,
          ),
        ),

      ),
      body: SingleChildScrollView(
        child:ConstrainedBox(
          constraints: BoxConstraints(maxHeight: MediaQuery.of(context).size.height),
          child: Container(
            height: double.infinity,
            child: Column(
              children: <Widget>[
                Stack(
                  children: <Widget>[
                     Container(
                      height: 20,
                      padding: const EdgeInsets.only(top:0,left:12.0,right: 12.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(bottomLeft:Radius.circular(25.0),bottomRight: Radius.circular(25.0)),
                        color: Theme.of(context).accentColor,
                      ),
                      ),
                    Padding(
                      padding: const EdgeInsets.only(top: 0.0,left: 12.0,right: 12.0),
                      child:SearchBarWidget(),
                    ),
                  ],
                ),
                Expanded(
                  child: Container(
                    decoration: BoxDecoration(
                     color: Colors.transparent,
                    ),
                    child: FutureBuilder(
                      future: ApiRequest.postDoctorsNearYou(
                          lat: widget.latitude,
                          lng: widget.longitude,
                          radius: "500",
                          limit: "10",
                          page: "0"
                      ),
                      builder: (context, snapshot) {

                        if(snapshot.hasData) {


                          doctorNearYouModel.DoctorNearYouModelList dList = doctorNearYouModel.DoctorNearYouModelList.fromSnapshot(snapshot);


                          return ListView.separated(
                            shrinkWrap: true,
                            primary: false,
                            itemCount: dList.doctorsNearYou.length,
                            separatorBuilder: (context, index) {
                              return SizedBox(height: 4.0);
                            },
                            itemBuilder: (context, index) {
                              debugPrint("ID : ${dList.doctorsNearYou.elementAt(index).id}");
                              return DoctorsNearYouWidget(
                                doctorsNearYou: dList.doctorsNearYou.elementAt(index),
                              );
                            },
                          );
                        } else if(snapshot.hasError){
                          MyToast.showToast(snapshot.error.toString(),context);
                          return Container();
                        } else{
                          return Center(
                            child:CircularProgressIndicator() ,
                          );
                        }
                      }
                      ,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

}
