import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_html/style.dart';
import 'package:infinite_health_care_doctor/config/app_config.dart' as config;
import 'package:infinite_health_care_doctor/config/const.dart';
import 'package:infinite_health_care_doctor/main.dart';
import 'package:infinite_health_care_doctor/models/doctor_profile_model.dart';
import 'package:infinite_health_care_doctor/ui/home/credit_card_payment.dart';


class DoctorBookSecondeStep extends StatefulWidget {
  static const String routeName = '/DoctorBookSecondeStep';
  DoctorProfileModel doctorProfileModel;
  DoctorBookSecondeStep({this.doctorProfileModel});
  @override
  _DoctorBookSecondeStepState createState() => _DoctorBookSecondeStepState();
}

class _DoctorBookSecondeStepState extends State<DoctorBookSecondeStep> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  TextEditingController _nameController;
  TextEditingController _emailController;
  TextEditingController _phoneNumberController;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _nameController = TextEditingController();
    _emailController = TextEditingController();
    _phoneNumberController = TextEditingController();
  }
  @override
  Widget build(BuildContext context) {
    _nameController.text = "${appUserModel.fullname}";
    _emailController.text = appUserModel.email;
    _phoneNumberController.text = appUserModel.mobile??'';
    
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Theme.of(context).primaryColor),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        backgroundColor: Theme.of(context).accentColor,
        title: Text(
          'Select a time slot',
          style: TextStyle(
            fontSize: 20.0,
            fontWeight: FontWeight.bold,
            color: Theme.of(context).primaryColor,
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                  height: 40,
                  padding:
                      const EdgeInsets.only(left: 0.0, right: 0.0, bottom: 8.0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(25.0),
                        bottomRight: Radius.circular(25.0)),
                    color: Theme.of(context).accentColor,
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(
                      top: 20.0, right: 12.0, left: 12.0, bottom: 12.0),
                  margin: EdgeInsets.only(
                      right: 12.0, left: 12.0, bottom: 12.0, top: 0),
                  width: double.maxFinite,
                  decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          ball(widget.doctorProfileModel.avatar_image, Colors.transparent),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                widget.doctorProfileModel.title,
                                style: TextStyle(
                                  fontSize: 14.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Container(
                                width: 200,
                                child: Html(
                                  data:widget.doctorProfileModel.description ,
                                  shrinkWrap: true,
                                  style: {
                                    'p':Style(
                                      textAlign: TextAlign.left,
                                    )
                                  },
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                      SizedBox(
                        height: 20.0,
                        child: Center(
                          child: Container(
                            height: 2.0,
                            color: Colors.grey.withOpacity(0.1),
                          ),
                        ),
                      ),
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "DATE & Time",
                                  style: TextStyle(
                                    fontSize: 14.0,
                                    color: Colors.grey,
                                  ),
                                ),
                                SizedBox(
                                  height: 15.0,
                                ),
                                Text(
                                  "9 Dec 4:45PM",
                                  style: TextStyle(
                                      fontSize: 14.0,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                            SizedBox(
                              height: 60.0,
                              width: 2,
                              child: Center(
                                child: Container(
                                  height: 60.0,
                                  color: Colors.grey.withOpacity(0.1),
                                ),
                              ),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "Consultation Fees",
                                  style: TextStyle(
                                    fontSize: 14.0,
                                    color: Colors.grey,
                                  ),
                                ),
                                SizedBox(
                                  height: 15.0,
                                ),
                                Text(
                                  "\₹ 600",
                                  style: TextStyle(
                                    fontSize: 14.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 20.0,
                        child: Center(
                          child: Container(
                            height: 2.0,
                            color: Colors.grey.withOpacity(0.1),
                          ),
                        ),
                      ),
                      Container(
                        child: Form(
                          key: _formKey,
                          child: Column(
                            children: <Widget>[
                              Container(

                                margin: const EdgeInsets.only(top: 12.0),
                                padding: const EdgeInsets.only(
                                    left: 12.0, right: 12.0),
                                decoration: BoxDecoration(
                                  border: Border.all(
                                      width: 1.5, color: Color(0xdddddddd)),
                                  borderRadius: BorderRadius.circular(12.0),
                                  color: Colors.grey[100].withOpacity(0.4),
                                ),
                                child: TextFormField(
                                  controller: _nameController,
                                  maxLines: 1,
                                  // attribute: "Full Name",
                                  initialValue: '', //for testing
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.all(0),
                                    hintText: "Name",
                                    hintStyle: TextStyle(),
                                    border: InputBorder.none,
                                  ),

                                  validator: (value) {
                                    if(value.length>70){
                                      return "Invalid length";
                                    }
                                    if(value.length==0){
                                      return "Required";
                                    }
                                    return null;
                                  },
                                ),
                              ),
                              Container(
                                margin: const EdgeInsets.only(top: 12.0),
                                padding: const EdgeInsets.only(
                                    left: 12.0, right: 12.0),
                                decoration: BoxDecoration(
                                  border: Border.all(
                                      width: 1.5, color: Color(0xdddddddd)),
                                  borderRadius: BorderRadius.circular(12.0),
                                  color: Colors.grey[100].withOpacity(0.4),
                                ),
                                child: TextFormField(
                                  controller: _emailController,
                                  maxLines: 1,
                                  // attribute: "Email",
                                  initialValue: '', //for testing
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.all(0),
                                    hintText: "E-mail",
                                    hintStyle: TextStyle(),
                                    border: InputBorder.none,
                                  ),

                                  validator: (value) {
                                    if(value.length>70){
                                      return "Invalid length";
                                    }
                                    if(value.length==0){
                                      return "Required";
                                    }
                                    return null;
                                  },
                                ),
                              ),
                              Container(
                                margin: const EdgeInsets.only(top: 12.0),
                                padding: const EdgeInsets.only(
                                    left: 12.0, right: 12.0),
                                decoration: BoxDecoration(
                                  border: Border.all(
                                      width: 1.5, color: Color(0xdddddddd)),
                                  borderRadius: BorderRadius.circular(12.0),
                                  color: Colors.grey[100].withOpacity(0.4),
                                ),
                                child: TextFormField(
                                  controller: _phoneNumberController,
                                  maxLines: 1,
                                  // attribute: "phone Number",
                                  initialValue: '',
                                  //for testing
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.all(0),
                                    border: InputBorder.none,
                                    hintText: "Phone Number",
                                    hintStyle: TextStyle(),
//                                    prefixText: "+",
                                  ),
                                  keyboardType: TextInputType.number,
                                  validator: (value) {

                                    if(value.length==0){
                                      return "Required";
                                    }
                                    return null;
                                  },

                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 25.0,
                      ),
                      Container(
                        child: Text(
                          "By Booking this appointment you agree to the T&C",
                          style: TextStyle(
                              color: Colors.grey,
                              fontSize: 11.0,
                              fontWeight: FontWeight.bold,
                              ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        elevation: 0,
        color: Colors.transparent,
        child: Container(
            padding:
                EdgeInsets.only(right: 0.0, left: 50.0, bottom: 0.0, top: 0),
            margin: EdgeInsets.all(12.0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20.0),
              border: Border.all(width: 1, color: Colors.grey.withOpacity(0.6)),
            ),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      'Give Feedback',
                      style: TextStyle(
                          fontSize: 12.0,
                          color: Colors.grey),
                    ),
                  ],
                ),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    elevation: 0,
                    // materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0)),
                    primary: Theme.of(context).accentColor,
                  ),

                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      Navigator.of(context)
                          .pushNamed(CreditCardPayment.routeName, arguments: [widget.doctorProfileModel]);
                    }
                  },
                  child: Container(
                    margin: EdgeInsets.only(
                        left: 55.0, right: 55.0, top: 12, bottom: 12),
                    child: Text(
                      'Book',
                      style: TextStyle(
                          fontSize: 12.0,
                          color: Theme.of(context).primaryColor,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                )
              ],
            )),
      ),
    );
  }

  Widget ball(String image, Color color) {
    return Container(
      height: 60,width: 60.0,
      child: ClipOval(
        child: FancyShimmerImage(
          imageUrl: image??Const.defaultProfileUrl,
          shimmerBaseColor: Colors.white,
          shimmerHighlightColor: config.Colors().mainColor(1),
          shimmerBackColor: Colors.green,
          errorWidget: Image.network('https://i0.wp.com/www.dobitaobyte.com.br/wp-content/uploads/2016/02/no_image.png?ssl=1'),
        ),
      ),
    );
  }
}
