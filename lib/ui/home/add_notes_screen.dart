import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:infinite_health_care_doctor/api/api_request.dart';
import 'package:infinite_health_care_doctor/config/const.dart';
import 'package:infinite_health_care_doctor/models/user_model.dart';
import 'package:infinite_health_care_doctor/main.dart';
import 'package:infinite_health_care_doctor/utils/app_style.dart';
import 'package:infinite_health_care_doctor/utils/my_colors.dart';
import 'package:infinite_health_care_doctor/utils/my_toast.dart';
import 'package:infinite_health_care_doctor/utils/utility.dart';
import 'package:infinite_health_care_doctor/widgets/app_button.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:infinite_health_care_doctor/models/single_customer.dart';

class AddNotesScreen extends StatefulWidget {
  static const String routeName = "/AddNotesScreen";
  SingleCustomer singleCustomer ;

  AddNotesScreen({this.singleCustomer});
  @override
  _AddNotesScreenState createState() => _AddNotesScreenState();
}

class _AddNotesScreenState extends State<AddNotesScreen> {

  String fcmRegToken = "";
  Set<String> listRegistretionIds = Set();
  String userid = '';
  String name = '';
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  SharedPreferences prefs;
  File photo ;
  final _titleController = TextEditingController();
  final _descriptionController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool isLoading = false;
  String eventDateString='';
  Timestamp eventDate ;
  String event_id = UniqueKey().toString();
  /// Declare startTime to InitState
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double devicePixelRatio = MediaQuery.of(context).devicePixelRatio;;

    return WillPopScope(
      onWillPop: () async{
        Navigator.of(context).pop(false);
        return true;
      },
      child: Scaffold(

        appBar: AppBar(
          elevation: 0,
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Theme.of(context).primaryColor),
            onPressed: () {
              Navigator.of(context).pop(false);
            },
          ),
          title: Text(
            "Add Note",
            style: TextStyle(
              fontSize: 22.0,
              fontWeight: FontWeight.bold,
              color: Theme.of(context).primaryColor,
            ),
          ),
          backgroundColor: Theme.of(context).accentColor,
        ),
        body: SingleChildScrollView(
          child: Container(
            color: Colors.white,
            constraints: BoxConstraints(
              minHeight: MediaQuery.of(context).size.height,
              minWidth: MediaQuery.of(context).size.width,
            ),
            child: Column(
              children: [
                Stack(
                  children: <Widget>[
                    Container(
                      height: 150,
                      padding: const EdgeInsets.only(top: 40),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(bottomLeft: Radius.circular(25.0), bottomRight: Radius.circular(25.0)),
                        color: Theme.of(context).accentColor,
                      ),
                    ),
                    Stack(
                      children: [
                        Container(
                          constraints: BoxConstraints(
                            minWidth: MediaQuery.of(context).size.width,
                            minHeight: MediaQuery.of(context).size.height - (devicePixelRatio*90)),
                          margin: EdgeInsets.only(left: 14.0, right: 14.0,bottom: 14),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20.0),
                            color: Theme.of(context).primaryColor,
                          ),
                          child: SingleChildScrollView(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Form(
                                  key: _formKey,
                                  child: Container(
                                    child: Column(

                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        Container(
                                          margin: EdgeInsets.only(left:devicePixelRatio * 12,right:devicePixelRatio * 12,top: devicePixelRatio * 12, bottom: devicePixelRatio * 12),

                                          decoration: BoxDecoration(
                                            border: Border.all(width: 0.5,color: MyColors.backgroundColor),
                                            borderRadius: BorderRadius.circular(10)
                                          ),
                                          height: devicePixelRatio * 125,
                                          width: double.infinity,
                                          child: Stack(
                                            children: [
                                              photo == null ?Container(
                                                width: double.infinity,
                                                child: ClipRRect(
                                                  borderRadius: BorderRadius.circular(10),
                                                  child: FancyShimmerImage(
                                                    boxFit: BoxFit.fill,
                                                    imageUrl: Const.defaultCoverUrl,
                                                    shimmerBaseColor: Colors.white,
                                                    shimmerHighlightColor: MyColors.backgroundColor,
                                                    shimmerBackColor: Colors.green,
                                                  ),
                                                ),
                                              ):Container(
                                                width: double.infinity,
                                                child: Image.file(photo,fit: BoxFit.fill,),
                                              ),
                                              GestureDetector(
                                                onTap: (){
                                                  _settingModalBottomSheet(context);
                                                },
                                                child: Container(
                                                  width: double.infinity,
                                                  height: devicePixelRatio * 125,
                                                  child: Column(
                                                    mainAxisAlignment: MainAxisAlignment.center,
                                                    children: [
                                                      Icon(Icons.cloud_upload,size: 100,color: Colors.grey,),
                                                      Container(
                                                        padding: EdgeInsets.all(10),
                                                        decoration: BoxDecoration(
                                                          shape: BoxShape.rectangle,
                                                          color: Colors.grey,
                                                          borderRadius: BorderRadius.circular(20)
                                                        ),
                                                        child: Text("Upload Image",style: TextStyle(
                                                            fontSize: 18,
                                                            color: Colors.white
                                                        ),),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              )

                                            ],
                                          ),
                                        ),
//                        event title

                                        Container(
                                          child: TextFormField(
                                            validator: (text) {
                                              if (text.length == 0) {
                                                return "Title is required";
                                              }
                                              return null;
                                            },
                                            controller: _titleController,
                                            keyboardType: TextInputType.text,
                                            decoration: InputDecoration(
                                              counterText: "",
                                              errorStyle: AppStyles.errorStyle,
                                              focusedBorder: AppStyles.focusBorder,
                                              prefixIcon: Icon(
                                                Icons.title,
                                                color: MyColors.backgroundColor,
                                              ),
                                              labelStyle: TextStyle(color: MyColors.backgroundColor),
                                              border: OutlineInputBorder(),
                                              labelText: "Enter Note Title",
                                            ),
                                          ),
                                          padding: EdgeInsets.only(left: 20, right: 20),
                                        ),

//                        description
                                        Container(
                                          child: TextFormField(
                                            validator: (text) {
                                              if (text.length == 0) {
                                                return "Description is required";
                                              }
                                              return null;
                                            },
                                            maxLines: 6,
                                            keyboardType: TextInputType.multiline,
                                            controller: _descriptionController,
                                            decoration: InputDecoration(
                                                errorStyle: AppStyles.errorStyle,
                                                focusedBorder: AppStyles.focusBorder,
                                               /* prefixIcon: Icon(
                                                  Icons.description,
                                                  color: MyColors.backgroundColor,
                                                ),*/
                                                labelStyle: TextStyle(color: MyColors.backgroundColor),
                                                border: OutlineInputBorder(),
                                              hintText: "Enter Note Description",),
                                          ),
                                          padding: EdgeInsets.all(20),
                                        ),

                                        InkWell(
                                            onTap: () async {
                                              debugPrint('1');
                                              if (_formKey.currentState.validate()) {
                                                debugPrint('2');
                                                /*if(photo == null){
                                                  debugPrint('3');
                                                  MyToast.showToast("Please select note image",context);
                                                  return;
                                                }
                                                else {
                                                  debugPrint('3');
                                                  onClickPost();
                                                }*/
                                                onClickPost();
                                              }
                                              setState(() {});
                                            },
                                            child: isLoading
                                                ? Padding(
                                                  padding: EdgeInsets.all(15.0),
                                                  child: Container(
                                                      height: 55.0,
                                                      child: Center(
                                                        child: SizedBox(
                                                          height: 40,
                                                          width: 40,
                                                          child: CircularProgressIndicator(
                                                            strokeWidth: 1,
                                                            valueColor: AlwaysStoppedAnimation<Color>(Theme.of(context).accentColor),
                                                          ),
                                                        ),
                                                      )),
                                                )
                                                : AppButton(
                                                    text: "Post",
                                                  )),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        SizedBox(
                                          height: 20,
                                        ),
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
  void _settingModalBottomSheet(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: new Wrap(
              direction: Axis.horizontal,
              children: <Widget>[
                ListTile(
                    leading: new Icon(Icons.delete),
                    title: new Text('Remove'),
                    onTap: (){
                      _onClickRemovePhoto();
                    }
                ),
                ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text('Camera'),
                    onTap: () {
                      _onClickCameraPhoto();
                    }),
                ListTile(
                  leading: new Icon(Icons.photo),
                  title: new Text('Gallery'),
                  onTap: () {
                    _onClickGalleryPhoto();
                  },
                ),
              ],
            ),
          );
        });
  }

  void _onClickCameraPhoto() async {
    Navigator.pop(context);

    PickedFile pickedFile = await ImagePicker().getImage(source: ImageSource.camera);
    File image;
    if (pickedFile != null) {
      image = File(pickedFile.path);
    } else {
      image = null;
    }
    if (image != null) {
      File cropped = await  Utility.cropImage(image);
      debugPrint("cropped Image : ${cropped.toString()}");
      setImagePath(cropped);
    }
  }

  void setImagePath(imgPath) async {
    if (imgPath == null) {
      photo = null;
    } else {
      photo = imgPath;
    }
    setState(() {

    });
  }
  void _onClickGalleryPhoto() async {
    Navigator.pop(context);
    PickedFile pickedFile = await ImagePicker().getImage(source: ImageSource.gallery);
    File image;
    if (pickedFile != null) {
      image = File(pickedFile.path);
    } else {
      image = null;
    }
    if (image != null) {
      File cropped = await  Utility.cropImage(image);
      debugPrint("cropped Image : ${cropped.toString()}");
      setImagePath(cropped);
    }
  }


  void _onClickRemovePhoto() {
    Navigator.pop(context);
    setImagePath(null);
  }

  void onClickPost() async {

      try {
        setState(() {
          isLoading = true;
        });

        String noteTitle = _titleController.text;
        String noteDescription = _descriptionController.text;
        final result = await ApiRequest.postAddNote(
            doctor_id: appUserModel.id,
            patient_id: '${widget.singleCustomer.id}',
            note: noteDescription,
            noteTile: noteTitle,
            image: photo);

        debugPrint("postAddNote Result : ${result.toString()}");
        setState(() {
          isLoading = false;
        });
        Navigator.of(context).pop(true);
      } catch (e) {
        debugPrint("Error Adding Note${e.message}");
        MyToast.showToast("Error Adding Note ${e.toString()}",context);
        setState(() {
          isLoading = false;
        });
      }
  }

  Future<void> saveUser(userData) async {
//    final timestamp = userData['${ConstMemberUser.last_blood_donate_date}'];
    /*Timestamp timestamp = userData['${ConstMemberUser.last_blood_donate_date}'];
    var date = DateTime.fromMillisecondsSinceEpoch(timestamp.millisecondsSinceEpoch);
    var formattedDate = DateFormat.yMd().add_jm().format(date); // Apr 8, 2020
    debugPrint("last_blood_donate_date : ${formattedDate}");

    Timestamp.fromDate(DateTime.now().toUtc());

    await prefs.setString(AppPreferences.pref_user_type, AppPreferences.MEMBER);
    await prefs.setBool(AppPreferences.pref_is_user_login, true);
    await prefs.setString(ConstMemberUser.address, userData['${ConstMemberUser.address}']);
    await prefs.setString(ConstMemberUser.any_issue, userData['${ConstMemberUser.any_issue}']);
    await prefs.setString(ConstMemberUser.blood_group, userData['${ConstMemberUser.blood_group}']);
    await prefs.setString(ConstMemberUser.city, userData['${ConstMemberUser.city}']);
    await prefs.setString(ConstMemberUser.last_blood_donate_date, formattedDate);
    await prefs.setString(ConstMemberUser.mobile_number, userData['${ConstMemberUser.mobile_number}']);
    await prefs.setString(ConstMemberUser.name, userData['${ConstMemberUser.name}']);
    await prefs.setString(ConstMemberUser.password, userData['${ConstMemberUser.password}']);
    await prefs.setString(ConstMemberUser.photo, userData['${ConstMemberUser.photo}']);
    await prefs.setString(ConstMemberUser.status, userData['${ConstMemberUser.status}']);
    await prefs.setString(ConstMemberUser.userid, userData['${ConstMemberUser.userid}']);
    await prefs.setString(ConstMemberUser.gender, userData['${ConstMemberUser.gender}']);
    await prefs.setInt(ConstMemberUser.weight, userData['${ConstMemberUser.weight}']);
    await prefs.setString(ConstMemberUser.altername_numbers, json.encode(userData['${ConstMemberUser.altername_numbers}']));
    debugPrint("alternate number : ${userData['${ConstMemberUser.altername_numbers}']}");*/
  }
}
