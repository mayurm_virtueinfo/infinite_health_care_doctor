import 'package:carousel_slider/carousel_slider.dart';
import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:infinite_health_care_doctor/config/app_config.dart' as config;
import 'package:infinite_health_care_doctor/config/const.dart';
class DoctorAlbumImages extends StatefulWidget {
  static const String routeName = '/DoctorAlbumImages';
  List album_images;
  int index;
  DoctorAlbumImages({Key key,this.index,this.album_images}) : super(key: key);
  @override
  _DoctorAlbumImagesState createState() => _DoctorAlbumImagesState();
}
class _DoctorAlbumImagesState extends State<DoctorAlbumImages> {

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        leading: IconButton(
          icon: Icon(
              Icons.arrow_back,
              color:Theme.of(context).primaryColor
          ),
          onPressed: (){
            Navigator.pop(context);
          },
        ),
        title: Text(
          "Album",
          style: TextStyle(
            fontSize:22.0,
            fontWeight: FontWeight.bold,
            color: Theme.of(context).primaryColor,
          ),
        ),
        backgroundColor: Theme.of(context).accentColor,
      ),
      body: SingleChildScrollView(
        child: Container(
          constraints: BoxConstraints(
            minHeight: MediaQuery.of(context).size.height,
            minWidth: MediaQuery.of(context).size.width,
          ),
          child: CarouselSlider(

            options: CarouselOptions(
              initialPage: widget.index,
            ),
            items: widget.album_images.asMap().entries.map((entries) {
              int index = entries.key;


              return Builder(
                builder: (context) {
                  return Container(
                    constraints: BoxConstraints(
                      minHeight: MediaQuery.of(context).size.height,
                      minWidth: MediaQuery.of(context).size.width,
                    ),
                    child: FancyShimmerImage(
                      imageUrl: entries.value['album_images'],
                      shimmerBaseColor: Colors.white,
                      shimmerHighlightColor:
                      config.Colors().mainColor(1),
                      shimmerBackColor: Colors.green,
                      boxFit: BoxFit.cover,
                      errorWidget: Image.network('https://i0.wp.com/www.dobitaobyte.com.br/wp-content/uploads/2016/02/no_image.png?ssl=1'),
                    ),
                  );
                },
              );
            }).toList()
            ,
          ),
        ),
        
      ),

    );
  }
  Widget ball(String image,Color color,){
    return Container(
      height: 60,width: 60.0,
      child: ClipOval(
        child: FancyShimmerImage(
          imageUrl: image??Const.defaultProfileUrl,
          shimmerBaseColor: Colors.white,
          shimmerHighlightColor: config.Colors().mainColor(1),
          shimmerBackColor: Colors.green,
          errorWidget: Image.network('https://i0.wp.com/www.dobitaobyte.com.br/wp-content/uploads/2016/02/no_image.png?ssl=1'),
        ),
      )
      ,

    );
  }
  Widget card(String image,String title,String subTitle,String rank){
    return Container(
    child:Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        ball(image, Colors.transparent),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
                title,
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 12.0
                ),
              ),
            Text(
                subTitle,
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontSize: 10.0
                ),
              ),
            Text(
                "\₹ 50",
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontSize: 10.0
                ),
              ), 
          ],
        ),
        Row(
          children: <Widget>[
            Icon(Icons.star,color: Colors.yellow,),
            Text(rank,style: TextStyle(),),
          ],
        ),
      ],
    ),);
  }
}