import 'dart:async';

import 'package:confetti/confetti.dart';
import 'package:flutter/material.dart';
import 'package:infinite_health_care_doctor/models/user_model.dart';
import 'package:infinite_health_care_doctor/ui/home_screen.dart';
import 'package:infinite_health_care_doctor/ui/intro/intro_screen.dart';
import 'package:infinite_health_care_doctor/ui/loginorsignup/signup.dart';
import 'package:infinite_health_care_doctor/utils/app_preferences.dart';
import 'package:shared_preferences/shared_preferences.dart';
class ConsultancyBooked extends StatefulWidget {
  static const String routeName = "/ConsultancyBooked";
  @override
  _ConsultancyBookedState createState() => _ConsultancyBookedState();
}

class _ConsultancyBookedState extends State<ConsultancyBooked> {
  ConfettiController _controllerCenter;

  /// Setting duration in splash screen
  startTime() async {
    return new Timer(Duration(milliseconds: 3000), NavigatorPage);
  }

  /// To navigate layout change
  void NavigatorPage() async{
    Navigator.of(context).pushNamedAndRemoveUntil(HomeScreen.routeName, (route) => false);

    /*bool isAppOpened = await FlutterSession().get(AppPreferences.is_app_opened)??false;
    UserModel user = await UserModel.user();
    if(!isAppOpened){
      await FlutterSession().set(AppPreferences.is_app_opened,true);
      Navigator.of(context).pushReplacementNamed(IntroScreen.routeName);
    }
    else if(isAppOpened){
      UserModel user = await UserModel.user();
      if(user.isAuthenticated){
        Navigator.of(context).pushNamedAndRemoveUntil(HomeScreen.routeName, (route) => false,arguments:  [user]);
      }
      else{
        Navigator.of(context).pushReplacementNamed(SignUp.routeName);
      }
    }*/

  }
  Future<bool> _onBackPressed() {
//    Navigator.of(context).pop(false);
  }


  /// Declare startTime to InitState
  @override
  void initState() {

    super.initState();
    _controllerCenter =
        ConfettiController(duration: const Duration(seconds: 10));
    _controllerCenter.play();
    startTime();
  }


  @override
  void dispose() {
    super.dispose();
    _controllerCenter.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('images/image-home.jpeg'),
              fit: BoxFit.cover,
              ),
          ),
          child: Container(
            decoration: BoxDecoration(
              color: Theme.of(context).accentColor.withOpacity(0.8),
            ),
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[

                  /*Align(
                    alignment: Alignment.center,
                    child: ConfettiWidget(
                      confettiController: _controllerCenter,
                      blastDirectionality: BlastDirectionality
                          .explosive, // don't specify a direction, blast randomly
                      shouldLoop:
                      true, // start again as soon as the animation is finished
                      colors: const [
                        Colors.green,
                        Colors.blue,
                        Colors.pink,
                        Colors.orange,
                        Colors.purple
                      ], // manually specify the colors to be used
                    ),
                  ),*/
                  Container(
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Theme.of(context).primaryColor
                    ),
                    child: Icon(Icons.check,size: 100,color: Theme.of(context).accentColor,)
                  ),
                  SizedBox(height: 20,),
                  Text("Order Confirmed",style: TextStyle(
                      color: Theme.of(context).primaryColor,
                      fontSize: 30,
                      fontWeight: FontWeight.bold
                  ),),

                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}