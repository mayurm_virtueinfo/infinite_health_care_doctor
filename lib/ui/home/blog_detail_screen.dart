import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:flutter/material.dart';
import 'package:infinite_health_care_doctor/models/health_blog.dart';
import 'package:infinite_health_care_doctor/models/blog_model.dart';
import 'package:infinite_health_care_doctor/utils/my_colors.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BlogDetailScreen extends StatefulWidget {
  static const String routeName = "/BlogDetailScreen";
  BlogModel blogModel;
  BlogDetailScreen({this.blogModel});
  @override
  _BlogDetailScreenState createState() => _BlogDetailScreenState();
}


class _BlogDetailScreenState extends State<BlogDetailScreen> {


  @override
  void initState() {
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Theme.of(context).primaryColor),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text(
          "${widget.blogModel.title}",
          style: TextStyle(
            fontSize: 22.0,
            fontWeight: FontWeight.bold,
            color: Theme.of(context).primaryColor,
          ),
        ),

        backgroundColor: Theme.of(context).accentColor,
      ),
      body: SingleChildScrollView(
        physics: ScrollPhysics(),
        child: Container(
          padding: EdgeInsets.only(top: 10,bottom: 10),
          color: Colors.grey,
          constraints: BoxConstraints(
            minWidth: MediaQuery.of(context).size.width,
            minHeight: MediaQuery.of(context).size.height
          ),
          child: Container(
            margin: EdgeInsets.only(left: 10,right: 10),
            width: double.infinity,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(
                  10.0),
              color: Theme
                  .of(context)
                  .primaryColor,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment
                  .start,
              children: [
                Container(
                  width: double.infinity,
                  child: ClipRRect(
                    borderRadius: BorderRadius
                        .circular(10),
                    child: FancyShimmerImage(
                      boxFit: BoxFit.fill,
                      imageUrl: "${widget.blogModel.blog_image}",
                      shimmerBaseColor: Colors.white,
                      shimmerHighlightColor: MyColors.backgroundColor,
                      shimmerBackColor: Colors.green,
                    ),
                  ),
                ),
                Container(
                    padding: EdgeInsets.only(
                        left: 15, top: 10, right: 15),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment
                          .start,
                      children: [
                        Text("${widget.blogModel.title}",
                          style: TextStyle(
                              color: MyColors
                                  .backgroundColor,
                              fontWeight: FontWeight
                                  .bold,
                              fontSize: 20),),
                        SizedBox(height: 10,),
                        Text("${widget.blogModel.description}",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 14),),
                        SizedBox(height: 10,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment
                              .spaceBetween,
                          children: [
                            Text(
                              "",
                              style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 10,
                                  fontStyle: FontStyle
                                      .italic),),
                            Text("${widget.blogModel.date}",
                              style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 12,
                                  fontStyle: FontStyle
                                      .italic),),
                          ],
                        ),
                        SizedBox(height: 10,),

                        Text("Categories",style: TextStyle(
                            fontSize: 16,
                            color: Colors.grey,
                            fontWeight: FontWeight.bold
                        ),),
                        Wrap(
                          direction: Axis.horizontal,
                          children: widget.blogModel.category.asMap().entries.map((e) {
                            return Container(
                              margin: EdgeInsets.only(right: 10),
                              child: Chip(
                                /* avatar: CircleAvatar(
                                  backgroundColor: Colors.grey.shade800,
                                  child: Text('AB'),
                                ),*/
                                label: Text(e.value['category_name']),
                              ),
                            );
                          }).toList(),
                        ),
                        SizedBox(height: 5,),
                        SizedBox(height: 10,),

                        Text("Tags",style: TextStyle(
                          fontSize: 16,
                          color: Colors.grey,
                          fontWeight: FontWeight.bold
                        ),),
                        Wrap(
                          direction: Axis.horizontal,
                          children: widget.blogModel.tags.asMap().entries.map((e) {
                            return Container(
                              margin: EdgeInsets.only(right: 10),
                              child: Chip(
                               /* avatar: CircleAvatar(
                                  backgroundColor: Colors.grey.shade800,
                                  child: Text('AB'),
                                ),*/
                                label: Text(e.value['tag_name']),
                              ),
                            );
                          }).toList(),
                        ),
                        SizedBox(height: 5,),
                      ],
                    )
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}




