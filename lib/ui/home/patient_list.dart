import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:flutter/material.dart';
import 'package:infinite_health_care_doctor/api/api_request.dart';
import 'package:infinite_health_care_doctor/models/patient_model.dart' as patientModel;
import 'package:infinite_health_care_doctor/ui/home/patient_detail.dart';
import 'package:infinite_health_care_doctor/utils/my_toast.dart';
import 'package:infinite_health_care_doctor/widgets/doctors_card_widget.dart';
import 'package:infinite_health_care_doctor/widgets/patients_card_widget.dart';
import 'package:infinite_health_care_doctor/widgets/searchWidget.dart';
import 'package:infinite_health_care_doctor/bloc/loading/loading_bloc_event.dart';
import 'package:infinite_health_care_doctor/bloc/loading/loading_bloc.dart';
import 'package:infinite_health_care_doctor/widgets/loading_widget.dart';
import 'package:infinite_health_care_doctor/main.dart';




class PatientList extends StatefulWidget {
  static const String routeName = '/PatientList';
  final String doctorId;
  final String title;
  PatientList({this.title,this.doctorId});
  @override
  _PatientListState createState() => _PatientListState();
}

class _PatientListState extends State<PatientList> {
  @override
  void initState() {
    super.initState();
  }
  @override
  void dispose() {
    // Clean up the focus node when the Form is disposed.
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(bottomLeft:Radius.circular(16.0),bottomRight: Radius.circular(16.0)),
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color:Theme.of(context).primaryColor ),
          onPressed: (){
            Navigator.of(context).pop(context);
          },
        ),
        backgroundColor: Theme.of(context).accentColor,
        title: Text(
          "${widget.title}"??"",
          style: TextStyle(
            fontSize:22.0,
            fontWeight: FontWeight.bold,
            color: Theme.of(context).primaryColor,
          ),
        ),

      ),
      body: Container(
        padding: EdgeInsets.only(left: 20,right: 20),
        constraints: BoxConstraints(
          minHeight: MediaQuery.of(context).size.height,
          minWidth: MediaQuery.of(context).size.width
        ),
        child: SingleChildScrollView(
          child:FutureBuilder(
            future: ApiRequest.getPatientList(doctorId: widget.doctorId),
            builder: (context, snapshot) {

              if(snapshot.hasData) {
                locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());

                List<dynamic> listData = snapshot.data['data'];
                if(listData.length == 0){
                  return Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height,
                    child: Center(
                      child: Text('No patients found',
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                  );
                }

                patientModel.PatientModelList dList = patientModel.PatientModelList.fromSnapshot(snapshot);
                return ListView.builder(
                  shrinkWrap: true,
                  primary: false,
                  itemCount: dList.patients.length,
                  itemBuilder: (context, index) {
                    return PatientsCardWidget(
                      patient: dList.patients.elementAt(index),
                    );
                  },
                );
              } else if(snapshot.hasError){
                locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
                MyToast.showToast(snapshot.error.toString(),context);
                return Container();
              } else{
                return LoadingWidget(initialData: true,);
              }
            }
            ,
          ),
        ),
      ),
    );
  }

}
