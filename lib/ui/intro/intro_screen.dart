import'package:flutter/material.dart';
import 'package:infinite_health_care_doctor/ui/intro/on_boarding_widget.dart';
class IntroScreen extends StatefulWidget {
  static const String routeName = '/IntroScreen';
  @override
  _IntroScreenState createState() => _IntroScreenState();
}

class _IntroScreenState extends State<IntroScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        pageSnapping: true,
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          OnBoardingWidget(),
        ],
      ),
    );
  }
}