import 'dart:convert';
import 'package:infinite_health_care_doctor/bloc/user_model_bloc.dart';
import 'dart:math';
import 'package:flutter/services.dart' show rootBundle;
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:infinite_health_care_doctor/api/api_request.dart';
import 'package:infinite_health_care_doctor/models/user_model.dart';
import 'package:infinite_health_care_doctor/ui/home_screen.dart';
import 'package:infinite_health_care_doctor/ui/loginorsignup/patient_registration_screen.dart';
import 'package:infinite_health_care_doctor/ui/loginorsignup/signup.dart';
import 'package:infinite_health_care_doctor/main.dart';
import 'package:infinite_health_care_doctor/ui/loginorsignup/verification_number.dart';
import 'package:infinite_health_care_doctor/utils/app_preferences.dart';
import 'package:infinite_health_care_doctor/utils/my_toast.dart';
import 'package:infinite_health_care_doctor/widgets/app_button_circular_progress.dart';
import 'package:infinite_health_care_doctor/widgets/loading_widget.dart';
import 'package:infinite_health_care_doctor/bloc/loading/loading_bloc.dart';
import 'package:infinite_health_care_doctor/bloc/loading/loading_bloc_event.dart';

class EnterValidMobileNumber extends StatefulWidget {
  static const String routeName = '/EnterValidMobileNumber';

  @override
  _EnterValidMobileNumberState createState() => _EnterValidMobileNumberState();
}

class _EnterValidMobileNumberState extends State<EnterValidMobileNumber> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String countryCode = "+91";
  String phoneNumber = "";
  final _phoneController = TextEditingController();
  final _codeController = TextEditingController();
  List<String> mCodes = [];
  List<DropdownMenuItem<String>> listCountryCodes = [];

  // bool isSendingCode = false;
  bool _autoValidate = false;

  Future<bool> loginUser(String phone, BuildContext context) async {
    FirebaseAuth _auth = FirebaseAuth.instance;
    debugPrint("mobile Number : $phone");
    setState(() {
      // isSendingCode = true;
      locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
    });
    _auth.verifyPhoneNumber(
      phoneNumber: phone,
      timeout: Duration(seconds: 120),
      verificationCompleted: (AuthCredential credential) async {
        debugPrint("verificationCompleted : $credential");
        try {
          final User user = (await _auth.signInWithCredential(credential)).user;
          if (user != null) {
            MyToast.showToast("OTP Verification is Successfull", context);
            print("${user.email}, ${user.phoneNumber}, ${user.uid}, ${user.displayName}, ${user.photoURL}, ");
            String firstName = '';
            String lastName = '';
            if (user.displayName != null && user.displayName.length > 0) {
              List<String> dispName = user.displayName.split(" ");
              if (dispName.length > 1) {
                firstName = dispName[0];
                lastName = dispName[1];
              }
            }
            UserModel userModel = UserModel(id: user.uid, email: user.email, fullname: '$lastName $firstName', avatar_image: null /*user.photoUrl ?? Const.defaultProfileUrl*/, mobile: user.phoneNumber);

            locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
            setState(() {});

            navigateToHomeScreen(userModel);
          } else {
            locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
            setState(() {});
          }
        } catch (e) {
          MyToast.showToast(e.message, context);
          locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
          setState(() {});
        }
      },
      verificationFailed: (FirebaseAuthException exception) {
        debugPrint("verificationFailed : $exception");
        setState(() {
          locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
        });
        MyToast.showToast("${exception.message}", context);
        print(exception);
      },
      codeSent: (String verificationId, [int forceResendingToken]) async {
        debugPrint("codeSent : $verificationId, $forceResendingToken");
        locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
        Navigator.pushNamed(context, VerificationNumber.routeName, arguments: [phone, verificationId, forceResendingToken]);
      },
      codeAutoRetrievalTimeout: (verificationId) {},
    );
  }

  void navigateToHomeScreen(UserModel userModel) async {
    locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
    Map<String, dynamic> mapResult = await ApiRequest.postValidateDoctor(id_firebase: userModel.id);
    locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
    final data = mapResult['data'];
    if (data is Map<String, dynamic> && data['id'] != 0) {
      userModel = UserModel.fromMap(data);
      locator<UserModelBloc>().userModelEventSink.add(userModel);
      Navigator.pushNamedAndRemoveUntil(context, HomeScreen.routeName, (route) => false);
    } else if (data is int && data == 0) {
      Navigator.pushNamedAndRemoveUntil(context, PatientRegistrationScreen.routeName, (route) => false, arguments: [userModel]);
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    mCodes.add("+91");
//    getCountryCode();
  }

  void getCountryCode() async {
    String strCountryCode = await loadCountryCodes();
    List<dynamic> data = json.decode(strCountryCode);
    List<String> lCodes = [];
    debugPrint("countries data $data");
    data.forEach((element) {
      String code = element['dial_code'];
      if (code != "" && code != null) {
        debugPrint("${code}");
        lCodes.add(code);
        /*final item = DropdownMenuItem<String>(
          value: code,
          child: Text(code),
        );
        listCountryCodes.add(item);*/
      }
    });
    mCodes.clear();
    setState(() {
      mCodes = lCodes;
    });
  }

  Future<String> loadCountryCodes() async {
    return await rootBundle.loadString('images/country_codes.json');
  }

  String validateMobile(String value) {
    String pattern = r'(^(?:[+0]9)?[0-9]{10,12}$)';
    RegExp regExp = new RegExp(pattern);
    if (value.length == 0) {
      return 'Please enter mobile number';
    } else if (!regExp.hasMatch(value)) {
      return 'Please enter valid mobile number';
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xeeffffff),
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.close, color: Colors.black),
          onPressed: () {
//            Navigator.of(context).pushNamed(SignUp.routeName);
            Navigator.of(context).pop();
          },
        ),
      ),
      body: Stack(
        children: [
          Column(
            children: [
              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.only(top: 30.0),
                child: Image(
                  image: AssetImage("images/verification.png"),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 12.0),
                child: Text(
                  "Enter your mobile number we will send \n you the OTP verify later",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.grey,
                    fontSize: 11.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 12.0, right: 12.0, left: 12.0),
                decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                  borderRadius: BorderRadius.circular(5.0),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Form(
                      key: _formKey,
                      autovalidate: _autoValidate,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            width: 80,
                            padding: EdgeInsets.only(left: 10, top: 3, bottom: 3),
                            margin: EdgeInsets.only(top: 20.0, left: 12.0),
                            decoration: BoxDecoration(
                              border: Border.all(width: 1.0, color: Color(0xdddddddd)),
                              borderRadius: BorderRadius.circular(5.0),
                            ),
                            child: Center(
                              child: mCodes.length > 0
                                  ? DropdownButtonHideUnderline(
                                      child: DropdownButton<String>(
                                        items: mCodes.map((value) {
                                          return DropdownMenuItem(
                                            value: value,
                                            child: Text(
                                              value,
                                              style: TextStyle(),
                                            ),
                                          );
                                        }).toList(),
                                        onChanged: (value) {
                                          setState(() {
                                            countryCode = value;
                                          });
                                        },
                                        value: countryCode,
                                        isExpanded: true,
                                      ),
                                    )
                                  : Container(),
                            ),
                          ),
                          Expanded(
                            child: Container(
                              padding: EdgeInsets.only(left: 10, top: 3, bottom: 3),
                              margin: EdgeInsets.only(top: 20.0, left: 12.0, right: 12.0),
                              decoration: BoxDecoration(
                                border: Border.all(width: 1.0, color: Color(0xdddddddd)),
                                borderRadius: BorderRadius.circular(5.0),
                              ),
                              child: TextFormField(
                                controller: _phoneController,
                                keyboardType: TextInputType.number,
                                validator: validateMobile,
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.only(bottom: 0),
                                  border: InputBorder.none,
                                  hintText: "Enter valid mobile number",
                                  hintStyle: TextStyle(color: Colors.grey),
                                  errorStyle: TextStyle(
                                    fontSize: 14,
                                    height: 0.2,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 20.0, bottom: 20.0, right: 30.0, left: 30.0),
                      height: 40,
                      child:
                          /* isSendingCode
                          ? Container(
                        width: double.infinity,
                          child: Center(
                            child: SizedBox(
                              width: 40,
                              height: 40,
                              child: CircularProgressIndicator(),
                            ),
                          )
                      )
                          : */
                          ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: Theme.of(context).accentColor,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0),
                          ),
                        ),
                        onPressed: () {
//                        Navigator.of(context).pushNamed(VerificationNumber.routeName);
                          FocusScope.of(context).requestFocus(FocusNode());
                          if (_formKey.currentState.validate()) {
                            loginUser("$countryCode${_phoneController.text}", context);
                          } else {
                            setState(() {
                              _autoValidate = true;
                            });
                          }
                        },
                        child: Container(
                          child: Center(
                            child: Text(
                              'Submit',
                              style: TextStyle(
                                fontSize: 18.0,
                                color: Theme.of(context).primaryColor,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          LoadingWidget()
        ],
      ),
    );
  }
}
