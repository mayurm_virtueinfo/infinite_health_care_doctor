import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:infinite_health_care_doctor/bloc/loading/loading_bloc.dart';
import 'package:infinite_health_care_doctor/bloc/loading/loading_bloc_event.dart';
import 'package:infinite_health_care_doctor/bloc/user_model_bloc.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:image_picker/image_picker.dart';
import 'package:infinite_health_care_doctor/api/api_request.dart';
import 'package:infinite_health_care_doctor/config/app_config.dart' as config;
import 'package:infinite_health_care_doctor/models/user_model.dart';
import 'package:infinite_health_care_doctor/ui/home_screen.dart';
import 'package:infinite_health_care_doctor/main.dart';
import 'package:infinite_health_care_doctor/utils/app_preferences.dart';
import 'package:infinite_health_care_doctor/utils/app_style.dart';
import 'package:infinite_health_care_doctor/utils/my_toast.dart';
import 'package:infinite_health_care_doctor/utils/utility.dart';
import 'package:infinite_health_care_doctor/utils/width_sizes.dart';
import 'package:infinite_health_care_doctor/widgets/app_button.dart';
import 'package:infinite_health_care_doctor/widgets/loading_widget.dart';
import 'package:infinite_health_care_doctor/widgets/profile_header_register.dart';
import 'package:multiselect_formfield/multiselect_formfield.dart';

class PatientRegistrationScreen extends StatefulWidget {
  UserModel user;
  static const String routeName = "/PatientRegistrationScreen";

  PatientRegistrationScreen({this.user});

  @override
  _PatientRegistrationScreenState createState() =>
      _PatientRegistrationScreenState();
}

enum PhotoUpload { none, profile, cover }

class _PatientRegistrationScreenState extends State<PatientRegistrationScreen> {
  GlobalKey<FormState> _formState = GlobalKey<FormState>();

  FocusNode fnFirstName = FocusNode();
  FocusNode fnMobileNumber = FocusNode();
  FocusNode fnFees = FocusNode();
  FocusNode fnExperience = FocusNode();
  FocusNode fnState = FocusNode();
  FocusNode fnCity = FocusNode();
  FocusNode fnAddress = FocusNode();
  FocusNode fnZipcode = FocusNode();
  FocusNode fnDescription = FocusNode();
  // bool isProfileUpdating = false;

  bool isImageUploading = false;
  bool isCoverUploading = false;

  String fullname = '';
  String email = '';
  String mobileNumber = '';
  String state = '';
  String city = '';
  String address = '';
  String zip = '';
  File photo = null;
  bool enableEmail = true;
  bool enablePhone = true;
  bool isEmailRequired = false;
  List<Map<String,dynamic>> listCategory;
  List<dynamic> selectedCategory = [];
  List<Map<String,dynamic>> listDegree;
  List<dynamic> selectedDegrees = [];
  List<Map<String,dynamic>> listService;
  List<dynamic> selectedServices = [];

  List<Map<String,dynamic>> listSpecialization;
  List<dynamic> selectedSpecialization = [];

  Mode _mode = Mode.overlay;
  final homeScaffoldKey = GlobalKey<ScaffoldState>();
// to get places detail (lat/lng)
  GoogleMapsPlaces _places = GoogleMapsPlaces(apiKey: Utility.googleApiKey);
  // final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  Position _currentPosition;
  String _currentAddress;
  @override
  void initState() {
    super.initState();
    
    initData();
  }
  void initData() async{
    listCategory = [];
    listService = [];
    listDegree = [];
    listSpecialization = [];
    fullname = widget.user.fullname ?? "";
    email = widget.user.email ?? "";
    mobileNumber = widget.user.mobile ?? "";
    state = widget.user.state ?? "";
    city = widget.user.city ?? "";
    address = widget.user.address ?? "";
    zip = widget.user.zipcode ?? "";

    _fullnameController.text = fullname;
    _emailController.text = email;
    _mobileNumberController.text = mobileNumber;
    _stateController.text = state;
    _cityController.text = city;
    _addressController.text = address;
    _zipController.text = zip;

    bool isMobile = await Utility.isUserSignInWithMobile();
    bool isGmail = await Utility.isUserSignInWithGmail();
    bool isFacebook = await Utility.isUserSignInWithFacebook();

    if(isGmail || isFacebook){
      enableEmail = false;
    }


    if(isMobile){
      enablePhone = false;
      isEmailRequired = true;
    }

    initCategoryServiceDegree();
    setState(() {});
  }
  _getCurrentLocation() async{
    locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
    Position position = await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    if (!mounted) {
      locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
      return;
    }
    setState(() {
      _currentPosition = position;
    });
    locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
    _getAddressFromLatLng();
  }
  _getAddressFromLatLng() async {
    try {
      locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
      List<Placemark> p = await placemarkFromCoordinates(
          _currentPosition.latitude, _currentPosition.longitude);

      Placemark place = p[0];

      setState(() {
        String subLocality = place.subLocality;
        String locality = place.locality;
        String administrativeArea = place.administrativeArea;
        String name = place.name;
        String postalCode = place.postalCode;
        String country = place.country;

        debugPrint("name : ${name}");
        debugPrint("subLocality : ${subLocality}");
        debugPrint("locality : ${locality}");
        debugPrint("administrativeArea : ${administrativeArea}");
        debugPrint("postalCode : ${postalCode}");
        debugPrint("country : ${country}");
        _cityController.text = locality;
        _stateController.text = administrativeArea;
        _zipController.text  = postalCode;

        _currentAddress =
        "${place.name}, ${place.subLocality}, ${place.locality}, ${place.administrativeArea}, ${place.postalCode}, ${place.country}";

        _addressController.text = _currentAddress;
        locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
      });
    } catch (e) {
      locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
      print(e);
    }
  }
  // bool isLoading = false;
  void initCategoryServiceDegree() async{
    // setState(() {
    // isLoading = true;
    locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
    // });
    final resultCatList = await ApiRequest.getCategoryList();
    List catList = resultCatList['data'];

    // listCategory = [];
    catList.forEach((element) {
      Map<String,dynamic> mapCat=Map();
      mapCat['id'] = '${element['id']}';
      mapCat['title'] = '${element['title']}';
      listCategory.add(mapCat);
    });
    // listCategory = catList;

    final resultDegreeList = await ApiRequest.getDegreeList();
    List degreeList = resultDegreeList['data'];
    listDegree = [];
    degreeList.forEach((element) {
      Map<String,dynamic> mapCat=Map();
      mapCat['id'] = '${element['id']}';
      mapCat['title'] = '${element['title']}';
      listDegree.add(mapCat);
    });

    final resultServiceList = await ApiRequest.getServiceList();
    List serviceList = resultServiceList['data'];
    listService = [];
    serviceList.forEach((element) {
      Map<String,dynamic> mapCat=Map();
      mapCat['id'] = '${element['id']}';
      mapCat['title'] = '${element['title']}';
      listService.add(mapCat);
    });

    final resultSpecializationList = await ApiRequest.getSpecializationList();
    List specializationList = resultSpecializationList['data'];
    listSpecialization = [];
    specializationList.forEach((element) {
      Map<String,dynamic> mapCat=Map();
      mapCat['id'] = '${element['id']}';
      mapCat['title'] = '${element['title']}';
      listSpecialization.add(mapCat);
    });

    setState(() {
      // isLoading = false;
      locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
    });

  }
  final _fullnameController = TextEditingController();
  final _emailController = TextEditingController();
  final _mobileNumberController = TextEditingController();
  final _stateController = TextEditingController();
  final _cityController = TextEditingController();
  final _addressController = TextEditingController();
  final _zipController = TextEditingController();
  final _descriptionController = TextEditingController();
  final _feesController = TextEditingController();
  final _experienceController = TextEditingController();

  Future<bool> _onBackPressed() {
    return showDialog(
          context: context,
          builder: (context) => new AlertDialog(
            title: new Text('Are you sure?'),
            content: new Text('Do you want to exit an App'),
            actions: <Widget>[
              new GestureDetector(
                onTap: () => Navigator.of(context).pop(false),
                child: Text("NO"),
              ),
              SizedBox(height: 16),
              new GestureDetector(
                onTap: () async {
                  bool result = await Utility.logoutUser();
                  if (result) {
                    Navigator.of(context).pop(true);
                  }
                },
                child: Text("YES"),
              ),
            ],
          ),
        ) ??
        false;
  }



  @override
  Widget build(BuildContext context) {
    double devicePixelRatio = Utility.getDevicePixelRatio(context);
    final img_bapasitaram_width =
        WidthSizes.splash_screen_img_bapasitaram_width * devicePixelRatio;
    final img_savealife_width =
        WidthSizes.splash_screen_img_savealife_width * devicePixelRatio;
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        key: homeScaffoldKey,
        appBar: AppBar(
          elevation: 0,
          leading: Container(),
          /*leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Theme
                .of(context)
                .primaryColor),
            onPressed: () {
              Navigator.pop(context);
            },
          ),*/
          centerTitle: true,
          title: Text(
            "Complete Profile",
            style: TextStyle(
              fontSize: 22.0,
              fontWeight: FontWeight.bold,
              color: Theme
                  .of(context)
                  .primaryColor,
            ),
          ),
          backgroundColor: config.Colors().mainColor(1),
        ),
        body: Stack(
          children: [
            SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Stack(
                    children: <Widget>[
                      Container(
                        height: 150,
                        padding: const EdgeInsets.only(top: 40),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(25.0),
                              bottomRight: Radius.circular(25.0)),
                          color: config.Colors().mainColor(1),
                        ),
                      ),
                      Stack(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left: 14.0, right: 14.0),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20.0),
                              color: Theme.of(context).primaryColor,
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
//                        photo
                                SizedBox(
                                  height: 30,
                                ),
                                ProfileHeaderRegister(
                                  imagePath: photo,
                                  onTap: () {
                                    _settingModalBottomSheet(context);
                                  },
                                  isImageUploading: isImageUploading,
                                  showCameraIcon: true,
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Form(
                                  key: _formState,
                                  child: Container(
                                      child: Wrap(
                                        direction: Axis.horizontal,
                                        children: <Widget>[
//                              fullname
                                          Container(
                                            child: TextFormField(
                                              focusNode: fnFirstName,
                                              textInputAction: TextInputAction.next,
                                              onFieldSubmitted: (term){
                                                fnMobileNumber.requestFocus();
                                              },
                                              validator: (text) {
                                                if (text.length == 0) {
                                                  return "Fullname is required";
                                                }

                                                return null;
                                              },
//                                            maxLength: 20,
                                              controller: _fullnameController,
                                              decoration: InputDecoration(
                                                  errorStyle: AppStyles.errorStyle,
                                                  focusedBorder: AppStyles.focusBorder,
                                                  prefixIcon: Icon(
                                                    Icons.account_circle,
                                                    color: config.Colors().mainColor(1),
                                                  ),
                                                  labelStyle: TextStyle(
                                                      color:
                                                      config.Colors().mainColor(1)),
                                                  border: OutlineInputBorder(),
                                                  labelText: "Enter Full Name"),
                                            ),
                                            padding: EdgeInsets.all(20),
                                          ),

//                              email
                                          Container(
                                              margin:
                                              EdgeInsets.only(left: 5, right: 5),
                                              child: Divider(
                                                height: 1,
                                                color: config.Colors().mainColor(1),
                                              )),
                                          Container(
                                            child: TextFormField(

                                              enabled: enableEmail,
                                              validator: (text) {
                                                if (isEmailRequired &&
                                                    text.length == 0) {
                                                  return "Email is required";
                                                }
                                                /*else if(text.length<6){
                                                    return "Password should have atleast 6 charecter";
                                                  }*/
                                                return null;
                                              },
//                                        maxLength: 15,
                                              controller: _emailController,
                                              decoration: InputDecoration(
                                                errorStyle: AppStyles.errorStyle,
                                                focusedBorder: AppStyles.focusBorder,
                                                prefixIcon: Icon(
                                                  Icons.mail,
                                                  color: config.Colors().mainColor(1),
                                                ),
                                                labelStyle: TextStyle(
                                                    color:
                                                    config.Colors().mainColor(1)),
                                                border: OutlineInputBorder(),
                                                labelText: "Enter Email",
                                              ),
                                            ),
                                            padding: EdgeInsets.all(20),
                                          ),

//                              mobile_number
                                          Container(
                                              margin:
                                              EdgeInsets.only(left: 5, right: 5),
                                              child: Divider(
                                                height: 1,
                                                color: config.Colors().mainColor(1),
                                              )),
                                          Container(
                                            child: TextFormField(
                                              focusNode: fnMobileNumber,
                                              textInputAction: TextInputAction.next,
                                              onFieldSubmitted: (term){
                                                fnFees.requestFocus();
                                              },
                                              enabled: enablePhone,
                                              validator: (text) {
                                                if (text.length == 0) {
                                                  return "Mobile Number is required";
                                                }
                                                /*else if(text.length < 10){
                                                    return "Invalid Mobile Number";
                                                  }*/
                                                return null;
                                              },
                                              controller: _mobileNumberController,
                                              keyboardType: TextInputType.number,
//                                        maxLength: 10,
                                              decoration: InputDecoration(
                                                errorStyle: AppStyles.errorStyle,
                                                focusedBorder: AppStyles.focusBorder,
                                                prefixIcon: Icon(
                                                  Icons.call,
                                                  color: config.Colors().mainColor(1),
                                                ),
                                                labelStyle: TextStyle(
                                                    color:
                                                    config.Colors().mainColor(1)),
                                                border: OutlineInputBorder(),
                                                labelText: "Enter Mobile Number",
                                              ),
                                            ),
                                            padding: EdgeInsets.all(20),
                                          ),
//                              category
                                          Container(
                                              margin:
                                              EdgeInsets.only(left: 5, right: 5),
                                              child: Divider(
                                                height: 1,
                                                color: config.Colors().mainColor(1),
                                              )),
                                          Container(
                                            padding: EdgeInsets.all(3),
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(5)),
                                                border: Border.all(width: 1,color: Colors.grey)),
                                            margin: EdgeInsets.all(20),
                                            child: MultiSelectFormField(

                                              fillColor: Colors.white,
                                              border: InputBorder.none,
                                              autovalidate: false,
                                              title: Text('Category'),
                                              validator: (value) {
                                                if (value == null ||
                                                    value.length == 0) {
                                                  return 'Please select one or more options';
                                                }
                                                return null;
                                              },
                                              dataSource: listCategory,
                                              textField: 'title',
                                              valueField: 'id',
                                              okButtonLabel: 'OK',
                                              cancelButtonLabel: 'CANCEL',
                                              // required: true,
                                              hintWidget: Text('Please choose one or more'),
                                              // value: _myActivities,
                                              onSaved: (value) {
                                                FocusScope.of(context).requestFocus(FocusNode());
                                                if (value == null) return;
                                                setState(() {
                                                  selectedCategory = value;
                                                });
                                              },
                                            ),
                                          ),
                                          ///                             service
                                          Container(
                                              margin:
                                              EdgeInsets.only(left: 5, right: 5),
                                              child: Divider(
                                                height: 1,
                                                color: config.Colors().mainColor(1),
                                              )),
                                          Container(
                                            padding: EdgeInsets.all(3),
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(5)),
                                                border: Border.all(width: 1,color: Colors.grey)),
                                            margin: EdgeInsets.all(20),
                                            child: MultiSelectFormField(

                                              fillColor: Colors.white,
                                              border: InputBorder.none,
                                              autovalidate: false,
                                              title: Text('Service'),
                                              validator: (value) {
                                                if (value == null ||
                                                    value.length == 0) {
                                                  return 'Please select one or more options';
                                                }
                                                return null;
                                              },
                                              dataSource: listService,
                                              textField: 'title',
                                              valueField: 'id',
                                              okButtonLabel: 'OK',
                                              cancelButtonLabel: 'CANCEL',
                                              // required: true,
                                              hintWidget:Text('Please choose one or more') ,
                                              // value: _myActivities,
                                              onSaved: (value) {
                                                FocusScope.of(context).requestFocus(FocusNode());
                                                if (value == null) return;
                                                setState(() {
                                                  selectedServices = value;
                                                });
                                              },
                                            ),
                                          ),
//                              degree
                                          Container(
                                              margin:
                                              EdgeInsets.only(left: 5, right: 5),
                                              child: Divider(
                                                height: 1,
                                                color: config.Colors().mainColor(1),
                                              )),
                                          Container(
                                            padding: EdgeInsets.all(3),
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(5)),
                                                border: Border.all(width: 1,color: Colors.grey)),
                                            margin: EdgeInsets.all(20),
                                            child: MultiSelectFormField(

                                              fillColor: Colors.white,
                                              border: InputBorder.none,
                                              autovalidate: false,
                                              title: Text('Degree'),
                                              validator: (value) {

                                                if (value == null ||
                                                    value.length == 0) {
                                                  return 'Please select one or more options';
                                                }
                                                return null;
                                              },
                                              dataSource: listDegree,
                                              textField: 'title',
                                              valueField: 'id',
                                              okButtonLabel: 'OK',
                                              cancelButtonLabel: 'CANCEL',
                                              // required: true,
                                              hintWidget: Text('Please choose one or more'),
                                              // value: _myActivities,
                                              onSaved: (value) {
                                                FocusScope.of(context).requestFocus(FocusNode());
                                                if (value == null) return;
                                                setState(() {
                                                  selectedDegrees = value;
                                                });
                                              },
                                            ),
                                          ),
//                              specialization
                                          Container(
                                              margin:
                                              EdgeInsets.only(left: 5, right: 5),
                                              child: Divider(
                                                height: 1,
                                                color: config.Colors().mainColor(1),
                                              )),
                                          Container(
                                            padding: EdgeInsets.all(3),
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(5)),
                                                border: Border.all(width: 1,color: Colors.grey)),
                                            margin: EdgeInsets.all(20),
                                            child: MultiSelectFormField(

                                              fillColor: Colors.white,
                                              border: InputBorder.none,
                                              autovalidate: false,
                                              title: Text('Specialization'),
                                              validator: (value) {

                                                if (value == null ||
                                                    value.length == 0) {
                                                  return 'Please select one or more options';
                                                }
                                                return null;
                                              },
                                              dataSource: listSpecialization,
                                              textField: 'title',
                                              valueField: 'id',
                                              okButtonLabel: 'OK',
                                              cancelButtonLabel: 'CANCEL',
                                              // required: true,
                                              hintWidget: Text('Please choose one or more'),
                                              // value: _myActivities,
                                              onSaved: (value) {
                                                FocusScope.of(context).requestFocus(FocusNode());
                                                if (value == null) return;
                                                setState(() {
                                                  selectedSpecialization = value;
                                                });
                                              },
                                            ),
                                          ),
//                              Experience
                                          Container(
                                              margin:
                                              EdgeInsets.only(left: 5, right: 5),
                                              child: Divider(
                                                height: 1,
                                                color: config.Colors().mainColor(1),
                                              )),
                                          Container(
                                            child: TextFormField(
                                              focusNode: fnExperience,
                                              textInputAction: TextInputAction.next,
                                              onFieldSubmitted: (term){
                                                debugPrint("tempExperience : $term");
                                                fnFees.requestFocus();
                                              },
                                              validator: (text){
                                                if(text.length == 0){
                                                  return "Enter experience";
                                                }
                                                return null;
                                              },
                                              controller: _experienceController,
                                              keyboardType: TextInputType.number,
                                              maxLength: 2,
                                              decoration: InputDecoration(
                                                errorStyle: AppStyles.errorStyle,
                                                focusedBorder: AppStyles.focusBorder,
                                                prefixIcon: Icon(
                                                  Icons.badge,
                                                  color: config.Colors().mainColor(1),
                                                ),
                                                labelStyle: TextStyle(
                                                    color:
                                                    config.Colors().mainColor(1)),
                                                border: OutlineInputBorder(),
                                                labelText: "Enter Experience",
                                              ),
                                            ),
                                            padding: EdgeInsets.all(20),
                                          ),
//                              fees
                                          Container(
                                              margin:
                                              EdgeInsets.only(left: 5, right: 5),
                                              child: Divider(
                                                height: 1,
                                                color: config.Colors().mainColor(1),
                                              )),
                                          Container(
                                            child: TextFormField(
                                              focusNode: fnFees,
                                              textInputAction: TextInputAction.next,
                                              onFieldSubmitted: (term){
                                                fnState.requestFocus();
                                              },
                                              validator: (text){
                                                if(text.length == 0){
                                                  return "Enter fees";
                                                }
                                                return null;
                                              },
                                              controller: _feesController,
                                              keyboardType: TextInputType.number,
//                                        maxLength: 10,
                                              decoration: InputDecoration(
                                                errorStyle: AppStyles.errorStyle,
                                                focusedBorder: AppStyles.focusBorder,
                                                prefixText: "₹",
                                                /*prefixIcon: Icon(
                                                    Icons.attach_money,
                                                    color: config.Colors().mainColor(1),
                                                  ),*/
                                                labelStyle: TextStyle(
                                                    color:
                                                    config.Colors().mainColor(1)),
                                                border: OutlineInputBorder(),
                                                labelText: "Enter Fees",
                                              ),
                                            ),
                                            padding: EdgeInsets.all(20),
                                          ),

//                              location
                                          Container(
                                              margin:
                                              EdgeInsets.only(left: 5, right: 5),
                                              child: Divider(
                                                height: 1,
                                                color: config.Colors().mainColor(1),
                                              )),
                                          Container(
                                            constraints: BoxConstraints(
                                                minHeight: 50
                                            ),

                                            padding: EdgeInsets.all(20),
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              children: <Widget>[

                                                Expanded(
                                                  flex: 9,
                                                  child: GestureDetector(
                                                    onTap: (){
                                                      _handlePressButton();

                                                    },
                                                    child: Column(
                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                      children: <Widget>[
                                                        Text(
                                                          'Search or Get Current Location',
                                                          style: TextStyle(
                                                              fontSize: 16,
                                                              color: Colors.grey
                                                          ),
                                                        ),
                                                        (_currentPosition != null &&
                                                            _currentAddress != null)?
                                                        Text(_currentAddress,
                                                            style:TextStyle(
                                                                fontSize: 16
                                                            )):Container(),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 8,
                                                ),
                                                Expanded(
                                                  flex: 1,
                                                  child: Container(
                                                    width: 50,
                                                    height: 50,
                                                    child: IconButton(
                                                        icon: Icon(Icons.my_location,color: config.Colors().mainColor(1),),
                                                        onPressed:()async{
                                                          debugPrint("Yes");
                                                          try {
                                                            locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
                                                            bool serviceEnabled;
                                                            serviceEnabled = await Geolocator.isLocationServiceEnabled();
                                                            if (!serviceEnabled) {
                                                              MyToast.showToast("Location services are disabled.",context);
                                                            }
                                                            await Utility.requestForLocationPermission();
                                                            locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
                                                            _getCurrentLocation();

                                                          } catch (e) {
                                                            locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
                                                            MyToast
                                                                .showToast(e.toString(),context);
                                                          }
                                                        }
                                                    ),
                                                  ),
                                                ),

                                              ],
                                            ),
                                          ),

//                              state
                                          Container(
                                              margin:
                                              EdgeInsets.only(left: 5, right: 5),
                                              child: Divider(
                                                height: 1,
                                                color: config.Colors().mainColor(1),
                                              )),
                                          Container(
                                            child: TextFormField(
                                              focusNode: fnState,
                                              textInputAction: TextInputAction.next,
                                              onFieldSubmitted: (term){
                                                fnCity.requestFocus();
                                              },
                                              validator: (text){
                                                if(text.length == 0){
                                                  return "State is required";
                                                }
                                                return null;
                                              },
                                              controller: _stateController,
                                              keyboardType: TextInputType.text,
                                              decoration: InputDecoration(
                                                errorStyle: AppStyles.errorStyle,
                                                focusedBorder: AppStyles.focusBorder,
                                                prefixIcon: Icon(
                                                  Icons.location_on,
                                                  color: config.Colors().mainColor(1),
                                                ),
                                                labelStyle: TextStyle(
                                                    color:
                                                    config.Colors().mainColor(1)),
                                                border: OutlineInputBorder(),
                                                labelText: "Enter State",
                                              ),
                                            ),
                                            padding: EdgeInsets.all(20),
                                          ),
//                              city
                                          Container(
                                              margin:
                                              EdgeInsets.only(left: 5, right: 5),
                                              child: Divider(
                                                height: 1,
                                                color: config.Colors().mainColor(1),
                                              )),
                                          Container(
                                            child: TextFormField(
                                              focusNode: fnCity,
                                              textInputAction: TextInputAction.next,
                                              onFieldSubmitted: (term){
                                                fnAddress.requestFocus();
                                              },
                                              validator: (text){
                                                if(text.length == 0){
                                                  return "City is required";
                                                }
                                                return null;
                                              },
                                              controller: _cityController,
                                              keyboardType: TextInputType.text,
                                              decoration: InputDecoration(
                                                errorStyle: AppStyles.errorStyle,
                                                focusedBorder: AppStyles.focusBorder,
                                                prefixIcon: Icon(
                                                  Icons.location_city,
                                                  color: config.Colors().mainColor(1),
                                                ),
                                                labelStyle: TextStyle(
                                                    color:
                                                    config.Colors().mainColor(1)),
                                                border: OutlineInputBorder(),
                                                labelText: "Enter City",
                                              ),
                                            ),
                                            padding: EdgeInsets.all(20),
                                          ),
//                              address
                                          Container(
                                              margin:
                                              EdgeInsets.only(left: 5, right: 5),
                                              child: Divider(
                                                height: 1,
                                                color: config.Colors().mainColor(1),
                                              )),
                                          Container(
                                            child: TextFormField(
                                              focusNode: fnAddress,
                                              textInputAction: TextInputAction.newline,
                                              /*onFieldSubmitted: (term){
                                                  fnZipcode.requestFocus();
                                                },*/
                                              textAlign: TextAlign.start,
                                              controller: _addressController,
                                              validator: (text){
                                                if(text.length == 0){
                                                  return "Address is required";
                                                }
                                                return null;
                                              },
                                              maxLines: 3,
                                              minLines: 3,
                                              keyboardType: TextInputType.multiline,
                                              decoration: InputDecoration(
                                                  errorStyle: AppStyles.errorStyle,
                                                  focusedBorder: AppStyles.focusBorder,
                                                  // contentPadding: EdgeInsets.symmetric(horizontal: 0,vertical: 0),
                                                  /*prefix: Icon(
                                                      Icons.location_on,
                                                      color: config.Colors().mainColor(1),
                                                    ),*/
                                                  labelStyle: TextStyle(
                                                      color:
                                                      config.Colors().mainColor(1)),
                                                  border: OutlineInputBorder(),
                                                  labelText: "Enter Address",
                                                  alignLabelWithHint: true
                                              ),
                                            ),
                                            padding: EdgeInsets.all(20),
                                          ),
//                              zipcode
                                          Container(
                                              margin:
                                              EdgeInsets.only(left: 5, right: 5),
                                              child: Divider(
                                                height: 1,
                                                color: config.Colors().mainColor(1),
                                              )),
                                          Container(
                                            child: TextFormField(
                                              focusNode: fnZipcode,
                                              textInputAction: TextInputAction.next,
                                              onFieldSubmitted: (term){
                                                fnDescription.requestFocus();
                                              },
                                              validator: (text){
                                                if(text.length == 0){
                                                  return "Zip Code is required";
                                                }
                                                return null;
                                              },
                                              controller: _zipController,
                                              keyboardType: TextInputType.number,
                                              decoration: InputDecoration(
                                                errorStyle: AppStyles.errorStyle,
                                                focusedBorder: AppStyles.focusBorder,
                                                prefixIcon: Icon(
                                                  Icons.home,
                                                  color: config.Colors().mainColor(1),
                                                ),
                                                labelStyle: TextStyle(
                                                    color:
                                                    config.Colors().mainColor(1)),
                                                border: OutlineInputBorder(),
                                                labelText: "Enter Zip Code",
                                              ),
                                            ),
                                            padding: EdgeInsets.all(20),
                                          ),
//                              description
                                          Container(
                                              margin:
                                              EdgeInsets.only(left: 5, right: 5),
                                              child: Divider(
                                                height: 1,
                                                color: config.Colors().mainColor(1),
                                              )),
                                          Container(
                                            child: TextFormField(
                                              focusNode: fnDescription,
                                              textInputAction: TextInputAction.newline,
                                              /*onFieldSubmitted: (term){
                                                  fnDescription.requestFocus();
                                                },*/
                                              /*validator: (text){
                                                  if(text.length == 0){
                                                    return "Zip Code is required";
                                                  }*/ /*else if(text.length < 10){
                                                    return "Invalid Mobile Number";
                                                  }*/ /*
                                                  return null;
                                                },*/
                                              controller: _descriptionController,
                                              keyboardType: TextInputType.multiline,
                                              maxLines: 3,
                                              textAlign: TextAlign.start,
                                              minLines: 3,
                                              decoration: InputDecoration(
                                                errorStyle: AppStyles.errorStyle,
                                                focusedBorder: AppStyles.focusBorder,
                                                /*prefix: Icon(
                                                    Icons.home,
                                                    color: config.Colors().mainColor(1),
                                                  ),*/
                                                labelStyle: TextStyle(
                                                    color:
                                                    config.Colors().mainColor(1)),
                                                border: OutlineInputBorder(),
                                                labelText: "Enter description",
                                                alignLabelWithHint: true,
                                              ),
                                            ),
                                            padding: EdgeInsets.all(20),
                                          ),
                                        ],
                                      )),
                                ),
                              ],
                            ),
                          ),
//                        Center(child:ball(currentDoctor.avatar, Theme.of(context).primaryColor,)),
                        ],
                      ),

                    ],
                  ),
                  InkWell(
                      onTap: () async {
                        if (_formState.currentState.validate()) {
                          _saveProfile(context);
                        }
                      },
                      child: AppButton(
                        text: "Register",
                      )),

                ],
              ),
            ),
            LoadingWidget()
          ],
        ),
      ),
    );
  }
  Future<void> _handlePressButton() async {
    // show input autocomplete with selected mode
    // then get the Prediction selected
    Prediction p = await PlacesAutocomplete.show(
      context: context,
      apiKey: Utility.googleApiKey,
      onError: onError,
      mode: _mode,
      language: "en",
      components: [Component(Component.country, "in")],
    );

    displayPrediction(p, homeScaffoldKey.currentState);
  }
  void onError(PlacesAutocompleteResponse response) {
    homeScaffoldKey.currentState.showSnackBar(
      SnackBar(content: Text(response.errorMessage)),
    );
  }

  Future<Null> displayPrediction(Prediction p, ScaffoldState scaffold) async {
    if (p != null) {
      // get detail (lat/lng)
      PlacesDetailsResponse detail = await _places.getDetailsByPlaceId(p.placeId);
      final lat = detail.result.geometry.location.lat;
      final lng = detail.result.geometry.location.lng;

      Position mPosition=Position(latitude: lat,longitude: lng);
      setState(() {
        _currentPosition = mPosition;
      });

      _getAddressFromLatLng();
      /*scaffold.showSnackBar(
        SnackBar(content: Text("${p.description} - $lat/$lng")),
      );*/
    }
  }

  void _saveProfile(context) async {
    try {
      locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());

      widget.user.fullname = _fullnameController.text;
      widget.user.email = _emailController.text;
      widget.user.mobile = _mobileNumberController.text;
      widget.user.state = _stateController.text;
      widget.user.city = _cityController.text;
      widget.user.address = _addressController.text;
      widget.user.zipcode = _zipController.text;
      widget.user.description = _descriptionController.text;
      widget.user.experience = _experienceController.text;

      List<String> mCategory = [];
      selectedCategory.forEach((element) {
        mCategory.add(element);
      });
      List<String> mDegree = [];
      selectedDegrees.forEach((element) {
        mDegree.add(element);
      });
      List<String> mServices = [];
      selectedServices.forEach((element) {
        mServices.add(element);
      });
      List<String> mSpecialization = [];
      selectedSpecialization.forEach((element) {
        mSpecialization.add(element);
      });


      UserModel user = widget.user;

      Map<String, dynamic> mapResult = await ApiRequest.postDoctorCreate(
        fullname: widget.user.fullname,
        id_firebase: "${widget.user.id}",
        mobile: widget.user.mobile,
        state: widget.user.state,
        email: widget.user.email,
        city: widget.user.city,
        address: widget.user.address,
        zipcode: widget.user.zipcode,
        image: photo,
        category: mCategory,
        degree: mDegree,
        services: mServices,
        specialization: mSpecialization,
        experience: widget.user.experience,
        fees: _feesController.text,
        description: _descriptionController.text,
        lat: _currentPosition==null?"":"${_currentPosition.latitude}",
        lng: _currentAddress==null?"":"${_currentPosition.longitude}"
      );
      Map<String, dynamic> mapCreateUser = mapResult['data'];
      debugPrint("mapCreateUser : ${mapCreateUser}");

      user = UserModel.fromMap(mapCreateUser);
      debugPrint("updated user : ${user.toJson().toString()}");

      locator<UserModelBloc>().userModelEventSink.add(user);
      locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
      _navigateToHomeScreen(user);
    } catch (e) {
      debugPrint("Error saving prifile : ${e.message}");
      locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
      MyToast.showToast("${e.message}",context);
    }
  }

  void _navigateToHomeScreen(user) async {
    Navigator.of(context).pushNamedAndRemoveUntil(
        HomeScreen.routeName, (route) => false,
        arguments: [user]);
  }

  void _settingModalBottomSheet(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: new Wrap(
              direction: Axis.horizontal,
              children: <Widget>[
                /*ListTile(
                    leading: new Icon(Icons.delete),
                    title: new Text('Remove'),
                    onTap: (){
                      _onClickRemovePhoto();
                    }
                ),*/
                ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text('Camera'),
                    onTap: () {
                      _onClickCameraPhoto();
                    }),
                ListTile(
                  leading: new Icon(Icons.photo),
                  title: new Text('Gallery'),
                  onTap: () {
                    _onClickGalleryPhoto();
                  },
                ),
              ],
            ),
          );
        });
  }

  void _onClickCameraPhoto() async {
    Navigator.pop(context);

    PickedFile pickedFile = await ImagePicker().getImage(source: ImageSource.camera);
    File image;
    if (pickedFile != null) {
      image = File(pickedFile.path);
    } else {
      image = null;
    }
    if (image != null) {
      File cropped = await Utility.cropImage(image);
      debugPrint("cropped Image : ${cropped.toString()}");
      setState(() {
        photo = cropped;
      });
//      setImagePath(cropped);
    }
  }

  /*void setImagePath(imgPath) async {
    setState(() {
      isImageUploading = true;
    });
    String fileURL;
    if (imgPath == null) {
      fileURL = Const.defaultProfileUrl;
      setState(() {
        photo = fileURL;
        isImageUploading = false;

      });
    } else {
      String storePath = "img_profile_${widget.user.id}";
      fileURL = await Utility.uploadFile(imgPath, storePath,"user_profile_photos");

      setState(() {
        photo = fileURL;
        isImageUploading = false;
      });
    }
  }*/

  void _onClickGalleryPhoto() async {
    Navigator.pop(context);
    PickedFile pickedFile = await ImagePicker().getImage(source: ImageSource.gallery);
    File image;
    if (pickedFile != null) {
      image = File(pickedFile.path);
    } else {
      image = null;
    }
    if (image != null) {
      File cropped = await Utility.cropImage(image);
      debugPrint("cropped Image : ${cropped.toString()}");
      setState(() {
        photo = cropped;
      });
//      setImagePath(cropped);
    }
  }

  void _onClickRemovePhoto() {
    Navigator.pop(context);
//    setImagePath(null);
    setState(() {
      photo = null;
    });
  }

  @override
  void dispose() {
    super.dispose();
  }
}


