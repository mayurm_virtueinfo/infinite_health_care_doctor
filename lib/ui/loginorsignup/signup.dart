import 'dart:convert';
import 'package:infinite_health_care_doctor/widgets/loading_widget.dart';
import 'package:infinite_health_care_doctor/bloc/loading/loading_bloc.dart';
import 'package:infinite_health_care_doctor/bloc/loading/loading_bloc_event.dart';
import 'package:infinite_health_care_doctor/bloc/user_model_bloc.dart';
import 'package:infinite_health_care_doctor/main.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:http/http.dart' as http;
import 'package:infinite_health_care_doctor/api/api_request.dart';
import 'package:infinite_health_care_doctor/models/user_model.dart';
import 'package:infinite_health_care_doctor/ui/home_screen.dart';
import 'package:infinite_health_care_doctor/ui/loginorsignup/enter_valid_mobile_number.dart';
import 'package:infinite_health_care_doctor/ui/loginorsignup/patient_registration_screen.dart';
import 'package:infinite_health_care_doctor/main.dart';
import 'package:infinite_health_care_doctor/utils/app_preferences.dart';
import 'package:infinite_health_care_doctor/utils/my_toast.dart';
import 'package:infinite_health_care_doctor/widgets/app_button_circular_progress.dart';
import 'package:infinite_health_care_doctor/widgets/app_button_facebook.dart';
import 'package:infinite_health_care_doctor/widgets/app_button_google.dart';

class SignUp extends StatefulWidget {
  static const String routeName = '/signup';

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {

  final FirebaseAuth _auth = FirebaseAuth.instance;

  final FacebookLogin facebookSignIn = FacebookLogin();
  final GoogleSignIn _googleSignIn = GoogleSignIn();
  // bool isLoadingGoogle = false;
  // bool isValidatingUser = false;
  // bool isLoadingFacebook = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Stack(
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      height: 400.0,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(50.0),
                            bottomRight: Radius.circular(50.0)),
                        image: DecorationImage(
                          image: AssetImage('images/image-home.jpeg'),
                          fit: BoxFit.cover,
                        ),
                      ),
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(50.0),
                              bottomRight: Radius.circular(50.0)),
                          color: Theme.of(context).accentColor.withOpacity(0.8),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 50.0, right: 50.0, left: 50.0),
                      height: 40,
                      decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                              color: Colors.grey.withOpacity(0.10),
                              offset: Offset(0, 4),
                              blurRadius: 10)
                        ],
                      ),
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          elevation: 0.2,
                          primary: Theme.of(context).primaryColor,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0),
                          ),
                        ),

                        onPressed: () {
                          Navigator.of(context).pushNamed(EnterValidMobileNumber.routeName);
                        },
                        child: Container(
                          child: Center(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Image(
                                  image: AssetImage('images/cellphone-line.png'),
                                ),
                                Text(
                                  ' Phone Number',
                                  style: TextStyle(
                                    fontSize: 16.0,
                                    color: Theme.of(context).focusColor,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    AppButtonFacebook(
                            onTap: _signInWithFacebook,
                          ),
                    AppButtonGoogle(
                            onTap: _signInWithGoogle,
                          ),
                    Container(
                      margin: EdgeInsets.only(top: 20.0, bottom: 20.0),
                      child: Center(
                        child: Text(
                          "By continuing, you agree to Terms & Conditions",
                          style: TextStyle(
                              color: Theme.of(context).accentColor,
                              fontSize: 11.0,
                              fontWeight: FontWeight.bold,
                              ),
                        ),
                      ),
                    ),
                    /*GestureDetector(
                      onTap: (){
                        Navigator.of(context).pushNamed(PatientRegistrationScreen.routeName);
                      },
                      child: Container(
                        margin: EdgeInsets.only(top: 20.0, bottom: 20.0),
                        child: Center(
                          child: Text(
                            "Register",
                            style: TextStyle(
                                color: Theme.of(context).accentColor,
                                fontSize: 11.0,
                                fontWeight: FontWeight.bold,
                                ),
                          ),
                        ),
                      ),
                    ),*/
                    SizedBox(
                      height: 40,
                    ),
                    /*Container(
                        child:Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children:[
                            Container(
                              height: 40.0,width: 70.0,
                              decoration: BoxDecoration(
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.transparent.withOpacity(0.10), offset: Offset(0,4), blurRadius: 10)
                                ],
                                borderRadius: BorderRadius.only(
                                  topLeft:Radius.circular(80.0),
                                  topRight: Radius.circular(0.0),
                                  bottomLeft: Radius.circular(0.0),
                                ),
                                color: Colors.transparent.withOpacity(0.1),
                              ),
                            ),
                          ],
                        ),
                      ),*/
                  ],
                ),


              ],
            ),
          ),
          LoadingWidget()
        ],
      ),
    );
  }

  void _signInWithFacebook() async {

    locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
    final FacebookLoginResult result = await facebookSignIn.logIn(['email']);

    switch (result.status) {
      case FacebookLoginStatus.loggedIn:
        final FacebookAccessToken accessToken = result.accessToken;

        final graphResponse = await http.get(
            Uri.parse('https://graph.facebook.com/v2.12/me?fields=name,picture.width(800).height(800),first_name,last_name,email&access_token=${accessToken.token}'));
        final profile = json.decode(graphResponse.body);

        debugPrint("facebook user : $profile");
        debugPrint("accessToken userId : ${accessToken.userId}");

        debugPrint("Facebook user creating");


        UserModel userModel = UserModel(
            email: profile['email'],
            fullname: '${profile['first_name']} ${profile['last_name']}',
            avatar_image: null/*profile['picture']['data']['url']*/,
            cover_image: null,
            id: accessToken.userId,
        );


        locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());

        _navigateToHomeScreen(userModel);

        break;
      case FacebookLoginStatus.cancelledByUser:
        MyToast.showToast('Login cancelled by the user.',context);

        locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
        break;
      case FacebookLoginStatus.error:
        MyToast.showToast('Something went wrong with the login process.\n'
            'Here\'s the error Facebook gave us: ${result.errorMessage}',context);

        locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
        break;
    }
  }

  void _signInWithGoogle() async {
    try {
      locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
      final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
      final GoogleSignInAuthentication googleAuth =
      await googleUser.authentication;
      final AuthCredential credential = GoogleAuthProvider.credential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );
      final User user =
          (await _auth.signInWithCredential(credential)).user;

      /* assert(user.email != null);
    assert(user.displayName != null);
    assert(!user.isAnonymous);
    assert(await user.getIdToken() != null);

    final FirebaseUser currentUser = await _auth.currentUser();
    assert(user.uid == currentUser.uid);*/
      if (user != null) {
        locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
        print("${user.email}, ${user.phoneNumber}, ${user.uid}, ${user.displayName}, ${user.photoURL}, ");
        String firstName = '';
        String lastName = '';
        if (user.displayName != null && user.displayName.length > 0) {
          List<String> dispName = user.displayName.split(" ");
          if (dispName.length > 1) {
            firstName = dispName[0];
            lastName = dispName[1];
          }
        }
        UserModel userModel = UserModel(
            id: user.uid,
            email: user.email,
            fullname: '$firstName $lastName',
            avatar_image: null /*user.photoUrl??Const.defaultProfileUrl*/,
            mobile: user.phoneNumber,
        );

        setState(() {});
        _navigateToHomeScreen(userModel);
      } else {
        locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
        await appPreference.clear();
        setState(() {});
      }
      locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
    }catch(e){
      debugPrint("Gmail login error : ${e.toString()}");
      locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
    }
  }

  void _navigateToHomeScreen(UserModel userModel) async {

    locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
    Map<String,dynamic> mapResult = await ApiRequest.postValidateDoctor(id_firebase: 'L0sRGNCwsKd0JBIXTvZzmJsauEB2'/*"${userModel.id}"*/);

    locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());


    final data = mapResult['data'];
    if (data is Map<String, dynamic> && data['id']!=0) {
      debugPrint("result : ${data}");

      UserModel updatedUser = UserModel.fromMap(data);
      locator<UserModelBloc>().userModelEventSink.add(updatedUser);
      debugPrint("result : ${data}");
      Navigator.pushNamedAndRemoveUntil(context, HomeScreen.routeName, (route) => false);

    } else if (data is int && data == 0) {
        Navigator.pushNamedAndRemoveUntil(context, PatientRegistrationScreen.routeName,(route) => false,arguments: [userModel]);
    }

  }
}
