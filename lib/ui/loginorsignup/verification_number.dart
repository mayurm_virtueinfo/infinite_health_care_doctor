import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:infinite_health_care_doctor/api/api_request.dart';
import 'package:infinite_health_care_doctor/bloc/loading/loading_bloc.dart';
import 'package:infinite_health_care_doctor/bloc/loading/loading_bloc_event.dart';
import 'package:infinite_health_care_doctor/bloc/user_model_bloc.dart';
import 'package:infinite_health_care_doctor/main.dart';
import 'package:infinite_health_care_doctor/models/user_model.dart';
import 'package:infinite_health_care_doctor/ui/home_screen.dart';
import 'package:infinite_health_care_doctor/ui/loginorsignup/patient_registration_screen.dart';
import 'package:infinite_health_care_doctor/utils/app_preferences.dart';
import 'package:infinite_health_care_doctor/utils/const_user.dart';
import 'package:infinite_health_care_doctor/utils/my_colors.dart';
import 'package:infinite_health_care_doctor/utils/my_toast.dart';
import 'package:infinite_health_care_doctor/widgets/loading_widget.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class VerificationNumber extends StatefulWidget {
  static const String routeName = '/VerificationNumber';
  final mobileNumber;

  String verificationId;
  int forceResendingToken;

  VerificationNumber({
    Key key,
    @required this.mobileNumber,
    @required this.verificationId,
    @required this.forceResendingToken,
  }) : super(key: key);

  @override
  _VerificationNumberState createState() => _VerificationNumberState();
}

class _VerificationNumberState extends State<VerificationNumber> {
  // bool isSendingCode = false;
  // bool isVerifyingCode = false;

  TextEditingController textEditingController = TextEditingController();
  bool isOnCompleted = false;
  String strCode = '';
  String newMobileString = '';
  FocusNode tFocusNode = FocusNode();
  @override
  void initState() {
    List<String> temp = widget.mobileNumber.toString().split('');
    for (int i = 0; i < temp.length; i++) {
      if (i >= 6 && i <= 10) {
        newMobileString += '*';
      } else {
        newMobileString += temp[i];
      }
    }
    setState(() {});
    super.initState();

  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xeeffffff),
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.close, color: Colors.black),
          onPressed: () {
//            Navigator.of(context).pushNamed(EnterValidMobileNumber.routeName);
            Navigator.of(context).pop();
          },
        ),
      ),
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  alignment: Alignment.center,
                  margin: EdgeInsets.only(top: 30.0),
                  child: Text(
                    "Enter Code",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 22.0,
                        fontWeight: FontWeight.bold,
                        ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 12.0),
                  child: Text(
                    'we have sent you an SMS on your ${newMobileString} \n with 6 digit verification cede.',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.grey,
                        fontSize: 11.0,
                        fontWeight: FontWeight.bold,
                        ),
                  ),
                ),

                Container(
                  margin: EdgeInsets.only(top: 12.0, right: 12.0, left: 12.0),
                  decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.circular(12.0),
                  ),
                  child: Column(
                    children: [
                      SizedBox(height: 20,),
                      Padding(
                          padding: const EdgeInsets.symmetric(vertical: 08, horizontal: 20),
                          child: PinCodeTextField(
                            focusNode: tFocusNode,
                            enablePinAutofill: true,
                            appContext: context,
                            pastedTextStyle: TextStyle(
                              color: Colors.green.shade600,
                              fontWeight: FontWeight.bold,
                            ),
                            length: 6,
                            // obscureText: true,
                            // obscuringCharacter: '*',
                            blinkWhenObscuring: true,
                            animationType: AnimationType.fade,
                            pinTheme: PinTheme(
                                shape: PinCodeFieldShape.underline,
                                fieldHeight: 50,
                                fieldWidth: 40,
                                activeColor: Colors.black,
                                borderWidth: 0,
                                activeFillColor: Colors.white,
                                inactiveColor: Colors.black,
                                inactiveFillColor: Colors.white,
                                selectedFillColor: Colors.white,
                                selectedColor: MyColors.getAppButtonColor

                              // selectedColor: Colors.yellow
                              // activeFillColor:                        hasError ? Colors.orange : Colors.white,
                            ),

                            cursorColor: Colors.black,
                            animationDuration: Duration(milliseconds: 300),
                            // backgroundColor: Colors.blue.shade50,
                            enableActiveFill: true,
                            // errorAnimationController: errorController,
                            controller: textEditingController,
                            inputFormatters: <TextInputFormatter>[
                              FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                            ],
                            keyboardType: TextInputType.number,
                            /*boxShadows: [
                          BoxShadow(
                            offset: Offset(0, 1),
                            color: Colors.black12,
                            blurRadius: 10,
                          )
                        ],*/
                            onCompleted: (v) {
                              print("Completed");
                              setState(() {
                                isOnCompleted = true;
                                strCode = v;
                              });
                              print(v);
                            },
                            // onTap: () {
                            //   print("Pressed");
                            // },
                            onChanged: (value) {
                              print(value);
                              setState(() {
                                if (value.length < 6) {
                                  isOnCompleted = false;
                                  strCode = '';
                                }
                              });
                            },
                            beforeTextPaste: (text) {
                              print("Allowing to paste $text");
                              //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
                              //but you can show anything you want here, like your pop up saying wrong paste format or etc
                              return true;
                            },
                          )),
                      Container(
                              margin: EdgeInsets.only(
                                  top: 40.0, bottom: 20.0, right: 30.0, left: 30.0),
                              height: 40,
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  primary: Theme.of(context).accentColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(30.0),
                                  ),
                                ),

                                onPressed: () async {


                                  FirebaseAuth _auth = FirebaseAuth.instance;
                                  String mCode = strCode;
                                  final code = mCode.trim();
                                  if (!isOnCompleted && code.length <= 6) {
                                    MyToast.showToast("You have entered invalid code",context);
                                    return;
                                  }
                                  FocusScope.of(context).requestFocus(FocusNode());
                                  debugPrint("Code : $code");
                                  debugPrint(
                                      "verificationId : ${widget.verificationId}");
                                  try {
                                    setState(() {
                                      locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
                                    });
                                    AuthCredential credential =
                                        PhoneAuthProvider.credential(
                                            verificationId: widget.verificationId,
                                            smsCode: code);

                                    _signInWithPhoneNumber(
                                        _auth, credential, widget.mobileNumber);
                                  } catch (e) {
                                    MyToast.showToast("${e.message}",context);
                                    setState(() {
                                      locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
                                    });
                                  }
                                },

                                child: Container(
                                  child: Center(
                                    child: Text(
                                      'Submit',
                                      style: TextStyle(
                                        fontSize: 18.0,
                                        color: Theme.of(context).primaryColor,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                    ],
                  ),
                ),
                Container(
                  alignment: Alignment.center,
                  margin: EdgeInsets.only(top: 30.0),
                  child: Text(
                    "Did not receive the code?",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 12.0,
                        fontWeight: FontWeight.bold,
                        ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(top: 12.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      TextButton(
                              onPressed: () {
                                loginUser(widget.mobileNumber, context);
                              },
                              child: Text(
                                "Re-send",
                                style: TextStyle(
                                    color: Theme.of(context).accentColor,
                                    fontSize: 14.0,
                                    fontWeight: FontWeight.bold,
                                    ),
                              ),
                            ),

                    ],
                  ),
                ),
              ],
            ),
          ),
          LoadingWidget()
        ],
      ),
    );
  }

  Future<bool> loginUser(String phone, BuildContext context) async {
    FirebaseAuth _auth = FirebaseAuth.instance;
    locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());

    _auth.verifyPhoneNumber(
      forceResendingToken: widget.forceResendingToken,
        phoneNumber: phone,
        timeout: Duration(seconds: 60),
        verificationCompleted: (AuthCredential credential) async {
          debugPrint("verificationCompleted : $credential");
          try{
            final User user =
                (await _auth.signInWithCredential(credential)).user;
            if (user != null) {
              MyToast.showToast("OTP Verification is Successfull",context);
              print(
                  "${user.email}, ${user.phoneNumber}, ${user.uid}, ${user.displayName}, ${user.photoURL}, ");
              String firstName = '';
              String lastName = '';
              if (user.displayName != null && user.displayName.length > 0) {
                List<String> dispName = user.displayName.split(" ");
                if (dispName.length > 1) {
                  firstName = dispName[0];
                  lastName = dispName[1];
                }
              }
              UserModel userModel = UserModel(
                  id: user.uid,
                  email: user.email,
                  fullname: '$lastName $firstName',
                  avatar_image: null ,
                  mobile: user.phoneNumber);

              // isSendingCode = false;
              locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
              setState(() {});

              navigateToHomeScreen(userModel);
            } else {
              // isSendingCode = false;
              locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
              setState(() {});
            }
          }catch(e){
            MyToast.showToast(e.message,context);
            // isSendingCode = false;
            locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
            setState(() {});
          }
        },
        verificationFailed: (FirebaseAuthException exception) {
          debugPrint("verificationFailed : $exception");
          // setState(() {
          //   isSendingCode = false;
            locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
          // });
          MyToast.showToast("${exception.message}",context);
          print(exception);
        },
        codeSent: (String verificationId, [int forceResendingToken]) async {
          debugPrint("codeSent : $verificationId, $forceResendingToken");
          setState(() {
            widget.verificationId = verificationId;
            widget.forceResendingToken = forceResendingToken;
            // isSendingCode = false;
            locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
          });
          MyToast.showToast("Code is sent to the entered number",context);
        },
      codeAutoRetrievalTimeout: (verificationId) {},
    );
  }

  Future<Map<String, dynamic>> createUser(
      {String userId,
      String email,
      String loginType,
      String first_name,
      String last_name}) async {
    Map<String, dynamic> mapData = {
      ConstUser.userId: userId,
      ConstUser.email: email,
      ConstUser.loginType: loginType,
      ConstUser.firstname: first_name,
      ConstUser.lastname: last_name
    };

    return mapData;
  }

  void _signInWithPhoneNumber(_auth, credential, phoneNumber) async {
    setState(() {
      locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
    });

    try{
    final User user =
        (await _auth.signInWithCredential(credential)).user;
    if (user != null) {
      MyToast.showToast("OTP Verification is Successfull",context);
      print(
          "${user.email}, ${user.phoneNumber}, ${user.uid}, ${user.displayName}, ${user.photoURL}, ");
      String firstName = '';
      String lastName = '';
      if (user.displayName != null && user.displayName.length > 0) {
        List<String> dispName = user.displayName.split(" ");
        if (dispName.length > 1) {
          firstName = dispName[0];
          lastName = dispName[1];
        }
      }
      UserModel userModel = UserModel(
          id: user.uid,
          email: user.email,
          fullname: '$lastName $firstName',
          avatar_image: null,
          mobile: user.phoneNumber);

      locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
      setState(() {});

      navigateToHomeScreen(userModel);
    } else {
      locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
      setState(() {});
    }
    }catch(e){
      MyToast.showToast(e.message,context);
      locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
      setState(() {});
    }
  }

  void navigateToHomeScreen(UserModel userModel) async {
    locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
    Map<String, dynamic> mapResult = await ApiRequest.postValidateDoctor(id_firebase: userModel.id);
    locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
    final data = mapResult['data'];
    if (data is Map<String, dynamic> && data['id'] != 0) {

      userModel = UserModel.fromMap(data);
      locator<UserModelBloc>().userModelEventSink.add(userModel);
      Navigator.pushNamedAndRemoveUntil(
          context, HomeScreen.routeName, (route) => false,
          arguments: [userModel]);
    } else if (data is int && data == 0) {
      Navigator.pushNamedAndRemoveUntil(
          context, PatientRegistrationScreen.routeName, (route) => false,
          arguments: [userModel]);
    }
  }
}
