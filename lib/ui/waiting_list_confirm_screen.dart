import 'dart:async';

import 'package:flutter/material.dart';
import 'package:infinite_health_care_doctor/utils/my_colors.dart';
import 'package:intl/intl.dart';

class WaitingListConfirmScreen extends StatefulWidget {
  static const String routeName = "/WaitingListConfirmScreen";
  static const String screenName = "WaitingListConfirmScreen";
  String idCategory;

  WaitingListConfirmScreen({this.idCategory});

  @override
  _WaitingListConfirmScreenState createState() => _WaitingListConfirmScreenState();
}

enum SelectedStatusButton { NONE, REJECT, ACCEPT }

class _WaitingListConfirmScreenState extends State<WaitingListConfirmScreen> {
  /// Setting duration in splash screen

  Future<bool> _onBackPressed() {
//    Navigator.of(context).pop(false);
  }

  SelectedStatusButton selectedStatusButton = SelectedStatusButton.NONE;
  bool isLoading = false;

  String bookingDate;
  bool displayApptAvail = true;
  bool displayAcceptAvail = false;
  bool displayRejectAvail = false;

  /// Declare startTime to InitState
  @override
  void initState() {
    super.initState();
    // startTime();
    // fetchBookingDetails();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    double paddingLeftRight = screenWidth / 10;
    double paddingTopBottom = screenHeight / 2.5;
    double dialogWidth = screenWidth - paddingLeftRight;
    double dialogHeight = screenHeight - paddingTopBottom;
    return WillPopScope(
        onWillPop: _onBackPressed,
        child: Scaffold(
            appBar: AppBar(backgroundColor: MyColors.getAppButtonColor, elevation: 0.0, leading: Container()),
            body: Container(
                constraints: BoxConstraints(
                    minWidth: MediaQuery.of(context).size.width,
                    minHeight: MediaQuery.of(context).size.height),
                color: MyColors.getAppButtonColor,
                child: Text("Notification clicked")
            )
        )
    );
  }
}
