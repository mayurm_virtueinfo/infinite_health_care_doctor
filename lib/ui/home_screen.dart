import 'package:flutter/material.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:infinite_health_care_doctor/models/user_model.dart';
import 'package:infinite_health_care_doctor/ui/account/account_tab.dart';
import 'package:infinite_health_care_doctor/ui/conversation/conversation_tab.dart';
import 'package:infinite_health_care_doctor/ui/home/home_tab.dart';
import 'package:infinite_health_care_doctor/main.dart';

class HomeScreen extends StatefulWidget {

  static const String routeName = '/HomeScreen';

  const HomeScreen({Key key}) : super(key: key);

  
  @override
  State<StatefulWidget> createState() {
    return _BubblesState();
  }
}

class _BubblesState extends State<HomeScreen> with SingleTickerProviderStateMixin {
  int _page = 0;
  String currentTitle = 'Home';
  Widget _currentPage (int page){
    switch (page){
      case 0 :
        currentTitle = 'Home';
        return HomeTab();
      case 1 :
        currentTitle = 'chat';  
        return ConversationTab();
      case 2 :
        currentTitle = 'profile';
        return AccountTab();
      default:
        currentTitle = 'Home';
        return HomeTab();

    }
    
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _currentPage(_page),
      bottomNavigationBar: CurvedNavigationBar(
        index: 0,
        items: <Widget>[
          Icon(Icons.home, size: 25,color: Theme.of(context).primaryColor,),
          Icon(Icons.chat, size: 25,color: Theme.of(context).primaryColor,),
          Icon(Icons.perm_identity, size: 25,color: Theme.of(context).primaryColor,),
        ],
        color: Theme.of(context).accentColor,
        buttonBackgroundColor: Theme.of(context).accentColor,
        backgroundColor: Colors.transparent,
        animationCurve: Curves.easeInOut,
        animationDuration: Duration(milliseconds: 300),
        onTap: (index) {
          setState(() {
            _page = index;
            _currentPage(_page);
          });
        },
      ),
    );
  }
}



