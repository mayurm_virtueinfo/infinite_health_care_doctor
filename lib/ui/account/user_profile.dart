import 'dart:io';
import 'package:infinite_health_care_doctor/bloc/user_model_bloc.dart';
import 'package:infinite_health_care_doctor/main.dart';
import 'package:infinite_health_care_doctor/bloc/loading/loading_bloc_event.dart';
import 'package:infinite_health_care_doctor/bloc/loading/loading_bloc.dart';
import 'package:infinite_health_care_doctor/widgets/loading_widget.dart';
import 'package:infinite_health_care_doctor/main.dart';

import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:image_picker/image_picker.dart';
import 'package:infinite_health_care_doctor/api/api_request.dart';
import 'package:infinite_health_care_doctor/config/app_config.dart' as config;
import 'package:infinite_health_care_doctor/config/const.dart';
import 'package:infinite_health_care_doctor/models/user_model.dart';
import 'package:infinite_health_care_doctor/ui/loginorsignup/search_address.dart';
import 'package:infinite_health_care_doctor/utils/app_preferences.dart';
import 'package:infinite_health_care_doctor/utils/app_style.dart';
import 'package:infinite_health_care_doctor/utils/const_user.dart';
import 'package:infinite_health_care_doctor/utils/my_colors.dart';
import 'package:infinite_health_care_doctor/utils/my_toast.dart';
import 'package:infinite_health_care_doctor/utils/utility.dart';
import 'package:infinite_health_care_doctor/widgets/app_button.dart';
import 'package:infinite_health_care_doctor/widgets/label_text.dart';
import 'package:infinite_health_care_doctor/widgets/label_textarea.dart';
import 'package:infinite_health_care_doctor/widgets/profile_header.dart';
import 'package:infinite_health_care_doctor/widgets/profile_header_update_profile.dart';
import 'package:multiselect_formfield/multiselect_formfield.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:flutter/material.dart';
import 'dart:math';


class UserProfile extends StatefulWidget {

  UserModel user;
  static const String routeName = "/UserProfile";

  UserProfile({this.user});
  @override
  _UserProfileState createState() => _UserProfileState();
}

enum PhotoUpload { none, profile, cover }

class _UserProfileState extends State<UserProfile> {
  PhotoUpload photoUpload;
  var coverPhoto;
  File photo;
  bool isImageUploading = false;
  bool isCoverUploading = false;
  Map<String, dynamic> appUser;
  UserModel currentUser;
  GlobalKey<FormState> _formState = GlobalKey<FormState>();
  bool enableEmail = true;
  bool enablePhone = true;
  bool isEmailRequired = false;

  List<Map<String,dynamic>> listSpecialization;
  List<dynamic> selectedSpecialization = [];

  List<Map<String,dynamic>> listCategory;
  List<dynamic> selectedCategory = [];
  List<Map<String,dynamic>> listDegree;
  List<dynamic> selectedDegrees = [];
  List<Map<String,dynamic>> listService;
  List<dynamic> selectedServices = [];
  bool isLoading = true;

  FocusNode fnFirstName = FocusNode();
  FocusNode fnMobileNumber = FocusNode();
  FocusNode fnFees = FocusNode();
  FocusNode fnExperience = FocusNode();
  FocusNode fnState = FocusNode();
  FocusNode fnCity = FocusNode();
  FocusNode fnAddress = FocusNode();
  FocusNode fnZipcode = FocusNode();
  FocusNode fnDescription = FocusNode();

  TextEditingController _fullnameController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _mobileNumberController = TextEditingController();
  TextEditingController _stateController = TextEditingController();
  TextEditingController _cityController = TextEditingController();
  TextEditingController _addressController = TextEditingController();
  TextEditingController _zipController = TextEditingController();
  TextEditingController _descriptionController = TextEditingController();
  TextEditingController _feesController = TextEditingController();
  TextEditingController _experienceController = TextEditingController();

  Mode _mode = Mode.overlay;
  final homeScaffoldKey = GlobalKey<ScaffoldState>();
// to get places detail (lat/lng)
  GoogleMapsPlaces _places = GoogleMapsPlaces(apiKey: kGoogleApiKey);
  // final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  Position _currentPosition;
  String _currentAddress;
  @override
  void initState() {
    initData();

    super.initState();
  }
  void initData() async{
    setState(() {
      isLoading = true;
    });
    photoUpload = PhotoUpload.none;

    bool isMobile = await Utility.isUserSignInWithMobile();
    bool isGmail = await Utility.isUserSignInWithGmail();
    bool isFacebook = await Utility.isUserSignInWithFacebook();
    if(isGmail || isFacebook){
      enableEmail = false;
    }
    if(isMobile){
      enablePhone = false;
      isEmailRequired = true;
    }

    await initCategoryServiceDegree();

    _fullnameController.text = widget.user.fullname;
    _emailController.text = widget.user.email;
    _mobileNumberController.text = widget.user.mobile;
    _stateController.text = widget.user.state;
    _cityController.text = widget.user.city;
    _addressController.text = widget.user.address;
    _zipController.text = widget.user.zipcode;
    _descriptionController.text = widget.user.description;
    _feesController.text = widget.user.fees;
    _experienceController.text = widget.user.experience;
    debugPrint("user lat : ${widget.user.lat}");
    debugPrint("user lng : ${widget.user.lng}");
    if((widget.user.lat !=null && widget.user.lat!='' ) && (widget.user.lng !=null && widget.user.lng != '')){
      _currentPosition = Position(latitude: double.parse(widget.user.lat),longitude: double.parse(widget.user.lng),);
      _getAddressFromLatLng();
      debugPrint("thats at yes");
    }

    setState(() {
      isLoading = false;
    });
  }
  Future<void> initCategoryServiceDegree() async{
    // locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());

    debugPrint("dieses_data : ${widget.user.dieses_data}");
    debugPrint("category : ${widget.user.category_data}");
    debugPrint("degree : ${widget.user.degree_data}");

    selectedCategory.clear();
    widget.user.category_data.forEach((element) {
      selectedCategory.add('${element['id']}');
    });

    selectedServices.clear();
    widget.user.dieses_data.forEach((element) {
      selectedServices.add('${element['id']}');
    });

    selectedDegrees.clear();
    widget.user.degree_data.forEach((element) {
      selectedDegrees.add('${element['id']}');
    });

    selectedSpecialization.clear();
    widget.user.specialization.forEach((element) {
      selectedSpecialization.add('${element['id']}');
    });

    debugPrint("selected service : ${selectedServices}");
    debugPrint("selected category : ${selectedCategory}");
    debugPrint("selected degree : ${selectedDegrees}");

    final resultCatList = await ApiRequest.getCategoryList();
    List catList = resultCatList['data'];

    listCategory = [];
    catList.forEach((element) {
      Map<String,dynamic> mapCat=Map();
      mapCat['id'] = '${element['id']}';
      mapCat['title'] = '${element['title']}';
      listCategory.add(mapCat);
    });

    // listCategory = catList;

    final resultDegreeList = await ApiRequest.getDegreeList();
    List degreeList = resultDegreeList['data'];
    listDegree = [];
    degreeList.forEach((element) {
      Map<String,dynamic> mapCat=Map();
      mapCat['id'] = '${element['id']}';
      mapCat['title'] = '${element['title']}';
      listDegree.add(mapCat);
    });

    final resultSpecializationList = await ApiRequest.getSpecializationList();
    List specializationList = resultSpecializationList['data'];
    listSpecialization = [];
    specializationList.forEach((element) {
      Map<String,dynamic> mapCat=Map();
      mapCat['id'] = '${element['id']}';
      mapCat['title'] = '${element['title']}';
      mapCat['status'] = '${element['status']}';
      listSpecialization.add(mapCat);
    });

    final resultServiceList = await ApiRequest.getServiceList();
    List serviceList = resultServiceList['data'];
    listService = [];
    serviceList.forEach((element) {
      Map<String,dynamic> mapCat=Map();
      mapCat['id'] = '${element['id']}';
      mapCat['title'] = '${element['title']}';
      listService.add(mapCat);
    });
  }
  UserModel previousNonUpdateUser = null;

  @override
  Widget build(BuildContext context) {
    UserModel previousNonUpdateUser = appUserModel;
    currentUser = appUserModel;


    double deviceWidth = MediaQuery.of(context).size.width;
    double deviceHeight = MediaQuery.of(context).size.height;
    double headerSize = (deviceHeight / 3.75);
    double devicePixelRatio = MediaQuery.of(context).devicePixelRatio;
    double coverHeight = Utility.getCoverHeight(context);
    double profilePicSize = Utility.getProfilePicSize(context);
    double topPadding = coverHeight - (profilePicSize / 2);

    return Scaffold(
      extendBodyBehindAppBar: true,
//      backgroundColor: Theme.of(context).accentColor,
      appBar: AppBar(
//        backgroundColor: Colors.transparent,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Theme.of(context).primaryColor),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        backgroundColor: Color(0x44000000),
        elevation: 0,
        title: Text(
          "Update Profile",
          style: TextStyle(
            fontSize: 22.0,
            fontWeight: FontWeight.bold,
            color: Theme.of(context).primaryColor,
          ),
        ),
      ),
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              children: [
                Stack(
                  children: <Widget>[
                    Container(
                      constraints: BoxConstraints(
                        minWidth: MediaQuery.of(context).size.width,
                        minHeight: coverHeight,
                      ),
                      child: Stack(
                        alignment: Alignment.bottomRight,
                        children: [
                          Container(
                            height: coverHeight,
                            width: MediaQuery.of(context).size.width,
                            child: currentUser.cover_image != null && coverPhoto == null
                                ? FancyShimmerImage(
                                    boxFit: BoxFit.cover,
                                    imageUrl: currentUser.cover_image,
                                    shimmerBaseColor: Colors.white,
                                    shimmerHighlightColor: Theme.of(context).accentColor,
                                    errorWidget: Image.network('https://i0.wp.com/www.dobitaobyte.com.br/wp-content/uploads/2016/02/no_image.png?ssl=1'),
                                  )
                                : (coverPhoto == null && currentUser.cover_image == null)
                                    ? FancyShimmerImage(
                                        boxFit: BoxFit.cover,
                                        imageUrl: Const.defaultCoverUrl,
                                        shimmerBaseColor: Colors.white,
                                        shimmerHighlightColor: Theme.of(context).accentColor,
                                        errorWidget: Image.network('https://i0.wp.com/www.dobitaobyte.com.br/wp-content/uploads/2016/02/no_image.png?ssl=1'),
                                      )
                                    : (coverPhoto is File)
                                        ? Image.file(
                                            coverPhoto,
                                            fit: BoxFit.cover,
                                          )
                                        : FancyShimmerImage(
                                            boxFit: BoxFit.cover,
                                            imageUrl: coverPhoto,
                                            shimmerBaseColor: Colors.white,
                                            shimmerHighlightColor: Theme.of(context).accentColor,
                                            errorWidget: Image.network('https://i0.wp.com/www.dobitaobyte.com.br/wp-content/uploads/2016/02/no_image.png?ssl=1'),
                                          ),
                          ),
                          Container(
                              margin: EdgeInsets.only(right: 10, bottom: 5),
                              alignment: Alignment.bottomRight,
                              child: InkWell(
                                onTap: () {
                                  photoUpload = PhotoUpload.cover;
                                  _settingModalBottomSheet(context);
                                },
                                child: Container(
                                  decoration: BoxDecoration(shape: BoxShape.circle, color: Theme.of(context).accentColor),
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Icon(
                                      Icons.photo_camera,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              )),
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: topPadding, left: 10, right: 10),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          ProfileHeaderUpdagteProfile(
                            imagePath: photo ?? currentUser.avatar_image,
                            onTap: () {
                              photoUpload = PhotoUpload.profile;
                              _settingModalBottomSheet(context);
                            },
                            isImageUploading: isImageUploading,
                            showCameraIcon: true,
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment
                                .center,
                            children: <Widget>[
                              Utility.getProfileString(currentUser.address
                                  , currentUser.city
                                  , currentUser.state
                                  , currentUser.zipcode) == ''?Container():Icon(Icons.location_on,
                                color: Theme.of(context).accentColor,size: 18,),
                              SizedBox(width: 5,),
                              Container(
                                constraints: BoxConstraints(
                                    maxWidth: MediaQuery.of(context).size.width-150,
                                    // minHeight: 50

                                ),
                                child: Text(Utility.getProfileString(currentUser.address
                                    , currentUser.city
                                    , currentUser.state
                                    , currentUser.zipcode),textAlign: TextAlign.center,),
                              )
                              ,
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(top: 10),
                      margin: EdgeInsets.only(top: 10.0, left: 14.0, right: 14.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20.0),
                        color: Theme.of(context).primaryColor,
                      ),
                      child: Form(
                        key: _formState,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
//                              fullname
                            Container(
                              child: TextFormField(
                                focusNode: fnFirstName,
                                textInputAction: TextInputAction.next,
                                onFieldSubmitted: (term){
                                  fnMobileNumber.requestFocus();
                                },
                                validator: (text) {
                                  if (text.length == 0) {
                                    return "Fullname is required";
                                  }

                                  return null;
                                },
//                                            maxLength: 20,
                                controller: _fullnameController,
                                decoration: InputDecoration(
                                    errorStyle: AppStyles.errorStyle,
                                    focusedBorder: AppStyles.focusBorder,
                                    prefixIcon: Icon(
                                      Icons.account_circle,
                                      color: config.Colors().mainColor(1),
                                    ),
                                    labelStyle: TextStyle(
                                        color:
                                        config.Colors().mainColor(1)),
                                    border: OutlineInputBorder(),
                                    labelText: "Enter Full Name"),
                              ),
                              padding: EdgeInsets.all(20),
                            ),

//                              email
                            Container(
                                margin:
                                EdgeInsets.only(left: 5, right: 5),
                                child: Divider(
                                  height: 1,
                                  color: config.Colors().mainColor(1),
                                )),
                            Container(
                              child: TextFormField(

                                enabled: enableEmail,
                                validator: (text) {
                                  if (isEmailRequired &&
                                      text.length == 0) {
                                    return "Email is required";
                                  }

                                  return null;
                                },
//                                        maxLength: 15,
                                controller: _emailController,
                                decoration: InputDecoration(
                                  errorStyle: AppStyles.errorStyle,
                                  focusedBorder: AppStyles.focusBorder,
                                  prefixIcon: Icon(
                                    Icons.mail,
                                    color: config.Colors().mainColor(1),
                                  ),
                                  labelStyle: TextStyle(
                                      color:
                                      config.Colors().mainColor(1)),
                                  border: OutlineInputBorder(),
                                  labelText: "Enter Email",
                                ),
                              ),
                              padding: EdgeInsets.all(20),
                            ),

//                              mobile_number
                            Container(
                                margin:
                                EdgeInsets.only(left: 5, right: 5),
                                child: Divider(
                                  height: 1,
                                  color: config.Colors().mainColor(1),
                                )),
                            Container(
                              child: TextFormField(
                                focusNode: fnMobileNumber,
                                textInputAction: TextInputAction.next,
                                onFieldSubmitted: (term){
                                  fnFees.requestFocus();
                                },
                                enabled: enablePhone,
                                validator: (text) {
                                  if (text.length == 0) {
                                    return "Mobile Number is required";
                                  }
                                  /*else if(text.length < 10){
                                                return "Invalid Mobile Number";
                                              }*/
                                  return null;
                                },
                                controller: _mobileNumberController,
                                keyboardType: TextInputType.number,
//                                        maxLength: 10,
                                decoration: InputDecoration(
                                  errorStyle: AppStyles.errorStyle,
                                  focusedBorder: AppStyles.focusBorder,
                                  prefixIcon: Icon(
                                    Icons.call,
                                    color: config.Colors().mainColor(1),
                                  ),
                                  labelStyle: TextStyle(
                                      color:
                                      config.Colors().mainColor(1)),
                                  border: OutlineInputBorder(),
                                  labelText: "Enter Mobile Number",
                                ),
                              ),
                              padding: EdgeInsets.all(20),
                            ),

                            // category

                            isLoading?Container():Column(
                              children: [
                                Container(
                                    margin: EdgeInsets.only(left: 5, right: 5),
                                    child: Divider(
                                      height: 1,
                                      color: config.Colors().mainColor(1),
                                    )),
                                Container(
                                  padding: EdgeInsets.all(3),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(5)),
                                      border: Border.all(width: 1,color: Colors.grey)),
                                  margin: EdgeInsets.all(20),
                                  child: MultiSelectFormField(
                                      initialValue: selectedCategory,
                                      fillColor: Colors.white,
                                      border: InputBorder.none,
                                      autovalidate: false,
                                      title: Text('Category'),
                                      validator: (value) {
                                        if (value == null ||
                                            value.length == 0) {
                                          return 'Please select one or more options';
                                        }
                                        return null;
                                      },
                                      dataSource: listCategory,
                                      textField: 'title',
                                      valueField: 'id',
                                      okButtonLabel: 'OK',
                                      cancelButtonLabel: 'CANCEL',
                                      // required: true,
                                      hintWidget: Text('Please choose one or more'),
                                      // value: _myActivities,
                                      onSaved: (value) {
                                        debugPrint("cat value : ${value.toString()}");
                                        FocusScope.of(context).requestFocus(FocusNode());
                                        if (value == null) return;
                                        setState(() {
                                          selectedCategory = value;
                                        });
                                      }
                                  ),
                                ),
                              ],
                            ),

                            // service

                            isLoading?Container():Column(
                              children: [
                                Container(
                                    margin: EdgeInsets.only(left: 5, right: 5),
                                    child: Divider(
                                      height: 1,
                                      color: config.Colors().mainColor(1),
                                    )),
                                Container(
                                  padding: EdgeInsets.all(3),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(5)),
                                      border: Border.all(width: 1,color: Colors.grey)),
                                  margin: EdgeInsets.all(20),
                                  child: MultiSelectFormField(
                                      initialValue: selectedServices,
                                      fillColor: Colors.white,
                                      border: InputBorder.none,
                                      autovalidate: false,
                                      title: Text('Service'),
                                      validator: (value) {
                                        if (value == null ||
                                            value.length == 0) {
                                          return 'Please select one or more options';
                                        }
                                        return null;
                                      },
                                      dataSource: listService,
                                      textField: 'title',
                                      valueField: 'id',
                                      okButtonLabel: 'OK',
                                      cancelButtonLabel: 'CANCEL',
                                      // required: true,
                                      hintWidget: Text('Please choose one or more'),
                                      // value: _myActivities,
                                      onSaved: (value) {
                                        FocusScope.of(context).requestFocus(FocusNode());
                                        if (value == null) return;
                                        setState(() {
                                          selectedServices = value;
                                        });
                                      }
                                  ),
                                ),
                              ],
                            ),

                            // degree

                            isLoading?Container():Column(
                              children: [
                                Container(
                                    margin: EdgeInsets.only(left: 5, right: 5),
                                    child: Divider(
                                      height: 1,
                                      color: config.Colors().mainColor(1),
                                    )),
                                Container(
                                  padding: EdgeInsets.all(3),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(5)),
                                      border: Border.all(width: 1,color: Colors.grey)),
                                  margin: EdgeInsets.all(20),
                                  child: MultiSelectFormField(
                                      initialValue: selectedDegrees,
                                      fillColor: Colors.white,
                                      border: InputBorder.none,
                                      autovalidate: false,
                                      title: Text('Degree'),
                                      validator: (value) {
                                        if (value == null ||
                                            value.length == 0) {
                                          return 'Please select one or more options';
                                        }
                                        return null;
                                      },
                                      dataSource: listDegree,
                                      textField: 'title',
                                      valueField: 'id',
                                      okButtonLabel: 'OK',
                                      cancelButtonLabel: 'CANCEL',
                                      // required: true,
                                      hintWidget: Text('Please choose one or more'),
                                      // value: _myActivities,
                                      onSaved: (value) {
                                        FocusScope.of(context).requestFocus(FocusNode());
                                        if (value == null) return;
                                        setState(() {
                                          selectedDegrees = value;
                                        });
                                      }
                                  ),
                                ),
                              ],
                            ),

                            // specialization

                            isLoading?Container():Column(
                              children: [
                                Container(
                                    margin: EdgeInsets.only(left: 5, right: 5),
                                    child: Divider(
                                      height: 1,
                                      color: config.Colors().mainColor(1),
                                    )),
                                Container(
                                  padding: EdgeInsets.all(3),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(5)),
                                      border: Border.all(width: 1,color: Colors.grey)),
                                  margin: EdgeInsets.all(20),
                                  child: MultiSelectFormField(
                                      initialValue: selectedSpecialization,
                                      fillColor: Colors.white,
                                      border: InputBorder.none,
                                      autovalidate: false,
                                      title: Text('Specialization'),
                                      validator: (value) {
                                        if (value == null ||
                                            value.length == 0) {
                                          return 'Please select one or more options';
                                        }
                                        return null;
                                      },
                                      dataSource: listSpecialization,
                                      textField: 'title',
                                      valueField: 'id',
                                      okButtonLabel: 'OK',
                                      cancelButtonLabel: 'CANCEL',
                                      // required: true,
                                      hintWidget: Text('Please choose one or more'),
                                      // value: _myActivities,
                                      onSaved: (value) {
                                        FocusScope.of(context).requestFocus(FocusNode());
                                        if (value == null) return;
                                        setState(() {
                                          selectedSpecialization = value;
                                        });
                                      }
                                  ),
                                ),
                              ],
                            ),

//                              fees
                            Container(
                                margin:
                                EdgeInsets.only(left: 5, right: 5),
                                child: Divider(
                                  height: 1,
                                  color: config.Colors().mainColor(1),
                                )),
                            Container(
                              child: TextFormField(
                                focusNode: fnFees,
                                textInputAction: TextInputAction.next,
                                onFieldSubmitted: (term){
                                  fnState.requestFocus();
                                },
                                validator: (text){
                                  if(text.length == 0) {
                                    return "Enter fees";
                                  }
                                  return null;
                                },
                                controller: _feesController,
                                keyboardType: TextInputType.number,
//                                        maxLength: 10,
                                decoration: InputDecoration(
                                  errorStyle: AppStyles.errorStyle,
                                  focusedBorder: AppStyles.focusBorder,
                                  prefixText: "₹",
                                  /*prefixIcon: Icon(
                                                Icons.attach_money,
                                                color: config.Colors().mainColor(1),
                                              ),*/
                                  labelStyle: TextStyle(
                                      color:
                                      config.Colors().mainColor(1)),
                                  border: OutlineInputBorder(),
                                  labelText: "Enter Fees",
                                ),
                              ),
                              padding: EdgeInsets.all(20),
                            ),

//                              Experience
                            Container(
                                margin:
                                EdgeInsets.only(left: 5, right: 5),
                                child: Divider(
                                  height: 1,
                                  color: config.Colors().mainColor(1),
                                )),
                            Container(
                              child: TextFormField(
                                focusNode: fnExperience,
                                textInputAction: TextInputAction.next,
                                onFieldSubmitted: (term){
                                  debugPrint("tempExperience : $term");
                                  fnFees.requestFocus();
                                },
                                validator: (text){
                                  if(text.length == 0){
                                    return "Enter experience";
                                  }
                                  return null;
                                },
                                controller: _experienceController,
                                keyboardType: TextInputType.number,
                                maxLength: 2,
                                decoration: InputDecoration(
                                  errorStyle: AppStyles.errorStyle,
                                  focusedBorder: AppStyles.focusBorder,
                                  prefixIcon: Icon(
                                    Icons.badge,
                                    color: config.Colors().mainColor(1),
                                  ),
                                  labelStyle: TextStyle(
                                      color:
                                      config.Colors().mainColor(1)),
                                  border: OutlineInputBorder(),
                                  labelText: "Enter Experience ( in year )",
                                ),
                              ),
                              padding: EdgeInsets.all(20),
                            ),

//                              location
                            Container(
                                margin:
                                EdgeInsets.only(left: 5, right: 5),
                                child: Divider(
                                  height: 1,
                                  color: config.Colors().mainColor(1),
                                )),
                            Container(
                              constraints: BoxConstraints(
                                  minHeight: 50
                              ),

                              padding: EdgeInsets.all(20),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[

                                  Expanded(
                                    flex: 9,
                                    child: GestureDetector(
                                      onTap: (){
                                        _handlePressButton();

                                      },
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          Text(
                                            'Search or Get Current Location',
                                            style: TextStyle(
                                                fontSize: 16,
                                                color: Colors.grey
                                            ),
                                          ),
                                          (_currentPosition != null &&
                                              _currentAddress != null)?
                                          Text(_currentAddress,
                                              style:TextStyle(
                                                  fontSize: 16
                                              )):Container(),
                                        ],
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 8,
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: Container(
                                      width: 50,
                                      height: 50,
                                      child: IconButton(
                                          icon: Icon(Icons.my_location,color: config.Colors().mainColor(1),),
                                          onPressed:()async{
                                            debugPrint("Yes");
                                            try {
                                              bool serviceEnabled;
                                              setState(() {
                                                isLoading = true;
                                              });
                                              serviceEnabled = await Geolocator.isLocationServiceEnabled();
                                              setState(() {
                                                isLoading = false;
                                              });
                                              if (!serviceEnabled) {
                                                MyToast.showToast("Location services are disabled.",context);
                                              }
                                              setState(() {
                                                isLoading = true;
                                              });
                                              await Utility.requestForLocationPermission();
                                              setState(() {
                                                isLoading = false;
                                              });

                                              _getCurrentLocation();

                                            } catch (e) {
                                              MyToast.showToast(e.toString(),context);
                                              setState(() {
                                                isLoading = false;
                                              });
                                            }
                                          }
                                      ),
                                    ),
                                  ),

                                ],
                              ),
                            ),

//                              state
                            Container(
                                margin:
                                EdgeInsets.only(left: 5, right: 5),
                                child: Divider(
                                  height: 1,
                                  color: config.Colors().mainColor(1),
                                )),
                            Container(
                              child: TextFormField(
                                focusNode: fnState,
                                textInputAction: TextInputAction.next,
                                onFieldSubmitted: (term){
                                  fnCity.requestFocus();
                                },
                                validator: (text){
                                  if(text.length == 0){
                                    return "State is required";
                                  }
                                  return null;
                                },
                                controller: _stateController,
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                  errorStyle: AppStyles.errorStyle,
                                  focusedBorder: AppStyles.focusBorder,
                                  prefixIcon: Icon(
                                    Icons.location_on,
                                    color: config.Colors().mainColor(1),
                                  ),
                                  labelStyle: TextStyle(
                                      color:
                                      config.Colors().mainColor(1)),
                                  border: OutlineInputBorder(),
                                  labelText: "Enter State",
                                ),
                              ),
                              padding: EdgeInsets.all(20),
                            ),
//                              city
                            Container(
                                margin:
                                EdgeInsets.only(left: 5, right: 5),
                                child: Divider(
                                  height: 1,
                                  color: config.Colors().mainColor(1),
                                )),
                            Container(
                              child: TextFormField(
                                focusNode: fnCity,
                                textInputAction: TextInputAction.next,
                                onFieldSubmitted: (term){
                                  fnAddress.requestFocus();
                                },
                                validator: (text){
                                  if(text.length == 0){
                                    return "City is required";
                                  }
                                  return null;
                                },
                                controller: _cityController,
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                  errorStyle: AppStyles.errorStyle,
                                  focusedBorder: AppStyles.focusBorder,
                                  prefixIcon: Icon(
                                    Icons.location_city,
                                    color: config.Colors().mainColor(1),
                                  ),
                                  labelStyle: TextStyle(
                                      color:
                                      config.Colors().mainColor(1)),
                                  border: OutlineInputBorder(),
                                  labelText: "Enter City",
                                ),
                              ),
                              padding: EdgeInsets.all(20),
                            ),
//                              address
                            Container(
                                margin:
                                EdgeInsets.only(left: 5, right: 5),
                                child: Divider(
                                  height: 1,
                                  color: config.Colors().mainColor(1),
                                )),
                            Container(
                              child: TextFormField(
                                focusNode: fnAddress,
                                textInputAction: TextInputAction.newline,
                                /*onFieldSubmitted: (term){
                                              fnZipcode.requestFocus();
                                            },*/
                                textAlign: TextAlign.start,
                                controller: _addressController,
                                validator: (text){
                                  if(text.length == 0){
                                    return "Address is required";
                                  }
                                  return null;
                                },
                                maxLines: 3,
                                minLines: 3,
                                keyboardType: TextInputType.multiline,
                                decoration: InputDecoration(
                                    errorStyle: AppStyles.errorStyle,
                                    focusedBorder: AppStyles.focusBorder,
                                    // contentPadding: EdgeInsets.symmetric(horizontal: 0,vertical: 0),
                                    /*prefix: Icon(
                                                  Icons.location_on,
                                                  color: config.Colors().mainColor(1),
                                                ),*/
                                    labelStyle: TextStyle(
                                        color:
                                        config.Colors().mainColor(1)),
                                    border: OutlineInputBorder(),
                                    labelText: "Enter Address",
                                    alignLabelWithHint: true
                                ),
                              ),
                              padding: EdgeInsets.all(20),
                            ),
//                              zipcode
                            Container(
                                margin:
                                EdgeInsets.only(left: 5, right: 5),
                                child: Divider(
                                  height: 1,
                                  color: config.Colors().mainColor(1),
                                )),
                            Container(
                              child: TextFormField(
                                focusNode: fnZipcode,
                                textInputAction: TextInputAction.next,
                                onFieldSubmitted: (term){
                                  fnDescription.requestFocus();
                                },
                                validator: (text){
                                  if(text.length == 0){
                                    return "Zip Code is required";
                                  }
                                  return null;
                                },
                                controller: _zipController,
                                keyboardType: TextInputType.number,
//                                        maxLength: 10,
                                decoration: InputDecoration(
                                  errorStyle: AppStyles.errorStyle,
                                  focusedBorder: AppStyles.focusBorder,
                                  prefixIcon: Icon(
                                    Icons.home,
                                    color: config.Colors().mainColor(1),
                                  ),
                                  labelStyle: TextStyle(
                                      color:
                                      config.Colors().mainColor(1)),
                                  border: OutlineInputBorder(),
                                  labelText: "Enter Zipcode",
                                ),
                              ),
                              padding: EdgeInsets.all(20),
                            ),
//                              description
                            Container(
                                margin:
                                EdgeInsets.only(left: 5, right: 5),
                                child: Divider(
                                  height: 1,
                                  color: config.Colors().mainColor(1),
                                )),
                            Container(
                              child: TextFormField(
                                focusNode: fnDescription,
                                textInputAction: TextInputAction.newline,
                                /*onFieldSubmitted: (term){
                                              fnDescription.requestFocus();
                                            },*/
                                /*validator: (text){
                                              if(text.length == 0){
                                                return "Zip Code is required";
                                              }*/ /*else if(text.length < 10){
                                                return "Invalid Mobile Number";
                                              }*/ /*
                                              return null;
                                            },*/
                                controller: _descriptionController,
                                keyboardType: TextInputType.multiline,
                                maxLines: 3,
                                textAlign: TextAlign.start,
                                minLines: 3,
                                decoration: InputDecoration(
                                  errorStyle: AppStyles.errorStyle,
                                  focusedBorder: AppStyles.focusBorder,
                                  /*prefix: Icon(
                                                Icons.home,
                                                color: config.Colors().mainColor(1),
                                              ),*/
                                  labelStyle: TextStyle(
                                      color:
                                      config.Colors().mainColor(1)),
                                  border: OutlineInputBorder(),
                                  labelText: "Enter description",
                                  alignLabelWithHint: true,
                                ),
                              ),
                              padding: EdgeInsets.all(20),
                            ),

                          ],
                        ),
                      ),
                    ),
//                        Center(child:ball(currentDoctor.avatar, Theme.of(context).primaryColor,)),
                  ],
                ),
                InkWell(
                    onTap: () {
                      debugPrint("pre fullname : ${currentUser.fullname}");
                      debugPrint("current fullname : ${previousNonUpdateUser.fullname}");
                      if(_formState.currentState.validate()) {
                        updateProfile(previousNonUpdateUser);
                      }
                    },
                    child: ButtonSave())
              ],
            ),
          ),
          isLoading?Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              color: Colors.white.withOpacity(0.7),
              child: Center(
                child: SizedBox(
                  width: 40,
                  height: 40,
                  child: CircularProgressIndicator(
                    strokeWidth: 1.5,
                    valueColor: AlwaysStoppedAnimation<Color>(config.Colors().mainColor(1),),
                  ),
                ),
              )):Container(),
        ],
      ),
    );
  }
  Future<void> _handlePressButton() async {
    // show input autocomplete with selected mode
    // then get the Prediction selected
    Prediction p = await PlacesAutocomplete.show(
      context: context,
      apiKey: kGoogleApiKey,
      onError: onError,
      mode: _mode,
      language: "en",
      components: [Component(Component.country, "in")],
    );

    displayPrediction(p, homeScaffoldKey.currentState);
  }
  void onError(PlacesAutocompleteResponse response) {
    homeScaffoldKey.currentState.showSnackBar(
      SnackBar(content: Text(response.errorMessage)),
    );
  }

  Future<Null> displayPrediction(Prediction p, ScaffoldState scaffold) async {
    if (p != null) {
      // get detail (lat/lng)
      PlacesDetailsResponse detail = await _places.getDetailsByPlaceId(p.placeId);
      final lat = detail.result.geometry.location.lat;
      final lng = detail.result.geometry.location.lng;

      Position mPosition=Position(latitude: lat,longitude: lng);
      setState(() {
        _currentPosition = mPosition;
      });

      _getAddressFromLatLng();
      /*scaffold.showSnackBar(
        SnackBar(content: Text("${p.description} - $lat/$lng")),
      );*/
    }
  }
  _getCurrentLocation() async{
    setState(() {
      isLoading = true;
    });
    Position position = await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    setState(() {
      isLoading = false;
    });
    if (!mounted) {
      return;
    }
    setState(() {
      _currentPosition = position;
    });
    _getAddressFromLatLng();
  }
  _getAddressFromLatLng() async {
    try {
      setState(() {
        isLoading = true;
      });
      List<Placemark> p = await placemarkFromCoordinates(
          _currentPosition.latitude, _currentPosition.longitude);

      Placemark place = p[0];

      setState(() {
        String subLocality = place.subLocality;
        String locality = place.locality;
        String administrativeArea = place.administrativeArea;
        String name = place.name;
        String postalCode = place.postalCode;
        String country = place.country;

        debugPrint("name : ${name}");
        debugPrint("subLocality : ${subLocality}");
        debugPrint("locality : ${locality}");
        debugPrint("administrativeArea : ${administrativeArea}");
        debugPrint("postalCode : ${postalCode}");
        debugPrint("country : ${country}");
        _cityController.text = locality;
        _stateController.text = administrativeArea;
        _zipController.text  = postalCode;

        _currentAddress = "${place.name}, ${place.subLocality}, ${place.locality}, ${place.administrativeArea}, ${place.postalCode}, ${place.country}";

        _addressController.text = _currentAddress;
        setState(() {
          isLoading = false;
        });
      });
    } catch (e) {
      setState(() {
        isLoading = false;
      });
      print(e);
    }
  }
  @override
  void dispose() {
    super.dispose();
  }

  void _settingModalBottomSheet(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: new Wrap(
              direction: Axis.horizontal,
              children: <Widget>[
                /*ListTile(
                    leading: new Icon(Icons.delete),
                    title: new Text('Remove'),
                    onTap: () {
                      _onClickRemovePhoto();
                    }),*/
                ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text('Camera'),
                    onTap: () {
                      _onClickCameraPhoto();
                    }),
                ListTile(
                  leading: new Icon(Icons.photo),
                  title: new Text('Gallery'),
                  onTap: () {
                    _onClickGalleryPhoto();
                  },
                ),
              ],
            ),
          );
        });
  }

  void _onClickCameraPhoto() async {
    Navigator.pop(context);

    PickedFile pickedFile = await ImagePicker().getImage(source: ImageSource.camera);
    File image;
    if (pickedFile != null) {
      image = File(pickedFile.path);
    } else {
      image = null;
    }
    if (image != null) {
      File cropped = await Utility.cropImage(image);
      debugPrint("cropped Image : ${cropped.toString()}");
      setState(() {
        if (photoUpload == PhotoUpload.profile) {
          photo = cropped;
        } else if (photoUpload == PhotoUpload.cover) {
          coverPhoto = cropped;
        }
      });
    }
  }

  void _onClickGalleryPhoto() async {
    Navigator.pop(context);
    PickedFile pickedFile = await ImagePicker().getImage(source: ImageSource.gallery);
    File image;
    if (pickedFile != null) {
      image = File(pickedFile.path);
    } else {
      image = null;
    }
    if (image != null) {
      File cropped = await Utility.cropImage(image);
      debugPrint("cropped Image : ${cropped.toString()}");
      setState(() {
        if (photoUpload == PhotoUpload.profile) {
          photo = cropped;
        } else if (photoUpload == PhotoUpload.cover) {
          coverPhoto = cropped;
        }
      });
    }
  }

  void _onClickRemovePhoto() {
    Navigator.pop(context);
    setState(() {
      if (photoUpload == PhotoUpload.profile) {
        photo = null;
      } else if (photoUpload == PhotoUpload.cover) {
        coverPhoto = null;
      }
    });
  }

  Widget ball(String image, Color color, profileSize) {
    return Container(
      alignment: Alignment.center,
      height: profileSize,
      width: profileSize,
      decoration: BoxDecoration(color: Colors.white, shape: BoxShape.circle),
      child: Container(
        height: profileSize - 5,
        width: profileSize - 5,
        child: ClipOval(
          child: FancyShimmerImage(
            imageUrl: image ?? Const.defaultProfileUrl,
            shimmerBaseColor: Colors.white,
            shimmerHighlightColor: config.Colors().mainColor(1),
            shimmerBackColor: Colors.green,
            errorWidget: Image.network('https://i0.wp.com/www.dobitaobyte.com.br/wp-content/uploads/2016/02/no_image.png?ssl=1'),
          ),
        ),
      ),
    );
  }

  void updateProfile(UserModel previousNonUpdateUser) async {
    try {
      locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
      setState(() {
        isLoading = true;
      });

      List<String> mCategory = [];
      selectedCategory.forEach((element) {
        mCategory.add(element);
      });
      List<String> mDegree = [];
      selectedDegrees.forEach((element) {
        mDegree.add(element);
      });
      List<String> mServices = [];
      selectedServices.forEach((element) {
        mServices.add(element);
      });
      List<String> mSpecializations = [];
      selectedSpecialization.forEach((element) {
        mSpecializations.add(element);
      });

      previousNonUpdateUser.fullname = _fullnameController.text;
      previousNonUpdateUser.email = _emailController.text;
      previousNonUpdateUser.mobile = _mobileNumberController.text;
      previousNonUpdateUser.fees = _feesController.text;
      previousNonUpdateUser.experience = _experienceController.text;
      previousNonUpdateUser.state = _stateController.text;
      previousNonUpdateUser.city = _cityController.text;
      previousNonUpdateUser.address = _addressController.text;
      previousNonUpdateUser.zipcode = _zipController.text;
      previousNonUpdateUser.description = _descriptionController.text;


      debugPrint("user ID : ${previousNonUpdateUser.id}");
      Map<String, dynamic> mapResult = await ApiRequest.postDoctorUpdate(
          fullname: previousNonUpdateUser.fullname,
          id: "${previousNonUpdateUser.id}",
          mobile: previousNonUpdateUser.mobile,
          state: previousNonUpdateUser.state,
          email: previousNonUpdateUser.email,
          city: previousNonUpdateUser.city,
          address: previousNonUpdateUser.address,
          zipcode: previousNonUpdateUser.zipcode,
          image: photo,
          cover_image: coverPhoto,
          degree: mDegree,
          category: mCategory,
          service: mServices,
          specialization: mSpecializations,
          fees: previousNonUpdateUser.fees,
          experience: previousNonUpdateUser.experience,
        description: previousNonUpdateUser.description,
          lat: _currentPosition==null?"":"${_currentPosition.latitude}",
          lng: _currentAddress==null?"":"${_currentPosition.longitude}"
      );
      UserModel user = widget.user;
      Map<String, dynamic> mapUpdateUser = mapResult['data'];
      debugPrint("Result user update : ${mapUpdateUser}");
      // mapUpdateUser['isAuthenticated'] = widget.user.isAuthenticated;
      // mapUpdateUser['loginType'] = widget.user.loginType;
      user = UserModel.fromMap(mapUpdateUser);
      debugPrint("updated user : ${user.toJson().toString()}");
      // await user.saveUser(user);
      locator<UserModelBloc>().userModelEventSink.add(user);
      // StateContainer.of(context).updateUserInfo(user);
      setState(() {
        isLoading = false;
      });
      locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
      MyToast.showToast("Profile Updated Successfully",context);
    } catch (e) {
      setState(() {
        isLoading = false;
      });
      locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
      MyToast.showToast("Error updating profile",context);
      debugPrint("Error : ${e.toString()}");
    }
  }
}

class ButtonSave extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(30.0),
      child: Container(
        height: 55.0,
        width: 600.0,
        child: Text(
          "Save",
          style: TextStyle(color: Colors.white, letterSpacing: 0.2, fontFamily: "Sans", fontSize: 18.0, fontWeight: FontWeight.w800),
        ),
        alignment: FractionalOffset.center,
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(30.0), gradient: LinearGradient(colors: <Color>[config.Colors().mainColor(1), config.Colors().mainColor(1)])),
      ),
    );
  }
}
