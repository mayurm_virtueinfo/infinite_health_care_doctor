import 'package:flutter/material.dart';
import 'package:infinite_health_care_doctor/bloc/loading/loading_bloc_event.dart';
import 'package:infinite_health_care_doctor/bloc/loading/loading_bloc.dart';
import 'package:infinite_health_care_doctor/widgets/loading_widget.dart';
import 'package:infinite_health_care_doctor/main.dart';
import 'package:infinite_health_care_doctor/api/api_request.dart';
import 'package:infinite_health_care_doctor/models/my_payment_model.dart';
import 'package:infinite_health_care_doctor/models/user_model.dart';
import 'package:infinite_health_care_doctor/main.dart';
import 'package:infinite_health_care_doctor/utils/my_toast.dart';
import 'package:infinite_health_care_doctor/widgets/item_my_payments.dart';
class MyPayments extends StatefulWidget {
  static const String routeName = '/MyPayments';
  @override
  _MyPaymentsState createState() => _MyPaymentsState();
}

class _MyPaymentsState extends State<MyPayments> {
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(bottomLeft:Radius.circular(16.0),bottomRight: Radius.circular(16.0)),
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color:Theme.of(context).primaryColor ),
          onPressed: (){
            Navigator.of(context).pop();
          },
        ),
        backgroundColor: Theme.of(context).accentColor,
        title: Text(
          'Payments',
          style: TextStyle(
            fontSize:22.0,
            fontWeight: FontWeight.bold,
            color: Theme.of(context).primaryColor,
          ),
        ),

      ),
      body: SingleChildScrollView(
        child:Container(
          padding: EdgeInsets.all(20),
          constraints: BoxConstraints(
            minHeight: MediaQuery.of(context).size.height,
            minWidth: MediaQuery.of(context).size.width,
          ),
          child: Column(
            children: <Widget>[
              FutureBuilder(
                future: ApiRequest.getMyPayments(userID: appUserModel.id),
                builder: (context, snapshot) {

                  if(snapshot.hasData) {
                    locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
                    List<dynamic> data = snapshot.data['data'] as List;
                    List<MyPaymentModel> mPaymentList = [];
                    data.forEach((element) {
                      MyPaymentModel paymentModel = MyPaymentModel.fromMap(element);
                      mPaymentList.add(paymentModel);
                    });



                    return mPaymentList.length>0?ListView.separated(
                      shrinkWrap: true,
                      primary: false,
                      itemCount: mPaymentList.length,
                      separatorBuilder: (context, index) {
                        return SizedBox(height: 4.0);
                      },
                      itemBuilder: (context, index) {
                        return ItemMyPayments(
                          myPaymentModel: mPaymentList.elementAt(index),
                        );
                      },
                    ):Container(
                      constraints: BoxConstraints(
                        maxHeight: MediaQuery.of(context).size.height,
                        maxWidth: MediaQuery.of(context).size.width,
                      ),
                      margin: EdgeInsets.only(top: 30),
                      alignment: Alignment.center,
                      child: Center(
                        child: Text(
                          "No payments found",
                          style: TextStyle(fontSize: 16),
                        ),
                      ),
                    );
                  } else if(snapshot.hasError){
                    locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
                    MyToast.showToast(snapshot.error.toString(),context);
                    return Container();
                  } else{
                    return LoadingWidget(initialData: true,);
                  }
                }
                ,
              ),
            ],
          ),
        ),      
      ),
    );
  }
  
}
