import 'package:flutter/material.dart';
import 'package:infinite_health_care_doctor/bloc/loading/loading_bloc_event.dart';
import 'package:infinite_health_care_doctor/bloc/loading/loading_bloc.dart';
import 'package:infinite_health_care_doctor/widgets/loading_widget.dart';
import 'package:infinite_health_care_doctor/main.dart';
import 'package:infinite_health_care_doctor/api/api_request.dart';
import 'package:infinite_health_care_doctor/models/appointment_type.dart';
import 'package:infinite_health_care_doctor/models/notifications_model.dart' as myNotificationModel;
import 'package:infinite_health_care_doctor/models/user_model.dart';
import 'package:infinite_health_care_doctor/ui/home_screen.dart';
import 'package:infinite_health_care_doctor/main.dart';
import 'package:infinite_health_care_doctor/utils/my_toast.dart';
import 'package:infinite_health_care_doctor/widgets/app_button_circular_progress.dart';
import 'package:infinite_health_care_doctor/widgets/conversation_item_widget.dart';
import 'package:infinite_health_care_doctor/widgets/item_notification.dart';
import 'package:infinite_health_care_doctor/widgets/my_doctors_card_widget.dart';
class NotificationList extends StatefulWidget {
  static const routeName = '/NotificationList';
  @override
  _NotificationListState createState() => _NotificationListState();
}

class _NotificationListState extends State<NotificationList> {
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(bottomLeft:Radius.circular(16.0),bottomRight: Radius.circular(16.0)),
        ),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Theme.of(context).primaryColor,
          ),
          onPressed: (){
//            Navigator.of(context).pushNamed(HomeScreen.routeName, arguments:[widget.currentUser.name,widget.currentUser.phoneNumber]);
            Navigator.of(context).pop();
          },

        ),
        backgroundColor: Theme.of(context).accentColor,
        title: Text(
          'Notifications',
          style: TextStyle(
            fontSize:22.0,
            fontWeight: FontWeight.bold,
            color: Theme.of(context).primaryColor,
          ),
        ),

      ),
      body: SingleChildScrollView(
        child:ConstrainedBox(
          constraints: BoxConstraints(
              minHeight: MediaQuery.of(context).size.height,
              minWidth: MediaQuery.of(context).size.width
          ),
          child: Container(
            padding: EdgeInsets.all(20),
            decoration: BoxDecoration(
             color: Colors.transparent,
            ),
            child: FutureBuilder(
              future: ApiRequest.getNotifications(userID: appUserModel.id),
              builder: (context, snapshot) {

                if(snapshot.hasData) {
                  locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
                  debugPrint("hasData");

                  myNotificationModel.NotificationModelList dList = myNotificationModel.NotificationModelList.fromSnapshot(snapshot);

                  return dList.notificationList.length>0?ListView.separated(
                    shrinkWrap: true,
                    primary: false,
                    itemCount: dList.notificationList.length,
                    separatorBuilder: (context, index) {
                      return SizedBox(height: 4.0);
                    },
                    itemBuilder: (context, index) {
                      AppointmentType aType = AppointmentType.SCHEDULED_APPOINTMENT;
                      return ItemNotification(
                        notification: dList.notificationList.elementAt(index),
                      );
                    },
                  ):Container(
                    constraints: BoxConstraints(
                      maxHeight: MediaQuery.of(context).size.height,
                      maxWidth: MediaQuery.of(context).size.width,
                    ),
                    margin: EdgeInsets.only(top: 30),
                    alignment: Alignment.center,
                    child: Center(
                      child: Text(
                        "No notifications found",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                  );
                } else if(snapshot.hasError){
                  locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
                  MyToast.showToast(snapshot.error.toString(),context);
                  return Container();
                } else{
                  return LoadingWidget(initialData: true,);
                }
              }
              ,
            ),
          ),
        ),      
      ),
    );
  }
  
}
