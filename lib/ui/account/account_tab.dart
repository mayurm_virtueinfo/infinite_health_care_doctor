import 'dart:convert';

import 'package:google_sign_in/google_sign_in.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:infinite_health_care_doctor/bloc/user_model_bloc.dart';
import 'package:infinite_health_care_doctor/main.dart';
import 'package:infinite_health_care_doctor/utils/dialog_mgr.dart';
import 'package:infinite_health_care_doctor/config/app_config.dart' as config;
import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:infinite_health_care_doctor/config/const.dart';
import 'package:infinite_health_care_doctor/models/user_model.dart';
import 'package:infinite_health_care_doctor/pages/health.dart';
import 'package:infinite_health_care_doctor/pages/offers.dart';
import 'package:infinite_health_care_doctor/ui/account/my_appointments.dart';
import 'package:infinite_health_care_doctor/ui/account/my_patients.dart';
import 'package:infinite_health_care_doctor/ui/account/my_payments.dart';
import 'package:infinite_health_care_doctor/ui/account/notification_list.dart';
import 'package:infinite_health_care_doctor/ui/account/user_profile.dart';
import 'package:infinite_health_care_doctor/ui/home/patient_list.dart';
import 'package:infinite_health_care_doctor/ui/home/upcoming_appointment_list.dart';
import 'package:infinite_health_care_doctor/ui/loginorsignup/signup.dart';

import 'package:infinite_health_care_doctor/utils/app_preferences.dart';
import 'package:infinite_health_care_doctor/utils/utility.dart';

class AccountTab extends StatefulWidget {
  const AccountTab({Key key}) : super(key: key);

  @override
  _AccountTabState createState() => _AccountTabState();
}

class _AccountTabState extends State<AccountTab> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    double deviceWidth = MediaQuery.of(context).size.width;
    double deviceHeight = MediaQuery.of(context).size.height;
    double headerSize = (deviceHeight / 3.75);
    double devicePixelRatio = MediaQuery.of(context).devicePixelRatio;
    double coverHeight = Utility.getCoverHeight(context);
    double profilePicSize = Utility.getProfilePicSize(context);
    double topPadding = coverHeight - (profilePicSize / 2);
    return Scaffold(
        body: SingleChildScrollView(
      child: Column(
        children: <Widget>[
          Stack(
            children: [
              StreamBuilder<UserModel>(
                initialData: appUserModel,
                stream: locator<UserModelBloc>().userModelStream,
                builder: (context,AsyncSnapshot<UserModel> snapshot) {
                  UserModel nUserModel = snapshot.data;
                  return Container(
                    constraints: BoxConstraints(
                      minWidth: MediaQuery.of(context).size.width,
                      minHeight: coverHeight,
                    ),
                    child: Stack(
                      alignment: Alignment.bottomRight,
                      children: [
                        Container(
                          height: coverHeight,
                          width: MediaQuery.of(context).size.width,
                          child: FancyShimmerImage(
                            imageUrl: nUserModel.cover_image ?? Const.defaultCoverUrl,
                            shimmerBaseColor: Colors.white,
                            shimmerHighlightColor: config.Colors().mainColor(1),
                            shimmerBackColor: Colors.green,
                            boxFit: BoxFit.cover,
                            errorWidget: Image.network('https://i0.wp.com/www.dobitaobyte.com.br/wp-content/uploads/2016/02/no_image.png?ssl=1'),
                          ),
                        ),
                      ],
                    ),
                  );
                }
              ),
              StreamBuilder<UserModel>(
                initialData: appUserModel,
                stream: locator<UserModelBloc>().userModelStream,
                builder: (context,AsyncSnapshot<UserModel> snapshot) {
                  UserModel nUserModel = snapshot.data;
                  return Padding(
                    padding: EdgeInsets.only(top: topPadding, left: 10, right: 10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        ball(nUserModel.avatar_image, Colors.transparent, profilePicSize, context),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          "${nUserModel.fullname}",
                          style: TextStyle(color: Theme.of(context).accentColor, fontSize: 14, fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Container(
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Utility.getProfileString(nUserModel.address, nUserModel.city, nUserModel.state, nUserModel.zipcode) == ''
                                  ? Container()
                                  : Icon(
                                Icons.location_on,
                                color: Theme.of(context).accentColor,
                                size: 18,
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              Container(
                                constraints: BoxConstraints(maxWidth: MediaQuery.of(context).size.width - 100),
                                child: Text(
                                  '${Utility.getProfileString(nUserModel.address, nUserModel.city, nUserModel.state, nUserModel.zipcode)}',
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          width: 170,
                          // height: 30,
                          decoration: BoxDecoration(
                            border: Border.all(width: 2, color: Theme.of(context).primaryColor),
                            borderRadius: BorderRadius.circular(30.0),
                          ),
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              primary: Theme.of(context).accentColor,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30.0),
                              ),
                            ),

                            onPressed: () {
                              /*Navigator.pushNamed(
                                    context, UserProfile.routeName);*/
                              Navigator.of(context).push(
                                PageRouteBuilder(
                                  transitionDuration: Duration(milliseconds: 500),
                                  pageBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation) {
                                    return UserProfile(
                                      user: appUserModel,
                                    );
                                  },
                                  transitionsBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
                                    return Align(
                                      child: FadeTransition(
                                        opacity: animation,
                                        child: child,
                                      ),
                                    );
                                  },
                                ),
                              );
                            },

                            child: Container(
                              child: Center(
                                child: Text(
                                  'Edit your profile',
                                  style: TextStyle(
                                    fontSize: 14.0,
                                    color: Theme.of(context).primaryColor,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  );
                },
              ),
            ],
          ),
          Container(
            padding: EdgeInsets.all(12.0),
            margin: EdgeInsets.all(12.0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(16.0),
              border: Border.all(width: 1, color: Colors.grey.withOpacity(0.2)),
              color: Theme.of(context).primaryColor,
            ),
            child: Column(
              children: <Widget>[
                _dropDownListe(
                    Icon(
                      Icons.bubble_chart,
                      color: Theme.of(context).accentColor,
                    ),
                    'My Patients',
                    1,
                    context, () async {

                  Navigator.of(context).pushNamed(PatientList.routeName, arguments: [appUserModel.id, 'My Patient']);
                }),
                _dropDownListe(
                    Icon(
                      Icons.calendar_today,
                      color: Theme.of(context).accentColor,
                    ),
                    'Appointments',
                    1,
                    context, () async {
                  // Navigator.of(context).pushNamed(MyAppointments.routeName);
                  Navigator.of(context).pushNamed(UpcomingAppointmentList.routeName);
                }),
                _dropDownListe(
                    Icon(
                      Icons.card_giftcard,
                      color: Theme.of(context).accentColor,
                    ),
                    'Health Tips',
                    1,
                    context, () async {
                  Navigator.of(context).pushNamed(HealthTips.routeName);
                }),
                _dropDownListe(
                    Icon(
                      Icons.payment,
                      color: Theme.of(context).accentColor,
                    ),
                    'My Payments',
                    1,
                    context, () async {
                  Navigator.of(context).pushNamed(MyPayments.routeName);
                }),
                _dropDownListe(
                    Icon(
                      Icons.notifications,
                      color: Theme.of(context).accentColor,
                    ),
                    'Notifications',
                    1,
                    context, () async {
                  Navigator.of(context).pushNamed(NotificationList.routeName);
                }),
                /*_dropDownListe(
                        Icon(
                          Icons.local_offer,
                          color: Theme
                              .of(context)
                              .accentColor,
                        ),
                        'Offers',
                        1,
                        OffersList.routeName,
                        context,() async {

                        Navigator.of(context).pushNamed(OffersList.routeName);

                    }),*/
                _dropDownListe(
                    Icon(
                      Icons.arrow_upward,
                      color: Theme.of(context).accentColor,
                    ),
                    'Logout',
                    0,
                    context, () async {
                  String result = await DialogMgr.showDialogLogout(context);
                  if (result != null && result == 'yes') {
                    bool result = await Utility.logoutUser();
                    if (result) {
                      Navigator.of(context).pushNamedAndRemoveUntil(SignUp.routeName, (route) => false);
                    }
                  }
                }),
              ],
            ),
          ),
        ],
      ),
    ));
  }



  Widget  _dropDownListe(Icon icon, String title, double borderWidth, BuildContext context, Function onClick) {
    return Container(
      decoration: BoxDecoration(
        border: Border(bottom: BorderSide(width: borderWidth, color: Colors.grey.withOpacity(0.2))),
      ),
      child: TextButton(
        onPressed: onClick,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Row(
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.only(right: 25.0),
                  child: icon,
                ),
                Container(
                  child: Text(
                    title,
                    style: TextStyle(
                      color: Colors.grey,
                      fontSize: 16.0,
                    ),
                  ),
                ),
              ],
            ),
            Container(
              child: Icon(
                Icons.chevron_right,
                color: Colors.grey,
                size: 20,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

Widget ball(String image, Color color, profileSize, context) {
  return Container(
    alignment: Alignment.center,
    height: profileSize,
    width: profileSize,
    decoration: BoxDecoration(color: Theme.of(context).accentColor, shape: BoxShape.circle),
    child: Container(
      height: profileSize - 2,
      width: profileSize - 2,
      child: ClipOval(
        child: FancyShimmerImage(
          imageUrl: image ?? Const.defaultProfileUrl,
          shimmerBaseColor: Colors.white,
          shimmerHighlightColor: config.Colors().mainColor(1),
          shimmerBackColor: Colors.green,
          errorWidget: Image.network('https://i0.wp.com/www.dobitaobyte.com.br/wp-content/uploads/2016/02/no_image.png?ssl=1'),
        ),
      ),
    ),
  );
}
