import 'package:flutter/material.dart';
import 'package:infinite_health_care_doctor/api/api_request.dart';
import 'package:infinite_health_care_doctor/models/appointment_type.dart';
import 'package:infinite_health_care_doctor/models/my_doctor_model.dart' as myDoctorModel;
import 'package:infinite_health_care_doctor/models/user_model.dart';
import 'package:infinite_health_care_doctor/main.dart';
import 'package:infinite_health_care_doctor/utils/my_toast.dart';
import 'package:infinite_health_care_doctor/widgets/my_doctors_card_widget.dart';
class MyPatients extends StatefulWidget {
  static const routeName = '/MyPatientsList';
  @override
  _MyPatientsState createState() => _MyPatientsState();
}

class _MyPatientsState extends State<MyPatients> {
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(bottomLeft:Radius.circular(16.0),bottomRight: Radius.circular(16.0)),
        ),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Theme.of(context).primaryColor,
          ),
          onPressed: (){
//            Navigator.of(context).pushNamed(HomeScreen.routeName, arguments:[widget.currentUser.name,widget.currentUser.phoneNumber]);
            Navigator.of(context).pop();
          },

        ),
        backgroundColor: Theme.of(context).accentColor,
        title: Text(
          'Doctors',
          style: TextStyle(
            fontSize:22.0,
            fontWeight: FontWeight.bold,
            color: Theme.of(context).primaryColor,
          ),
        ),

      ),
      body: SingleChildScrollView(
        child:ConstrainedBox(
          constraints: BoxConstraints(maxHeight: MediaQuery.of(context).size.height),
          child: Container(
            child: Column(
              children: <Widget>[
                Expanded(
                  child: Container(
                    padding: EdgeInsets.all(20),
                    decoration: BoxDecoration(
                     color: Colors.transparent,
                    ),
                    child: FutureBuilder(
                      future: ApiRequest.getMyDoctors(userID: appUserModel.id),
                      builder: (context, snapshot) {

                        if(snapshot.hasData) {
                          debugPrint("hasData");

                          myDoctorModel.MyDoctorModelList dList = myDoctorModel.MyDoctorModelList.fromSnapshot(snapshot);

                          return dList.myDoctorList.length>0?ListView.separated(
                            shrinkWrap: true,
                            primary: false,
                            itemCount: dList.myDoctorList.length,
                            separatorBuilder: (context, index) {
                              return SizedBox(height: 4.0);
                            },
                            itemBuilder: (context, index) {
                              AppointmentType aType = AppointmentType.SCHEDULED_APPOINTMENT;
                              return MyDoctorsCardWidget(
                                appointmentType: aType,
                                doctors: dList.myDoctorList.elementAt(index),
                              );
                            },
                          ):Container(
                            constraints: BoxConstraints(
                              maxHeight: MediaQuery.of(context).size.height,
                              maxWidth: MediaQuery.of(context).size.width,
                            ),
                            margin: EdgeInsets.only(top: 30),
                            alignment: Alignment.center,
                            child: Center(
                              child: Text(
                                "No patient found",
                                style: TextStyle(fontSize: 16),
                              ),
                            ),
                          );
                        } else if(snapshot.hasError){
                          MyToast.showToast(snapshot.error.toString(),context);
                          return Container();
                        } else{
                          return Center(
                            child:CircularProgressIndicator() ,
                          );
                        }
                      }
                      ,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),      
      ),
    );
  }
  
}
