import 'package:flutter/material.dart';
import 'package:infinite_health_care_doctor/api/api_request.dart';
import 'package:infinite_health_care_doctor/models/my_appointment_model.dart' as myAppointmentModel;
import 'package:infinite_health_care_doctor/models/appointment_type.dart';
import 'package:infinite_health_care_doctor/models/user_model.dart';
import 'package:infinite_health_care_doctor/ui/home_screen.dart';
import 'package:infinite_health_care_doctor/main.dart';
import 'package:infinite_health_care_doctor/utils/my_toast.dart';
import 'package:infinite_health_care_doctor/widgets/app_button_circular_progress.dart';
import 'package:infinite_health_care_doctor/widgets/appointments_card_widget.dart';
import 'package:infinite_health_care_doctor/widgets/conversation_item_widget.dart';
class MyAppointments extends StatefulWidget {
  static const String routeName = '/appointment';
  @override
  _MyAppointmentsState createState() => _MyAppointmentsState();
}

class _MyAppointmentsState extends State<MyAppointments> {
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(bottomLeft:Radius.circular(16.0),bottomRight: Radius.circular(16.0)),
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color:Theme.of(context).primaryColor ),
          onPressed: (){
//            Navigator.of(context).pushNamed(HomeScreen.routeName, arguments:[widget.currentUser.name,widget.currentUser.phoneNumber]);
            Navigator.of(context).pop();
          },
        ),
        backgroundColor: Theme.of(context).accentColor,
        title: Text(
          'Appointments',
          style: TextStyle(
            fontSize:22.0,
            fontWeight: FontWeight.bold,
            color: Theme.of(context).primaryColor,
          ),
        ),

      ),
      body: SingleChildScrollView(
        child:ConstrainedBox(
          constraints: BoxConstraints(maxHeight: MediaQuery.of(context).size.height),
          child: Container(
            child: Column(
              children: <Widget>[
                Expanded(
                  child: Container(
                    decoration: BoxDecoration(
                     color: Colors.transparent,
                    ),
                    child: FutureBuilder(
                      future: ApiRequest.getMyAppointments(userID: appUserModel.id),
                      builder: (context, snapshot) {

                        if(snapshot.hasData) {


                          myAppointmentModel.MyApointmentListModel dList = myAppointmentModel.MyApointmentListModel.fromSnapshot(snapshot);


                          return ListView.separated(
                            shrinkWrap: true,
                            primary: false,
                            itemCount: dList.myAppointment.length,
                            separatorBuilder: (context, index) {
                              return SizedBox(height: 4.0);
                            },
                            itemBuilder: (context, index) {
                              AppointmentType aType = AppointmentType.SCHEDULED_APPOINTMENT;
                              return AppointmentsWidget(
                                appointmentType: aType,
                                appointment: dList.myAppointment.elementAt(index),
                              );
                            },
                          );
                        } else if(snapshot.hasError){
                          MyToast.showToast(snapshot.error.toString(),context);
                          return Container();
                        } else{
                          return Center(
                              child: CircularProgressIndicator()
                          );
                        }
                      }
                      ,
                    )/*ListView.separated(
                      shrinkWrap: true,
                      primary: false,
                      itemCount: appointmentList.appointment.length,
                      separatorBuilder: (context,index){
                        return SizedBox(height: 4.0);
                      },
                      itemBuilder: (context,index){

                        return AppointmentsWidget(
                          appointment: appointmentList.appointment.elementAt(index),
                        );
                      },
                    )*/,
                  ),
                ),
              ],
            ),
          ),
        ),      
      ),
    );
  }
  
}
