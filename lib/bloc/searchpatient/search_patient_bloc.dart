import 'dart:async';

import 'package:infinite_health_care_doctor/bloc/searchpatient/search_patient_bloc_event.dart';
import 'package:infinite_health_care_doctor/models/patient_model.dart';



class SearchPatientBloc {
  List<PatientModel> listSearchDoctor ;

  final searchDoctorController = StreamController<List<PatientModel>>.broadcast();
  StreamSink<List<PatientModel>> get searchDoctorSink => searchDoctorController.sink;
  // For state, exposing only a stream which outputs data
  Stream<List<PatientModel>> get searchDoctorStream => searchDoctorController.stream;

  final searchDoctorEventController = StreamController<SearchPatientBlocEvent>.broadcast();
  // For events, exposing only a sink which is an input
  Sink<SearchPatientBlocEvent> get searchDoctorEventSink => searchDoctorEventController.sink;

  SearchPatientBloc() {
    // Whenever there is a new event, we want to map it to a new state
    searchDoctorEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(SearchPatientBlocEvent event) {
    if (event is SearchPatientBlocEvent) {
      searchDoctorSink.add(event.listSearchDoctor);
    }
  }

  void dispose() {
    searchDoctorController.close();
    searchDoctorEventController.close();
  }
}
