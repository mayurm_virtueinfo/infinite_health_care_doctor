import 'dart:async';



class OnlineOfflineBloc {


  final onlineOfflineController = StreamController<String>.broadcast();
  StreamSink<String> get onlineOfflineSink => onlineOfflineController.sink;
  // For state, exposing only a stream which outputs data
  Stream<String> get onlineOfflineStream => onlineOfflineController.stream;

  final onlineOfflineEventController = StreamController<String>.broadcast();
  // For events, exposing only a sink which is an input
  Sink<String> get onlineOfflineEventSink => onlineOfflineEventController.sink;

  OnlineOfflineBloc() {
    // Whenever there is a new event, we want to map it to a new state
    onlineOfflineEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(String event) {
    onlineOfflineSink.add(event);
  }

  void dispose() {
    onlineOfflineController.close();
    onlineOfflineEventController.close();
  }
}
