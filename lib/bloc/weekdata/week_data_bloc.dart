import 'dart:async';

import 'package:infinite_health_care_doctor/bloc/duration/duration_bloc_event.dart';
import 'package:infinite_health_care_doctor/bloc/weekdata/week_data_event.dart';





class WeekDataBloc {
  List<dynamic>  weekData ;


  final weekDataController = StreamController<List<dynamic>>.broadcast();
  StreamSink<List<dynamic>> get weekDataSink => weekDataController.sink;
  // For state, exposing only a stream which outputs data
  Stream<List<dynamic>> get weekDataStream => weekDataController.stream;

  final weekDataEventController = StreamController<WeekDataEvent>.broadcast();
  // For events, exposing only a sink which is an input
  Sink<WeekDataEvent> get weekDataEventSink => weekDataEventController.sink;

  WeekDataBloc() {
    // Whenever there is a new event, we want to map it to a new state
    weekDataEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(WeekDataEvent event) {
    if (event is WeekDataEvent) {
      weekDataSink.add(event.weekData);
    }
  }

  void dispose() {
    weekDataController.close();
    weekDataEventController.close();
  }
}
