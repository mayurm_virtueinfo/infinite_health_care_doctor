import 'dart:async';

import 'package:infinite_health_care_doctor/bloc/duration/duration_bloc_event.dart';





class DurationBloc {

  final durationController = StreamController<String>.broadcast();
  StreamSink<String> get _inDuration => durationController.sink;
  // For state, exposing only a stream which outputs data
  Stream<String> get durationStream => durationController.stream;

  final durationEventController = StreamController<DurationBlocEvent>.broadcast();
  // For events, exposing only a sink which is an input
  Sink<DurationBlocEvent> get durationEventSink => durationEventController.sink;

  DurationBloc() {
    // Whenever there is a new event, we want to map it to a new state
    durationEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(DurationBlocEvent event) {
    if (event is DurationBlocEvent) {
      _inDuration.add(event.duration);
    }
  }

  void dispose() {
    durationController.close();
    durationEventController.close();
  }
}
